<?php
defined ( 'IN_TVM' ) or die ( 'Restricted Access' );
class Image
{
    static function getImage($imageName,$imagePath = "item",$width,$height,$class="")
    {
		if(!$imageName) return Image::getDefaultImage($width,$height,$class);
        $src = IMAGE_LINK.$imagePath.DS."thumb_".$width."x".$height.DS.$imageName;
        
		if($class)
			$img = "<img class='{$class}' alt='' src='{$src}' width='{$width}' height='{$height}' />";
		else
			$img = "<img alt='' src='{$src}' width='{$width}' height='{$height}' />";
        return $img;
    }
	
	static function getDefaultImage($width=120,$height=80,$class="")
	{
		if($class)
			$img = "<img class='{$class}' alt='' src='".IMAGE_DEFAULT."' width='{$width}' height='{$height}' />";
		else
			$img = "<img alt='' src='".IMAGE_DEFAULT."' width='{$width}' height='{$height}' />";
        return $img;
	}
	
	static function getFlashImage($imageName,$imagePath = "item",$width,$height,$class="")
    {
		if(!$imageName) return '';
        $src = IMAGE_LINK.$imagePath.DS.$imageName;
        
		$img = '<object width="'.$width.'" height="'.$height.'" border="0" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="swf_ADS_64_15s_0">
			<param value="'.$src.'" name="movie">
			<param value="always" name="AllowScriptAccess">
			<param value="High" name="quality">
			<param value="transparent" name="wmode">
			<embed width="'.$width.'" height="'.$height.'" allowscriptaccess="always" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="'.$src.'">
		</object>';
        return $img;
    }
	
    static function getFlashImageSrc($src,$width,$height,$class="")
    {
		if(!$src) return '';
        
		$img = '<object width="'.$width.'" height="'.$height.'" border="0" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="swf_ADS_64_15s_0">
			<param value="'.$src.'" name="movie">
			<param value="always" name="AllowScriptAccess">
			<param value="High" name="quality">
			<param value="transparent" name="wmode">
			<embed width="'.$width.'" height="'.$height.'" allowscriptaccess="always" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="'.$src.'">
		</object>';
        return $img;
    }
    
    static function getUrlImage($src)
    {
		if(!$src) return '';
        $img = "<img alt='' src='{$src}' />";
        return $img;
    }
    static function urlImage($imageName,$imagePath = "item",$width,$height)
    {
        $src = IMAGE_LINK.$imagePath.DS."thumb_".$width."x".$height.DS.$imageName;
        return $src;
    }
    static function urlOriginalImage($imageName,$imagePath = "item")
    {
        $src = IMAGE_LINK.$imagePath.DS.$imageName;
        return $src;
    }
    static function unlinkImage($imageName,$imagePath = "item",$width,$height)
    {
        $src = DATA_PATH.$imagePath.DS."thumb_".$width."x".$height.DS.$imageName;
        unlink($src);
    }
    static function unlinkImageOriginal($imageName,$imagePath = "item")
    {
		if($imageName) return "";
        $src = DATA_PATH.$imagePath.DS.$imageName;
        unlink($src);
    }
    static function unlink_image($imageName,$imagePath = "item",$width,$height)
    {
		if($imageName) return "";
        $src_img = DATA_PATH.$imagePath.DS.$imageName;
        $src_thumb = DATA_PATH.$imagePath.DS."thumb_".$width."x".$height.DS.$imageName;
        unlink($src_img);
        unlink($src_thumb);
    }
	static function get_image_extension($name) 
    {
        $name=strtolower($name);
        $i=strrpos($name,".");
        if(!$i){ return ""; }
        $l=strlen($name)-$i;
        $extension=substr($name,$i+1,$i);
        return $extension;
    }
}
?>