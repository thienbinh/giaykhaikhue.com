<?php
require_once LIBRARIES_DIR.'resize'.DS.'ThumbLib.inc.php';
class UploadSD
{
    public  $upload_dir         = "";
    public  $upload_dir_thumb   = "";
    private $width              = "";
    private $height             = "";
    function __construct()
    {
        
    }
    
    function uploadImage($inputName, $uploadDir,$width,$height)
    {
        $this->width = $width;
        $this->height = $height;
        $thumb = 'thumb_'.$this->width.'x'.$this->height;
        $image = $_FILES[$inputName];
        
        $imagePath = '';
        if (trim($image['tmp_name']) != '')
        {
            $ext = substr(strrchr($image['name'], "."), 1);
            $imagePath=md5(rand() * time()).".$ext";
            $extension=$this->get_image_extension($imagePath);
            if(($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif") )
            {
				if($extension == "swf") 
				{
					return $this->upload_files($inputName,$uploadDir);
				}
                exit();
            } 
            else 
            {
                $this->upload_dir = DATA_DIR.$uploadDir .DS;
                $this->createDir($thumb);
                $move=@copy($image['tmp_name'], $this->upload_dir. $imagePath);
                if($move)
                {
                    $this->upload_dir_thumb = DATA_DIR.$uploadDir.DS.$thumb.DS;
                    $this->create_thumbnail($this->upload_dir. $imagePath,$this->upload_dir_thumb.$imagePath,$this->width,$this->height);
                } 
                else 
                {
                    exit();
                }
            }
        }
        return $imagePath;
    }
    /**
     * Upload v?i nhi?u c? anh
     * $arraySize : M?ng c? : $arraySize = array('140x140','80x90','450x500');
    */
    function uploadImageMultySize($inputName, $uploadDir,$arraySize)
    {
        $date = date('Y/m/d');
        $date = explode('/',$date);
        $thumb = array();
        foreach($arraySize as $key => $size)
        {
            $z = explode('x',$key); 
            $width[] = $z[0];
            $height[] = $z[1];
            $thumb[] = $size;   
        }
        //var_dump($thumb);die;
        $image = $_FILES[$inputName]; 
        $imagePath = '';
        if (trim($image['tmp_name']) != '')
        {
            $ext = substr(strrchr($image['name'], "."), 1);
            $imagePath = str_replace('/','_',date('Y/m/d')).'_'.time().".$ext";
            $extension = $this->get_image_extension($imagePath);
            if(($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif") )
            {
                exit();
            } 
            else 
            {
                $this->upload_dir = DATA_DIR.$uploadDir .DS.$date[0].DS.$date[1].DS.$date[2].DS;
                foreach($thumb as $tb)
                {
                    $this->createDirItem($date,$tb);
                }
                
                $move=@copy($image['tmp_name'], $this->upload_dir. $imagePath);
                if($move)
                {
                    $i = 0;
                    foreach($thumb as $tb)
                    {
                        $this->upload_dir_thumb = $this->upload_dir.$tb.DS;
                        $this->create_thumbnail($this->upload_dir. $imagePath,$this->upload_dir_thumb.$imagePath,$width[$i],$height[$i]);
                        $i++;
                    }
                } 
                else 
                {
                    exit();
                }
            }
        }
        return array('image' => $imagePath,'url_org' => $date[0].'/'.$date[1].'/'.$date[2].'/','dir_org' => $this->upload_dir. $imagePath);
    }
    
    function uploadImageMultySizePath($image, $uploadDir,$arraySize)
    {
        $date = date('Y/m/d');
        $date = explode('/',$date);
        $thumb = array();
        foreach($arraySize as $key => $size)
        {
            $z = explode('x',$key); 
            $width[] = $z[0];
            $height[] = $z[1];
            $thumb[] = $size;   
        }
        
        $imagePath = '';
        if (trim($image['tmp_name']) != '')
        {
            $ext = substr(strrchr($image['name'], "."), 1);
            $imagePath = str_replace('/','_',date('Y/m/d')).'_'.time().".$ext";
            $extension = $this->get_image_extension($imagePath);
            if(($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif") )
            {
                exit();
            } 
            else 
            {
                $this->upload_dir = DATA_DIR.$uploadDir .DS.$date[0].DS.$date[1].DS.$date[2].DS;
                foreach($thumb as $tb)
                {
                    $this->createDirItem($date,$tb);
                }
                
                $move=@copy($image['tmp_name'], $this->upload_dir. $imagePath);//var_dump($image);die;
                if($move)
                {
                    $i = 0;
                    foreach($thumb as $tb)
                    {
                        $this->upload_dir_thumb = $this->upload_dir.$tb.DS;
                        $this->create_thumbnail($this->upload_dir. $imagePath,$this->upload_dir_thumb.$imagePath,$width[$i],$height[$i]);
                        $i++;
                    }
                } 
                else 
                {
                    exit();
                }
            }
        }
        return array('image' => $imagePath,'url_org' => $date[0].'/'.$date[1].'/'.$date[2].'/','dir_org' => $this->upload_dir. $imagePath);
    }
         
    function create_thumbnail($source, $destination,$thumb_width = false,$thumb_height = false) 
    {
        $thumb = PhpThumbFactory::create($source);
        $thumb->resize($thumb_width, $thumb_height);
        //$thumb->show();
        $thumb->save($destination);
    }
    function get_image_extension($name) 
    {
        $name=strtolower($name);
        $i=strrpos($name,".");
        if(!$i){ return ""; }
        $l=strlen($name)-$i;
        $extension=substr($name,$i+1,$i);
        return $extension;
    }
    
    function createDir($thumb)
    {
        if(!is_dir($this->upload_dir))
        {
            @mkdir($this->upload_dir,0777);
        }
        
        if(!is_dir($this->upload_dir.DS.$thumb))
        {
            @mkdir($this->upload_dir.DS.$thumb,0777,true);
        }
    }
    
    function createDirItem($date, $thumb)
    {
        if(!is_dir($this->upload_dir.DS.$thumb))
        {
            @mkdir($this->upload_dir.DS.$thumb,0777,true);
        }
    }
    
    function upload_files($file,$folder="medias")
    {
        if ($_FILES[$file]["error"] > 0)
        {
            return false;
        }
        else
        {
            if(!is_dir(DATA_DIR."{$folder}")) mkdir(DATA_DIR."{$folder}",0777);
            if (file_exists(DATA_DIR."{$folder}/" . time().'_'.$_FILES[$file]["name"]))
            {
                return time().'_'.$_FILES[$file]["name"];
            }
            else
            {
				//$move=@copy($_FILES[$file]["tmp_name"], $this->upload_dir. $imagePath);
                move_uploaded_file($_FILES[$file]["tmp_name"],DATA_DIR."{$folder}/" . time().'_'.$_FILES[$file]["name"]);
                return time().'_'.$_FILES[$file]["name"];
            }
        }
    }
    
    function upload_file($file,$path_file)
    {
        if ($_FILES[$file]["error"] > 0)
        {
            return false;
        }
        else
        {
            $name = time().'_'.$_FILES[$file]["name"];
            move_uploaded_file($_FILES[$file]["tmp_name"],$path_file.DS.$name);
            return $name;
        }
    }
	
	function unlinkFile($fileName,$filePath = "document_news")
    {
        $src = DATA_DIR.$filePath.$fileName;
        @unlink($src);
        foreach(Item::$arrThunb as $thumb)
        {
            $src = DATA_DIR.$filePath.$thumb.DS.$fileName;
            @unlink($src);
        }
        return true;
    }
    
    static function folderSize($path) {
        $total_size = 0;
        $files = scandir($path);
        foreach($files as $t) {
            if (is_dir(rtrim($path, '/') . '/' . $t)) {
                if ($t<>"." && $t<>"..") {
                    $size = UploadSD::folderSize(rtrim($path, '/') . '/' . $t);
                    $total_size += $size;
                }
            } else {
                $size = filesize(rtrim($path, '/') . '/' . $t);
                $total_size += $size;
            }   
        }
        return $total_size;
    }
    
    static function formatSize($size) {
        $mod = 1024;
        $units = explode(' ','B KB MB GB TB PB');    
        for ($i = 0; $size > $mod; $i++) {    
            $size /= $mod;
        }    
        return round($size, 2) . ' ' . $units[$i];    
    }
}
?>