<?php
/**
 * Flywheel datetime
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: DateTime.php 732 2010-12-11 17:03:55Z phannq $
 * @package		Flywheel
 * @subpackage	DateTime
 */
class Flywheel_DateTime {
	/**
	 * make time from string
	 * 
	 * @param string		$date
	 * @param string		$format format of $date
	 * 
	 * @return Array[month, year, day, hour, minute, second]
	 * @throws Flywheel_Exception if format not defined
	 */
	public static function makeTimeFromString($date, $format ='YYYY-MM-DD HH:II:SS') {		
		$format = strtoupper($format);
		switch ($format) {	
			case 'YYYY/MM/DD HH:II:SS' :
            case 'YYYY-MM-DD HH:II:SS' :
            case 'YYYY.MM.DD HH:II:SS' :
            case 'Y/M/D H:I:S' :
            case 'Y-M-D H:I:S' :
            case 'Y.M.D H:I:S' :
				@list($y, $m, $d, $h, $i, $s) = preg_split('/[-\.\/ :]/', $date);
            	break;

            case 'YYYY/DD/MM HH:II:SS' :
            case 'YYYY-DD-MM HH:II:SS' :
            case 'YYYY.DD.MM HH:II:SS' :
            case 'Y/D/M H:I:S' :
            case 'Y-M-D H:I:S' :
            case 'Y.M.D H:I:S' :
            	list($y, $d, $m, $h, $i, $s) = preg_split( '/[-\.\/ :]/', $date );
            	break;

            case 'DD-MM-YYYY HH:II:SS' :
            case 'DD/MM/YYYY HH:II:SS' :
            case 'DD.MM.YYYY HH:II:SS' :
            case 'D-M-Y H:I:S' :
            case 'D/M/Y H:I:S' :
            case 'D.M.Y H:I:S' :
            	list($d, $m, $y,$h, $i, $s) = preg_split( '/[-\.\/ :]/', $date );
            	break;

            case 'MM-DD-YYYY HH:II:SS' :
            case 'MM/DD/YYYY HH:II:SS' :
            case 'MM.DD.YYYY HH:II:SS' :
            case 'M-D-Y H:I:S' :
            case 'M/D/Y H:I:S' :
            case 'M.D.Y H:I:S' :
            	list($m, $d, $y ,$h, $i, $s) = preg_split( '/[-\.\/ :]/', $date );
            	break;
            	
            default: 
            	throw new Flywheel_Exception('Invalid datetime format!');		
		}
		$h = (null == $h)? 0 : (int) $h;
		$i = (null == $i)? 0 : (int) $i;
		$s = (null == $s)? 0 : (int) $s;
		
		return array((int) $m, (int) $d, (int) $y, $h, $i, $s);
	}
	
	/**
	 * convert date to unix timestamp
	 * @param string	$date
	 * @param string	$format
	 * 
	 * @return integer
	 */
	public static function dateToUnixTimestamp($date, $format = 'YYYY-MM-DD HH:II:SS') {
		$dt = self::makeTimeFromString($date, $format);
		return mktime($dt[3], $dt[4], $dt[5], $dt[0], $dt[1], $dt[2]);		
	}
	
	/**
	 * validate a Gregorian date
	 * 
	 * @param string	$date
	 * @return boolean
	 */
	public static function validate($date, $format = 'YYYY-MM-DD HH:II:SS') {
		$date = self::makeTimeFromString($date, $format);
		return checkdate($date[0], $date[1], $date[2]);		
	}
}