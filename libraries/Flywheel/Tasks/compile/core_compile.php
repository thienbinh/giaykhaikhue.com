<?php
function core_compile_execute() {
	$params = is_array($argument = func_get_arg())? $argument[0] : array();	
	$coreClass = require GLOBAL_CONFIGS_DIR .'core_classes.php';
	if (isset($params['db-driver'])) {
		$drivers = explode(',', $params['db-driver']);
		for ($i = 0, $size = sizeof($drivers); $i < $size; ++$i) {
			switch (trim(strtolower($drivers[$i]))){
				case 'mysql': //MySQL					
					$coreClass[] = 'Flywheel_DB_Driver_MySQL';
					break;
				case 'mysqli': //MySQLi
					$coreClass[] = 'Flywheel_DB_Driver_MySQLi';		
					break;
				default: //pdo
					$coreClass[] = 'Flywheel_DB_Driver_PDO';
			}
		}
	}
	else { //default using Flywheel_DB_Driver_PDO
		$coreClass[] = 'Flywheel_DB_Driver_PDO';		
	}
	
	$coreCompile = new CoreCompile();
	$coreCompile->coreClasses = array_unique($coreClass);
    
	$coreCompile->run();
}

class CoreCompile {
	public $coreClasses;
	
	public function __construct() {		
	}
	
	public function run() {
		$targer = $target = CACHE_DIR .'core' .DS .'core_compile.php';
		$ret = array();
		for($i = 0, $size = sizeof($this->coreClasses); $i < $size; ++$i) {				
			$file = str_replace('Flywheel_', '', $this->coreClasses[$i]);
			$file = Flywheel_DIR .str_replace('_', DS, $file) .'.php';
			$refl = new ReflectionClass($this->coreClasses[$i]);				
            $file  = $refl->getFileName();	            
            $lines = file($file);	
            $start = $refl->getStartLine() - 1;
            $end   = $refl->getEndLine();
            
            //var_dump($lines);

            $ret = array_merge($ret, array_slice($lines, $start, ($end - $start)));
		}
		//var_dump($ret); exit;
		
		 // first write the 'compiled' data to a text file, so
        // that we can use php_strip_whitespace (which only works on files)
        $fp = @fopen($target, 'w');

        if ($fp === false) {
            throw new Flywheel_Exception("Couldn't write compiled data. Failed to open $target");
        }
        
        fwrite($fp, "<?php ". implode('', $ret));
        fclose($fp);

        $stripped = php_strip_whitespace($target);
        $fp = @fopen($target, 'w');
        if ($fp === false) {
            throw new Flywheel_Exception("Couldn't write compiled data. Failed to open $file");
        }
        
        fwrite($fp, $stripped);
        fclose($fp);
		//Write compile file		
	}	
}