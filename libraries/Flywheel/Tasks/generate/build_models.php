<?php
require_once FLYWHEEL_UTIL .DS .'FolderUtil.php';

function build_models_execute() {
	$params = func_get_arg(0);
	
	if (isset($params['k'])) { // using key
		$conn = Flywheel_DB::getConnection($params['k']);				
	}
	else if(isset($params['h']) && isset($params['u']) 
			&& isset($params['p']) && isset($params['db'])) {
		$config = array(
			'host'		=> $params['h'],
			'username'	=> $params['u'],
			'password'	=> $params['p'],
			'dbname'	=> $params['db']
		);
		$conn = new Flywheel_DB_Driver_PDO($config);		
	}
	
	$builder = new BuildModels($conn);
	$builder->mod = (isset($params['m']))? $params['m'] : 'preserve';
	$builder->tbPrefix = (isset($params['prefix']))? $params['prefix'] : '';
	$builder->package = (isset($params['pack']))? $params['pack']: '';
	if (isset($params['igtbl'])) {
		$builder->setIgnoreTables($params['igtbl']);
	}
	
	if (isset($params['igprefix'])) {
		$builder->setIgnoreTablePrefixes($params['igprefix']);
	}
	
	if (isset($params['allow_tbl'])) {
		$builder->setAllowTables($params['allow_tbl']);
	}
	
	if (isset($params['allow_prefix'])) {
		$builder->setAllowTablePrefixes($params['allow_prefix']);
	}
	
	$builder->run();
}

class BuildModels {
	public $mod;
	public $tbPrefix;	
	public $package;
	
	/**
	 * Connection Object
	 * @var Flywheel_DB_Driver_PDO
	 */		
	private $conn;
	private $db;
	
	private $pkField;
	
	/**
	 * ignore table
	 * @var array
	 */
	private $_igrTbls = array();
	
	/**
	 * ignore by table prefix
	 * @var array
	 */
	private $_igrTblPrefix = array();
	
	private $_allowTbls = array();
	
	private $_allowTblPrefix = array();
	
	/**
	 * Construct BuildModels object
	 * @param Flywheel_DB_Abstract $conn
	 */
	public function __construct(Flywheel_DB_Abstract $conn) {
		$this->conn = $conn;
		$this->db = $conn->getDatabase();				
	}
	
	/**
	 * set ignore tables list
	 * @param list $list
	 */
	public function setIgnoreTables($list) {
		if (null == $list) {
			return;
		}
		$this->_igrTbls = explode(',', $list);
	}
	
	/**
	 * set ignore tables by prefixes
	 * @param unknown_type $prefix
	 */
	public function setIgnoreTablePrefixes($prefixes) {
		$prefixes = trim($prefixes);
		
		if (null == $prefixes) {
			return;
		}
		
		$this->_igrTblPrefix = explode(',', $prefixes);
	}
	
	public function setAllowTables($list) {
		if (null == $list) {
			return;
		}
		$this->_allowTbls = explode(',', $list);
	}
	
	/**
	 * set ignore tables by prefixes
	 * @param unknown_type $prefix
	 */
	public function setAllowTablePrefixes($prefixes) {
		$prefixes = trim($prefixes);
		
		if (null == $prefixes) {
			return;
		}
		
		$this->_allowTblPrefix = explode(',', $prefixes);
	}
	
	/**
	 * Get Tables List
	 */
	private function _getTablesList() {
		$tables = array();
		
		$this->conn->query('SHOW TABLES');
		return $this->conn->fetchColumns();
	}
	
	/**
	 * Get List Table Column
	 * 
	 * @param string $table
	 */
	private function _getListTableColumn($table) {
		$column = array();
		$this->conn->query('DESCRIBE ' . $this->conn->nameQuote($table));
		$result = $this->conn->fetchAll();		
		for ($i = 0, $size = sizeof($result); $i < $size; ++$i) {
			$val = array_change_key_case($result[$i], CASE_LOWER);
			$decl = $this->getPortableDeclaration($val);
			$values = isset($decl['values']) ? $decl['values'] : array();
            $val['default'] = $val['default'] == 'CURRENT_TIMESTAMP' ? null : $val['default'];
            
			$description = array(				
				'type'          => $decl['type'][0],
				'alltypes'      => $decl['type'],
				'ntype'         => $val['type'],
				'length'        => $decl['length'],
				'fixed'         => (bool) $decl['fixed'],
				'unsigned'      => (bool) $decl['unsigned'],
				'values'        => $values,
				'primary'       => (strtolower($val['key']) == 'pri'),
				'default'       => $val['default'],
				'notnull'       => (bool) ($val['null'] != 'YES'),
				'autoincrement' => (bool) (strpos($val['extra'], 'auto_increment') !== false),
			);
			
			$column[$val['field']] = $description;
		}

		return $column;
	}
	
	/**
     * Maps a native array description of a field to a MDB2 datatype and length
     *
     * @param array  $field native field description
     * @return array containing the various possible types, length, sign, fixed
     */
    public function getPortableDeclaration(array $field)
    {
        $dbType = strtolower($field['type']);
        $dbType = strtok($dbType, '(), ');
        if ($dbType == 'national') {
            $dbType = strtok('(), ');
        }
        if (isset($field['length'])) {
            $length = $field['length'];
            $decimal = '';
        } else {
            $length = strtok('(), ');
            $decimal = strtok('(), ');
            if ( ! $decimal ) {
                $decimal = null;
            }
        }
        $type = array();
        $unsigned = $fixed = null;

        if ( ! isset($field['name'])) {
            $field['name'] = '';
        }

        $values = null;
        $scale = null;

        switch ($dbType) {
            case 'tinyint':
                $type[] = 'integer';
                $type[] = 'boolean';
                if (preg_match('/^(is|has)/', $field['name'])) {
                    $type = array_reverse($type);
                }
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 1;
            break;
            case 'smallint':
                $type[] = 'integer';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 2;
            break;
            case 'mediumint':
                $type[] = 'integer';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 3;
            break;
            case 'int':
            case 'integer':
                $type[] = 'integer';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 4;
            break;
            case 'bigint':
                $type[] = 'integer';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 8;
            break;
            case 'tinytext':
            case 'mediumtext':
            case 'longtext':
            case 'text':
            case 'text':
            case 'varchar':
                $fixed = false;
            case 'string':
            case 'char':
                $type[] = 'string';
                if ($length == '1') {
                    $type[] = 'boolean';
                    if (preg_match('/^(is|has)/', $field['name'])) {
                        $type = array_reverse($type);
                    }
                } elseif (strstr($dbType, 'text')) {
                    $type[] = 'clob';
                    if ($decimal == 'binary') {
                        $type[] = 'blob';
                    }
                }
                if ($fixed !== false) {
                    $fixed = true;
                }
            break;
            case 'enum':
                $type[] = 'enum';
                preg_match_all('/\'((?:\'\'|[^\'])*)\'/', $field['type'], $matches);
                $length = 0;
                $fixed = false;
                if (is_array($matches)) {
                    foreach ($matches[1] as &$value) {
                        $value = str_replace('\'\'', '\'', $value);
                        $length = max($length, strlen($value));
                    }
                    if ($length == '1' && count($matches[1]) == 2) {
                        $type[] = 'boolean';
                        if (preg_match('/^(is|has)/', $field['name'])) {
                            $type = array_reverse($type);
                        }
                    }

                    $values = $matches[1];
                }
                $type[] = 'integer';
                break;
            case 'set':
                $fixed = false;
                $type[] = 'text';
                $type[] = 'integer';
            break;
            case 'date':
                $type[] = 'date';
                $length = null;
            break;
            case 'datetime':
            case 'timestamp':
                $type[] = 'timestamp';
                $length = null;
            break;
            case 'time':
                $type[] = 'time';
                $length = null;
            break;
            case 'float':
            case 'double':
            case 'real':
                $type[] = 'float';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
            break;
            case 'unknown':
            case 'decimal':
                if ($decimal !== null) {
                    $scale = $decimal;
                }
            case 'numeric':
                $type[] = 'decimal';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
            break;
            case 'tinyblob':
            case 'mediumblob':
            case 'longblob':
            case 'blob':
            case 'binary':
            case 'varbinary':
                $type[] = 'blob';
                $length = null;
            break;
            case 'year':
                $type[] = 'integer';
                $type[] = 'date';
                $length = null;
            break;
            case 'bit':
                $type[] = 'bit';
            break;
            case 'geometry':
            case 'geometrycollection':
            case 'point':
            case 'multipoint':
            case 'linestring':
            case 'multilinestring':
            case 'polygon':
            case 'multipolygon':
                $type[] = 'blob';
                $length = null;
            break;
            default:
                $type[] = $field['type'];
                $length = isset($field['length']) ? $field['length']:null;
        }

        $length = ((int) $length == 0) ? null : (int) $length;
        $def =  array('type' => $type, 'length' => $length, 'unsigned' => $unsigned, 'fixed' => $fixed);
        if ($values !== null) {
            $def['values'] = $values;
        }
        if ($scale !== null) {
            $def['scale'] = $scale;
        }
        return $def;
    }
	
	/**
	 * Help
	 * print help message
	 */
	public function help() {		
	}
	
	public function modelTemplate() {
		$temp =  '/**'.PHP_EOL
                    . '%s' . PHP_EOL
                    . ' */' . PHP_EOL
                    . 'class %s extends Flywheel_Model {' .PHP_EOL
                    . '%s' . PHP_EOL
                    . '%s' . PHP_EOL
                    . '}';
         return $temp;
	}
	
	private function _writeClassComment($className) {
		return ' * ' .$className .PHP_EOL 
					.' *  This class has been auto-generated at ' .date('d/m/Y H:i:s', time()) .PHP_EOL
					.' * @version		$Id: build_models.php 337 2010-11-06 20:24:12Z mylifeisskidrow@gmail.com $' .PHP_EOL
					.' * @package		Model' .PHP_EOL
					.' * @subpackage	' .((null != $this->package)? $this->package : $this->db) . PHP_EOL
					.' * @copyright		VCCorp (c) 2010';
	}
	
	private function _writeClassProperties($column, $infos) {
		$properties = array();
		$properties[] = $column;
		if ($infos['primary'] == true) {			
			$properties[] = 'primary';
			$this->pkField = $column;
		}
		if ($infos['autoincrement'] == true) {			
			$properties[] = 'auto-increment';
		}
		$properties[] = 'type : ' .$infos['ntype'];
		
		if ($infos['type'] == 'string') {			
			$properties[] = 'maxlength : ' .$infos['length'];
		} 
		$properties[] = '@var ' .$infos['type'] .' $' .$column;
		$properties = '    /**' .PHP_EOL .'     * ' 
						.implode(PHP_EOL . '     * ', $properties) .PHP_EOL
						.'     */' .PHP_EOL;
		$properties .= '    public $'.$column;
		if (isset($infos['default']) && $infos['default'] != null) {
			$properties .= ' = ';
			if ($infos['type'] == 'float' || $infos['type'] == 'integer' || $infos['type'] == 'decimal') {
				$properties .= $infos['default'];
			}
			else {
				$properties .= '\'' .$infos['default'] .'\'';
			}
		}
		else if ($infos['notnull'] == true) {
			$properties .= ' = ';
			if ($infos['type'] == 'float' || $infos['type'] == 'integer' || $infos['type'] == 'decimal') {
				$properties .= '0';
			}
			else {
				$properties .= '\'\'';
			}
		}
		$properties .= ';' .PHP_EOL .PHP_EOL;				
		return $properties;		
	}
	
	private function _writeClassTrunk($modelClass) {
		$buffer =  '    /**' .PHP_EOL
				.'     * Read object from database' .PHP_EOL
				.'     * @param int		$id	default null' .PHP_EOL
				.'     * @return '.$modelClass .PHP_EOL
				.'     * @throws Flywheel_DB_Exception' .PHP_EOL
				.'     */'. PHP_EOL
				.'    public function read($id = null) {}'.PHP_EOL.PHP_EOL;

		$buffer .=  '    /**' .PHP_EOL
				.'     * Save model' .PHP_EOL
				.'     * @return int	object id after save' .PHP_EOL
				.'     * @throws Flywheel_DB_Exception' .PHP_EOL
				.'     */'. PHP_EOL
				.'    public function save() {}'.PHP_EOL.PHP_EOL;
				
		$buffer .=  '    /**' .PHP_EOL
				.'     * Read object from database' .PHP_EOL
				.'     * @return boolean' .PHP_EOL
				.'     * @throws Flywheel_DB_Exception' .PHP_EOL
				.'     */'. PHP_EOL
				.'    public function delete() {}'.PHP_EOL.PHP_EOL;
		return $buffer;
	}
	
	private function _writeTableInfoProperties($databaseName, $tableName, $columns) {
		$properties = '';
		//write database name propery
		$properties .= '    /**' .PHP_EOL .'     * database name' .PHP_EOL							
							.'     */'.PHP_EOL
							.'    const DATABASE = \'' .$databaseName .'\';' .PHP_EOL;
		//write table name property
		$properties .= '    /**' .PHP_EOL .'     * database table name' .PHP_EOL
							.'     */'.PHP_EOL
							.'    const TABLE =\'' .$tableName .'\';' .PHP_EOL;
		//write primary field property						
		$properties .= '    /**' .PHP_EOL .'     * primary key field' .PHP_EOL
							.'     */' .PHP_EOL
							.'    const PRIMARY_FIELD = \'' .$this->pkField .'\';' .PHP_EOL;
		//write schema info						
		$fields = array();
		for ($j = 0, $size = sizeof($columns) ; $j < $size; ++$j) {
			$fields[$j] = $tableName.'.`'.$columns[$j] .'`';										
		}		
							
		$properties .= '    /**' .PHP_EOL .'     * database table schema' .PHP_EOL 
							.'     * @static' .PHP_EOL
							.'     * @var array $schema' .PHP_EOL .'     */' .PHP_EOL
							.'    public static $schema = array(' .PHP_EOL
							.'                    \'fields\' => array(\''
							. implode('\', \'', $fields) .'\'),' .PHP_EOL
							.'                    \'columns\' => array(\''
							. implode('\',\'', $columns) .'\'));' .PHP_EOL;
		unset($columns);
		unset($fields);
		return $properties;				
	}
	
	private function registerModelPath() {
		$modelsPathFile = MODELS_DIR .'models_path.php';		
		$fp = @fopen($modelsPathFile, 'w');
		if ($fp === false) {
            throw new Flywheel_Exception("Couldn't write model data. Failed to open $file");
        }
        
        fwrite($fp, "<?php ".PHP_EOL .$buffer);
        fclose($fp);				
		return false;
	}
	
	private function _isInIgnoreList($column) {
		if(count($this->_igrTbls) > 0) {
			if (in_array($column, $this->_igrTbls)) {
				return true;
			}
		}
		
		if (count($this->_igrTblPrefix) > 0) {
			foreach ($this->_igrTblPrefix as $igPrefix) {
				if(strpos($column, $igPrefix) === 0) {
					return true;
				}
			}
		}

		return false;
	}
	
	private function _isInAllowList($column) {
		$return = true;
		if(count($this->_allowTbls) > 0) {
			$return = false;
			if (in_array($column, $this->_allowTbls)) {
				return true;
			}
		} else {
			$return = true;
		}
		
		if (count($this->_allowTblPrefix) > 0) {
			$return = false;
			foreach ($this->_allowTblPrefix as $allowPrefix) {
				if(strpos($column, $allowPrefix) === 0) {
					return true;
				}
			}
		} else {
			$return = true;
		}

		return $return;
	}
	
	public function run() {
		$tables = $this->_getTablesList();
		$db = $this->conn->getDatabase();
		for($i = 0, $size = sizeof($tables); $i < $size; ++$i) {
			$package = (null != $this->package)? $this->package .'_': '';			
			$modelClass = str_replace(' ', '', ucwords(str_replace('_', ' ', trim(str_replace($this->tbPrefix, '', $tables[$i]), '_'))));
			$modelDir = MODELS_DIR .((null != $this->package)? $this->package .DS : '');
			folder_create($modelDir);
			$file = $modelDir .$modelClass .'.php';
			$modelClass = ucfirst($package) .str_replace($package, '', $modelClass);			
			$build = false;
			if ($this->mod == 'all') {
				$build = true;
			}
			else {
				$build = !file_exists($file);
			}			
			
			//check is in ignore list		
			$ignore = $this->_isInIgnoreList($tables[$i]);
			
			//check allow list
			$allow = $this->_isInAllowList($tables[$i]);
			
			if (true === $build) {
				$build = (!$ignore) && $allow;
			}
			
			if ($build) { //code build here				
				$columns = $this->_getListTableColumn($tables[$i]);
				$classComment = $this->_writeClassComment($modelClass);
				$classProperties = '';				
				foreach ($columns as $name => $property) {
					$classProperties .= $this->_writeClassProperties($name, &$property);									
				}
				unset($name);
				unset($property);
				$columns = array_keys($columns);				
				$classProperties .= $this->_writeTableInfoProperties($db, $tables[$i], &$columns);
				unset($columns);
				$template = $this->modelTemplate();
				$buffer = sprintf($this->modelTemplate(), 
										$classComment, 
										$modelClass, 
										$classProperties, 
										$this->_writeClassTrunk($modelClass));
				
				$fp = @fopen($file, 'w');
				if ($fp === false) {
	            	throw new Flywheel_Exception("Couldn't write model data. Failed to open $file");
		        }
		        
		        fwrite($fp, "<?php ".PHP_EOL .$buffer);
		        fclose($fp);
		        
		        unset($buffer);
		        unset($columns);
		        unset($classComment);
		        unset($classProperties);
		        $modelsPath[$modelClass] = $file;		        
				echo 'generate model object ' .$modelClass ." success!\n";							
			}
			else {
				if (true === $allow && false === $ignore) {
					echo 'model object ' .$modelClass ." existed!\n";
				}				
			}
		}
	}
}