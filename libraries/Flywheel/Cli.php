<?php
/**
 * Flywheel CLI
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Cli.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 */

class Flywheel_Cli {
	private $_script;
	private $_tasklist = array(
		'generate:build_models',
		'compile:core_compile'
	);
		
	public function __construct() {}

	public function run($args) {
		$this->_script = $args[0];
		$package = null;

		$task = isset($args[1])? $args[1] : 'help';
		if ($task == 'help') {
			return;
		}		
				
		if (!$this->_checkTaskExists($task)) {
			return;
		}
		
		if (strpos($task, ':') !== false) {
			$task = explode(':', $task);
			$package = $task[0];
			$task = $task[1];						
		}
		
		$fileTask = FLYWHEEL_DIR .'Tasks' .DS 
			.(($package !== null)? $package .DS .$task .'.php' : $task .'.php');
			
		include_once $fileTask;
		
		call_user_func($task .'_execute', $this->_parseArgs(array_slice($args, 2)));
	}
	
	/**
	 * Check task exists
	 * 
	 * @param string $task task name
	 * @return	boolean
	 */
	private function _checkTaskExists($task) {
		return in_array($task, $this->_tasklist);
	}
	
	protected function _parseArgs($args) {		
		$parseParrams = array();
		for($i = 0, $size = sizeof($args); $i < $size; ++$i) {
			if (strpos($args[$i], '--') === 0) {
				$_p = explode('=', str_replace('--', '', $args[$i]));
				$parseParrams[$_p[0]] = $_p[1];
			}
			else {
				$parseParrams[$args[$i]] = $args[$i];
			}
		}

		return $parseParrams;
	}
	
	/**
	 * Get Task List
	 * 
	 * @return array
	 */
	private function _getTaskList() {	
	}
}