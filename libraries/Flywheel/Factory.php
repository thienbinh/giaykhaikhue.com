<?php

class Flywheel_Factory {
	protected static $classes;
	
	/**
	 * get module
	 * 
	 * @param string	$name
	 * @param string	$file
	 * @param array		$params
	 * 
	 * @return	Flywheel_Module
	 */
	public static function getModule($name, $file, $params = array()) {
		if (!isset(self::$classes['modules'])) {
			self::$classes['modules'] = array();
		}
		
		if (null == $name) {
			throw new Flywheel_Module_Exception('module name is empty!');
		}
		
		$app = Flywheel_Application::getApp();
		if ($app->getType() != Flywheel_Application::TYPE_WEB) {
			return false;
		}
		
		if (isset(self::$classes['modules'][$file])) {
			$isGlobalModule = false;
			if (file_exists($modulePath = MODULES_DIR .$file .DS .$file .'.php')) {				
			} else if (file_exists($modulePath = GLOBAL_MODULES_DIR .$file .DS .$file .'.php')) {
				$isGlobalModule = true;
			} else {
				throw new Flywheel_Module_Exception("module $file not found. Please check \"" 
						.GLOBAL_MODULES_DIR .'" or "' .MODELS_DIR .'"');			
			}
			require $modulePath;
			self::$classes['modules'][$file] = new $file($file, $isGlobalModule);
		} else {
			self::$classes['modules'][$file]->name = $name;
			self::$classes['modules'][$file]->setParams($params);			
		}
		
		return self::$classes['modules'][$file];
	}
	
	/**
	 * Get Router
	 * 
	 * @return Flywheel_Application_Router
	 */
	public static function getRouter($type = null) {
		if (!isset(self::$classes['router'])) {
			self::$classes['router'] = array();			
		}
		
		if (null === $type) {
			$type = Flywheel_Config::get('router');			
		}
		
		if (null === $type) { //default web router
			if (!isset(self::$classes['router']['web'])) {
				self::$classes['router']['web'] = new Flywheel_Application_Router();
			}
			
			return self::$classes['router']['web'];			
		}
		
		$router = 'Flywheel_Application_Router' . ucfirst($type);
		if (!isset(self::$classes['router'][$type])) {
			self::$classes['router'][$type] = new $router();
		}
		
		return self::$classes['router'][$type];
	}
	
	/**
	 * Get Request
	 * 
	 * @return Flywheel_Application_Request
	 */
	public static function getRequest() {
		if (!isset(self::$classes['request'])) {
			self::$classes['request'] = new Flywheel_Application_Request();
		}
		
		return self::$classes['request'];
	}
	
	/**
	 * Get Response
	 * 
	 * @return Flywheel_Application_Response
	 */
	public static function getResponse() {
		if (!isset(self::$classes['response'])) {
			self::$classes['response'] = new Flywheel_Application_Response();			
		}
		
		return self::$classes['response'];
	}
	
	/**
	 * Get Template Engine
	 * 
	 * @return Flywheel_Template_Smarty
	 */
	public static function getTemplate() {
		if (!isset(self::$classes['template'])) {
			self::$classes['template'] = new Flywheel_Template_Smarty();
		}
		
		return self::$classes['template'];
	}
	
	/**
	 * Get Document
	 * 
	 * @param string $type. Document type default 'html'
	 * @return Flywheel_Document_Html
	 */
	public static function getDocument($type = 'html') {
		if (!isset(self::$classes['document'][$type])) {
			$class = 'Flywheel_Document_' .ucfirst($type);
			self::$classes['document'][$type] = new $class();
		}
		
		return self::$classes['document'][$type];
	}
	
	/**
	 * Get Session
	 * 
	 * @return Flywheel_Session
	 */
	public static function getSession() {
		if (!isset(self::$classes['session'])) {
			$configFile = (file_exists($configFile = CONFIG_DIR .DS .'session.ini'))? 
							$configFile : GLOBAL_CONFIGS_DIR .DS .'session.ini';
			$config = Flywheel_Config::load($configFile, 'session');
			self::$classes['session'] = new Flywheel_Session($config);
			self::$classes['session']->start();
		}
		return self::$classes['session'];
	}
	
	/**
	 * Get View
	 * 
	 * @return Flywheel_View
	 */
	public static function getView() {
		if (!isset(self::$classes['view'])) {
			self::$classes['view'] = new Flywheel_View();
		}
		
		return self::$classes['view'];
	}
	
	/**
	 * Get Memcache
	 * @param string $key
	 * @return Flywheel_Cache_Memcache
	 */
	public static function getMemcache($key) {
		return Flywheel_Cache_Memcache::getInstances($key);			
	}
}