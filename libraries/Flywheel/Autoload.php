<?php
/**
 * @version 	SVN:$Id: Autoload.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @author 		Volunteer_developer <mysteries.mine@gmail.com>
 * @copyright 	Copyright (c) 2010 Volunteer_developer. All right reseverd.
 * @license 	GNU/GPL see license.php
 */

/**
 * Flywheel Autoload
 * 
 * @package		Flywheel
 * @subpackage	Autoload
 *
 */
class Flywheel_Autoload {
	/**
	 * Register fAutoload in spl autoloader
	 * 
	 * @return void
	 */
	public static function register() {
		ini_set('unserialize_callback_func', 'spl_autoload_call');
		if (false === spl_autoload_register('flywheel_autoload')) {
			throw new RuntimeException(sprintf('Unable to register flywheel_autoload() as an autoloading method.'));	      
	    }
	}
}

function flywheel_autoload($class) {
	if (class_exists($class, false) || interface_exists($class, false)) {
			return true;
	}
	
	if (strpos($class, 'Flywheel') === 0) {
		$class = str_replace('Flywheel_', '', $class);
		$class = FLYWHEEL_PATH .str_replace('_', DS, $class) .'.php';
		if (file_exists($class)) {
			require $class;
			return true;
		}
	}
	
	return false;
}