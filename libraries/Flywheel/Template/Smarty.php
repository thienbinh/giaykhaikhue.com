<?php
/**
 * Flywheel Template Smarty
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Smarty.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Template
 * @copyright	Flywheel Team (c) 2010
 */
require_once LIBRARIES_DIR .'Smarty' .DS .'Smarty.class.php';

class Flywheel_Template_Smarty extends Smarty {
	private $_name = 'default';
	
	public function __construct() {
		parent::__construct();
		$this->_init();
	}
	
	public function setName($name) {
		$this->_name = $name;
	}
	
	/**
	 * Initilaze template
	 */
	private function _init() {
		$this->compile_check = false;		
		if (Flywheel_Application::getApp()->getEnvironment() == Flywheel_Application::ENV_PROD) {
			$this->force_compile = false;			
		}
		else {
			$this->force_compile = true;
		}
		
		//Set template dir	
		$this->template_dir = APP_DIR .'templates' .DS;
		$this->compile_dir = APP_DIR .'templates_c' .DS;
		$this->cache_dir = CACHE_DIR . Flywheel_Application::getApp()->getName() .DS .'temp' .DS;
		
		$this->_createPath($this->compile_dir);
		
		if (Flywheel_Config::get('debug')) {
			$this->debugging = true;
			$this->debug_tpl = LIBRARIES_DIR .'Smarty' .DS.'debug.tpl';
		}
	}
	
	/**
	 * Render
	 *
	 * @param string $layout
	 * @return HTML String
	 */
	public function render($view) {
		$this->_createPath($this->template_dir.$view);		
		return $this->fetch($view .'.tpl', $_SERVER['REQUEST_URI'], null, false);						
	}
	
	/**
	 * Display
	 * 	overight method display on Smarty class
	 *  
	 * @param string $view
	 */
	public function display($view, $cacheId = null, $compileId = null) {
		$this->_createPath($this->template_dir.$view);		
		parent::display($view.'.tpl', $cacheId, $compileId);		
	}
	
	/**
	 * Create Template Path
	 *
	 * @param string $path
	 * @return void
	 */
	private function _createPath($path) {				
		$tempArr = explode(DS, $path);
		array_pop($tempArr);
		$templateDir = implode(DS, $tempArr);
		folder_create($templateDir, '0777');
	}
}