<?php
/**
 * Flywheel Redis Response
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Response.php 46 2010-08-27 09:45:42Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Redis
 */
class Flywheel_Redis_Response {
	/**
     * Represents an error reply (such as -ERR unknown command or -ERR wrong number of arguments for 'select' command)
     *
     * @var int
     */
    const ERROR = 0;

    /**
     * Indicates an integer reply.
     *
     * @var int
     */
    const INT = 1;

    /**
     * Indicates a status reply.
     *
     * @var int
     */
    const STATUS = 2;

    /**
     * Indicates a bulk reply.
     *
     * @var int
     */
    const BULK = 3;

    /**
     * Indicates a multi-bulk reply.
     *
     * @var int
     */
    const MULTIBULK = 4;
    
    /**
     * Indicates server is up and running.
     *
     * @var string
     */
    const STATUS_PONG = 'PONG';

    /**
     * Represents a status code reply that indicates a success response (+OK)
     *
     * @var string
     */
    const STATUS_OK = 'OK';
    
	private $_socket;
	
	private $_type;
	public $_error;
	
	/**
     * Status code for status reply type such as OK or PONG.
     *
     * @var string
     */
	public $status;
	
	/**
     * Data length for integer or bulk or multi-bulk reply.
     *
     * @var string
     */
	public $length;	
	
	public function __construct($socket) {
		$this->_socket = $socket;
		$this->_processReply();		
	}
	
	/**
	 * Check response is error command
	 * 
	 * @return boolean
	 */
	public function isError() {
		return $this->_error != '';
	}
	
	/**
	 * Get Error Message
	 * 
	 * @return string
	 */
	public function getErrorMessage() {
		return $this->_error;
	}
	
	/**
	 * Get Type of redis response {@link http://code.google.com/p/redis/wiki/ReplyTypes}
	 * 
	 * @return int type
	 */
	public function getType() {
		return $this->_type;		
	}
	
	/**
	 * Process Reply
	 */
	private function _processReply() {
		$socket = $this->_socket;
		$c = fgetc($socket);

		switch ($c) {
            case '-': // Error reply such as -ERR unknown command
                $this->_error = substr(trim(fgets($socket), Flywheel_Redis_Command::SEP), 4); // ignore ERR and one white space
                $this->_type  = self::ERROR;
                break;

            case '+': // Status code reply like +OK or +PONG
                $this->status = rtrim(fgets($socket), Flywheel_Redis_Command::SEP);
                $this->_type   = self::STATUS;
                break;

            case ':': // Integer reply which indicates something does not exist like :0
                $this->length = rtrim(fgets($socket), Flywheel_Redis_Command::SEP);
                $this->_type   = self::INT;
                break;

            case '$': // Bulk reply
                // Gets first line for data length
                $this->length = rtrim(fgets($socket), Flywheel_Redis_Command::SEP);
                $this->_type   = self::BULK;

                if (false === is_numeric($this->length)) {
                    throw new Exception('Cannot parse '.$this->length .' as data length');
                }

                break;

            case '*': // Multi-bulk reply
                // Number of data items
                $this->length = trim(fgets($socket), Flywheel_Redis_Command::SEP);
                $this->_type   = self::MULTIBULK;

                if (false === is_numeric($this->length)) {
                    throw new Exception('Cannot parse '.$this->length .' as data length');
                }

                break;

            default:
                throw new Exception('Invalid reply type byte: "' .$c .'"');
        }
	}
	
	/**
	 * Get Bulk
	 * 
	 * @return 
	 * @throws Flywheel_Redis_Exception if type not is a bulk
	 */
	public function getBulk() {
		$length = (int) $this->length;
		if ($this->_type !== self::BULK) {
			throw new Flywheel_Redis_Exception('Redis reply type not a bulk :' .$this->_type);			
		}
		
		$result =  $this->_getBulk($this->_socket, (int) $this->length);
		if ($length < 1) {
			$length = 0;
		}
		return $result;
	}
	
	/**
	 * Get Mutil Bulk
	 * 
	 * @return array
	 * @throws Flywheel_Redis_Exception if type not is multi-bulk
	 */
	public function getMutilBulk() {
		if (self::MULTIBULK !== $this->_type) {
            throw new Flywheel_Redis_Exception('Not a multi-bulk reply: '.$this->_type);
        }
		
        $lenght = (int) $this->length;
        
		if (-1 === $lenght) {			
            return null;
        }

        if (0 === $lenght) {
        	fread($this->_socket, 2);
            return array();
        }
        
        $result = array();
        for ($i = 0; $i<$lenght; ++$i) {
        	$_temp = new self($this->_socket);
        	$result[] = $this->_getBulk($this->_socket, (int) $_temp->length);
        	unset($_temp);
        }        

		if ($lenght < 1) {
			fread($this->_socket, 2);                    
		} else {
			$meta = stream_get_meta_data($this->_socket);
			if ($meta['unread_bytes'] > 0) {
				fread($this->_socket, $meta['unread_bytes']);
			}
		}                              
        
        return $result;
	}
	
	/**
	 * _Get Bulk
	 * @param $socket
	 * @param $length
	 */
	private function _getBulk(&$socket, $length) {
		if ($length === -1) {
            return null;
        }

        if ($length === 0) {
            return '';
        }        
        $buffer = stream_get_contents($socket, $length);
        fread($socket, 2); // move pointer forward up to 2 bytes (\r\n)                
        return $buffer;		
	}
}