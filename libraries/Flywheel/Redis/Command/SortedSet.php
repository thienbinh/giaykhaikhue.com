<?php
/**
 * Flywheel Redis Command SortedSet
 * 
 * @author		Volunteer_developer <hieuluutrong@vccorp.vn>
 * @version		$Id: SortedSet.php 46 2010-08-27 09:45:42Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Redis/Command
 */
class Flywheel_Redis_Command_SortedSet extends Flywheel_Redis_Command {
	/**
	 * Add member to sorted set
	 * using ZADD command {@link http://code.google.com/p/redis/wiki/ZaddCommand}
	 * 
	 * @param string 	$name  Key name
	 * @param mixed  	$value Member
	 * @param number 	$score Score of member
	 * 
	 * @return boolean
	 */	
	public function addToSortedSet($name, $value, $score) {
		$command = array();				
		$command = array('ZADD', $name, $score, $value);
		$command = $this->serializeMultiBulkCommand($command);
		$status = $this->_getResponseResult($this->_conn->send($command));		
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug') && $status) {
			Flywheel_Debug_Profiler::mark('Add item ' .$value .' to sorted set ' 
								.$name .' with score .' .$score .' server', get_class($this));						
		}
		//<-- /end debug code -->		
		return $status;
	}
	
	/**
	 * Delete the specified member from the sorted set by value
	 * - Date: 		12-08-2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param string $key  Key name
	 * @param mixin  $value Member
	 * @return boolean
	 */
	public function removeToSortedSet($key, $value) {
        $command = array();
		$command[] = 'ZREM';
		$command[] = $key;
		$command[] = $value;
		return $this->_getResponseResult(
			$this->_conn->send($this->serializeMultiBulkCommand($command)));
	}
	
	/**
	 * Get all the members of the Sorted Set value at key
	 *
	 * @param string	$name key name
	 * @param boolean	$withScores return values with scores
	 * 					if $withScores = true. method will return nested array
	 * 						ex: array(
	 * 							0 => array('item' => 'item1', 'score' => 1)
	 * 							1 => array('item' => 'item2', 'score' => 2)	 
	 * @param int 		$start start index
	 * @param int 		$end end index
	 * 					if $start = 0 and $end = -1 mean get all sorted value
	 * @param boolean 	$revert revert elements (not used in sorting)
	 * @return array
	 * 
	 * @throws Flywheel_Redis_Exception if $start or $end not is integer
	 */	
	public function getSortedSet($name, $withScores = false, $start = 0, $end = -1, $revert = false) {
		if (!is_int($start) || !is_int($end)) {
			throw new Flywheel_Redis_Exception('Start index or end index must be integer!');			
		}
		
		$command = array($revert ? 'ZREVRANGE' : 'ZRANGE', $name, $start, $end);
		if (true == $withScores) {
        	$command[] = 'WITHSCORES';
        }
        $command = $this->serializeMultiBulkCommand($command);
		$datas = $this->_getResponseResult($this->_conn->send($command));
		
		if ($withScores) {
			$result = array();
			for($i = 0, $size = sizeof($datas); $i< $size; $i+=2) {
				$result[] = array(
						'item' => $datas[$i],
						'score' => $datas[$i+1]
				);					
			}
			$datas = $result;	
		}
		
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug') && is_array($datas)) {
			if (0 === $start && -1 === $end) {
				$label = 'Retrieve all member of sorted set ' .$name;
			}
			else {
				$label = 'Retrieve memver of sorted set ' .$name .' from ' .$start .' to ' .$end;
			}
			Flywheel_Debug_Profiler::mark($label, get_class($this));						
		}
		//<-- /end debug code -->
		
		return $datas;
	}
	
	/**
	 * Get length of Sorted Set
	 * - Date: 		12-08-2010	 
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param string $key Key name
	 * @return integer
	 */
	public function getSortedSetLength($key) {
        $command = "ZCARD $key" ;
        return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Get members from sorted set by min and max score
	 * - Date: 		12-08-2010	 
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @throws Flywheel_Redis_Exception
	 * @param string  $key       Key name
	 * @param number  $min        Min score
	 * @param number  $max        Max score
	 * @param boolean $withScores Get with scores
	 * @param integer $limit      Limit
	 * @param integer $offset     Offset
	 * @return array
	 */	
	public function getFromSortedSetByScore($key, $min, $max, $withScores = null, $limit = null, $offset = null) {
		if (!is_null($limit) && !is_integer($limit)) {
            throw new Flywheel_Redis_Exception("Limit must be integer");
        }
        if (!is_null($offset) && !is_integer($offset)) {
            throw new Flywheel_Redis_Exception("Offset must be integer");
        }
        $command = "ZRANGEBYSCORE  $key $min $max";

        if (!is_null($limit)) {
            if (is_null($offset)) {
                $offset = 0;
            }
            $command .= " LIMIT $offset $limit";
        }
        if ($withScores) {
            $command .= " WITHSCORES";
        }
        return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Increment score of sorted set element
	 * 
	 * @param string	$name key name
	 * @param mixed		$value member
	 * @param number	$score score to increment
	 * @return int
	 */	
	public function incrementScoreInSortedSet($name, $value, $score) {}
	
	/**
	 * Remove all the elements in the sorted set at key with a score between min and max (including elements with score equal to min or max).
	 * - Date: 		12-08-2010	 
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn> 
	 * @param string  $key  Key name
	 * @param numeric $min   Min value
	 * @param numeric $max   Max value
	 * @return integer
	 */
	public function deleteFromSortedSetByScore($key, $min, $max) {
        $command = array();
		$command[] = 'ZREMRANGEBYSCORE';
		$command[] = $key;
		$command[] = $min;
		$command[] = $max;
		return $this->_getResponseResult(
			$this->_conn->send($this->serializeMultiBulkCommand($command)));
	}
	
	/**
	 * Get member score from Sorted Set
	 * - Date: 		12-08-2010
	 * 	 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param string $key
	 * @param mixin $value
	 * @return number
	 */
	public function getScoreFromSortedSet($key, $value) {
		$command = array();
		$command[] = 'ZSCORE';
		$command[] = $key;
		$command[] = $value;
		return $this->_getResponseResult(
			$this->_conn->send($this->serializeMultiBulkCommand($command)));
	}
	
	/**
	 * Get rank of member from sorted set
	 * - Date: 		12-08-2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param string  $key   Key name
	 * @param integer $value  Member value
	 * @param boolean $revert Revert elements (not used in sorting)
	 * @return integer
	 */
	public function getRankFromSortedSet($key, $value, $revert) {
        $command = $revert ? 'ZREVRANK' : 'ZRANK';
        $command .= " " .$key . " " . $value;
        return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Store to key union between the sorted sets
	 * - Date: 		14-08-2010
	 *  
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param array		$keys	array of key names or associative array with weights
	 * @param string	$storeName result sorted set key name
	 * @param string	$aggregation aggregation method: SUM (for default), MIN, MAX.
	 * @return int
	 */	
	public function unionSortedSets($keys, $storeName, $aggregation) {
		if (empty($keys)) {
            throw new Rediska_Command_Exception('You must specify sorted sets');
        }
       	$command = array('ZUNION', $storeName, count($keys));
		foreach($keys as $key) {
        	$command[] = $key;
		}
        if (strtolower($aggregation) != 'sum') {
        	$command[] = 'AGGREGATE';
            $command[] = strtoupper($aggregation);
		}
        return $this->_getResponseResult(
			$this->_conn->send($this->serializeMultiBulkCommand($command)));
	}
		
	/**
	 * Store to key intersection between sorted sets
	 * 
	 * @param array		$names array of key names or associative array with weights
	 * @param string	$storeName result sorted set key name
	 * @param string	$aggregation aggregation method: SUM (for default), MIN, MAX.
	 * @return int
 	*/
	public function intersectSortedSets($names, $storeName, $aggregation) {}
	
	/**
	 * Get sorted elements contained in the List, Set, or Sorted Set value at key.
	 * 
	 * @param string        $name key name
	 * @param string|array  $value opptions or SORT query string (http://code.google.com/p/redis/wiki/SortCommand).
	 * 	 
	 * @return array
	 */
	public function sort($name, $value) {}
}