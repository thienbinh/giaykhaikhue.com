<?php
/**
 * Flywheel Redis Command List
 * 
 * @author		Volunteer_developer <hieuluutrong@vccorp.vn>
 * @version		$Id: List.php 46 2010-08-27 09:45:42Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Redis/Command
 */
class Flywheel_Redis_Command_List extends Flywheel_Redis_Command {
	/**
	 * Prepend
	 * LPUSH
	 * 	Append an element to the head of the List value at key 
	 * 
	 * @param string $list
	 * @param mixed $value
	 * @param RedisDB $conn
	 * 
	 * @return boolean
	 */
	public function prepend($list, $value) {
		$value = json_encode($value);
		$command = array('LPUSH ' .$list .' ' .strlen($value), $value);
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug')) {			
			Flywheel_Debug_Profiler::mark('append item to list ' .$list , get_class($this));						
		}
		//<-- /end debug code -->
		
		return (boolean) $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Append
	 * RPUSH
	 * 	Append an element to the tail of the List value at key 
	 * 
	 * @param string $list
	 * @param mixed $value
	 * @param RedisDB $conn
	 * 
	 * @return boolean
	 */
	public function append($list, $value) {		
		$value = json_encode($value);
		$command = array('RPUSH ' .$list .' ' .strlen($value), $value);
		
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug')) {			
			Flywheel_Debug_Profiler::mark('append item to list ' .$list , get_class($this));						
		}
		//<-- /end debug code -->

		return (boolean) $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * List Length
	 * LLEN
	 * 
	 * @param string	$list
	 * @param RedisDB	$conn
	 * 
	 * @return int
	 */
	public function length($list) {
		return $this->_getResponseResult($this->_conn->send('LLEN ' .$list));
	}
	
	/**
	 * Get list
	 * LRANGE
	 * 
	 * @param string	$list
	 * @param int		$start
	 * @param int		$end
	 * @param RedisDB	$conn
	 * 
	 * @return array
	 */
	public function getList($list, $start = 0, $end = -1) {		
		/*if ($offset == 0) {
			$offset = $this->length($list);			
		}*/
		
//		$end = ($start + $end) -1;
		
		$command = 'LRANGE '.$list .' ' .$start .' ' .$end;
		
		$data = $this->_getResponseResult($this->_conn->send($command));
		$result = array();
		
		for($i = 0, $size = sizeof($data); $i < $size; ++$i) {
			$result[] = json_decode($data[$i]);
		}
		
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug')) {
			if (0 === $start && -1 === $end) {
				$label = 'Retrieve full list  ' .$list .' from server';
			}
			else {
				$label = 'Retrieve '.$offset .' items of list ' 
								.$list .' start ' .$start .' position from server';
			}
			Flywheel_Debug_Profiler::mark($label, get_class($this));						
		}
		//<-- /end debug code -->
		
		return $result;
	}
	
	/**
	 * Trim
	 * LTRIM
	 * 
	 * @param string	$list list name
	 * @param int		$start 
	 * @param int		$end
	 * @param RedisDB	$conn
	 */
	public function trimList($list, $start, $end) {
		return $this->_getResponseResult($this->_conn->send('LTRIM ' .$list .' ' .$start .' ' .$end));
	}
	/**
	 * Return element of List by index at key
	 * - Author: 	Cao Hoang Long <longcaohoang@vccorp.vn>
	 * - Date: 		12-08-2010
	 * 
	 * @throws Flywheel_Redis_Exception
	 * @param string  $key  Key name
	 * @param integer $index Index
	 * @return mixin
	 */
	public function getByIndex($key,$index){
		if (!is_integer($index)) {
            throw new Flywheel_Redis_Exception("Index must be integer");
        }
        $command = "LINDEX $key $index";
       	$data = $this->_getResponseResult($this->_conn->send($command));
		$result = json_decode($data);
       	return $result;
	} 
	/**
	 * Delete element from list by value at key
	 * - Author: 	Cao Hoang Long <longcaohoang@vccorp.vn>
	 * - Date: 		12-08-2010
	 * 
	 * @throws Flywheel_Redis_Exception
	 * @param string $key Key name
	 * @param string $value Element value
	 * @param integer $count Limit of deleted items
	 * @return integer
	 */
	public function removeFromList($key, $value, $count = 0){
		if (!is_integer($count)) {
            throw new Flywheel_Redis_Exception("Count must be integer");
        }
        $value = json_encode($value);
        $command = "LREM $key $count " . strlen($value) . self::SEP . $value;
        return $this->_getResponseResult($this->_conn->send($command));
	}
	/**
	 * Return and remove the last element of the List at key 
	 * - Author: 	Cao Hoang Long <longcaohoang@vccorp.vn>
	 * - Date: 		13-08-2010
	 * 
	 * @param string $key       Key name
	 * @return mixin
	 */
	public function rightPop($key){
		$command = "RPOP $key";
		$data = $this->_getResponseResult($this->_conn->send($command));
		return json_decode($data);
	}
	/**
	 * Return and remove the first element of the List at key 
	 * - Author: 	Cao Hoang Long <longcaohoang@vccorp.vn>
	 * - Date: 		13-08-2010
	 * 
	 * @param string $key       Key name
	 * @return mixin
	 */
	public function leftPop($key){
		 $command = "LPOP $key";
		 $data = $this->_getResponseResult($this->_conn->send($command));
		 return json_decode($data);
	}
	/**
	 * Return and remove the first element of the List at key 
	 * - Date: 		14-08-2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param array $keys       Key name
	 * @param int $timeOut		Time out
	 * @return array
	 */
	public function blockLeftPop($keys, $timeOut = 0){
		//$command = array();
		$command = 'BLPOP';
		foreach($keys as $key){
			$command .= " ".$key;	
		}
		$command .= " ".$timeOut;
		//$command[] = $timeOut;
		$datas = $this->_getResponseResult($this->_conn->send($command));
		$results = array();
		foreach($datas as $data){
			$results[] = json_decode($data);
		}
		return $results;
	}
	/**
	 * Return and remove the last element of the List at key 
	 * - Date: 		14-08-2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param array $keys       Key name
	 * @param int $timeOut		Time out
	 * @return array
	 */
	public function blockRightPop($keys, $timeOut = 0){
		//$command = array();
		$command = 'BRPOP';
		foreach($keys as $key){
			$command .= " ".$key;	
		}
		$command .= " ".$timeOut;
		//$command[] = $timeOut;
		$datas = $this->_getResponseResult($this->_conn->send($command));
		$results = array();
		foreach($datas as $data){
			$results[] = json_decode($data);
		}
		return $results;
	}
}