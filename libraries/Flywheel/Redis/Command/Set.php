<?php
/**
 * Flywheel Redis Command Set
 * 
 * @author		Volunteer_developer <hieuluutrong@vccorp.vn>
 * @version		$Id: Set.php 46 2010-08-27 09:45:42Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Redis/Command
 */

class Flywheel_Redis_Command_Set extends Flywheel_Redis_Command {
	/**
	 * Add the specified member to the Set value at key
	 * - Date: 		12-08-2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param string $key  Key name
	 * @param mixin  $value Value
	 * @return boolean
	 */
	public function addToSet($key, $value) {
		$value = json_encode($value);		
        $command = "SADD $key " . strlen($value) . self::SEP . $value;
        return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Test if the specified value is a member of the Set at key
	 * - Date: 		12-08-2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param string $key  Key value
	 * @prarm mixin  $value Value
	 * @return boolean
	 */
	public function existsInSet($key, $value) {
		$value = json_encode($value);	
        $command = "SISMEMBER  $key " . strlen($value) . self::SEP . $value;
        return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Get random element from the Set value at key
	 * - Date: 		12-08-2010
	 *
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param string  $name Key name
	 * @param boolean $pop If true - pop value from the set
	 * @return mixin
	 */	
	public function getRandomFromSet($key, $pop) {
        if ($pop) {
            $command = "SPOP  $key";
        } else {
            $command = "SRANDMEMBER  $key";
        }
        return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Return the number of elements (the cardinality) of the Set at key
	 * - Date: 		12-08-2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param string $name Key name
	 * @return integer
	 */
	public function countSetMember($key) {
        $command = "SCARD $key";
        return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Return the union between the Sets stored at key1, key2, ..., keyN
	 * - Date: 14/08/2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @throws Flywheel_Redis_Exception
	 * @param array			$keys     Array of key names
	 * @param string|null	$storeName Store union to set with key name
	 * @return array|boolean
	 * 
	 */
	public function unionSet($keys, $storeName = null) {
		if (empty($keys)) {
            throw new Flywheel_Redis_Exception('You must specify sets');
        }

        if (!is_null($storeName)) {
			$command = "SUNIONSTORE " . $storeName;
		} else {
            $command = "SUNION ";
        }

        foreach($keys as $key) {
        	$command .= " " . $key;
		}

        return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Return all the members of the Set value at key
	 * - Date: 		12-08-2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @throws Flywheel_Redis_Exception
	 * @param string $name Key name
	 * @param string $sort Deprecated
	 * @return array
	 */
	public function getAllSetMember($key, $sort = null) {
        if (is_null($sort)) {
            $command = "SMEMBERS $key";
        } else {
            throw new Flywheel_Redis_Exception("This attribute is depricated. You must use 'sort' command for it.");
        }
        $data = $this->_getResponseResult($this->_conn->send($command));
		$result = array();
		for($index = 0, $size = sizeof($data); $index < $size; ++$index)
			$result[$index] = json_decode($data[$index]);
        return $result;
	}
	
	/**
	 * Remove the specified member from the Set value at key
	 * - Date: 		12-08-2010
	 * 
	 * @author Cao Hoang Long <longcaohoang@vccorp.vn>
	 * @param string $name  Key name
	 * @param mixin  $value Value
	 * @return boolean
	 */
	public function deleteFromSet($key, $value) {
		$value = json_encode($value);	
        $command = "SREM $key " . strlen($value) . self::SEP . $value;
        return $this->_getResponseResult($this->_conn->send($command));
	}
}