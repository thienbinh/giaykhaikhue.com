<?php
/**
 * Ming Redis Command Scalar
 * 
 * @author		Volunteer_developer <hieuluutrong@vccorp.vn>
 * @version		$Id: Scalar.php 46 2010-08-27 09:45:42Z mylifeisskidrow@gmail.com $
 * @package		Ming
 * @subpackage	Redis/Command
 */

class Ming_Redis_Command_Scalar extends Ming_Redis_Command {	
	/**
	 * Increment the number value of key by integer
	 * - Author:	Cao Hoang Long <longcaohoang@vccorp.vn>
	 * - Date: 		12-08-2010
	 * 
	 * @throws Ming_Redis_Exception
	 * @param string $key Key name
	 * @param integer $amount Amount to increment
	 * @return integer New value
	 */
	public function increment($key, $amount = 1) {
		if (!is_integer($amount) || $amount <= 0) {
            throw new Ming_Redis_Exception("Amount must be positive integer");
        }
        if ($amount == 1) {
            $command = "INCR $key";
        } else {
            $command = "INCRBY $key $amount";
        }
		return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Decrement the number value of key by integer
	 * - Author:	Cao Hoang Long <longcaohoang@vccorp.vn>
	 * - Date: 		12-08-2010
	 *  
	 * @throws Ming_Redis_Exception
	 * @param string $key Key name
	 * @param integer $amount Amount to decrement
	 * @return integer New value
	 */
	public function decrement($key, $amount = 1) {
		if (!is_integer($amount) || $amount <= 0) {
            throw new Ming_Redis_Exception("Amount must be positive integer");
        }
        if ($amount == 1) {
            $command = "DECR $key";
        } else {
            $command = "DECRBY $key $amount";
        }
        return $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Get value, if value not present set it from chain method
	 * 
	 * @param string	$key
	 * @param mixed		$value
	 */
	public function getOrSetValue($key, $value) {}
}