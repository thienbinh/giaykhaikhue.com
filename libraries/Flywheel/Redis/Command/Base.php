<?php
/**
 * Flywheel Redis Command List
 * 
 * @author		Volunteer_developer <hieuluutrong@vccorp.vn>
 * @version		$Id: Base.php 46 2010-08-27 09:45:42Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Redis/Command
 */
class Flywheel_Redis_Command_Base extends Flywheel_Redis_Command {
	/**
	 * Set a time to live in seconds or timestamp on a key 
	 * 
	 * @throws Flywheel_Redis_Exception
	 * @param string  $name               Key name
	 * @param integer $secondsOrTimestamp Time in seconds or timestamp
	 * @param boolean $isTimestamp        Time is timestamp. For default is false.
	 * @return boolean
	 */
	public function expire($key, $secondsOrTimestamp, $isTimestamp = false){
		if (!is_integer($secondsOrTimestamp) || $secondsOrTimestamp <= 0) {
            throw new Flywheel_Redis_Exception(($isTimestamp ? 'Time' : 'Seconds') . ' must be positive integer');
        }
        if ($isTimestamp) {
            //$this->_checkVersion('1.1');
            $command = 'EXPIREAT';
        } else {
            $command = 'EXPIRE';
        }
        $command = "$command $key $secondsOrTimestamp";
		return (boolean) $this->_getResponseResult($this->_conn->send($command));
	}
	
	/**
	 * Delete
	 * @param string|Array $keys
	 */
	public function delete($keys) {
		$keys = (array) $keys;
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::mark('Remove item(s) ' .implode(', ' ,$keys) .' from server', get_class($this));						
		}
		//<-- /end debug code -->
		return $this->_getResponseResult($this->_conn->send('DEL '. implode(' ', $keys)));
	}	
	
	/**
	 * Exist
	 * 
	 * @param string $key
	 */
	public function exists($key) {
		return (boolean) $this->_getResponseResult($this->_conn->send('EXISTS ' .$key));
	}
}