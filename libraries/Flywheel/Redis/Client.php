<?php
/**
 * Flywheel Redis Client
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Client.php 46 2010-08-27 09:45:42Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Redis
 */

class Flywheel_Redis_Client {
	/**
	 * Redis servers info
	 * 
	 * @var Array
	 */
	protected static $info;
	/**
	 * Instances
	 * 
	 * @staticvar Array of Flywheel_Redis_Clients
	 */	
	protected static $instances = array();
	
	/**
	 * Socket connection stream.
	 * 
	 * @var resource A resource of type "stream"
	 */
	private $_socket;
	
	/**
	 * Config
	 * @var Array
	 */
	private $_config = array(
		'protocol'	=> 'tcp://',
		'host'		=> '127.0.0.1',
		'port'		=> 6379,
		'timeout'	=> 3,
		'send_buffer'	=> 0,
		'send_timeout'	=> 9,
	);
	
	/**
	 * is Connected status
	 * @var boolean
	 */
	private $_established = false;
	
	/**
	 * Db 
	 * @var int
	 */
	private $_db;
	private $_selected = false;
	
	/**
	 * Id of connection
	 * @var string
	 */
	private $_id;
	
	/**
	 * register redis commands method
	 * @var array
	 */
	public $registerCommands = array(	
			//Base commnads
			'expire'			=> 'Flywheel_Redis_Command_Base',
			//Scalar commnads
			'increment'			=> 'Flywheel_Redis_Command_Scalar',
			'decrement'		=> 'Flywheel_Redis_Command_Scalar',			
			'delete'			=> 'Flywheel_Redis_Command_Scalar',
			'exists'			=> 'Flywheel_Redis_Command_Scalar',
			//List commands
			'getByIndex'		=> 'Flywheel_Redis_Command_List',
			'removeFromList'	=> 'Flywheel_Redis_Command_List',
			'leftPop'			=> 'Flywheel_Redis_Command_List',
			'rightPop'			=> 'Flywheel_Redis_Command_List',
			'blockLeftPop'		=> 'Flywheel_Redis_Command_List',
			'blockRightPop' 	=> 'Flywheel_Redis_Command_List',	
			'prepend'			=> 'Flywheel_Redis_Command_List',
			'append'			=> 'Flywheel_Redis_Command_List',
			'length'			=> 'Flywheel_Redis_Command_List',
			'getList'			=> 'Flywheel_Redis_Command_List',
			'trimList'			=> 'Flywheel_Redis_Command_List',
			'removeFromList'	=> 'Flywheel_Redis_Command_List',
			//Set Commands
			'addToSet'			=> 'Flywheel_Redis_Command_Set',
			'existsInSet'		=> 'Flywheel_Redis_Command_Set',
			'countSetMember'	=> 'Flywheel_Redis_Command_Set',
			'unionSet'			=> 'Flywheel_Redis_Command_Set',
			'getAllSetMember'	=> 'Flywheel_Redis_Command_Set',
			'deleteFromSet' 	=> 'Flywheel_Redis_Command_Set',
			//Sorted Set commands
			'removeToSortedSet'	=> 'Flywheel_Redis_Command_SortedSet',
			'getSortedSetLength'=> 'Flywheel_Redis_Command_SortedSet',
			'getFromSortedSetByScore'	=> 'Flywheel_Redis_Command_SortedSet',
			'deleteFromSortedSetByScore'=> 'Flywheel_Redis_Command_SortedSet',
			'getScoreFromSortedSet'		=> 'Flywheel_Redis_Command_SortedSet',
			'getRankFromSortedSet'		=> 'Flywheel_Redis_Command_SortedSet',
			'unionSortedSets' 	=> 'Flywheel_Redis_Command_SortedSet',
			'addToSortedSet'	=> 'Flywheel_Redis_Command_SortedSet',
			'getSortedSet'		=> 'Flywheel_Redis_Command_SortedSet'
	);
	
	protected static $_commandIntances = array();
	
	public function __construct($id, $config = array()) {
		if (isset($config['db']) && $config['db'] != 0) {
			$this->_db = $config['db'];
			unset($config['db']);
		}
		
		$this->_id = $id;
		
		$this->_config = array_merge($this->_config, $config);
		register_shutdown_function(array($this, 'close'));
	}
	
	/**
	 * Get Id of this connection
	 * 
	 * @return string
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Get Redis Servers Info
	 */
	public static function getServersInfo() {
		if (self::$info == null || sizeof(self::$info) == 0) {
			self::$info = Flywheel_Config::load(CACHE_DIR .'redis.ini', 'redis', false);
			if (self::$info === false) {
				self::$info = Flywheel_Config::load(GLOBAL_CONFIGS_DIR .'redis.ini', 'redis');
			}
		}
	}
	
	/**
	 * Get All Instance
	 */
	public static function getAllConnects() {
		return self::$instances;
	}
	
	/**
	 * Get Instances
	 * 
	 * @param string $instance instance name
	 * 
	 * @return Flywheel_Redis_Client
	 */
	public static function getInstance($connect = null) {
		self::getServersInfo();		
		if (!isset(self::$instances[$connect])) {
			if ((null == $connect) || (!isset(self::$info[$connect]))) {
				$connect = self::$info['__default__'];										
			}		
			
			self::$instances[$connect] = new self($connect, self::$info[$connect]);
		}
		
		return self::$instances[$connect];
	}
	
	/**
	 * Is Connect
	 * check connected
	 * 
	 * @return true
	 */
	public function isConnect() {
		return $this->_established;
	}
	
	/**
	 * Send command
	 * @param string $command. Redis Command
	 * 
	 * @return RedisResponse
	 */
	public function send($command) {
		if ($this->_socket == null || $this->_established === false) {
			$this->open();
		}
		
		if ($this->_db != 0 && $this->_selected === false) {
			$ping = fwrite($this->_socket, 'SELECT '. $this->_db .Flywheel_Redis_Command::SEP);
			if ($ping < 1) {
				 throw new Exception('Unable to send SELECT database command to Redis server.');				
			}	

			$reply = new Flywheel_Redis_Response($this->_socket);
			if ($reply->getType() !== Flywheel_Redis_Response::STATUS 
					&& $reply->status !== Flywheel_Redis_Response::STATUS_OK) {
				throw new Flywheel_Redis_Exception('Unable to use database index: '.$this->_db.'.');
			}
			$this->_selected = true;
		}
		
		if (is_array($command)) {
			$command = implode(Flywheel_Redis_Command::SEP, $command);
		}
		$command .= Flywheel_Redis_Command::SEP;
		fwrite($this->_socket, $command);					
		return new Flywheel_Redis_Response($this->_socket);		
	}
	
	/**
	 * Open Socket Connect
	 */
	public function open() {		
		$remoteSocket = $this->_config['protocol'] .$this->_config['host'] .':' .$this->_config['port'];
		$this->_socket = stream_socket_client($remoteSocket, $errorNo, $errorMess, 
										$this->_config['timeout'], STREAM_CLIENT_CONNECT);
		if ($this->_socket === false) {
			$this->close();
			throw new Flywheel_Redis_Exception('Error occurred: ' . $errorMess . '(' .$errorNo .')');
		}		
		
		stream_set_write_buffer($this->_socket, (int) $this->_config['send_buffer']);		
		stream_set_timeout($this->_socket, (int) $this->_config['send_timeout']);
		stream_set_blocking($this->_socket, true);
		$this->_established = true;
		
		return true;
	}
	
	/**
	 * Select database
	 * 
	 * @param string	$db. Database name
	 */
	public function selectDB($db) {
		if ($this->_db != $db) {
			$this->_db = $db;
			$this->_selected = false;
		}
	}
	
	/**
	 * Close Socket Connect
	 */
	public function close() {
		$this->_selected = false;
		if ($this->_established) {
			@fclose($this->_socket);
			$this->_established = false;
		}
	}
	
	/**
	 * Set 
	 * 
	 * @param mixed string|Array $keys. Using array for multi set
	 * @param mixed $value. Key value for set single key
	 * @param boolean $preserve. True for perserver set. {@see SETX and MSETX}
	 */
	public function set($keys, $value = null, $preserve = false) {
	if ($keys == null) {
			return false;
		}
		
		if (is_array($keys)) {// set multipile using MSET of MSETX
			$command = ($preserve)? 'MSETX' : 'MSET';			
			$writeCmd = array();			
			$writeCmd[] = '*' .(sizeof($keys)*2 + 1);
			$writeCmd[] = '$' .strlen($command) .Flywheel_Redis_Command::SEP .$command;
			foreach ($keys as $key => $value) {
				$writeCmd[] = '$' .strlen($key) .Flywheel_Redis_Command::SEP .$key;
				$value = json_encode($value);
				$writeCmd[] = '$' .strlen($value) .Flywheel_Redis_Command::SEP .$value;
			}
			
			//<-- begin debug code -->
			if (true === Flywheel_Config::get('debug')) {
				Flywheel_Debug_Profiler::mark('Stored items ' .implode(', ' ,$keys)	.' to server'
													, get_class($this));						
			}
			//<-- /end debug code -->

			return Flywheel_Redis_Command::getResponseResult($this->send($writeCmd));
		}
		else {
			$command = ($preserve)? 'SETX' : 'SET';
			$value = json_encode($value);
			$command .= ' ' .$keys .' ' .strlen($value);
			
			//<-- begin debug code -->
			if (true === Flywheel_Config::get('debug')) {
				Flywheel_Debug_Profiler::mark('Stored items ' .$keys .' to server', get_class($this));						
			}
			//<-- /end debug code -->			
			
			return Flywheel_Redis_Command::getResponseResult($this->send(array($command, $value)));
		}
	}
	
	/**
	 * Get
	 * 
	 * @param string|Array $keys. if $keys is array, using MGET for get multipile
	 * @param boolean $associative. in MGET mod, if associative = true return associative array
	 */
	public function get($keys, $associative= false) {
		if (is_array($keys)) { //MGET
			$command = 'MGET ' .implode(' ', $keys);
			$data = Flywheel_Redis_Command::getResponseResult($this->send($command));
			$result = array();
			
			for($index = 0, $size = sizeof($data); $index < $size; ++$index) {
				if ($associative) {
					$result[$keys[$index]] = json_decode($data[$index]);
				}
				else {
					$result[$index] = json_decode($data[$index]);
				}
			}
		}
		else {
			$result = Flywheel_Redis_Command::getResponseResult($this->send('GET '.$keys));
			$result = json_decode($result);
		}
		
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::mark('Retrieve item(s) ' 
									.((is_array($keys))? implode(', ' ,$keys) : $keys) 
									.' from server', get_class($this));						
		}
		//<-- /end debug code -->

		return $result;
	}
	
	public function __call($method, $args) {
		static $intances = array();
		
		$commandClass = $this->registerCommands[$method];
		if (!isset($intances[$commandClass])) {
			$intances[$commandClass] = new $commandClass(&$this);
		}
		
		return call_user_method_array($method, &$intances[$commandClass], $args);
	}
	
	public function __destruct() {
		self::$_commandIntances = array();
		$this->close();		
	}
	
	/**
     * Insert data to redis
     * - Date: 19-08-2010
     * 
     * @author 				Cao Hoang Long <longcaohoang@vccorp.vn>
     * @author				Volunteer_developer <hieuluutrong@vccorp.vn>		
     * @param string 		$table table name
     * @param Flywheel_Model	$object
     * @param string		$key, key of value set to redis
     * @param string		$pk, the primary field of table
     * 
     * @return int			object id after insert
     */
	public function insert($table, $object, $key, $pk = 'id') {		
		$id = $this->increment($table .'_pk');
		$object->id = $id;
		$this->set($key .$id, $object);
    	return $id;
    }
}