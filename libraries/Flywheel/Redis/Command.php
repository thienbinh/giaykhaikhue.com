<?php
/**
 * Flywheel Redis Command
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Command.php 46 2010-08-27 09:45:42Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Redis
 */
class Flywheel_Redis_Command {
	const SEP = "\r\n";	
	/**
	 * @var Flywheel_Redis_Client
	 */
	protected $_conn;
	
	public function __construct($conn) {
		if (null === $this->_conn || ($this->_conn->getId() !== $conn->getId())) {
			$this->setCurrentConnect($conn);			
		}
	}
	
	public function __call($method, $args) {
		static $intances = array();
		
		$commandClass = $this->registerCommands[$method];
		if (!isset($intances[$commandClass])) {
			$intances[$commandClass] = new $commandClass(); 
		}
		
		return call_user_method($method, $intances[$commandClass], $args);
	}
	
	/**
	 * Get Response Result
	 * 
	 * @param Flywheel_Redis_Response $response
	 * @param boolean $getRaw
	 */
	protected function _getResponseResult(Flywheel_Redis_Response $response, $getRaw = false) {
		return self::getResponseResult($response, $getRaw);				
	}
	
	
	/**
	 * Get Response Result
	 * 
	 * @param Flywheel_Redis_Response $response
	 * @param boolean $getRaw
	 */
	public static function getResponseResult(Flywheel_Redis_Response $response, $getRaw = false) {
		switch ($response->getType()) {
			case Flywheel_Redis_Response::ERROR :
				throw new Flywheel_Redis_Exception($response->getErrorMessage());
				
			case Flywheel_Redis_Response::STATUS :
				if ($response->status == Flywheel_Redis_Response::STATUS_OK && !$getRaw) {
					return true;
				}
				else {
					return $response->status;
				}
				break;
				
			case Flywheel_Redis_Response::INT :
				return (int) $response->length;
				
			case Flywheel_Redis_Response::BULK :				
				return $response->getBulk();
			case Flywheel_Redis_Response::MULTIBULK :
				return $response->getMutilBulk();
				
			default:
				return false;				
		}		
	}
	
	/**
	 * Set Current Connect
	 * 
	 * @param Flywheel_Redis_Client $conn
	 */
	public function setCurrentConnect(Flywheel_Redis_Client $conn) {
		$this->_conn = $conn;
		return true;
	}
	
	/**
	 * serialize multi bulk command
	 * 
	 * @param array $command
	 * @return string redis command
	 */
	public function serializeMultiBulkCommand($command) {
		if (!is_array($command)) {
			return false;			
		}
		
		$buffer = array();
		$buffer[] = '*' .sizeof($command) .self::SEP;
		for($i = 0, $size = sizeof($command); $i < $size; ++$i) {
			$buffer[] = '$' .strlen($command[$i]) .self::SEP;
			$buffer[] = $command[$i] .self::SEP;			
		}
		
		return implode('', $buffer);
	}
}