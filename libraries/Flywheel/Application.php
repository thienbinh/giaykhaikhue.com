<?php

/**
 * Flywheel Application
 * 
 * @abstract
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Application.php 941 2011-01-03 08:51:08Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage	Application
 * @copyright	Flywheel Team (c) 2010
 */
abstract class Flywheel_Application {
    const ENV_PROD = 1;
    const ENV_TEST = 2;
    const ENV_DEV = 3;

    const TYPE_WEB = 1;
    const TYPE_API = 2;
    const TYPE_CONSOLE = 3;

    /**
     * Application Instance
     * 
     * @var Flywheel_Application_Web
     */
    protected static $_app;
    /**
     * Environment
     * 
     * @var string
     */
    protected $_env;
    /**
     * Application name
     * @var string
     */
    protected $_name;
    /**
     * Controller
     * 
     * @var Flywheel_Controller
     */
    protected $_controller;
    /**
     * type of this application {@see class constant}
     * @var integer $_type
     */
    protected $_type;
    protected $_isCli = false;

    /**
     * Create Web Application
     * 
     * @param string $name
     * @param int $environment ENV_PROD|ENV_TEST|ENV_DEV
     * @param boolean $debug
     * 
     * @return Flywheel_Application_Web
     */
    public static function createWebApp($name, $environment, $debug = false) {
        if ($environment == self::ENV_PROD || $environment == self::ENV_TEST) {
            self::_coreCompile();
        }

        Flywheel_Config::set('debug', $debug);
        self::$_app = new Flywheel_Application_Web($name, $environment);
        self::$_app->_type = self::TYPE_WEB;
        self::$_app->_init();

        return self::$_app;
    }

    /**
     * create api application
     * 
     * @param string	$name
     * @param integer	$enviroment @see self::ENV_*
     * @param boolean	$debug
     * 
     * @return Flywheel_Application_Api
     */
    public static function createApiApp($name, $enviroment, $debug = false) {
        Flywheel_Config::set('debug', $debug);
        self::$_app = new Flywheel_Application_Api($name, $enviroment);
        self::$_app->_type = self::TYPE_API;
        self::$_app->_init();
        return self::$_app;
    }

    /**
     * create console application
     * 
     * @param string	$name
     * @param integer	$environment @see self::ENV_*
     * @param boolean	$debug
     * 
     * @return Flywheel_Application_Console
     */
    public static function createConsoleApp($name, $environment, $debug = false) {
        Flywheel_Config::set('debug', $debug);
        self::$_app = new Flywheel_Application_Console($name, $environment);
        self::$_app->_type = self::TYPE_CONSOLE;
        self::$_app->_init();

        return self::$_app;
    }

    /**
     * create debug shower
     */
    public static function createDebugShower() {
        Flywheel_Config::set('debug', true);
        define('CONFIG_DIR', GLOBAL_CONFIGS_DIR);
    }

    /**
     * Core Compile
     */
    protected static function _coreCompile() {
        if (file_exists($target = CACHE_DIR . 'core' . DS . 'core_compile.php')) {
            require_once $target;
            return;
        } else {
            require_once FLYWHEEL_DIR . 'Tasks' . DS . 'compile' . DS . 'core_compile.php';
            core_compile_execute();
        }
    }

    /**
     * Get Name
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * Get Application enviroment
     *  framework support three environment
     *  	- product: @see self::ENV_PROD
     *  	- test: @see self::ENV_TEST
     *  	- develop: @see self:: ENV_DEV
     */
    public function getEnvironment() {
        return $this->_env;
    }

    /**
     * get type of this application
     * 	framework support three app type: 
     *  	- web @see self::TYPE_WEB
     *  	- api @see self::TYPE_API
     *  	- console @see self::TYPE_CONSOLE
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * get controller object
     * @return Flywheel_Controller
     */
    public function getController() {
        return $this->_controller;
    }

    /**
     * Get Application
     * 
     * @return Flywheel_Application
     */
    public static function getApp() {
        return self::$_app;
    }

    /**
     * get execute environment
     * 
     * @return string
     */
    public static function getAppEnvironment() {
        if (self::$_app instanceof self) {
            return self::$_app->_env;
        }

        return null;
    }

    /**
     * End Application
     * @param string|int $mess
     */
    public static function end($mess = 0) {
        exit($mess);
    }

    protected abstract function _init();

    public abstract function run();

    public static function isCli() {
        return (substr(php_sapi_name(), 0, 3) == 'cli');
    }

}