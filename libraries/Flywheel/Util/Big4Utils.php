<?php
/**
 * Generate a random base code 64
 */
function randStr() {
  return base64_encode('vni_token='.microtime().'+'.rand());
}
function joinBig4WithFlywheel($Flywheel_user, $fu){
	$username	=	$Flywheel_user['username'];
	$password	=	$Flywheel_user['password'];
	$arrError	=	array(
						'error'		=>	false,
						'message'	=>	'',
						'user_id'	=>	''
					);
	if(!$username || !$password){
		$arrError['error']	=	true;
		$arrError['message'] = 	"Bạn phải nhập tên đăng nhập và mật khẩu.";
	}else{
		$user	=	Users::findByUsername( $username );
		if($user){
			require_once LIBRARIES_DIR.'Flywheel/Util/RandomUtils.php';
			$saltedpass =	generateHash($password, substr($user->password, 0, SALT_LENGTH));
			if($saltedpass == $user->password){
				$auth_user		=	Flywheel_Authen::getInstance();
				$auth_user->authenticate($username, $password);
				$foreign_user	=	new ForeignUsers();
				$foreign_user->user_id			=	$user->id;
				$foreign_user->foreign_id		=	$fu['foreign_id'];
				$foreign_user->foreign_type		=	$fu['foreign_type'];
				$foreign_user->foreign_avatar	=	$fu['foreign_avatar'];
				$foreign_user->foreign_fullname	=	$fu['foreign_fullname'];
				$foreign_user->foreign_username	=	$fu['foreign_username'];
				if(!$foreign_user->getIdByUserid($user->id, $type)){
					$foreign_user->save();
				}
				$arrError['user_id']	=	$user->id;
			}else{
				$arrError['error']	=	true;
				$arrError['message'] = 	"Mật khẩu không đúng.";
			}
		}else{
			$arrError['error']	=	true;
			$arrError['message'] = 	"Tài khoản này không tồn tại.";
		}
	}
	return $arrError;
}

function get_access_token($user_id){
	include_once LIBRARIES_DIR.'Oauth/OAuth.php';
	include_once LIBRARIES_DIR.'Oauth/LHOAuthDataStore.php';
	$info	=	explode('-Flywheel-', base64_decode( Flywheel_Authen::getCookie('site_key_secret') ));
	$consumer_key		=	$info[0];
	$consumer_secret	=	$info[1];
	$callback			=	$info[2];
	$infoApp	=	AppCustomer::findByAppkey($consumer_key);
	if($infoApp){
		$oauth_store	=	new LHOAuthDataStore();
		$access_token	=	$oauth_store->new_access_for_big4($consumer_key, $user_id);
		if($access_token){
			if($callback != ''){
				$access_token['callback']	=	$callback;
			}else{
				$access_token['callback']	=	$infoApp->app_callback;
			}
			return $access_token;
		}
	}
	return false;
}
?>