<?php
/**
 * Washes strings from unwanted noise.
 *
 * Helpful methods to make unsafe strings usable.
 * 
 * @author 		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id$
 * @package		Flywheel
 * @subpackage	Util
 *
 */

/**
 * Removes any non-alphanumeric characters.
 *
 * @param string $string String to sanitize
 * @param array $allowed An array of additional characters that are not to be removed.
 * @return string Sanitized string
 */
function sanitize_paranoid($string, $allowed = array()) {
	$allow = null;
	if (!empty($allowed)) {
		foreach ($allowed as $value) {
			$allow .= "\\$value";
		}
	}
	
	if (is_array($string)) {
		$cleaned = array();
		foreach ($string as $key => $clean) {
			$cleaned[$key] = preg_replace("/[^{$allow}a-zA-Z0-9_-]/", '', $clean);
		}
	} else {
		$cleaned = preg_replace("/[^{$allow}a-zA-Z0-9]/", '', $string);
	}
	return $cleaned;
}

/**
 * Strips extra whitespace from output
 *
 * @param string $str String to sanitize
 * @return string whitespace sanitized string 
 */
function sanitize_strip_whitespace($str) {
	$r = preg_replace('/[\n\r\t]+/', '', $str);
	return preg_replace('/\s{2,}/', ' ', $r);
}

/**
 * Strips image tags from output
 *
 * @param string $str String to sanitize
 * @return string Sting with images stripped.
 */
function sanitize_strip_images($str) {
	$str = preg_replace('/(<a[^>]*>)(<img[^>]+alt=")([^"]*)("[^>]*>)(<\/a>)/i', '$1$3$5<br />', $str);
	$str = preg_replace('/(<img[^>]+alt=")([^"]*)("[^>]*>)/i', '$2<br />', $str);
	$str = preg_replace('/<img[^>]*>/i', '', $str);
	return $str;
}

/**
 * Strips scripts and stylesheets from output
 *
 * @param string $str String to sanitize
 * @return string String with <script>, <style>, <link> elements removed.
 */
function sanitize_strip_scripts($str) {
	return preg_replace('/(<link[^>]+rel="[^"]*stylesheet"[^>]*>|<img[^>]*>|style="[^"]*")|<script[^>]*>.*?<\/script>|<style[^>]*>.*?<\/style>|<!--.*?-->/is', '', $str);
}

/**
 * Strips extra whitespace, images, scripts and stylesheets from output
 *
 * @param string $str String to sanitize
 * @return string sanitized string
 */
function sanitize_strip_all($str) {
	$str = sanitize_strip_whitespace($str);
	$str = sanitize_strip_images($str);
	$str = sanitize_strip_scripts($str);
	return $str;	
}

/**
 * Sanitizes given array or value for safe input. Use the options to specify
 * the connection to use, and what filters should be applied (with a boolean
 * value). Valid filters:
 *
 * - odd_spaces - removes any non space whitespace characters
 * - encode - Encode any html entities. Encode must be true for the `remove_html` to work.
 * - dollar - Escape `$` with `\$`
 * - carriage - Remove `\r`
 * - unicode - 
 * - backslash -
 * - remove_html - Strip HTML with strip_tags. `encode` must be true for this option to work.
 *
 * @param mixed $data Data to sanitize
 * @param mixed $options If string, DB connection being used, otherwise set of options
 * @return mixed Sanitized data
 */
function sanitize_clean($data, $options = array()) {
	if (empty($data)) {
		return $data;
	}

	if (!is_array($options)) {
		$options = array();
	}

	$options = array_merge(array(
		'odd_spaces' => true,
		'remove_html' => false,
		'dollar' => true,
		'carriage' => true,
		'unicode' => true,
		'backslash' => true
	), $options);

	if (is_array($data)) {
		foreach ($data as $key => $val) {
			$data[$key] = sanitize_clean($val, $options);
		}
		return $data;
	} else {
		if ($options['odd_spaces']) {
			$data = str_replace(chr(0xCA), '', str_replace(' ', ' ', $data));
		}
		if ($options['dollar']) {
			$data = str_replace("\\\$", "$", $data);
		}
		if ($options['carriage']) {
			$data = str_replace("\r", "", $data);
		}

		$data = str_replace("'", "'", str_replace("!", "!", $data));

		if ($options['unicode']) {
			$data = preg_replace("/&amp;#([0-9]+);/s", "&#\\1;", $data);
		}
		if ($options['backslash']) {
			$data = preg_replace("/\\\(?!&amp;#|\?#)/", "\\", $data);
		}
		return $data;
	}	
}