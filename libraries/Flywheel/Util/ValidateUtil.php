<?php
/**
 * Check is an Email address is valid
 * @param String $email
 * 
 * @return boolean
 */
function is_valid_email($email) {
	return preg_match('/^[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-\.]+\.[a-zA-Z]{2,4}$/', $email);
}

/**
 * check is valid username
 * @param string	$username
 * @param integer	$minLenght
 * @param integer	$maxLength
 * 
 * @return boolean
 */
function is_valid_username($username, $minLenght = 3, $maxLength = 16) {
	$pattern = "/^[a-z0-9_-]{" .$minLenght .',' .$maxLength ."}$/";
	return preg_match($pattern, $username);		
}

function is_empty() {	
}

/**
 * Validate a multiple select.
 *
 * Valid Options
 *
 * - in => provide a list of choices that selections must be made from
 * - max => maximun number of non-zero choices that can be made
 * - min => minimum number of non-zero choices that can be made
 *
 * @param mixed $check Value to check
 * @param mixed $options Options for the check.
 * @return boolean Success
 */
function is_valid_multiple($check, $options = array()) {
	$defaults = array('in' => null, 'max' => null, 'min' => null);
	$options = array_merge($defaults, $options);
	$check = array_filter((array)$check);
	if (empty($check)) {
		return false;
	}
	if ($options['max'] && count($check) > $options['max']) {
		return false;
	}
	if ($options['min'] && count($check) < $options['min']) {
		return false;
	}
	if ($options['in'] && is_array($options['in'])) {
		foreach ($check as $val) {
			if (!in_array($val, $options['in'])) {
				return false;
			}
		}
	}
	return true;
}

/**
 * Check that a value is a valid phone number.
 *
 * @param mixed $check Value to check (string or array)
 * @param string $regex Regular expression to use
 * @param string $country Country code (defaults to 'all')
 * @return boolean Success 
 */
function is_valid_phone_no($check, $regex = null, $country = 'all') {
}