<?php
/**
 * Folder Util
 * 	
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: FolderUtil.php 1205 2011-03-09 10:08:54Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage	Util
 *
 */
/**
 * folder explode
 *
 * @param string 	$path
 * @param string 	$filter filter mime type
 * @param int 		$deep deep of sub folder scan
 * @param boolean 	$fullPath	 
 * @return Array
 */

function folder_list_files($path, $filter = '.', $deep = 1, $fullPath = false, $exclude = array('.svn', 'CVS')) {	
	$arr = array();

	//Check path
	$path = folder_clean($path);

	// Is the path a folder?
	if (!is_dir($path)) {
		return false;
	}

	// read the source directory
	$handle = opendir($path);
	while (($file = readdir($handle)) !== false) {
		if (($file != '.') && ($file != '..') && (!in_array($file, $exclude))) {
			$dir = $path . DS . $file;			
			if (is_dir($dir)) {
				$deep--;
				if ($deep !== 0) {
					$arr2 = folder_list_files($dir, $filter, $deep, $fullPath);
					if (false !== $arr2) {						
						$arr = array_merge($arr, $arr2);
					}
				}
			} 
			else {
				if (strpos($file, $filter) !== false) {					
					if ($fullPath) {
						$arr[] = $path . DS . $file;
					} 
					else {
						$arr[] = $file;
					}
				}
			}
		}
	}
	closedir($handle);

	asort($arr);
	return $arr;
}

/**
 * Utility function to read the folders in a folder.
 *
 * @param	string	The path of the folder to read.
 * @param	string	A filter for folder names.
 * @param	mixed	True to recursively search into sub-folders, or an integer to specify the maximum depth.
 * @param	boolean	True to return the full path to the folders.
 * @param	array	Array with names of folders which should not be shown in the result.
 * @return	array	Folders in the given folder. 
 */
function folder_list_folders($path, $filter = '.', $recurse = false, $fullpath = false, $exclude = array('.svn', 'CVS')) {
	// Initialize variables
	$arr = array();

	// Check to make sure the path valid and clean
	$path = folder_clean($path);

	// Is the path a folder?
	if (!is_dir($path)) {		
		return false;
	}

	// read the source directory
	$handle = opendir($path);
	while (($file = readdir($handle)) !== false) {
		if (($file != '.') && ($file != '..') && (!in_array($file, $exclude))) {
			$dir = $path . DS . $file;
			$isDir = is_dir($dir);
			if ($isDir) {
				// Removes filtered directories
				if (preg_match("/$filter/", $file)) {
					if ($fullpath) {
						$arr[] = '/'.$dir;
					} else {
						$arr[] = $dir.'/'.$file;
					}
				}
				if ($recurse) {
					if (is_integer($recurse)) {
						$arr2 = folder_list_folders($dir, $filter, $recurse - 1, $fullpath);
					} else {
						$arr2 = folder_list_folders($dir, $filter, $recurse, $fullpath);
					}
					
					$arr = array_merge($arr, $arr2);
				}
			}
		}
	}
	closedir($handle);

	asort($arr);
	return $arr;
}
/**
 * Create
 * 
 * Tao folder, chmod
 *
 * @param string $path
 * @param string $mode
 */
function folder_create($path, $mode = 0755) {		
	// Check if dir already exists		
	if (folder_exists($path)) {
		return true;
	}
	$path = folder_clean($path);
	static $nested = 0;
	
	//Check parrent directory
	$parent = dirname($path);
	if(!folder_exists($parent)) {
		$nested++;
		
		if (($nested > 20) || ($parent == $path)) {				
			$nested--;
			return false;
		}
		
		//create parent
		if (folder_create($parent, $mode) !== true) {
			$nested--;
			return false;
		}
		$nested--;
	}
	$old = umask(0000); 		
	@mkdir($path, $mode, true);		
    umask($old);

	return true;
}

/**
 * Check exists folder
 *
 * @param string $path
 * @return boolean
 */
function folder_exists($path) {
	return is_dir(folder_clean($path));
}

/**
 * Clean 
 *  strip '/', '\' trong path
 *
 * @static
 * @param	string	$path	Duong dan
 * @param	string	$ds		Directory separator
 * @return	string	The cleaned path
 */
function folder_clean($path, $ds = DS) {
	$path = trim($path);

	if (empty($path)) {
		$path = ''; //wait for define
	} else {
		// Remove double slashes and backslahses and convert all slashes and backslashes to DS
		$path = preg_replace('#[/\\\\]+#', $ds, $path);
	}

	return $path;
}

function folder_clean_file_name($filename) {
	$bad = array(
		"<!--",
		"-->",
		"'",
		"<",
		">",
		'"',
		'&',
		'$',
		'=',
		';',
		'?',
		'/',
		"%20",
		"%22",
		"%3c",		// <
		"%253c", 	// <
		"%3e", 		// >
		"%0e", 		// >
		"%28", 		// (
		"%29", 		// )
		"%2528", 	// (
		"%26", 		// &
		"%24", 		// $
		"%3f", 		// ?
		"%3b", 		// ;
		"%3d"		// =
	);
				
	$filename = str_replace($bad, '', $filename);

	return stripslashes($filename);	
}

/**
 * Lists folder in format suitable for tree display.
 *
 * @access	public
 * @param	string	The path of the folder to read.
 * @param	string	A filter for folder names.
 * @param	int	The maximum number of levels to recursively read, defaults to three.
 * @param	int	The current level, optional.
 * @param	int	Unique identifier of the parent folder, if any.
 * @return	array	Folders in the given folder. 
 */
function folder_tree($path, $filter, $maxLevel = 3, $level = 0, $parent = 0) {
}

/**
 * Read file with each line is index of array
 *
 * @param string	$file			The path of file to read
 * @param boolean $skipEmptyLine	Skip empty line in result
 * @return array |boolean
 */
function file_read_to_array($file, $skipEmptyLine = true) {
	if (true !== file_exists($file)) {
		return false;
	}
	
	$result = array();
	$fp = fopen($file, 'r');
	if (false == $fp) {
		return false;
	}
	while (!feof($fp)) {
		$_b = trim(fgets($fp));
		if (null == $_b && true == $skipEmptyLine) {
			continue;
		}
		$result[] = $_b;
	}
	fclose($fp);
	if (count($result) == 0) {
		return array();
	}
	return $result;
}

/**
 * make download single file
 *
 * @param string	$filePath
 * @param string	$mime mime type of file download @see http://php.net/manual/en/book.mime-magic.php
 * @param sting		$asfName file name after download, 
 * 							using raw file name if $asfName is null
 * 
 * @return boolean
 */
function make_download_file($filePath, $mime, $asfName = null) {
	if (null == $asfName) {
		$asfName	= basename($filePath);		
	}
	$fileSize	= filesize($filePath);
	
	//set response header
	$response	= Flywheel_Factory::getResponse();
	$response->setHeader('Pragma', 		'public');
	$response->setHeader('Expires', 	0);
	$response->setHeader('Cache-Control', 	'must-revalidate, post-check=0, pre-check=0');
	$response->setHeader('Cache-Control', 	'public', true);	
	$response->setHeader('Content-Description', 'File Transfer');
	$response->setHeader('Content-Type', 	$mime);
	$response->setHeader('Content-Disposition', 	'attachment; filename="' .$asfName .'"');
	$response->setHeader('Content-Transfer-Encoding',	'binary');
	$response->setHeader('Content-Length',	$fileSize);
	
	// download
	// @readfile($file_path);
	$file = @fopen($filePath, 'rb');
	if ($file) {
		while(!feof($file)) {
			print(fread($file, 1024*8));
			flush();
			if (connection_status()!=0) {
				@fclose($file);
				die();
			}
		}
		@fclose($file);
		return true;
	}
	
	return false;
}

/**
 * make zip multi files and down load them
 *
 * @param array		$files association array with key is source file and value is location name
 * 					Ex:
 * 					$files = array('path/to/server_file.jpg' => 'client.jpg',
 * 									'path/to/server_file.txt' => 'client.txt');
 * @param string	$destination	file name of the ZIP archive to open.
 * @param boolean	$overwrite		overwrite exists file or not
 * 
 * @return boolean
 */
function make_download_zip_file($files, $destination, $overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { 
		return false;
	}
	
	//vars
	$existsFiles = array();
  	//if files were passed in...
	if(false == is_array($files)) {
		$files = array($files => $files);
	}
    //cycle through each file
    foreach($files as $name => $file) {
      	//make sure the file exists
		if(file_exists($name)) {
			$existsFiles[$name] = $file;
		}
	}
	
	if (sizeof($existsFiles) == 0) {
		return false;
	}
	
	$zip	= new ZipArchive();
	if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
      return false;
    }	
    //add the files
    foreach($existsFiles as $name => $file) {
      $zip->addFile($file, $name);
    }
    //close the zip -- done!
    $zip->close();
    return make_download_file($destination, 'application/zip');
}