<?php

/**
 * Define parse_ini_string if it doesn't exist.
 */
if (!function_exists('parse_ini_string')) {

    /**
     * parse ini string to array
     * 
     * @param $string
     */
    function parse_ini_string($string) {
        $array = array();

        $lines = explode("\n", $string);

        foreach ($lines as $line) {
            $statement = preg_match("/^(?!;)(?P<key>[\w+\.\-]+?)\s*=\s*(?P<value>.+?)\s*$/", $line, $match);

            if ($statement) {
                $key = $match['key'];
                $value = $match['value'];
                if ('on' == $value) {
                    $value = '1';
                } else if ('off' == $value) {
                    $value = '0';
                }

//                $value = str_replace(array('&quot;', '"', "'"), '', $value);               
                // Remove quote
                if (preg_match("/^\".*\"$/", $value) || preg_match("/^'.*'$/", $value)) {
                    $value = mb_substr($value, 1, mb_strlen($value) - 2);
                }

                if (0 !== strpos(';')) {
                    $array[$key] = $value;
                }
            }
        }
        return $array;
    }

}

/**
 * check string length
 * 
 * @param string	$string
 * @param integer	$min
 * @param integer	$max
 * 
 * @return boolean
 */
function check_string_length($string, $min = 0, $max = 0) {
    return true;
}

function write_ini_file($assoc_arr, $path) {
    $content = "";
    foreach ($assoc_arr as $key => $value) {
        if (is_array($value)) {
            $content .= "[" . $key . "]\n";
            foreach ($value as $k => $v) {
                $content .= $k . " = " . $v . "\n";
            }
            continue;
        }
        $content .= $key . " = " . $value . "\n";
    }

    if (!$handle = fopen($path, 'w')) {
        return false;
    }
    if (!fwrite($handle, $content)) {
        return false;
    }
    fclose($handle);
    return true;
}