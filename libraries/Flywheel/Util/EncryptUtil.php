<?php
/**
 * Encrypt has string using function $method
 * 
 * @param string	$string
 * @param string	$method md5/sha1/sha256
 * @param boolean	$salt, using salt.
 * 
 * @return encrypt string
 */
function encrypt_has($string, $method = 'md5', $salt = true) {
	if (true === $salt) {		
		$salt = sha1(uniqid(rand(), true));		
	}
	$string .= $salt;
	
	switch ($method) {
		case 'sha1':
			return sha1($string);
		case 'sha256' :
			return bin2hex(mhash(MHASH_SHA256, $string));
		default:
			return md5($string);
	}		
}