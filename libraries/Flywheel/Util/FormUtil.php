<?php

/**
 * render form select elemnt 
 * 	by $name, and $option, selected $selectedValue option
 * 
 * @param string	$name.
 * @param array		$options. association options array with key is option
 * @param mixed		$selectedValue
 * @param string	$id, html id="" parameter
 * @param int		$size. Size of select, > 0 mean that mutil choise
 * 
 * @return Html string
 */
function render_form_select($name, $options, $selectedValue = 0, $id = null, $class = null, $size = 1) {
	$html = '';
	if (is_array($options) && sizeof($options) > 0) {
		$html = '<select name="' .$name .'"' 
					.((null != $id)? ' id="' .$id .'"' : '')
					.((null != $class)? ' class="' .$class .'"' : '')
					.(($size > 1)? ' multiple="multiple" size="' .$size .'"' : '') 
					.'>';
		$html .= render_select_option($options, $selectedValue);
		$html .= '</select>';
	}
	return $html;	
}

/**
 * render select option by $options selected at $selectedValue
 * 
 * @param array		$options
 * @param mixed		$selectedValue
 * 
 * @return Html string
 */
function render_select_option($options, $selectedValue = 0) {	
	$html = '';
	if (is_array($options) && sizeof($options) > 0) {
		foreach ($options as $option => $value) {
			if (is_array($value)) { // option group
				$html .= '<optgroup>' .$option;
				$html .= render_select_option($value, $selectedValue);
				$html .= '</optgroup>';				
			} else {
				$html .= '<option value="' .$value .'"';
				if(is_array($selectedValue))
				{
					if(in_array($value, $selectedValue))
						$html .= ' selected="selected"';
				}
				else 
				{	
					if($value == $selectedValue){
						$html .= ' selected="selected"'; 
					}
				}	
				$html .= '>' .$option .'</option>';				
			}
		}	
	}
	
	return $html;
}

/**
 * render form radio button
 * 
 * @param string	$name
 * @param array		$options
 * @param mixed		$checkValue
 * @param string	$id
 * @param boolean	$labelFist
 * 
 * @return Html string
 */
function render_radio_option($name, $options, $checkValue = 0, $id = null, $class = null, $labelFist = true) {
	$html = '';
	if (is_array($options) && sizeof($options) > 0) {
		foreach ($options as $label => $value) {
			$label = ' ' .$label .' ';
			$_option = '<input name="' .$name .'" type="radio" value="' .$value .'"' 
				.((null != $id)? ' id="' .$id .'"' : '')
				.((null != $class)? ' id="' .$class .'"' : '')
				.(($value == $checkValue)? ' checked="checked"' : '') .' />';
			$html .= '<label>' 
						.((false === $labelFist)? $_option .$label : $label.$_option) 
					.'</label>';			
		}
	}
	
	return $html;
}

/**
 * render select date
 * 
 * @param integer $date
 * @param integer $month
 * @param integer $year
 * 
 * @return Html string
 */
function render_select_date($date, $month, $year) {
	$dateArr = array();
	$dateArr['Ngày'] = 0;
	for ($i = 1; $i <= 31; ++$i) {
		$dateArr[$i] = $i;				
	}
	
	$monthArr = array();
	$monthArr['Tháng'] = 0;
	for ($i = 1; $i <= 12; ++$i) {
		$monthArr['Tháng ' .$i] = $i;	
	}
	
	$yearArr = array();
	$yearArr['Năm'] = 0;	
	$currentYear = (int) date('Y', time());	
	for ($i = 1905; $i<= $currentYear; ++$i) {
		$yearArr[$i] = $i;		
	}	
	return render_form_select('birth_day', $dateArr, $date, 'birth_day')
			.' / ' .render_form_select('birth_month', $monthArr, $month, 'birth_month')
			.' / ' .render_form_select('birth_year', $yearArr, $year, 'birth_year');
}

function render_select_province($name, $id=null, $selectedValue = '', $class = null, $size = 1){
    $options = array("An Giang", "Bà Rịa - Vũng Tàu", "Bạc Liêu", "Bắc Cạn", "Bắc Giang", "Bắc Ninh", "Bến Tre", "Bình Dương", "Bình Định", "Bình Phước", "Bình Thuận", "Cà Mau", "Cao Bằng", "Cần Thơ", "Đà Nẵng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Nội", "Hà Tĩnh", "Hải Dương", "Hải Phòng", "Hậu Giang", "Hòa Bình", "Tp Hồ Chí Minh", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lạng Sơn", "Lào Cai", "Lâm Đồng", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ","Phú Yên", "Quảng Bình", "Quảng nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long","Vĩnh Phúc", "Yên Bái");
    $html = '';
	if (is_array($options) && sizeof($options) > 0) {
		$html = '<select name="' .$name .'"'
					.((null != $id)? ' id="' .$id .'"' : '')
					.((null != $class)? ' class="' .$class .'"' : '')
					.(($size > 1)? ' multiple="multiple" size="' .$size .'"' : '')
					.'>';
		if (is_array($options) && sizeof($options) > 0) {
		foreach ($options as $option) {
                $html .= '<option value="' .$option .'"';

					if($option == $selectedValue){
						$html .= ' selected="selected"';
					}
				
				$html .= '>' .$option .'</option>';
			
		}
	}
		$html .= '</select>';
	}
	return $html;
}