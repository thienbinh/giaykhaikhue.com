<?php
/**
 * Message Util
 * 
 * @author			Volunteer_developer <mysteries.mine@gmail.com>
 * @version			$Id: MessageUtil.php 205 2010-10-26 19:18:20Z mylifeisskidrow@gmail.com $
 * @package			Flywheel
 * @subpackage		Util
 * @copyright		Flywheel Team (c) 2010
 *
 */
class MessageUtil {
	const TYPE_SUCCESS = 'SUCCESS';
	const TYPE_WARNING = 'WARNING';
	const TYPE_ERROR = 'ERROR';
	
	private $_type = array(self::TYPE_SUCCESS, self::TYPE_WARNING, self::TYPE_ERROR);
	
	/**
	 * Message Data
	 *
	 * @var array
	 */
	protected $_data = array();
	
	/**
	 * Message object name
	 *
	 * @var string
	 */
	private $_name;
	
	private $_fieldsMess = array();
	
	private function __construct($name = null) {
		$this->_name = $name;
		if (sizeof($this->_data) === 0) {
			$this->clear();	
		}
		
//		register_shutdown_function(array($this, 'shutdown'));
		$name = 'Message'. ((is_null($name))? '': '.' . $name);
		$this->_data = Flywheel_Factory::getSession()->getFlash($name);		
	}
	
	/**
	 * Get MessageUtil instance by name
	 * 
	 * @param string $name
	 * @return MessageUtil
	 */
	public static function getMessage($name) {
		static $instances;
		if (null == $instances) {
			$instances = array();
		}
		
		if (!isset($instances[$name])) {
			$instances[$name] = new self($name);
		}
		
		return $instances[$name];
	}
	
	/**
	 * Render
	 *
	 * @param fMessage $msg
	 */
	public static function render(MessageUtil $msg) {
		$html = '';
		$data = $msg->get();
		if (is_array($data) && sizeof($data) > 0) {
			foreach ($data as $msgGroup => $body) {
				if (is_array($body) && sizeof($body) > 0) {
					$html .= '<dl class="system-message msg-'. strtolower($msgGroup) .'">';
					foreach ($body as $title => $message) {
						if ('__NULL__' != $title) {
							$html .= '<dt>' .$title .'</dt>';							
						}						
						
						for ($i = 0, $size = sizeof($message); $i<$size; ++$i) {
							$html .= '<dd>' . $message[$i] .'</dd>';										
						}
					}
					$html .= '</dl>';				
				}
			}
		}
		$msg->clear();
		return $html;
	}
	
	/**
	 * Add Message
	 *
	 * @param mixed string|MessageUtil $mess
	 * @param string $type
	 * @param mixed string|array $data
	 * @return boolean
	 */
	public static function addMsg($mess, $type = null, $data = null) {
		if ($mess instanceof MessageUtil) {			
			$data = $mess->get();
			$name = $mess->getname();
			Flywheel_Factory::getSession()->setFlash('Message.'.$name, $data);								
		}
		else {
			if ($type == null || $data == null) {
				return false;
			}
			$data = (array) $data;
			$data = array(strtoupper($type) => $data);
			Flywheel_Factory::getSession()->setFlash('Message.'.$mess, $data);
		}
		
		return true;
	}
	
	/**
	 * Get name
	 * 	get object name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Get list type of message
	 * 
	 * @return array
	 */
	public function getTypeList() {
		return $this->_type;		
	}
	
	/**
	 * Check
	 * 	return true if message was set
	 *
	 * @param string $type
	 * @param string $title
	 * @return boolean
	 * @throws fException if type has not set
	 */
	public function check($type, $title = null) {
		$type = strtoupper($type);
		if (!in_array($type, $this->_type)) {
			throw new Flywheel_Exception('Dont support message type "' . $title .'"');			
		}		
		if ($title == null) {
			return (boolean) sizeof($this->_data[$type]);
		}
		else {			
			return (boolean) count($this->_data[$type][$title]);
		}
	}
	
	/**
	 * Sum
	 * 	return number of message.
	 *
	 * @param string $type	Type of message, null is return number of all message
	 * @return int
	 * @throws fException if type has not set
	 */
	public function sum($type = null) {
		$type = strtoupper($type);
		if ($type != null) {			
			return count($this->_data[$type]); 
		}
		
		if (!in_array($type, $this->_type)) {
			throw new Flywheel_Exception('Dont support message type "' . $type .'"');			
		}
		
		$i = 0;
		for($j = 0, $size = sizeof($this->_data); $j<$size; ++$j) {
			$i += count($this->_data[$i]);
		}
		
		return $i;
	}
	
	/**
	 * Get 
	 *
	 * @param string $type
	 * @return array of messages
	 * @throws fException if type has not set
	 */
	public function get($type = null) {
		$type = strtoupper($type);
		if ($type == null) {
			return $this->_data;		
		}
		else {
			if (!in_array($type, $this->_type)) {
				throw new Flywheel_Exception('Dont support message type "' . $type .'"');			
			}
			return $this->_data[$type];
		}
	}
	
	/**
	 * Add message
	 *
	 * @param string	$message		Message content
	 * @param string	$messageTitle	Message title
	 * @param string	$type
	 * @param boolean	$display		display now or next page
	 * @throws Flywheel_Exception if type has not set
	 */
	public function add($message, $messageTitle = null,  $type = 'SUCCESS', $display = false) {
		$type = strtoupper($type);
		if (!in_array($type, $this->_type)) {
			throw new Flywheel_Exception('Dont support message type "' . $type .'"');			
		}

		// handle PEAR errors gracefully
        if ($message instanceof PEAR_Error) {
            $message = $message->getMessage();            
        }
        
        if (null == $messageTitle) {
        	$messageTitle = '__NULL__';
        }
        //Create array of message title
        if (!isset($this->_data[$type][$messageTitle])) {
        	$this->_data[$type][$messageTitle] = array();
        }
        
        //Not repeat message
        if (!in_array($message, $this->_data[$type][$messageTitle])) {
        	$this->_data[$type][$messageTitle][] = $message;
        	if (true !== $display) {
        		$name = 'Message' . ((is_null($this->_name))? '' : '.' . $this->_name);		
				Flywheel_Factory::getSession()->setFlash($name, $this->_data);
        	}
        }
	}
	
	/**
	 * display error message
	 * @param string	$message
	 * @param string	$messageTitle
	 * @param string	$type
	 */
	public function display($message, $messageTitle = null, $type = 'SUCCESS') {
		$this->add($message, $messageTitle, $type, true);		
	}
	
	/**
	 * set message of form field use for message in form
	 * 
	 * @param string	$field		field name
	 * @param string	$message 	message
	 * @param string	$messageTitle Title
	 * @param string	$type		type of message
	 */
	public function setMessageOnField($field, $message, $messageTitle = null, $type = 'ERROR', $display = false) {
		if (!isset($this->_fieldsMess[$field])) {
			$this->_fieldsMess[$field] = array();
			$this->_fieldsMess[$field][self::TYPE_ERROR] = array();
			$this->_fieldsMess[$field][self::TYPE_WARNING] = array();
			$this->_fieldsMess[$field][self::TYPE_SUCCESS] = array();
		}
		
		if (!in_array($message, $this->_fieldsMess[$field][$type])) {
			$this->_fieldsMess[$field][$type][] = $message;
			$this->add($message, $messageTitle, $type, true);			
		}
	}
	
	/**
	 * display message on form field. like @link self::setMessageOnField().
	 * @param $field
	 * @param $message
	 * @param $messageTitle
	 */
	public function displayMessageOnField($field, $message, $messageTitle = null, $type = self::TYPE_ERROR) {
		$this->setMessageOnField($field, $message, $messageTitle, $type, true);		
	}
	
	/**
	 * check field had error message
	 * @param string	$field field name
	 * 
	 * @return boolean
	 */
	public function hadErrorOnField($field) {
		return (boolean) @sizeof($this->_fieldsMess[$field][self::TYPE_ERROR]);		
	}
	
	/**
	 * get message of form field if field has set message
	 * 
	 * @param string	$field
	 * @param string	$type
	 * 
	 * @return array
	 */
	public function getMessageOfField($field, $type = self::TYPE_ERROR, $join = false) {
		$mess = (!isset($this->_fieldsMess[$field][$type]))?  array() : $this->_fieldsMess[$field][$type];
		if (false === $join) {
			return $mess;
		}
		
		return implode($join, $mess);			
	}
	
	/**
	 * Remove
	 *
	 * @param string $title		Message title
	 * @param string $type		Message type
	 * @throws fException if type has not set
	 */
	public function remove($title, $type) {
		$type = strtoupper($type);
		if (!in_array($type, $this->_type)) {
			throw new Flywheel_Exception('Dont support message type "' . $type .'"');			
		}
		unset($this->_data[$type][$title]);	
	}
	
	/**
	 * Clear
	 *
	 */
	public function clear() {
		$type = $this->_type;
		for ($i = 0, $size = sizeof($type); $i < $size; ++$i) {
			$this->_data[$type[$i]] = array();  		
		}
//		$name = 'Message' . ((is_null($this->_name))? '' : '.' . $this->_name);
//		Flywheel_Factory::getSession()->removeFlash($name);
	}
}