<?php
function convert_price_int_to_string($price) {	
	$original 	= $price;
	$sign 		= ($price == ($price = abs($price)));
	$price		= (string) $price;	
	$len		= strlen($price);	
	for ($i = 0; $i < floor(($len-(1+$i))/3); ++$i) {
		$price = substr($price, 0, $len- (4*$i+3)) 
				.'.' .substr($price, $len-(4*$i+3));
	}
	
	return ((($sign)? '' : '-') . $price);
}