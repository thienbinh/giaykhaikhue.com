<?php
/**
 * Image Util class
 * 	class with static method image's helper or util
 * @author 		Volunteer_developer <mysteries.mine@gmail.com>
 * @version 	$Id: ImageUtil.php 1294 2011-04-08 04:50:46Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage	Utils
 *
 */
class ImageUtil {
	/**
	 * get image src
	 * @param string	$file relative path exp: items/dir1/file1.jpg 
	 * @param boolean	$version concat version string "?v=" ?
	 * 
	 * @return string
	 */
	public static function getImgSrc($file, $version = true) {
		$link = '';
//		if (Flywheel_Factory::getRequest()->isSecure()) {
//			$link = 'https://';
//		} else {
//			$link = 'http://';
//		}
		
		$link .= self::getStaticBaseUrl() .$file;
		if (true == $version) {
			$link .= '?v=' .hash('crc32b', filemtime(STATIC_DIR. $file));			
		}

		return $link;
	}

	/**
	 * get static base url
	 * 	static base url is a address to request static resource 
	 * 	like images, videos etc. static base url such as 
	 * 		- static.yourdomain.com,
	 * 		- 123.256.78.9
	 * 		or front controller path {@see Flywheel_Application_Router}
	 * 	you can be define static base url, or using front controller path  
	 * 
	 * @return string
	 */
	public static function getStaticBaseUrl() {
		$base = defined('STATIC_SERVER')
				? STATIC_SERVER : 
				Flywheel_Factory::getRouter()->getFrontControllerPath() .'medias' .DS;
		$base = rtrim($base, '\\') . '/';

		return $base;
	}
}