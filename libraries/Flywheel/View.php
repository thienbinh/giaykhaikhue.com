<?php
/**
 * Flywheel View
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: View.php 157 2010-10-07 15:53:41Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	View
 * @copyright	Flywheel Team (c) 2010
 */

class Flywheel_View {
	/**
	 * Assign template vars
	 * @var array
	 */
	private $_vars = array();
	
	private $_ext = '.phtml';
	
	/**
	 * Template Path
	 * @var string
	 */
	public $templatePath;
	
	public function __construct() {}
	
	/**
	 * Assign value to template variable
	 * @param string|array $var
	 * @param mixed $value	default null
	 * 
	 * @return boolean	true if assign success
	 * @throws Flywheel_View_Exception if $var's type not is string or array
	 */
	public function assign($var, $value = null) {
		if (is_string($var)) {
			if ($var == '') {
				return false;
			}
			$this->_vars[$var] = $value;
			return true;
		} else if (is_array($var)) {
			$this->_vars = array_merge($this->_vars, $var);			
			return true;
		}		
		throw new Flywheel_View_Exception('assign() expects a string or array, received ' . gettype($var));
	}
	
	/**
     * clear all the assigned template variables.
     */
	public function clearAllAssign() {
		$this->_vars = array();
	}
	
	/**
     * clear the given assigned template variable.
     *
     * @param string|array $vars the template variables to clear
     */
	public function clearAssign($vars) {
		if (is_array($vars)) {
			for ($i = 0, $size = sizeof($vars); $i < $size; ++$i) {				
				unset($this->_vars[$vars[$i]]);
			}
			return true;
		}
		unset($this->_vars[$vars]);
	}
	
	/**
	 * Render template with data
	 * 
	 * @param string $file
	 * @param array $vars. Default null
	 * 
	 * @return html string
	 * 
	 * @throws Flywheel_View_Exception if file template not found
	 */
	public function render($file, $vars = null) {
		if (is_array($vars)) {
			$this->assign($vars);			
		}		
		return $this->_render($file);
	}
	
	/**
	 * Display
	 * 
	 * @param string $file
	 * @param array $vars
	 * 
	 * @return void
	 * 
	 * @throws Flywheel_View_Exception if file template not found
	 */
	public function display($file, $vars = null) {
		if (is_array($vars)) {
			$this->assign($vars);
		}		
		
		echo $this->_render($file);
	}
	
	/**
	 * Render 
	 * @access	private
	 * @param string $file
	 */
	private function _render($file) {
		if (!file_exists($temFile = $this->templatePath .$file .$this->_ext)) {
			throw new Flywheel_View_Exception('Template file not found:' .$temFile);
		}

		extract($this->_vars, EXTR_SKIP);						
		ob_start();
		include $temFile;
		$ouput = ob_get_clean();
		return $ouput;		
	}
}