<?php
class Flywheel_Base {
	public static function load($lib, $dir = null) {
		if (null == $dir) {
			$dir = APP_LIB_DIR;
		}

		require_once $dir.$lib.'.php';
	}
}