<?php

class Flywheel_Authen {
	/**
	 * authenticated status
	 * @var $_authenticated boolean
	 */
	protected $_authenticated = false;
	
	/**
	 * authenticated application
	 * @var $_authenticatedApps array
	 */
	protected $_authenticatedApps = array();
	
	/**
	 * model user of authen user
	 * @var $_user Users
	 */
	protected $_user;

	/**
     * authenticat error
     * @var $_error
     */
    private $_error = '';
    
    /**
	 * @staticvar
	 * @var Flywheel_Authen
	 */
	protected static $_instance;
//&& $_SESSION['user']['id'] != 0
	private function __construct() {
		$session = Flywheel_Factory::getSession();
		if ($session->has('user') ) {
			$this->_authenticated	= true;
		} elseif (isset($_COOKIE['flywheel_key']) && isset($_COOKIE['flywheel_user'])){ // cookie
			$userInfo = explode(':', base64_decode($_COOKIE['flywheel_key']));
			if(crypt($userInfo[0], 22) === $userInfo[1] 
					&& $_COOKIE['flywheel_user'] === $userInfo[0]) {
   				$user = Users::findByUsername($_COOKIE['flywheel_user']);
   				if (false !== $user && (true === UsersPeer::comparePassword($userInfo[2], $user->password))) {   					
   					$_SESSION['user'] = $user->toArray(true);   					
   					$this->setAuthenCookie();
   					$this->_user = $user;   					
   					$this->_authenticated = true;
   				}
   			}
		}
	}
	
	/**
	 * get Flywheel_Authen instance
	 * 
	 * @return Flywheel_Authen
	 */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		
		return self::$_instance;
	}
	
	/**
	 * check user is authenticated
	 * 
	 * @return boolean
	 */
	public function isAuthenticated() {
		return $this->_authenticated;
	}
	
	/**
	 * chekc user authenticated by application
	 * 
	 * @param string $appName name of application
	 * @return boolean
	 */
	public function isAuthenticatedByApplication($appName) {
		if (!isset($this->_authenticatedApps[$appName])) {
			return false;			
		}
		
		return $this->_authenticatedApps[$appName];
	}
	
	public function setAuthenCookie() {
		// create cookies, login				
		$strCookie = base64_encode(join(':', array($_SESSION['user']['username']
												,crypt($_SESSION['user']['username'], 22)
												,$_SESSION['user']['password'])));
		$lifeTime = 30*24*3600;
		setcookie('flywheel_user', $_SESSION['user']['username'], time() + $lifeTime, '/');
		setcookie('flywheel_key', $strCookie, time() + $lifeTime, '/');		
	}
	
	/**
	 * get user
	 * 
	 * @return Users
	 */
	public function getUser() {
		if (false === $this->_authenticated) {
			return false;
		} 
		
		if (null == $this->_user) {
			//print_r(Flywheel_Factory::getSession()->get('user'));
			//exit;
			$this->_user = new Users(Flywheel_Factory::getSession()->get('user'));
			
		}
		return $this->_user;			
	}
	
	/**
	 * authenticate user
	 * 	 
	 * @param string	$username
	 * @param string	$password
	 */
	public function authenticate($username, $password) {
          $user = Users::findByUsername($username);
            
          if (false === $user) {
               return false;
          }
         
          if (true === UsersPeer::comparePassword($password, $user->password)) {
               
               if($user->lock_status > 0){
                  $this->_authenticated = false;
               }else{
                  Flywheel_Factory::getSession()->set('user', $user->toArray(true));
                  $this->_user 	     = $user;
                  $this->_authenticated = true;
               }
               return true;
          }
          return false;
	}
	
	public function _authenticate($username){
		$user = Users::findByUsername($username);
		Flywheel_Factory::getSession()->set('user', $user->toArray(true));
       	$this->_user 	     = $user;
        $this->_authenticated = true;
        return true;
	}
	/**
	 * reset session user
	 * @param Users $user
	 */
	public function resetSessionUser($user) {
		if (false === $this->isAuthenticated()) {
			return false;
		}
		
		$_SESSION['user'] = $user->toArray(true);
	}
	
	/**
	 * reset session user by data from database
	 */
	public function resetSessionUserFromDb() {
		if (false === $this->isAuthenticated()) {
			return false;
		}
		$user = @Users::retrieveByPk($_SESSION['user']['id']);
		$this->resetSessionUser($user);
	}
	
	/**
	 * logout
	 */

	public function logout() {
		setcookie('flywheel_user', '', time() - 24*3600*30, '/');
		setcookie('flywheel_key', '', time() - 24*3600*30, '/');
		unset($_SESSION['user']);
		$this->_authenticated = false;
		$this->_authenticatedApps = array();		
	}

    /**
     * get Authen error
     */
    public function getError(){
        return $this->_error;
    }
}
