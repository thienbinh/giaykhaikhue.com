<?php
/**
 * interface decorator
 * 	the interface of all Ff's decorator
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id$
 * @since		Ff v2.0
 * @package		Flywheel
 * @subpackage	Decorator
 *
 */
interface Flywheel_Decorator_Interface {
	public function setParams();
}