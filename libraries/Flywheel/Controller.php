<?php
/**
 * Flywheel Controller
 * 
 * @abstract
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Controller.php 1205 2011-03-09 10:08:54Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage	Controller
 *
 */
abstract class Flywheel_Controller {
	protected $_name;
	/**
	 * Render Mode
	 *
	 * @var string
	 */
	protected $_renderMode = 'COMPONENT';	
	
	protected $_view = 'default';
	
	public function __construct($name) {
		$this->_name = $name;		
	}
	
	/**
	 * Before execute action
	 */
	public function beforeExecute() {
	}
	
	/**
	 * Execute
	 * 
	 * @return component process result
	 *
	 */
	final public function execute() {
		$action = 'execute' 
					. str_replace(' ', '', ucwords(str_replace(array('-', '_'), ' ',
											Flywheel_Factory::getRouter()->getAction())));		
		
		if (!method_exists($this, $action)) {
			throw new Flywheel_Controller_Error404Exception("Action \"".Flywheel_Factory::getRouter()->getController().'/'.$action ."\" doesn't exist");						
		}
	
		if (Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::mark('Before execute action ' . $action, get_class($this));				
		}
		
		$this->beforeExecute();		
		$buffer = $this->$action();
		$this->afterExecute();
		
		if (Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::mark('After execute action ' . $action, get_class($this));				
		}
		
		if ('NO_RENDER' == $this->_renderMode) {
			return null;
		}
		
		if (null !== $buffer) {
			return $buffer;
		}
				
		if ('COMPONENT' == $this->_renderMode) { //Default render Component
			return $this->renderComponent();
		}
	}
	
	/**
	 * after execute action
	 */
	public function afterExecute() {}
	
	/**
	 * Render Partial
	 * 	only render a web page's partial. 
	 *
	 * @param string
	 */
	protected function renderPartial($vars = null) {
		$this->_renderMode = 'PARTIAL';
		
		$view = Flywheel_Factory::getView();
		$viewFile = 'controllers'. DS .$this->_name. DS .$this->_view;
		if (Flywheel_Config::get('debug')) {
			$buffer = $view->render($viewFile);
			Flywheel_Debug_Profiler::mark('After render page partial', get_class($this));
			return $buffer;				
		}		
		return $view->render($viewFile, $vars);
	}
	
	/**
	 * Render Component
	 * 	render web page
	 *
	 * @return string
	 */
	protected function renderComponent() {		
		$buffer = $this->renderPartial();		
		$this->_renderMode = 'COMPONENT';
		$this->loadModules();	
		
		return $buffer;
	}
	
	/**
	 * Render Text
	 *
	 * @param string $text
	 * @return string
	 */
	protected function renderText($text) {		
		$this->_renderMode = 'TEXT';
        
        return $text;
	}
	
	protected function setNoRender($render) {
		if (true === $render) {
			$this->_renderMode = 'NO_RENDER';
		}			
	}

	/**
	 * Set Layout
	 * 	set layout template
	 *
	 * @param string $layout
	 */
	public function setLayout($layout) {
		$application = Flywheel_Application::getApp();
		if ($application instanceof Flywheel_Application) {
			$application->setLayout($layout);
		}
	}
	
	/**
	 * Get current layout template
	 * 
	 * @return mixed: string|null
	 *
	 */
	public function getLayout() {
		$application = Flywheel_Application::getApp();
		if ($application instanceof Flywheel_Application) {
			return $application->getLayout();				
		}
		
		return null;
	}
	
	/**
	 * get view path of curent controller
	 * @param boolean $fullPath return path included template dir or not. default false
	 * 
	 * @return string
	 */
	public function getViewPath($fullPath = false) {
		return (($fullPath)? TEMPLATE_DIR : '') .'controllers' .DS .$this->_name .DS;		
	}
	
	/**
	 * Set view
	 *
	 * @param string $view
	 */
	protected function setView($view) {
		$this->_view = $view;	
	}
	
	abstract public function executeDefault();
	
	/**
	 * Get Render Mode	 
	 *
	 * @return string
	 */
	final public function getRenderMode() {
		return $this->_renderMode;
	}
	
	public function importHelper($helper, $ext = '.php') {
		require_once CONTROLLERS_DIR .$this->_path .'helper' .DS .$helper .$ext;		
	}
	
	protected function loadModules() {		
	}
    protected function getRequest(){
        return Flywheel_Factory::getRequest();
    }
}