<?php

/**
 * Flywheel Application Web
 * 
 * @author 		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Web.php 1205 2011-03-09 10:08:54Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage	Application
 * @copyright	Flywheel Team (c) 2010
 *
 */
class Flywheel_Application_Web extends Flywheel_Application {

    /**
     * Page layout
     * 
     * @var $_layout string
     */
    protected $_layout = 'default';

    public function __construct($name, $environment) {
        $this->_name = $name;
        $this->_env = $environment;
    }

    /**
     * Get Layout
     * 
     * @return application layout
     */
    public function getLayout() {
        return $this->_layout;
    }

    /**
     * Set layout
     * @param string $layout
     */
    public function setLayout($layout) {
        $this->_layout = $layout;
    }

    /**
     * Initialize
     * 
     * @return void
     */
    protected function _init() {
        define('APP_DIR', APPS_DIR . $this->_name . DS);
        define('CONTROLLERS_DIR', APP_DIR . 'controllers' . DS);
        define('MODULES_DIR', APP_DIR . 'modules' . DS);
        define('HELPER_DIR', APP_DIR . 'helper' . DS);
        define('CONFIG_DIR', APP_DIR . 'configs' . DS);
        define('TEMPLATE_DIR', APP_DIR . 'templates' . DS);
        define('APP_LIB_DIR', APP_DIR . 'libraries' . DS);

        ini_set('display_errors', Flywheel_Config::get('debug') ? 'on' : 'off');
        //Error reporting

        if ($this->_env == parent::ENV_DEV) {
            error_reporting(E_ALL ^ E_NOTICE);
        } else if ($this->_env == parent::ENV_TEST) {
            error_reporting(E_ALL ^ E_NOTICE);
        }

        date_default_timezone_set(@date_default_timezone_get());

        //Global config
        if (file_exists($file = CONFIG_DIR . 'setting.ini')) {
            Flywheel_Config::load($file);
        } else {
            Flywheel_Config::load(GLOBAL_CONFIGS_DIR . 'setting.ini', 'default', false);
        }

        Flywheel_Factory::getView()->templatePath = APP_DIR . 'templates' . DS;
    }

    /**
     * Run application
     * 
     * @return void
     * @throws Flywheel_Exception
     */
    public function run() {
        if (Flywheel_Config::get('debug')) {
            Flywheel_Debug_Profiler::mark('Begin run web application', get_class($this));
        }

        $buffer = $this->_loadController();

        $renderMode = $this->_controller->getRenderMode();

        switch ($renderMode) {
            case 'NO_RENDER':
                Flywheel_Factory::getResponse()->send();
                break;
            case 'TEXT':
            case 'PARTIAL':
                $response = Flywheel_Factory::getResponse();
                $response->setBody($buffer);
                $response->send();
                break;
            case 'COMPONENT':
                $this->_renderPage($buffer);
                break;
            default:
                break;
        }
    }

    /**
     * Load Controller
     */
    private function _loadController($isRemap = false) {
        $router = Flywheel_Factory::getRouter();

        $controllerName = str_replace(' ', '', ucwords(str_replace(array('-', '_'), ' ', $router->getController())));

        $className = $controllerName . 'Controller';
        $file = CONTROLLERS_DIR . $controllerName . DS . $className . '.php';

        /**
         * remap controller option
         * @todo	for SE0, we need remove component and action control in url.
         * 			exp: http://id.ming.vn/{param} 
         * 			When controller not found, application using remap controller in routing config
         * @since	05/10/2010 
         */
        if (file_exists($file)) {
            require_once $file;
        } else {
            if (file_exists($file = GLOBAL_CONTROLLERS_DIR . $controllerName . DS . $className)) {
                require_once $file;
            } else if (false == $isRemap && $router->remap()) {
                return $this->_loadController(true);
            } else {
                throw new Flywheel_Controller_Exception('Controller "' . $controllerName . '" [' . $file . '] does not exist!');
            }
        }

        $this->_controller = new $className($controllerName);

        return $this->_controller->execute();
    }

    /**
     * Render Page
     * 
     * @param html string $buffer
     */
    protected function _renderPage($buffer) {
        $document = Flywheel_Factory::getDocument();
        $response = Flywheel_Factory::getResponse();
        $document->setBuffer($buffer, 'component');
        $view = Flywheel_Factory::getView();
        if ($this->_layout == null) {
            $config = Flywheel_Config::get('template');
            $this->_layout = (isset($config['default_layout'])) ?
                    $config['default_layout'] : 'default'; //load default layout					
        }

        ob_start();
        ob_implicit_flush(0);
        try {
            $view->display($this->_layout);
            $view->clearAllAssign();
            if (Flywheel_Config::get('debug')) {
                Flywheel_Debug_Profiler::mark('Template display', get_class($this));
            }
        } catch (Exception $e) {
            ob_end_clean();
            throw new Flywheel_Exception($e);
        }
        $content = ob_get_clean();
        //<-- begin debug code -->
        if (Flywheel_Config::get('debug')) {
            if ($this->getEnvironment() == Flywheel_Application::ENV_DEV) {
                $count = 0;
                $debug = Flywheel_Debug_Profiler::debug();
                $content = str_ireplace('</body>', $debug . '</body>', $content, $count);
                if (!$count) {
                    $content .= $debug;
                }
            } else {
                Flywheel_Factory::getSession()->set('debug', Flywheel_Debug_Profiler::debug());
            }
        }
        //<-- /end debug code -->

        $response->setBody($content);
        $response->send();
    }

}