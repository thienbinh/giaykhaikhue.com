<?php
/**
 * Flywheel Application Router
 * 
 * @author 		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Router.php 517 2010-11-21 19:10:03Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Application
 * @copyright	Flywheel Team (c) 2010
 */
class Flywheel_Application_Router {
	protected $_controller;
	protected $_component;
	protected $_action;
	protected $_url;
	protected $_baseUrl;
	protected $_config;
	protected $_scriptName;
	protected $_uri;
	
	/**
	 * Domain
	 * @var string
	 */
	protected $_domain;
	
	/**
	 * front controller path
	 * 
	 * @var $_path string
	 */
	protected $_path;
	
	//URL parameters
	protected $_params = array();
	
	protected static $instance;	
	
	public function __construct() {
		//Load config
         $request = Flywheel_Factory::getRequest();
        $com = $request->get('com');
        $act = $request->get('act');
		if (file_exists($file = CONFIG_DIR .DS .'routing.ini')) {
			$this->_config = Flywheel_Config::load($file, 'routing');
		}
		else {
			$this->_config = Flywheel_Config::load(GLOBAL_CONFIGS_DIR .DS .'routing.ini', 'routing', false);
		}
        
		$this->_url = $url = $this->getPathInfo();		
		
		if ($this->_config['friendly_url'] == 1) {
             if($com and $act){
                 $defController = (isset($this->_config['default']['component']))
                     ? $this->_config['default']['component'] : null;

                 $defAction = (isset($this->_config[$defController]['action']))
                     ? $this->_config[$defController]['action'] : null;

                 $this->_controller = $this->_component = $request->get('com', Flywheel_Filter::TYPE_STRING, $defController);
                 $this->_action = $request->get('act', Flywheel_Filter::TYPE_STRING, $defAction);

                 if (isset($this->_configs[$this->_component])) {
                     $this->_controller = $this->_configs[$this->_component]['controller'];
                     if (isset($this->_configs[$this->_component]['action'])) {
                         $this->_action = $this->_configs[$this->_component]['action'];
                     }
                 }
             }else{
			    $this->_parseUrl($url);
             }
		}else {
                   

			$defController = (isset($this->_config['default']['component']))
					? $this->_config['default']['component'] : null;
								
			$defAction = (isset($this->_config[$defController]['action']))
					? $this->_config[$defController]['action'] : null;
							
			$this->_controller = $this->_component = $request->get('com', Flywheel_Filter::TYPE_STRING, $defController);
			$this->_action = $request->get('act', Flywheel_Filter::TYPE_STRING, $defAction);
			
			if (isset($this->_configs[$this->_component])) {
				$this->_controller = $this->_configs[$this->_component]['controller'];
				if (isset($this->_configs[$this->_component]['action'])) {
					$this->_action = $this->_configs[$this->_component]['action'];
				}
			}			
		}
		
		$this->_domain = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')? 'https://':'http://') .$_SERVER['HTTP_HOST'];		
	
		$this->_baseUrl = $this->_domain .str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
		$path = explode('/', $this->_baseUrl);
		$script = array_pop($path);		
		if (false === strpos($script, '.php')) {
			$path[] = $script;
		}
		$this->_path = rtrim(implode('/', $path), '/') .'/';
		$this->_uri = $this->_domain .$_SERVER['REQUEST_URI'];		
					
		if ($this->_action == null) {
			$this->_action = 'default';
		}
		
		if (Flywheel_Config::get('debug')) {			
			Flywheel_Debug_Profiler::mark('Routing application', get_class($this));			
		}
	}
	
	/**
	 * Get Path Info
	 * 
	 * @return url
	 */
	public function getPathInfo() {
		if (isset($_SERVER['PATH_INFO']) && ($_SERVER['PATH_INFO'] != '')) {
			$pathInfo = $_SERVER['PATH_INFO'];
		}
		else {
			$pathInfo = preg_replace('/^'.preg_quote($_SERVER['SCRIPT_NAME'], '/').'/', '', $_SERVER['REQUEST_URI']);
			$pathInfo = preg_replace('/^'.preg_quote(preg_replace('#/[^/]+$#', '', $_SERVER['SCRIPT_NAME']), '/').'/', '', $pathInfo);                
			$pathInfo = preg_replace('/\??'.preg_quote($_SERVER['QUERY_STRING'], '/').'$/', '', $pathInfo);
			if ($pathInfo == '') $pathInfo = '/';                   
		}
		
		if (isset($this->_config['url_suffix']) && $this->_config['url_suffix'] != '') {                
			$pathInfo = str_replace($this->_config['url_suffix'], '', $pathInfo);
        }
		return $pathInfo;
	}
	
	/**
	 * Get Controller
	 *
	 * @return string
	 */
	public function getComponentName() {
		return $this->_component;
	}
	
	/**
	 * Get Controller
	 * 	ten controller
	 *
	 * @return string
	 */
	public function getController() {
		return $this->_controller;			
	}
	
	/**
	 * Get Method
	 *
	 * @return string
	 */
	public function getAction() {
		return $this->_action;
	}
	
	/**
	 * Get Base Url
	 *
	 * @return string
	 */
	public function getBaseUrl() {
		return $this->_baseUrl;
	}
	
	/**
	 * remap controller action
	 * 
	 * @return boolean
	 */
	public function remap() {
		if (!isset($this->_config['remap']) 
				|| !isset($this->_config['remap']['controller'])) {
			return false;			
		}
		
		$_params = $this->_params;
		$this->_params = array();
		
		$this->_params[] = $this->_controller;		
		if ($this->_action != 'default') {
			$this->_params[] = $this->_action;			
		}
		
		$this->_component = $this->_controller = $this->_config['remap']['controller'];
		$this->_action = (isset($this->_config['remap']['action']))
						? $this->_config['remap']['action']: 'default';
		if (isset($this->_config['remap']['param'])) {
			for ($i = 0; $i < sizeof($this->_config['remap']['param']); ++$i) {
				$this->_params[] = $this->_config['remap']['param'][$i];
			}			
		}
		
		for ($i = 0; $i < sizeof($_params); ++$i) {
			$this->_params[] = $_params[$i];
		}
		
		return true;
	}
	
	/**
	 * Get Params
	 * @param int $index. default null mean return all params
	 * 
	 * @return mixed|array
	 */
	public function getParams($index = null) {
		if (null === $index) {
			return $this->_params;
		}
		
		return isset($this->_params[$index])? $this->_params[$index] : null;
	}
	
	/**
	 * Get Script Name
	 * 
	 * @return string
	 */
	public function getDomain() {
		return $this->_domain;
	}
	
	/**
	 * Get Controller Path
	 */
	public function getFrontControllerPath() {
		return $this->_path;	
	}
	
	/**
	 * Get absolute uri included domain
	 * 
	 * @return string
	 */
	public function getAbsoluteUri() {
		return $this->_uri;
	}
	
	/**
	 * get url surfix in config
	 */
	public function getUrlSuffix() {
		if ($this->_config['friendly_url'] == 1) {
			return ((isset($this->_config['url_suffix']))? 
				$this->_config['url_suffix'] : null);			
		}

		return null;
	}
	
	/**
	 * Parse URL
	 *
	 * @param string $url
	 */
	private function _parseUrl($url) {
		$url = trim($url, '/');
                
		$segment = explode('/', $url);		
		$actionIndexStart = 2;
		
		// Get component name		
        if(isset($this->_config[$url])){
           $this->_component = $url;
        }else if (isset($segment[0]) and $segment[0]!= '') {
             $this->_component = $segment[0];
                     			
		} else {
			if (!isset($this->_config['default'])) {
				throw new Flywheel_Exception('Default component not found');
			}
			
			$defConfig = $this->_config['default'];			
			$this->_component = $defConfig['component'];
		}
		
		//set controller name, action and param
		if (isset($this->_config[$this->_component])) {
                   
			$comConfig = $this->_config[$this->_component];
                        
			$this->_controller = (isset($comConfig['controller']))
                        ? $comConfig['controller'] 
                        : $this->_component;
                        
			if (isset($comConfig['controller']['action'])) {
				$this->_action = $comConfig['action'];
				$actionIndexStart = 1;								
			} else if (isset($segment[1])) {
				$this->_action = $segment[1];							 			
			}
			
			if (isset($comConfig['param'])) {
				$this->_params = $comConfig['param'];				
			}			
			if (isset($segment[$actionIndexStart])) {
				$_s = array_slice($segment, $actionIndexStart);
				for ($index = 0, $sizeOfParams = sizeof($_s); $index < $sizeOfParams; ++$index) {				
					array_push($this->_params, $_s[$index]);
				}
				unset($_s);
			}			
			
		} else {
                   
			$this->_controller = $this->_component;
			$this->_action = isset($segment[1])? $segment[1] : null;
			if (isset($segment[$actionIndexStart])) {
				$_s = array_slice($segment, $actionIndexStart);
				for ($index = 0, $sizeOfParams = sizeof($_s); $index < $sizeOfParams; ++$index) {				
					array_push($this->_params, $_s[$index]);
				}
				unset($_s);
			}		
		}
               
	}
}