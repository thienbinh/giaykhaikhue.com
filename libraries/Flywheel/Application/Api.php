<?php
class Flywheel_Application_Api extends Flywheel_Application {
	
	private $_version;

	public function __construct($name, $environment) {
		$this->_name = $name;
		$this->_env = $environment;				
	}
	
	/**
	 * Get Version
	 * 
	 * get current version of execute API
	 */
	public function getVersion() {
		return $this->_version;		
	}
	
	/**
	 * Initialize
	 * 
	 * @return void
	 */	
	protected function _init() {
		define('APP_DIR', 			APPS_DIR .$this->_name .DS);
		define('VERSIONS_DIR',		APP_DIR .'versions' .DS);
		define('CONFIG_DIR',		APP_DIR .'configs' .DS);		
		
		ini_set('display_errors', Flywheel_Config::get('debug')? 'on' : 'off');
				
		//Error reporting
		if ($this->_env == parent::ENV_DEV) {
			error_reporting(E_ALL);
		}
		else if ($this->_env == parent::ENV_TEST) {
			error_reporting(E_ALL ^ E_NOTICE);
		}
		
		date_default_timezone_set(@date_default_timezone_get());
		
		//Global config
		if (file_exists($file = CONFIG_DIR .'setting.ini')) {
			Flywheel_Config::load($file);			
		}
		else {
			Flywheel_Config::load(GLOBAL_CONFIGS_DIR .'setting.ini', 'default', false);
		}
		
		//Get version to run
		$version = Flywheel_Factory::getRequest()->get('v', Flywheel_Filter::TYPE_CMD);
		if (null === $version ) {// if client not define version, using current newest version
			 $version = Flywheel_Config::get('current_version');
		}		
		
		$this->_checkAvailabeVersion($version); // need checking version is exist
		$this->_version = $version;
		
		if (!Flywheel_Config::has('router')) {
			Flywheel_Config::set('router', 'api');			
		}
		
		//Start session
		Flywheel_Factory::getSession()->start();		
//		Flywheel_Factory::getView()->templatePath = APP_DIR .'templates' .DS;		
	}
	
	/**
	 * Check Availabe Version
	 * 
	 * @param string $version. Version Id
	 */
	private function _checkAvailabeVersion($version) {		
		if (!file_exists($versionDir = VERSIONS_DIR .$version)) {
			throw new Flywheel_Exception('Version not found!');			
		}
		define('VERSION_DIR',		$versionDir .DS);	
		define('CONTROLLERS_DIR',	VERSION_DIR .'controllers' .DS);				
	}
	
	/**
	 * Run
	 * 
	 * @throws Flywheel_Exception
	 */
	public function run() {
		//<-- begin debug code -->
		if (Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::mark('Begin run API application', get_class($this));
		}
		//<-- /end debug code -->
		
		$content = $this->_loadController();
		//<-- begin debug code -->
		if (Flywheel_Config::get('debug')) {
		$count = 0;
			$debug = Flywheel_Debug_Profiler::debug();
			$content = str_ireplace('</body>', $debug .'</body>', $content, $count);
			if (!$count) {
				$content .= $debug; 			
			}		
		}
		//<-- /end debug code -->
		
		$response = Flywheel_Factory::getResponse();
		$response->setBody($content);
		$response->send();
		
		//<-- begin debug code -->
		if (Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::mark('Begin run API application', get_class($this));
		}
		//<-- /end debug code -->
	}
	
	/**
	 * Load controller
	 */
	protected function _loadController() {
		$controllerName = str_replace(' ', '', 
				ucwords(str_replace(array('-', '_'), ' ',
								Flywheel_Factory::getRouter()->getController())));
		$className = $controllerName.'Controller';
		$file = CONTROLLERS_DIR .$controllerName .DS .$className .'.php';		
		$className = 'API'.$className;
		if (!file_exists($file)) {
			throw new Flywheel_Controller_Exception('Controller "' .$controllerName .'" does not exist!');			 			
		}
		require_once $file;
		$this->_controller = new $className($controllerName);
		return $this->_controller->execute();
	}
}