<?php
/**
 * Flywheel Application Router API
 * 
 * @author 		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: RouterApi.php 139 2010-09-20 11:22:16Z mylifeisskidrow@gmail.com $
 *
 */
class Flywheel_Application_RouterApi {
	/**
	 * controll url
	 * 
	 * @var string
	 */
	protected $_url;
	
	/**
	 * entide url
	 * @var string
	 */
	protected $_baseUrl;
	
	/**
	 * public folder path included domain 
	 * @var string
	 */
	protected $_path;
	
	/**
	 * controller name
	 * @var string
	 */
	protected $_controller;
	
	/**
	 * action name
	 * @var $_action string
	 */
	protected $_action;
	
	/**
	 * RESTful method request
	 * @var $_methodRequest string
	 */
	protected $_methodRequest;
	
	/**
	 * ex request param
	 * @var array
	 */
	protected $_params = array();
	
	protected $_uri;
			
	public function __construct() {
		/**
		 * !Routing GET/component/action/param1/param2
		 */
		$this->_url = $this->getPathInfo();		
		$this->_parseUrl($this->_url);

		$this->_domain = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')? 'https://':'http://') 
						.$_SERVER['HTTP_HOST'];
		$this->_baseUrl = $this->_domain .str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
		
		$path = explode('/', $this->_baseUrl);
		$script = array_pop($path);
		if (false === strpos($script, '.php')) {
			$path[] = $script;
		}	
		$this->_path = rtrim(implode('/', $path), '/') .'/';
		$this->_uri = $this->_domain .$_SERVER['REQUEST_URI'];
		
		//<-- begin debug code -->
		if (Flywheel_Config::get('debug')) {			
			Flywheel_Debug_Profiler::mark('Routing application', get_class($this));			
		}
		//<-- /end debug code -->
	}
	
	/**
	 * @return the $_url
	 */
	public function getUrl() {
		return $this->_url;
	}

	/**
	 * @return the $_baseUrl
	 */
	public function getBaseUrl() {
		return $this->_baseUrl;
	}
	
	/**
	 * get public folder path included domain
	 * 
	 * @return string
	 */
	public function getPublicPath() {
		return $this->_path;
	}

	/**
	 * @return the $_controller
	 */
	public function getController() {
		return $this->_controller;
	}

	/**
	 * @return the $_action
	 */
	public function getAction() {
		return $this->_action;
	}
	
	/**
	 * Get absolute uri included domain
	 * 
	 * @return string
	 */
	public function getAbsoluteUri() {
		return $this->_uri;
	}

	/**
	 * @return the $_methodRequest
	 */
	public function getMethodRequest() {
		return $this->_methodRequest;
	}
	
	public function getParams($index = null) {
		if (null == $index) {
			return $this->_params;
		}
		
		if (!isset($this->_params[$index])) {
			return null;
		}
		
		return $this->_params[$index];
	}

	/**
	 * Get Path Info
	 * 
	 * @return url
	 */
	public function getPathInfo() {
		if (isset($_SERVER['PATH_INFO']) && ($_SERVER['PATH_INFO'] != '')) {
			$pathInfo = $_SERVER['PATH_INFO'];
		}
		else {
			$pathInfo = preg_replace('/^'.preg_quote($_SERVER['SCRIPT_NAME'], '/').'/', '', $_SERVER['REQUEST_URI']);
			$pathInfo = preg_replace('/^'.preg_quote(preg_replace('#/[^/]+$#', '', $_SERVER['SCRIPT_NAME']), '/').'/', '', $pathInfo);                
			$pathInfo = preg_replace('/\??'.preg_quote($_SERVER['QUERY_STRING'], '/').'$/', '', $pathInfo);
			if ($pathInfo == '') $pathInfo = '/';                   
		}
		
		if (isset($this->_config['url_suffix']) && $this->_config['url_suffix'] != '') {                
			$pathInfo = str_replace($this->_config['url_suffix'], '', $pathInfo);
        }
		return $pathInfo;
	}
	
	/**
	 * Parse Url
	 * @param string $url
	 * 
	 * @throws Flywheel_Exception if request not define method/component/
	 * 				exp: GET/component_name/
	 */
	private function _parseUrl($url) {
		$url = trim($url, '/');
		$segment = explode('/', $url);
		if (sizeof($segment) < 2) {
			throw new Flywheel_Exception('Invalid request !');
		}
		
		$methodRequest = strtoupper($segment[0]);
		switch ($methodRequest) {
			case 'POST' :
				$this->_methodRequest = 'POST';
				break;
			case 'PUT' :
				$this->_methodRequest = 'PUT';
				break;
			case 'DELETE' :
				$this->_methodRequest = 'DELETE';
				break;
			default:
				$this->_methodRequest = 'GET';
		}
		
		$this->_controller = $segment[1];
		$this->_action = (isset($segment[2]))? $segment[2] : 'default';
		$this->_params = array_slice($segment, (isset($segment[2])? 3 : 2));
	}
}