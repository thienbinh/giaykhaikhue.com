<?php
/**
 * class Flyhweel_Application_Console
 * 	console application
 * 
 * @author 		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Console.php 870 2010-12-26 07:05:22Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Application
 */
class Flywheel_Application_Console extends Flywheel_Application {
	protected static $_params = array();
	
	/**
	 * execute task name
	 * 
	 * @var string
	 */
	protected static $_task;
	
	/**
	 * action
	 * 
	 * @var string
	 */
	protected static $_act;
	
	protected $_runned = false;
	
	public function __construct($name, $environment) {
		$this->_name	= $name;
		$this->_env		= $environment;		
	}
	
	/**
	 * add more parrams
	 */
	public function addParams($args) {
		foreach ($args as $arg) {
			self::$_params[] = $arg;
		}		
	}
	
	/**
	 * set execute task name
	 * @param string $task
	 */
	public function setTask($task) {
		if (self::$_task != $task) {
			self::$_task = $task;
		}
	}
	
	/**
	 * initiliaze
	 */
	protected function _init() {
	define('APP_DIR', 			APPS_DIR .$this->_name .DS);
		define('TASK_DIR',			APP_DIR .'tasks' .DS);
		define('HELPER_DIR',		APP_DIR .'helper' .DS);
		define('CONFIG_DIR',		APP_DIR .'configs' .DS);

		ini_set('display_errors', Flywheel_Config::get('debug')? 'on' : 'off');		
		//Error reporting
		if ($this->_env == parent::ENV_DEV) {
			error_reporting(E_ALL ^ E_NOTICE);
		}
		else if ($this->_env == parent::ENV_TEST) {
			error_reporting(E_ALL ^ E_NOTICE);
		}
		
		date_default_timezone_set(@date_default_timezone_get());
		
		$this->_isCli = (substr(php_sapi_name(), 0, 3) == 'cli');
		
		if (null !== ($task = Flywheel_Factory::getRequest()->get('task'))) {
			self::$_task = $task;
		}
		
		if (null !== ($act = Flywheel_Factory::getRequest()->get('act'))) {
			self::$_act = $act;
		}
		
		if (true === $this->_isCli) {
			$argv = $_SERVER['argv'];
			$seek = 1;
			if (null == self::$_task) {
				self::$_task = $argv[$seek];
				++$seek;			
			}
			
			if (null == self::$_act) {
				self::$_act = $argv[$seek];
				++$seek;
			}
			
			self::$_params = array_splice($argv, $seek);
		}
	}
	
	public function run() {	    
		if (null == self::$_task) {
			$_t = self::$_params[0];
			$_t = explode(':', $_t);
			self::$_task = $_t[0];
			$act = (isset($_t[1]))? $_t[1] : null;									
		}
		
		if (null == self::$_act) {
			if (null != $act) {
				self::$_act = $act;
			} else {
				self::$_act = 'default';
			}
		}
		$class = Flywheel_Inflector::hungaryNotationToCamel(self::$_task);
		if (file_exists($file = TASK_DIR .self::$_task .DS .$class .'Controller.php')) {
			require_once $file;
			$class = $class.'Controller';				
			$this->_controller = new $class(self::$_task);
			$this->_controller->execute(self::$_act);
			$this->_runned = true;
			if (true == Flywheel_Config::get('debug')) {
				echo Flywheel_Debug_Profiler::debug();
			}
		} else {
			Flywheel_Application::end('ERROR: task ' .self::$_task .' not existed' .PHP_EOL);			
		}
	}
}