<?php
/**
 * Flywheel Application Request
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Request.php 1118 2011-03-03 22:12:41Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage	Application
 * @copyright 	Flywheel Team (c) 2010
 *
 */
class Flywheel_Application_Request {
	const POST = 'POST';
	const GET = 'GET';
	const PUT = 'PUT';
	const DELETE = 'DELETE';
	const HEAD = 'HEAD';
	
	protected $_method;
	
	private $_getVars = array();
	
	public function __construct() {
		$this->_method = isset($_SERVER['REQUEST_METHOD'])? 
							$_SERVER['REQUEST_METHOD'] : 'GET';
	}
	
	/**
	 * Get request method
	 *
	 * @return string GET|POST|PUT|DELETE|HEAD
	 */
	public function getMethod() {
		return $this->_method;
	}
	
/**
	 * Retrieves the uniform resource identifier for the current web request.
	 *
	 * @return string Unified resource identifier
	 */
	public function getUri() {
		$uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
		
		return $this->isAbsUri() ? $uri : $this->getUriPrefix().$uri;		
	}
	
	/**
	 * See if the client is using absolute uri
	 *
	 * @return boolean true, if is absolute uri otherwise false
	 */
	public function isAbsUri() {
	  return isset($_SERVER['REQUEST_URI']) ? preg_match('/^http/', $_SERVER['REQUEST_URI']) : false;
	}
	
	/**
	 * Returns Uri prefix, including protocol, hostname and server port.
	 *
	 * @return string Uniform resource identifier prefix
	 */
	public function getUriPrefix() {	  
		if ($this->isSecure()) {
		  $standardPort = '443';
		  $protocol = 'https';
		} else {
		  $standardPort = '80';
		  $protocol = 'http';
		}
		
		$host = explode(':', $_SERVER['HTTP_HOST']);
		if (count($host) == 1) {
			$host[] = isset($_SERVER['SERVER_PORT']) ? $_SERVER['SERVER_PORT'] : '';
		}
		
		if ($host[1] == $standardPort || empty($host[1])) {
			unset($host[1]);
		}
		
		return $protocol.'://'.implode(':', $host);
	}
	
	/**
	 * Returns true if the current request is secure (HTTPS protocol).
	 *
	 * @return boolean
	 */
	public function isSecure() {
		return (
		  (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on' || $_SERVER['HTTPS'] == 1))
		  ||
		  (isset($_SERVER['HTTP_SSL_HTTPS']) && (strtolower($_SERVER['HTTP_SSL_HTTPS']) == 'on' || $_SERVER['HTTP_SSL_HTTPS'] == 1))
		  ||
		  (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https')
		);
	}
	
	/**
	 * Returns true if the request is a XMLHttpRequest.
	 *
	 * It works if your JavaScript library set an X-Requested-With HTTP header.
	 * Works with Prototype, Mootools, jQuery, and perhaps others.
	 *
	 * @return bool true if the request is an XMLHttpRequest, false otherwise
	 */
	public function isXmlHttpRequest() {
		return ($this->getHttpHeader('X_REQUESTED_WITH') == 'XMLHttpRequest');
	}
	
	public function getHttpHeader($name, $prefix = 'http') {
	  if ($prefix) {
		$prefix = strtoupper($prefix).'_';
	  }
	
	  $name = $prefix.strtoupper(strtr($name, '-', '_'));
	  return isset($_SERVER[$name]) ? $this->stripSlashes($_SERVER[$name]) : null;
	}
	
	/**
	 * Strip Slashes	 
	 * 	
	 * @param mixed data 		Data to be processed
	 * @return mixed 			Processed data
	 */
	public function stripSlashes(&$data) {
		return is_array($data)?array_map(array($this,'stripSlashes'),$data):stripslashes($data);
	}
	
	/**
	 * Post
	 *
	 * @param string $name
	 * @param int $type
	 * @param mixed $default
	 */
	public function post($name, $type = Flywheel_Filter::TYPE_STRING, $default = null, $mark = 1, $nofollow = false) {
		return $this->get($name, $type, $default, 'POST', $mark, $nofollow);
	}
	
	/**
	 * Get
	 *
	 * @param string $name 		Variable name
	 * @param int $type 		Kieu du lieu tra ve type for the variable, for valid values see {@link Filter::clean()}
	 * @param mixed $default	Default value if value not exist
	 * @param string $_method	Method	
	 * @return mixed
	 */
	public function get($name , $type = Flywheel_Filter::TYPE_STRING, $default = null, $method = 'GET', $mark = 0, $nofollow = false) {		
		$method = strtoupper($method);		
		switch ($method) {
			case 'GET'	:
				if (isset($this->_getVars[$name]) && $this->_getVars[$name] !== null) 
					return $this->_getVars[$name];
				$var = isset($_GET[$name])? $_GET[$name] : $default;
				break;
			case 'POST' :
				$var = isset($_POST[$name])? $_POST[$name] : $default;				
				break;
			case 'FILES'	:
				$var = $_FILES[$name];
				break;
			case 'COOKIE'	:
				$var = isset($_COOKIE[$name])? $_COOKIE[$name] : $default;				
				break;
			case 'ENV'	:
				$var = $_ENV[$name];
				break;
			case 'SERVER':
				$var = $_SERVER[$name];
				break;
			default:
				$var = isset($_REQUEST[$name])? $_REQUEST[$name] : $default;
				break;	
		}		
		if (get_magic_quotes_gpc() && $method != 'FILES' && null !== $var) {			
			$var = self::stripSlashes($var);			
		}
		$var = Flywheel_Filter::clean($var, $type,$nofollow);		
		if (1 == $mark && $type == Flywheel_Filter::TYPE_STRING && null !== $var) {
			$var = htmlspecialchars($var, ENT_QUOTES);													
		}		
		if ($method == 'GET' && $var !== null) $this->getVars[$name] = $var;		
		return $var;
	}	
	
	/**
	 * Redirect
	 *
	 * @param string $url
	 * @param int $code the HTTP status code. 
	 * 	Defaults to 302. See {@link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html} 
	 * @return void
	 */
	public static function redirect($url, $code = 302) {		
		if (strpos($url, 'http') === false) { //not entire url
			$baseUrl = Flywheel_Factory::getRouter()->getBaseUrl();
			if (false === strpos($baseUrl, '.php')) {
				$baseUrl = rtrim($baseUrl, '/') .'/';
			}						
			$url = $baseUrl .ltrim($url,'/');
		}
		header('Location: '.$url, true, $code);
		Flywheel_Application::end();
	}
	
	/**
	 * check submited
	 * 
	 * @static
	 * @return boolean
	 */
	public static function isPost() {
		return (self::POST == Flywheel_Factory::getRequest()->getMethod());		
	}
	
	/**
	 * Check Token 
	 *
	 * @param _method $_method
	 */
	public static function checkToken($method = Flywheel_Application_Request::POST) {
		$token = Flywheel_Factory::getSession()->get('token');
		$value = Flywheel_Factory::getRequest()->get($token, Flywheel_Filter::TYPE_ALNUM, null, $method);		
		if ($value === '1') {
			return true;					
		}
		
		return false;
	}
	
	/**
	 * generate form Cross-Site Request Forgery	 
	 * 
	 * @return HTML string
	 */
	public static function generateFormCSRF() {
		return '<input type="hidden" name="' .Flywheel_Factory::getSession()->getToken() .'" value="1" />';
	}
}

/**
	 * Redirect
	 *
	 * @param string $url
	 * @param int $code the HTTP status code. 
	 * 	Defaults to 302. See {@link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html} 
	 * @return void
	 */
function redirect($url, $code = 302) {
	Flywheel_Application_Request::redirect($url, $code);	
}