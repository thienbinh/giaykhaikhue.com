<?php
class Flywheel_Cache_Memcache {
	/**
	 * Intances
	 * @static
	 * @var array
	 */
	protected static $_intances;
	
	/**
	 * Config
	 * @static
	 * @var array
	 */
	protected static $_config;
	
	/**
	 * Id is a alias of config key
	 * @var string
	 */
	private $_id;
	
	/**
	 * Memcache
	 * @var Memcache
	 */
	private $_memcache;
	
	/**
	 * Get Flywheel_Cache_Memcache instance
	 * 
	 * @param string $key	default null
	 * 
	 * @return Flywheel_Cache_Memcache
	 * @throws Flywheel_Exception if connect server false
	 */
	public static function getInstance($key = null) {
		self::getConfig();
		
		if (null == $key || !isset(self::$_config[$key])) {
			foreach (self::$_config as $key => $config) {
				break;
			}			
//			$config = current(self::$_config);					
		}
		else {
			$config = self::$_config[$key];			
		}
		$name = md5(serialize($config));
		if (false === isset(self::$_intances[$name])) {
			self::$_intances[$name] = new Flywheel_Cache_Memcache($key, $config);
		}
		
		return self::$_intances[$name];
	}
	
	/**
	 * Get Config
	 */
	public static function getConfig() {
		if ((null == self::$_config)  || (0 === sizeof(self::$_config))) {
			self::$_config = Flywheel_Config::load(CONFIG_DIR .'memcached.ini', 'memcache', false);
			if (false === self::$_config) {
				self::$_config = Flywheel_Config::load(GLOBAL_CONFIGS_DIR .'memcached.ini', 'memcache');			}			
		}

		return self::$_config;
	}
	
	/**
	 * Contruct new Flywheel_Cache_Memcache object
	 * 
	 * @access private
	 * @param array $config
	 */
	private function __construct($id, $config = array()) {
		$this->_id = $id;
		$_config = array(
			'host' => '127.0.0.1',
			'port' => 11211,
			'persistent' => false,
			'weight' => 1		
		);		
		$this->_memcache = new Memcache();
		for ($i = 0, $size = sizeof($config['servers']); $i < $size ; ++$i) {
			if (!isset(self::$_config[$config['servers'][$i]])) {
				continue;
			}
			
			$serverConfig = array_merge($_config, self::$_config[$config['servers'][$i]]);		
			if (!$this->_memcache->addServer($serverConfig['host'], (int) $serverConfig['port']
							, (bool) $serverConfig['persistent'], (int) $serverConfig['weight'])) {
				$mess = 'Unable to ' .(($serverConfig['persistent'])? 'pool ' : '') 
							.'connect memcached server ' .$serverConfig['host'] 
							.':' .$serverConfig['port']; 
				throw new Flywheel_Exception($mess);				
			}
		}			
	}
	
	public function getId() {
		return $this->_id;		
	}
	
	/**
	 * Get memcache object
	 * 
	 * @return Memcache
	 */
	public function getMemcache() {
		return $this->_memcache;
	}
	
	/**
	 * Add an item to server if item did not exists 
	 * {@link http://www.php.net/manual/en/memcache.add.php}
	 * 	 	
	 * @param string	$key
	 * @param mixed		$value
	 * @param boolean	$compress
	 * @param int		$expire
	 * 
	 * @return boolean
	 */
	public function add($key, $value, $compress = false, $expire = 0) {
		if (false === Flywheel_Config::get('debug')) {
			return $this->_memcache->set($key, $value, $compress? MEMCACHE_COMPRESSED: 0, $expire); 
		} 

		$status = $this->_memcache->set($key, $value, $compress? MEMCACHE_COMPRESSED: 0, $expire);
		if (true === $status) {
			Flywheel_Debug_Profiler::mark("Add new item $key into server", get_class($this));			
		}
		return $status;
	}
	
	/**
	 * Replace value of the existing item {@link http://www.php.net/manual/en/memcache.replace.php}
	 * 
	 * @param string	$key
	 * @param mixed		$var
	 * @param boolean	$compress
	 * @param int		$expire
	 * 
	 * @return boolean
	 */
	public function replace($key, $var, $compress = false, $expire = 0) {
		if (false === Flywheel_Config::get('debug')) {
			return $this->_memcache->replace($key, $var, $compress? MEMCACHE_COMPRESSED: 0, $expire);
		}

		$status = $this->_memcache->replace($key, $var, $compress? MEMCACHE_COMPRESSED: 0, $expire);
		if (true === $status) {
			Flywheel_Debug_Profiler::mark('Replaced value of item ' .$key, get_class($this));
		}
		
		return $status;
	}
	
	/**
	 * Store cache data by key to server
	 * 
	 * @param string		$key
	 * @param mixed			$value
	 * @param boolean		$compress	.default false mean not compress
	 * @param int			$expire		.default 0
	 * 
	 * @return boolean
	 */
	public function set($key, $value, $compress = false, $expire = 0) {
		if (false === Flywheel_Config::get('debug')) {
			return $this->_memcache->set($key, $value, $compress? MEMCACHE_COMPRESSED: 0, $expire);
		}
		$status = $this->_memcache->set($key, $value, $compress? MEMCACHE_COMPRESSED: 0, $expire);
		if (true === $status) {			
			Flywheel_Debug_Profiler::mark('Set ' .$key .' to memcached servers' 
								.(($expire)? 'expire on ' .date('d/m/Y H:i:s', time() + $expire):'')
								, get_class($this));
		}
		return $status;
	}
	
	/**
	 * Retrieve items from memcached server
	 * 
	 * @params string|array $keys
	 */
	public function get($keys) {
		if (false === Flywheel_Config::get('debug')) {
			return $this->_memcache->get($keys);			
		}
		
		$value = $this->_memcache->get($keys);
		$keys = (array) $keys;
		if (false !== $value) {
			Flywheel_Debug_Profiler::mark('Retrieve ' .implode(', ', $keys) .' from servers', get_class($this));
		}
		return $value;
	}
		
	/**
	 * Delete
	 * 
	 * @param string $key
	 * @param int $timeout
	 * 
	 * @return boolean.
	 */
	public function delete($key, $timeout = 0) {
		if (false === Flywheel_Config::get('debug')) {
			return $this->_memcache->delete($key, $timeout);
		}
		
		$status = $this->_memcache->delete($key, $timeout);
		if (false !== $status) {
			Flywheel_Debug_Profiler::mark('Remove item ' .$key .' from server', get_class($this));
		}
		return $status;
	}
	
	/**
	 * Encrupt memcache key
	 * 
	 * @param string $key
	 * @param string md5|sha1 $function
	 * 
	 * @return md5 string
	 */
	private function _encryptKey($key, $function = 'md5') {
		switch ($function) {
			case 'md5' :
			default :
				return md5($key);
		}
	}
	
	/**
	 * Flush all item
	 * 
	 * @return boolean
	 */
	public function flush() {
		return $this->_memcache->flush();
	}
	
	public function __destruct() {	
		$this->_memcache->close();					
	}
}