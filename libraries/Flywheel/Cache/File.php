<?php
/**
 * Flywheel Cache file
 * 
 * @author tronghieu
 * @version		$Id: File.php 137 2010-09-20 05:21:43Z mylifeisskidrow@gmail.com $
 *
 */
class Flywheel_Cache_File {
	private $_ext = '.cache';
	private $_path = '';
	
	private static $_instances = array();
	
	private function __construct($path = null, $ext = null) {
		$this->_path = (null != $path)? rtrim($path, DS) .DS : CACHE_DIR;
		folder_create($this->_path, 0777);
		if (null  != $ext) {
			$this->_ext = $ext;
		}				
	}
	
	/**
	 * @return the $_ext
	 */
	public function getExt() {
		return $this->_ext;
	}

	/**
	 * @return the $_path
	 */
	public function getPath() {
		return $this->_path;
	}

	/**
	 * @param $_ext the extension of cache file to set
	 */
	public function setExt($_ext) {
		$this->_ext = $_ext;
	}

	/**
	 * @param $_path the path storing cache file to set
	 */
	public function setPath($_path) {
		$this->_path = $_path;
	}

	/**
	 * Get instance
	 * 
	 * @param	string $path
	 * @param 	extension
	 * 
	 * @return Flywheel_Cache_File
	 */
	public function getInstance($path = null, $ext = null) {
		$key = md5($path . $ext);
		if (!isset(self::$_instances[$key])) {
			self::$_instances[$key] = new self($path, $ext);			
		}
		
		return self::$_instances[$key];
	}
	
	/**
	 * Set
	 *
	 * @param string $file
	 * @param string $content
	 * @param int $expiration
	 * @return boolean
	 */
	public function set($file, $content, $expiration = 0, $prefix = '', $path = null) {		
		if ($path === null) {
			$path = $this->_path;
		}
		
		$prefix = (null != $prefix)? $prefix .'_' : '';
		
		$_temp = '["' . $prefix . $file .'"]';
		$file = $prefix .md5($file) . $this->_ext;
		$_temp .= $file;
		
		if ($expiration <= 0) {
			$expiration = 15552000; //default expire on 180 days			
		}
		
		$expiration  += time();
		
		$f = @fopen($filePath = $path . $file, 'w');		
		if (false === $f) {
			return false;
		}
		$bytes = fwrite($f, serialize($content));
		fclose($f);
		@chmod($filePath, 0777);
		
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug')) {
			$label = 'Saved ' .$_temp .' (' .$bytes .' bytes) in ' .$this->_path.
					(($expiration > 0) ? ', expire on ' .date('d/m/Y H:i:s', $expiration)
									: '.');
			Flywheel_Debug_Profiler::mark($label, get_class($this));			
		}
		//<-- /end debug code -->
		
		if ($expiration > 0 ) {		
			return @touch($path . $file, $expiration);
		}
		
		return true;
	}
	
	/**
	 * Get
	 *
	 * @param string $files
	 * @return string $content
	 */
	public function get($file, $prefix = '', $path = null) {
		if ($path === null) {
			$path = $this->_path;
		}
		$prefix = (null != $prefix)? $prefix .'_' : '';
		
		$id = $prefix . $file;
		$file = $path . $prefix .md5($file) . $this->_ext;
				
		if (!is_file($file)) {
			return false;			
		}	
		
		if (filemtime($file) < time()) {
			@unlink($file);
			return false;	
		}
		
		$result = file_get_contents($file);		
		$result = unserialize($result);
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::mark('Retrieve "' . $id . '" from cache', get_class($this));			
		}	
		//<-- /end debug code -->
		return $result;
	}
	
	/**
	 * Call Back
	 *
	 * @param string $files ten file
	 * @param array $callback callback function
	 * @param array $args parameter 
	 * @param int $expiration seconds
	 * @return mixed
	 */
	public function callback($callback, $args = null, $expiration = 0, $prefix = '' , $path = null) {
		$args = (array) $args;
		
		$key = implode('::', $callback) . '(' . implode(',', $args) . ')';
		
		if (($result = $this->get($key, $prefix, $path )) === false) {
			$result = call_user_func_array($callback, $args);						
			$this->set($key, $result, $expiration, $prefix, $path);
		}
		
		//<-- begin debug code -->
		if (true === Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::mark('Call back result method ' . $key, get_class($this));			
		}	
		//<-- /end debug code -->
		
		return $result;
	}
	
	/**
	 * Delete
	 * Delete cache by $file
	 *
	 * @param string $file
	 * @param string $prefix
	 * @param string $path
	 */
	public function delete($file, $prefix = '', $path = null) {
		if ($path === null) {
			$path = $this->_path;
		}
		$prefix = (null != $prefix)? $prefix .'_' : '';
			
		$_temp = $prefix .$file;
		$file = $prefix .md5($file) . $this->_ext;		
		$result = @unlink($path . $file);
		
		//<-- begin debug code -->
		if ((true == Flywheel_Config::get('debug')) && ($result === true)) {
			Flywheel_Debug_Profiler::mark('Remove cache ["' . $_temp . '"]' . $file, get_class($this));
		}
		//<-- /end debug code -->
												
		return $result;
	}
	
	/**
	 * Flush
	 * flush all cache in path
	 * 
	 * @param $path.
	 *
	 */
	public function flush($path = null) {
		if (null == $path) {
			$path = $this->_path;
		}
		
		if(false === ($handle=opendir($path)))
			return false;
			
		while($file = readdir($handle)) {
			if($file [0] === '.')
				continue;
			$fullPath = $path . DS .$file;
			if (is_file($fullPath)) {
				@unlink($fullPath);				
			}	
		}
		
		closedir($handle);
		
		//<-- begin debug code -->
		if (true == Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::mark('Flush all cache in ' .$path, get_class($this));
		}
		//<-- /end debug code -->
		return true;
	}
	
	/**
	 * Garbage collect expired cache data
	 * 
	 * @return void
	 *
	 */
	public function gc() {
		$path = $this->_path;
		
		if(($handle=opendir($path))===false)
			return;
			
		while($file = readdir($handle)) {
			if($file [0] === '.') {
				continue;				
			}								
			$fullPath = $path . DS .$file;
			if (is_file($fullPath)) {
				if (filemtime($fullPath) < time())
					@unlink($fullPath);
			}			
		}
		closedir($handle);	
	}
}