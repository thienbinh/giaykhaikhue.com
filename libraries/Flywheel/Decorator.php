<?php

/**
 * Flywheel Decorator
 * 	the class manager all framework's decorator like message pagination
 * @author Trong Hieu Luu
 *
 */
class Flywheel_Decorator {
	public static function getDecorator($type, $name, $params = array()) {		
	}
	
	/**
	 * get message decorator
	 * @static
	 * @param string	$name
	 * 
	 * @return Flywheel_Decorator_Message
	 */
	public static function getMessage($name) {
		static $instances;
		static $class;
		if (null == $class) {
			$class = 'Message';
			if (false === file_exists($file = APP_LIB_DIR.$class.'.php')) {
				$file	= FLYWHEEL_DIR .'Decorator' .$class .'.php';
				$class	= 'Flywheel_Decorator_Message.php';			 		
			}
			require_once $file;			
		}
		
		if (null == $instances) {
			$instances = array();
		}
		if (!isset($instances[$name])) {			
			$intances[$name] = new $class($name);
		}
		
		return $intances[$name];
	}
}