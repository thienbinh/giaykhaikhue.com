<?php
/**
 * @version 	$Id: Application.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @author 		Volunteer_developer<tronghieu1012@yahoo.com>
 * @copyright 	Copyright (c) 2010 Volunteer_developer. All right reserved.
 * @license 	GNU/GPL. see license.php
 */
/**
 * Flywheel Config Application
 * 
 * @package		Flywheel
 * @subpackage	Config
 *
 */
class Flywheel_Config_Application {
	public function initConfiguration() {
		if (Flywheel_Config::has('default_time_zone')) {
			date_default_timezone_set(Flywheel_Config::get('default_time_zone'));			
		}
		else {
			date_default_timezone_set(@date_default_timezone_get());
		}

		Flywheel_Config::load(GLOBAL_PATH.'config'.DS.'config.ini', 'default', false, false);
		Flywheel_Config::load(APP_CONFIGURATION_PATH.'config.ini');
		if (Flywheel_Config::get('debug')) {
			Flywheel_Debug_Profiler::getInstance()->mark('After init application configuaration', 'system.'.get_class($this));		
		}		
	}
}