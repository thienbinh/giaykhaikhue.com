<?php
class Flywheel_Config {
	protected static $_data = array();
	
	/**
	 * Set
	 * 
	 * @param string $config
	 * @param mixed $value
	 * @param string $namespace	.Config namespace
	 */
	public static function set($config, $value, $namespace = 'default') {
		self::$_data[$namespace][$config] = $value;		
	}
	
	/**
	 * Get config setting
	 * @param string $config
	 * @param string $namespace
	 */
	public static function get($config, $namespace = 'default') {
		$deep = explode('/', $config);        
		if (isset(self::$_data[$namespace][$deep[0]])) {
			$data = self::$_data[$namespace][$deep[0]];   
	        $i = 0;
	        while ($i < (sizeof($deep)-1)) {
	            ++$i;
	            $data = $data[$deep[$i]];            
	        }
	        return $data;
		}
		return null;
	}
	
	/**
	 * Add Config setting
	 * @param array $config
	 * @param string $namespace
	 */
	public static function add($config, $namespace = 'default') {
		if (!isset(self::$_data[$namespace])) {
			self::$_data[$namespace] = $config;
		}
		else {
			self::$_data[$namespace] = array_merge(self::$_data[$namespace], $config);
		}
	}
	
	/**
	 * Load config
	 * @param string $file . ini file include path
	 * @param string $namespace
	 * @param boolean $require
	 */
	public static function load($file, $namespace = 'default', $require = true) {
		if (!file_exists($file)) {
			if ($require)
				throw new Flywheel_Config_Exception('File config: "'. $file .'" does not exists!');
			else 
				return false;
		}
		
		if (Flywheel_Application::ENV_DEV == Flywheel_Application::getAppEnvironment() 
				|| null === Flywheel_Application::getAppEnvironment()) {
			$config = parse_ini_file($file, true);
		} else { // handler config file
			$cacheFileName = basename($file , '.ini') .'.ini.php';
			if (strpos($file, GLOBAL_CONFIGS_DIR) === 0) { // global config
				$cachePath = CACHE_DIR .'config' .DS .'global' .DS;				
			}
			else {
				$cachePath = CACHE_DIR .'config' .DS .Flywheel_Application::getApp()->getName() .DS;
			}
			
			folder_create($cachePath, 0777);
			
			if (file_exists($cacheFile = $cachePath.$cacheFileName)) {												
				$config = require $cacheFile;
			}
			else {								
				$config = parse_ini_file($file, true);				
				$hander = self::_configHandle($config, $cacheFileName);
				$fp = @fopen($cacheFile, 'w');	
		        if ($fp === false) {
		           //throw new Flywheel_Config_Exception("Couldn't write compiled data. Failed to open $cacheFile");
		        }
		        
		        fwrite($fp, $hander);
		        fclose($fp);
				//file_put_contents($cacheFile, $hander);
			}
		}
		
		self::add($config, $namespace);
		
		return $config;
	}
	
	/**
	 * Has
	 * 
	 * @param string $config
	 * @param string $namespace
	 * 
	 * @return boolean
	 */
	public static function has($config, $namespace='default') {
		return isset(self::$_data[$namespace][$config]);		
	}
	
	/**
	 * check config namespace exists
	 * @param string $namespace
	 * @return boolean
	 */
	public static function configNamespaceExists($namespace) {
		return isset(self::$_data[$namespace]);		
	}
	
	/**
	 * Config Handle
	 *
	 * @param array	 $data	config array handle
	 * @param string $file	config file
	 * @return data to be write cache file
	 */
	private static function _configHandle($data, $file) {
		$retval = sprintf("<?php\n".
                      "// Config cache file %s.ini\n".
                      "// date: %s\nreturn %s;\n",
                      $file, date('Y/m/d H:i:s'), var_export($data, true));

		return $retval;
	}
}