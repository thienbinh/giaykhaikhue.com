<?php
/**
 * Flywheel Pagination
 * 
 * @author 		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id$
 * @package		Flywheel
 * @subpackage	Pagination
 *
 */
class Flywheel_Pagination {
	public $pageSize;
	public $total;
	public $currentPage = 1;
	public $numPageShow		= 5;
	public $showTotalPage	= true;
	public $showJumpPage	= false;
	public $showSelectPage	= false;
	public $urlStructure;
	public $template;	
	public $totalPage;	
	public $startPage;
	public $items = '';
	
	public function __construct($options = array()) {
		if (null == $this->template) {
			$this->template = FLYWHEEL_DIR .'Pagination' .DS .'template' .DS .'default';
		}		
	}
	
	public function initialize($baseUrl=true) {
        if($baseUrl)
            $this->urlStructure = Flywheel_Factory::getRouter()->getBaseUrl() .$this->urlStructure;
		else $this->urlStructure = $this->urlStructure;
		$this->totalPage = ceil($this->total / $this->pageSize);				
		if (($this->currentPage < 0) || ($this->currentPage > $this->totalPage)) {
			$this->currentPage = 1;			
		}

		if ($this->currentPage > $this->numPageShow / 2) {
			$this->startPage = $this->currentPage - floor($this->numPageShow / 2);
			if(($this->totalPage - $this->startPage) < $this->numPageShow) {
				$this->startPage = $this->totalPage - $this->numPageShow + 1;
			}
		} else {
			$this->startPage = 1;
		}
		
		if ($this->startPage < 1) {
			$this->startPage = 1;
		}
	}
	
	public function render() {
		$view = new Flywheel_View();
		$view->templatePath = $this->template;
		$buffer = $view->render(null, array('pagination' => $this));
		unset($view);
		return $buffer;		
	}
	
	public function createLink($page) {
		return str_replace('<page>', $page, $this->urlStructure);		
	}
}