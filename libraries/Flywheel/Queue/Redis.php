<?php
/**
 * Flywheel Queue Exception
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Redis.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Queue
 */
class Flywheel_Queue_Redis extends Flywheel_Queue {
	protected $_adapter;
			
	public function __construct($name, $config) {
		$this->_name = $name;
		$this->_adapter = new Flywheel_Redis_Client('Queue' .$name, $config);				
	}
	
	/**
	 * push
	 * 
	 * @param string $name
	 * @param mixed $item
	 */
	public function push($item) {		
		return $this->_adapter->prepend($this->_name, $item);
	}
	
	/**
	 * pop the end of queue
	 */
	public function pop() {
		return $this->_adapter->rightPop($this->_name);
	}
	
	/**
	 * Get all queue item
	 */
	public function getAllQueueItem() {
		return $this->_adapter->getList($this->_name);		
	}
}