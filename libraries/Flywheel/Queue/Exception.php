<?php
/**
 * Flywheel Queue Exception
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Exception.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Queue
 */
class Flywheel_Queue_Exception extends Flywheel_Exception {}