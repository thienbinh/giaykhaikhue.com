<?php
/**
 * Ming module
 * class base of modules
 * 
 * @abstract
 * @author 		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id$
 * @package		Flywheel
 * @subpackage	Module
 */
abstract class Flywheel_Module {
	public $name;
	protected $_params = array();
	protected $_model;
	protected $_template;
	protected $_file;
	
	public function __construct($file, $global) {
		$this->_file = $file;		
		$this->_template = ((true === $global)? GLOBAL_TEMPLATE_DIR : TEMPLATE_DIR)
							.'modules' .DS .$file;  
	}
	
	final public function setParams($params) {		
		$this->_params = $params;		
	}
	
	abstract function render();
}