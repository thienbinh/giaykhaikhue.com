<?php
/**
 * Flywheel Filter
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Filter.php 1016 2011-02-18 19:10:15Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage	Filter
 *
 */
class Flywheel_Filter {
	const TYPE_STRING = 1;
	const TYPE_INT = 2;
	const TYPE_FLOAT = 3;
	const TYPE_DOUBLE = 4;	
	const TYPE_BOOLEAN = 5;
	const TYPE_ALNUM = 6;
	const TYPE_BASE64 = 7;
	const TYPE_CMD = 8;	
	const TYPE_ARRAY = 9;
    const TYPE_EDITOR = 10;
	
	public static function clean($value, $type = self::TYPE_STRING, $nofollow = false) {
		if ($value === null) return null;
		
		switch ($type) {
			case self::TYPE_INT:
				preg_match('/-?[0-9]+/', (string) $value, $matches);
				$result = @ (int) $matches[0];				
				break;
			case self::TYPE_FLOAT:
			case self::TYPE_DOUBLE:
				preg_match('/-?[0-9]+(\.[0-9]+)?/', (string) $value, $matches);
				$result = @ (float) $matches[0];
				break;
			case self::TYPE_BOOLEAN:
				if ($value === 'false') {
					$result = false;					
				} else {
					$result = (boolean) $value;
				}
				break;
			case self::TYPE_ALNUM:
				$result = (string) preg_replace( '/[^A-Z0-9]/i', '', $value);
				break;
			case self::TYPE_CMD:
				$result = (string) preg_replace( '/[^A-Z0-9_\.-]/i', '', $value);
				$result = ltrim($result, '.');
				break;
			case self::TYPE_ARRAY:
				if (null == $value || 'null' == $value) {
					$result = array();			
				} else {
					$result = (array) $value;
				}
				break;
			case self::TYPE_BASE64:				
				$result = (string) preg_replace( '/[^A-Z0-9\/+=]/i', '', $value);
				break;
            case self::TYPE_EDITOR:
                $result = trim((string) $value);
                if($nofollow) $result = str_replace('<a','<a rel="nofollow"',$result);
                break;
			case self::TYPE_STRING:
			default:
				$result = trim((string) $value);
				break;
		}		
		
		return $result;
	}
}