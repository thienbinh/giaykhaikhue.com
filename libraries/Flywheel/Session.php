<?php

/**
 * Flywheel	Session
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Session.php 1037 2011-02-23 10:43:30Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage	Session
 *
 */
class Flywheel_Session {

    /**
     * Session started
     * @var $_started boolean
     */
    protected $_started = false;
    /**
     * internal state
     *
     * @access protected
     * @var    string $_state one of 'active'|'expired'|'destroyed|'error'
     * @see getState()
     */
    protected $_state;
    protected $_handler;
    protected $_config = array(
        'lifetime' => 3600,
    );

    public function __construct($config) {
//		if (session_id()) {
//			session_unset();
//			session_destroy();
//		}
        $this->_config = array_merge($this->_config, $config);
        ini_set('session.save_handler', 'files');
        ini_set('session.use_trans_sid', '0');

        if (isset($this->_config['handler']) && $this->_config['handler']) {
            $handlerClass = 'Flywheel_Session_Handler_' . ucfirst($this->_config['handler']);
            unset($this->_config['handler']);
            $handler = new $handlerClass($this->_config);

            session_set_save_handler(
                    array(&$handler, 'open'), array(&$handler, 'close'), array(&$handler, 'read'), array(&$handler, 'write'), array(&$handler, 'destroy'), array(&$handler, 'gc')
            );

            $this->_handler = $handler;
            //<-- begin debug code -->
            if (Flywheel_Config::get('debug')) {
                Flywheel_Debug_Profiler::mark('Installed ' . $handlerClass, get_class($this));
            }
            //<-- /end debug code -->		
        }

        if (isset($this->_config['name'])) {
            session_name($this->_config['name']);
        }

        ini_set('session.gc_maxlifetime', $this->_config['lifetime']);

        //define the lifetime of the cookie
        if (isset($this->_config['cookie_ttl'])
                || isset($this->_config['cookie_domain']) || isset($this->_config['cookie_path'])) {
            // cross subdomain validity is default behavior
            $ttl = (isset($this->_config['cookie_ttl'])) ?
                    (int) $this->_config['cookie_ttl'] : 0;
            $domain = (isset($this->_config['cookie_domain'])) ?
                    $this->_config['cookie_domain'] : '.' . Flywheel_Factory::getRouter()->getBaseUrl();
            $path = (isset($this->_config['cookie_path'])) ?
                    '/' . trim($this->_config['cookie_path'], '/') . '/' : '/';
            session_set_cookie_params($ttl, $path, $domain);
        } else {
            $cookie = session_get_cookie_params();
            session_set_cookie_params($cookie['lifetime'], $cookie['path'], $cookie['domain']);
        }

        if (Flywheel_Factory::getRequest()->isSecure()) {
            ini_set('session.cookie_secure', true);
        }
        ini_set('session.use_only_cookies', 1);
    }

    public function id() {
        return session_id();
    }

    /**
     * Start session
     */
    public function start() {
        if (true === $this->_started) {
            return false;
        }
        session_cache_limiter('none');
        $this->_started = session_start();

        // Send modified header for IE 6.0 Security Policy
        header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');

        if (isset($_SESSION['__flash/new__'])) {
            $_SESSION['__flash/old__'] = array_keys($_SESSION['__flash/new__']);
        } else {
            $_SESSION['__flash/old__'] = array();
        }
        Flywheel_Debug_Profiler::mark('session start', get_class($this));
        return $this->_started;
    }

    /**
     * Get
     * 
     * @param string $name session param name	 
     */
    public function get($name) {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    /**
     * Has
     * 
     * @param string $name session param name	 
     */
    public function has($name) {
        return isset($_SESSION[$name]);
    }

    /**
     * Set
     * 
     * @param string $name session param name
     * @param mixed $data
     */
    public function set($name, $data) {
        $_SESSION[$name] = $data;
    }

    /**
     * Remove
     *
     * @param string $name
     * @param string $namespace
     * @return void
     */
    public function remove($name) {
        unset($_SESSION[$name]);
    }

    /**
     * Create a token-string
     *
     * @access protected
     * @param int $length lenght of string
     * @return string $id generated token
     */
    public function createToken($length = 32) {
        $chars = '!@#$%^&*()0123456789abcdef';
        $max = strlen($chars) - 1;
        $token = '';
        $name = session_name();
        for ($i = 0; $i < $length; ++$i) {
            $token .= $chars[(rand(0, $max))];
        }

        return md5($token . $name . php_uname());
    }

    /**
     * Create a session id
     *
     * @access private
     * @return string Session ID
     */
    private function _createId() {
        $id = 0;
        while (strlen($id) < 32) {
            $id .= mt_rand(0, mt_getrandmax());
        }

        $id = md5(uniqid($id, true));
        return $id;
    }

    /**
     * Get Token
     *
     * @param boolean $reset. default = false
     * @return string
     */
    public function getToken($reset = false) {
        if (false === ($this->has('token'))
                || (null === ($token = $this->get('token')))
                || (true === $reset)) {
            $token = $this->createToken();
            $this->set('token', $token);
        }

        return $token;
    }

    /**
     * Get Session Data
     *
     * @param string $namespace
     * @return array
     */
    public function getSessionData($namespace = null) {
        if ($namespace === null)
            return $_SESSION;
        $namespace = '__' . $namespace;
        if (isset($_SESSION[$namespace])) {
            return $_SESSION[$namespace];
        }

        return array();
    }

    /**
     * Set Flash
     * 
     * @param string $name
     * @param mixed $data flash data
     */
    public function setFlash($name, $data) {
        $_SESSION['__flash/new__'][$name] = $data;
        unset($_SESSION['__flash/old__'][$name]);
    }

    public function removeFlash($name) {
        unset($_SESSION['__flash/new__'][$name]);
    }

    /**
     * Get Flash
     * @param string $name
     */
    public function getFlash($name) {
        if (isset($_SESSION['__flash/new__'][$name])) {
            return $_SESSION['__flash/new__'][$name];
        }
        return array();
    }

    /**
     * Create Session Id
     *
     * @return md5 string
     */
    private function _createSessionId() {
        $id = 0;
        while (strlen($id) < 32) {
            $id .= mt_rand(0, mt_getrandmax());
        }

        $id = 'sse_' . md5(uniqid($id, true) . $_SERVER['SERVER_ADDR'] . $_SERVER['SERVER_NAME']);
        return $id;
    }

    public function restart($id = null) {
        $this->destroy();
        if ($this->_state !== 'destroyed') {
            // @TODO :: generated error here
            return false;
        }

        // Re-register the session handler after a session has been destroyed, to avoid PHP bug

        $this->_state = 'restart';
        //regenerate session id
        if (null == $id) {
            $id = $this->_createSessionId();
        }

        session_id($id);
        $this->_started = false;
        if ($this->start()) {
            $this->_state = 'active';
            return true;
        }

        return false;
    }

    /**
     * Frees all session variables and destroys all data registered to a session
     *
     * This method resets the $_SESSION variable and destroys all of the data associated
     * with the current session in its storage (file or DB). It forces new session to be
     * started after this method is called. It does not unset the session cookie.
     *
     * @see    session_unset()
     * @see    session_destroy()
     */
    public function destroy() {
        // session was already destroyed
        if ($this->_state === 'destroyed') {
            return true;
        }

        // In order to kill the session altogether, like to log the user out, the session id
        // must also be unset. If a cookie is used to propagate the session id (default behavior),
        // then the session cookie must be deleted.
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time() - 42000, '/');
        }
        session_unset();
        session_destroy();
        $this->_state = 'destroyed';
        return true;
    }

    /**
     * Close
     */
    public function close() {
        if (isset($_SESSION['__flash/old__'])) {
            for ($i = 0, $size = sizeof($_SESSION['__flash/old__']); $i < $size; ++$i) {
                unset($_SESSION['__flash/new__'][$_SESSION['__flash/old__'][$i]]);
            }
        }
        session_write_close();
    }

    public function __destruct() {
        $this->close();
    }

}
