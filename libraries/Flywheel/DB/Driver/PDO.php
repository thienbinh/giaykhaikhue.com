<?php

/**
 * Flywheel DB Driver PDO
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: PDO.php 1293 2011-04-07 17:41:41Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage          DB/Driver
 * @copyright           Flywheel team (c) 2010
 *
 */
class Flywheel_DB_Driver_PDO extends Flywheel_DB_Abstract {

    /**
     * Current execute sql
     * @var string
     */
    private $_sql;
    /**
     * Database name
     * @var string
     */
    private $_dbname;
    /**
     * PHP data object
     * @var PDO
     */
    private $pdo;
    /**
     * Statement
     * 
     * @var PDOStatement
     */
    private $stmt;
    /**
     * Use Transaction
     *
     * @var boolean
     */
    private $_transaction;
    /**
     * Whether to cache prepared statements.
     * 
     * @TODO 	  
     * @var boolean
     */
    private $_cachePrepareStatements = false;
    /**
     * Cache of prepare statements association array keyed by md5 of SQL
     * @var array
     */
    private $_prepareStatements = array();

    public function __construct($config) {
        $server = $config['host'];
        $server = explode(':', $server);
        $driver = (isset($config['db_driver']) ? $config['db_driver'] : 'mysql');
        $dsn = $driver . ':host=' . $server[0]
                . (isset($server[1]) ? ';port=' . $server[1] : '');

        if (isset($config['dbname'])) {
            $this->_dbname = $config['dbname'];
            $dsn .= ';dbname=' . $config['dbname'];
        }

        $driverOptions = array();
        if (isset($config['option'])) {
            $this->_processDriverOptions($config['option'], $driverOptions);
        }
        if (isset($config['cache_prepare']) && (true === $config['cache_prepare'])) {
            $this->_cachePrepareStatements = true;
        }
        try {
            //echo $dsn;
            $this->pdo = new PDO($dsn, $config['username'], $config['password'], $driverOptions);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->query("SET NAMES 'utf8'");
            $this->_id = md5($dsn . $config['username'] . $config['password']);
        } catch (PDOException $e) {
            throw new Flywheel_DB_Exception('Unable to open PDO connection', $e);
        }

        parent::__construct($config);
    }

    /**
     * Process Driver Options
     *
     * @param Array $options
     * @param Array &$writeOn	 
     */
    private function _processDriverOptions($options, &$writeOn) {
        foreach ($options as $attr => $value) {
            if (is_string($attr) && strpos($attr, 'PDO::') !== false) {
                $attr = strtoupper($attr);
            } else {
                $attr = 'PDO::ATTR_' . strtoupper($attr);
            }

            $attr = constant($attr);
            $writeOn[$attr] = $value;
        }
    }

    /**
     * Get Type
     * Type of Object Connection
     *
     * @return int
     */
    public function getType() {
        return parent::TYPE_PDO;
    }

    /**
     * Get SQL String
     * 
     * @return String cau lenh SQL
     */
    public function getSql() {
        return $this->_sql;
    }

    /**
     * Set SQL Query
     * 
     * @param String $_sql
     */
    public function setSql($sql) {
        $this->_sql = (string) $sql;
    }

    /**
     * Select Database
     *
     * @param string $dbname Database Name
     */
    public function selectDb($dbname) {
        $this->_dbname = $dbname;
    }

    /**
     * Get Database
     * get current DB name
     * 
     * @return string
     */
    public function getDatabase() {
        return $this->_dbname;
    }

    /**
     * Is Connected
     * 
     * @return boolean
     */
    public function isConnected() {
        return!is_null($this->pdo);
    }

    /**
     * SQL Name Quote
     * 
     * @param string $text
     * @return string
     */
    public function nameQuote($text) {
        return '`' . $text . '`';
    }

    /**
     * Quote
     * 
     * @param string $text
     * @return string
     */
    public function quote($text) {
        return $this->pdo->quote($text);
    }

    /**
     * Clears any stored prepared statements for this connection.
     */
    public function clearAllPrepareStatementCache() {
        $this->_prepareStatements = array();
    }

    /**
     * Affected Rows
     * the affected rows of the last query
     *
     * @return int
     */
    public function affectedRows() {
        if ($this->stmt == null)
            return 0;

        return $this->stmt->rowCount();
    }

    /**
     * Last Insert Id
     *
     * @return int
     */
    public function lastInsertId() {
        return $this->pdo->lastInsertId();
    }

    /**
     * Query
     *
     * @param string $sql
     */
    public function query($sql = null) {
        try {
            $begin = $this->getMicrotime();
            $this->stmt = $this->pdo->query($sql);
            $end = $this->getMicrotime();

            //<-- begin debug code -->
            if (Flywheel_Config::get('debug')) {
                $this->log($sql, ($end - $begin));
            }
            //<-- /end debug code -->

            return $this->stmt;
        } catch (PDOException $pe) {
            throw new Flywheel_Exception('Error when exec sql=' . $sql, $pe);
        }
    }

    /**
     * Diagnostic function
     *
     * @return	string
     */
    public function explain($sql = null) {
        if ($sql === null) {
            $sql = $this->_sql;
        }

        $temp = $sql;

        $sql = 'EXPLAIN ' . $sql;

        if (!($cur = $this->query($sql))) {
            return null;
        }
        $first = true;

        $buffer = '<table id="explain-sql">';
        $buffer .= '<thead><tr><td colspan="99"> ' . $temp . ' </td></tr>';
        while ($row = $cur->fetch(PDO::FETCH_ASSOC)) {
            if ($first) {
                $buffer .= '<tr>';
                foreach ($row as $k => $v) {
                    $buffer .= '<th>' . $k . '</th>';
                }
                $buffer .= '</tr>';
                $first = false;
            }
            $buffer .= '</thead><tbody><tr>';
            foreach ($row as $k => $v) {
                $buffer .= '<td>' . $v . '</td>';
            }
            $buffer .= '</tr>';
        }
        $buffer .= '</tbody></table>';
        $this->freeResult();

        $this->_sql = $temp;

        return $buffer;
    }

    /**
     * prepare
     *
     * @param string $sql	 
     * 
     * @throws Flywheel_DB_Exception
     */
    public function prepare($sql = null) {
        if (null == $sql) {
            $sql = $this->_sql;
        } else {
            $this->_sql = (string) $sql;
        }

        if (null == $sql) {
            throw new Flywheel_DB_Exception('SQL query string is Null');
        }

        try {
            if (true === $this->_cachePrepareStatements) {
                $cacheKey = md5($sql);
                if (!isset($this->_prepareStatements[$cacheKey])) {
                    $begin = $this->getMicrotime();
                    $this->stmt = $this->pdo->prepare($sql);
                    $end = $this->getMicrotime();
                    $this->_prepareStatements[$cacheKey] = $this->stmt;

                    //<-- begin debug code -->
                    if (Flywheel_Config::get('debug')) {
                        $this->log('Prepare: ' . $sql, ($end - $begin));
                    }
                    //<-- /end debug code -->
                } else {
                    $this->stmt = $this->_prepareStatements[$cacheKey];
                }
            } else {
                $begin = $this->getMicrotime();
                $this->stmt = $this->pdo->prepare($sql);
                $end = $this->getMicrotime();

                //<-- begin debug code -->
                if (Flywheel_Config::get('debug')) {
                    $this->log('Prepare: ' . $sql, ($end - $begin));
                }
                //<-- /end debug code -->
            }
        } catch (PDOException $e) {
            throw new Flywheel_DB_Exception('Error prepare query =' . $sql, $e);
        }
    }

    /**
     * Bind param of prepare statement
     * 
     * @param mixed $param
     * @param mixed $value
     * @param $type PDO::PARAM_*
     * 
     * @return boolean
     */
    public function bindParam($param, $value, $type = null) {
        try {
            if (null === $type) {
                return $this->stmt->bindParam($param, $value);
            }

            return $this->stmt->bindParam($param, $value, $type);
        } catch (PDOException $e) {
            throw new Flywheel_DB_Exception('Error when call PDOStatement::bindParam', $e);
        }
    }

    /**
     * Bind value of prepare statment
     * 
     * @param mixed $param
     * @param mixed $value
     * @param $type PDO::PARAM_*
     * 
     * @return boolean
     */
    public function bindValue($param, $value, $type = null) {
        try {
            if (null === $type) {
                return $this->stmt->bindValue($param, $value);
            }

            return $this->stmt->bindValue($param, $value, $type);
        } catch (PDOException $e) {
            throw new Flywheel_DB_Exception('Error when call PDOStatement::bindParam', $e);
        }
    }

    /**
     * Execute
     *
     */
    public function execute() {
        try {
            $begin = $this->getMicrotime();

            $this->stmt->execute();
            $end = $this->getMicrotime();

            //<-- begin debug code -->
            if (Flywheel_Config::get('debug')) {
                $this->log('Execute: ' . $this->_sql, ($end - $begin));
            }
            //<-- /end debug code -->
        } catch (PDOException $e) {
            throw new Flywheel_DB_Exception('Error execute query = ' . $this->_sql, $e);
        }
    }

    /**
     * Free Result	 
     */
    public function freeResult() {
        $this->stmt->closeCursor();
    }

    /**
     * Fetch
     * fetch ket qua dau tien cua cau lenh SELECT
     *
     * @return Array
     */
    public function fetch() {
        $result = $this->stmt->fetch(PDO::FETCH_ASSOC);
        $this->freeResult();
        return $result;
    }

    /**
     * Fetch All	 
     *
     * @param string $key
     * @return Array | false of not found any record
     */
    public function fetchAll($key = '') {
        $result = array();

        while ($row = $this->stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($key != '') {
                $result[$row[$key]] = $row;
            } else {
                $result[] = $row;
            }
        }
        $this->freeResult();

        if (count($result) > 0) {
            return $result;
        }

        return false;
    }

    /**
     * Fetch All Column
     */
    public function fetchColumns() {
        return $this->stmt->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * Select
     *
     * @param string $table DBTable Name
     * @param string $fields Table Columns
     * @param string $where SQL Query WHERE clause
     * @param string $order SQL Query ORDER clause
     * @param string $limit SQL Query LIMT
     */
    public function select($table, $fields, $where = null, $order = null, $limit = null, $key = '') {
        if (is_array($fields)) {
            $fields = implode(',', $fields);
        }
        $sql = 'SELECT ' . $fields . ' FROM ' . $this->nameQuote($table)
                . (($where != null) ? ' WHERE ' . $where : '')
                . (($order != null) ? ' ORDER BY ' . $order : '')
                . (($limit != null) ? ' LIMIT ' . $limit : '');
        $this->query($sql);

        return $this->fetchAll($key);
    }

    /**
     * Select One
     *
     * @param string $table DBTable
     * @param string $fields Table columns
     * @param string $where SQL Query WHERE clause
     */
    public function selectOne($table, $fields, $where = null, $order = null) {
        if (is_array($fields)) {
            $fields = implode(',', $fields);
        }
        $sql = 'SELECT ' . $fields . ' FROM ' . $this->nameQuote($table)
                . (($where != null) ? ' WHERE ' . $where : '')
                . (($order != null) ? ' ORDER BY ' . $order : '');
        $sql .= ' LIMIT 1';
        $this->query($sql);
        return $this->fetch();
    }

    /**
     * Insert Single Record
     *
     * @param string $table DBTable name
     * @param array $data
     * @return int Last Insert Id
     * 
     * @throws SystemException
     */
    public function insert($table, $data, $quoted = false) {
        if (!is_array($data))
            throw new Flywheel_DB_Exception('Empty insert data or data not valid');

        $datas = array();
        $datas[] = $data;

        return $this->insertMulti($table, $datas, $quoted);
    }

    /**
     * Insert many records in single query
     *
     * @param string $table DBTable name
     * @param array $data
     * @return int Last Insert Id
     * 
     * @throws SystemException
     */
    public function insertMulti($table, $datas, $quoted = false) {
        $sql = $this->_buildInsertMutipleRecords($table, $datas, $quoted);

        /**
         * TODO tao ra mot mang 1 chieu chua cac gia tri danh cho viec bind data
         */
        $databind = array();
        for ($i = 0, $size = sizeof($datas); $i < $size; ++$i) {
            $databind = array_merge($databind, array_values($datas[$i]));
        }

        $begin = $this->getMicrotime();
        $this->prepare($sql);
        $this->_populateStmtValues($databind);
        $this->execute();

        $end = $this->getMicrotime();

        if (Flywheel_Config::get('debug')) {
            $this->log($sql, ($end - $begin));
        }
        return $this->lastInsertId();
    }

    /**
     * Build Insert Mutiple Records
     * 	 
     * @param string $table ten table
     * @param array $data : Moi ban ghi can insert se la mot phan tu cua mang nay
     * PHP PDO MySQLi 
     * @return string sql
     */
    private function _buildInsertMutipleRecords($table, $data, $quoted = false) {
        $columns = array_keys($data[0]);

        if (true == $quoted) {
            $sql = "INSERT INTO " . $this->nameQuote($table)
                    . " (" . implode(', ', $columns)
                    . ") VALUES \n";
        } else {
            $sql = "INSERT INTO " . $this->nameQuote($table)
                    . " (`" . implode('`, `', $columns)
                    . "`) VALUES \n";
        }

        $sizeOfColumn = sizeof($columns);

        for ($i = 1, $numberRecord = sizeof($data); $i <= $numberRecord; $i++) {
            $sql .= '(';
            for ($p = 1; $p <= $sizeOfColumn; $p++) {
                $sql .= ':p' . ($p + ($i - 1) * $sizeOfColumn);
                if ($p !== $sizeOfColumn)
                    $sql .= ',';
            }
            $sql .= ")\n";

            if ($i !== $numberRecord)
                $sql .= ',';
        }
        return $sql;
    }

    /**
     * Build insert Into on duplicate update
     *     
     * @param string $table ten table
     * @param array $data : Moi ban ghi can insert se la mot phan tu cua mang nay
     * PHP PDO MySQLi 
     * @return string sql
     */
    private function _buildInsertIntoOnDuplicateUpdate($table, $data, $quoted = false, $updateFields=false) {
        //nÃ¡ÂºÂ¿u khÃƒÂ´ng nhÃ¡ÂºÂ­p danh sÃƒÂ¡ch trÃ†Â°Ã¡Â»ï¿½ng cÃ¡ÂºÂ§n update thÃƒÂ¬ return false
        if (!$updateFields)
            return false;
        //danh sÃƒÂ¡ch trÃ†Â°Ã¡Â»ï¿½ng
        $columns = array_keys($data[0]);

        if (true == $quoted) {
            $sql = "INSERT INTO " . $this->nameQuote($table)
                    . " (" . implode(', ', $columns)
                    . ") VALUES \n";
        } else {
            $sql = "INSERT INTO " . $this->nameQuote($table)
                    . " (`" . implode('`, `', $columns)
                    . "`) VALUES \n";
        }

        $sizeOfColumn = sizeof($columns);
        $numberRecord = sizeof($data);
        for ($i = 1; $i <= $numberRecord; $i++) {
            $sql .= '(';
            for ($p = 1; $p <= $sizeOfColumn; $p++) {
                $sql .= ':p' . ($p + ($i - 1) * $sizeOfColumn);
                if ($p !== $sizeOfColumn)
                    $sql .= ',';
            }
            $sql .= ")\n";

            if ($i !== $numberRecord)
                $sql .= ',';
        }
        //build on duplicate

        $sub = '';
        if (!empty($updateFields)) {
            foreach ($updateFields as $updateField) {
                if ($sub == '')
                    $sub.=$updateField . '=VALUES(' . $updateField . ')';
                else
                    $sub.=',' . $updateField . '=VALUES(' . $updateField . ')';
            }
            $sql.=' ON DUPLICATE KEY UPDATE ' . $sub;
        }
        return $sql;
    }

    /**
     * Build insert Into on ignore
     *     
     * @param string $table ten table
     * @param array $data : Moi ban ghi can insert se la mot phan tu cua mang nay
     * PHP PDO MySQLi 
     * @return string sql
     */
    private function _buildInsertIntoIgnore($table, $data, $quoted = false, $updateFields=false) {
        //nÃ¡ÂºÂ¿u khÃƒÂ´ng nhÃ¡ÂºÂ­p danh sÃƒÂ¡ch trÃ†Â°Ã¡Â»ï¿½ng cÃ¡ÂºÂ§n update thÃƒÂ¬ return false
        if (!$updateFields)
            return false;
        //danh sÃƒÂ¡ch trÃ†Â°Ã¡Â»ï¿½ng
        $columns = array_keys($data[0]);

        if (true == $quoted) {
            $sql = "INSERT IGNORE INTO " . $this->nameQuote($table)
                    . " (" . implode(', ', $columns)
                    . ") VALUES \n";
        } else {
            $sql = "INSERT IGNORE INTO " . $this->nameQuote($table)
                    . " (`" . implode('`, `', $columns)
                    . "`) VALUES \n";
        }

        $sizeOfColumn = sizeof($columns);
        $numberRecord = sizeof($data);
        for ($i = 1; $i <= $numberRecord; $i++) {
            $sql .= '(';
            for ($p = 1; $p <= $sizeOfColumn; $p++) {
                $sql .= ':p' . ($p + ($i - 1) * $sizeOfColumn);
                if ($p !== $sizeOfColumn)
                    $sql .= ',';
            }
            $sql .= ")\n";

            if ($i !== $numberRecord)
                $sql .= ',';
        }
        //build on duplicate

        $sub = '';
        if (!empty($updateFields)) {
            foreach ($updateFields as $updateField) {
                if ($sub == '')
                    $sub.=$updateField . '=VALUES(' . $updateField . ')';
                else
                    $sub.=',' . $updateField . '=VALUES(' . $updateField . ')';
            }
            $sql.=' ON DUPLICATE KEY UPDATE ' . $sub;
        }
        return $sql;
    }

    /**
     * insert into ignore
     *
     * @param string $table DBTable name
     * @param array $data
     * @return int Last Insert Id
     * 
     * @throws SystemException
     */
    public function insertIgnoreIntoMulti($table, $datas, $quoted = false) {
        $sql = $this->_buildInsertIntoIgnore($table, $datas, $quoted);

        /**
         * TODO tao ra mot mang 1 chieu chua cac gia tri danh cho viec bind data
         */
        $databind = array();
        for ($i = 0, $size = sizeof($datas); $i < $size; ++$i) {
            $databind = array_merge($databind, array_values($datas[$i]));
        }

        $begin = $this->getMicrotime();

        $this->prepare($sql);
        $this->_populateStmtValues($databind);
        $this->execute();

        $end = $this->getMicrotime();

        if (Flywheel_Config::get('debug')) {
            $this->log($sql, ($end - $begin));
        }
        return $this->lastInsertId();
    }

    /**
     * Build Replace Into Mutiple Records
     *     
     * @param string $table ten table
     * @param array $data : Moi ban ghi can insert se la mot phan tu cua mang nay
     * PHP PDO MySQLi 
     * @return string sql
     */
    private function _buildReplaceIntoMutipleRecords($table, $data, $quoted = false) {
        $columns = array_keys($data[0]);

        if (true == $quoted) {
            $sql = "REPLACE INTO " . $this->nameQuote($table)
                    . " (" . implode(', ', $columns)
                    . ") VALUES \n";
        } else {
            $sql = "REPLACE INTO " . $this->nameQuote($table)
                    . " (`" . implode('`, `', $columns)
                    . "`) VALUES \n";
        }

        $sizeOfColumn = sizeof($columns);
        $numberRecord = sizeof($data);
        for ($i = 1; $i <= $numberRecord; $i++) {
            $sql .= '(';
            for ($p = 1; $p <= $sizeOfColumn; $p++) {
                $sql .= ':p' . ($p + ($i - 1) * $sizeOfColumn);
                if ($p !== $sizeOfColumn)
                    $sql .= ',';
            }
            $sql .= ")\n";

            if ($i !== $numberRecord)
                $sql .= ',';
        }
        return $sql;
    }

    /**
     * Rreplace into many records in single query
     *
     * @param string $table DBTable name
     * @param array $data
     * @return int Last Insert Id
     * 
     * @throws SystemException
     */
    public function replaceIntoMulti($table, $datas, $quoted = false) {
        $sql = $this->_buildReplaceIntoMutipleRecords($table, $datas, $quoted);

        /**
         * TODO tao ra mot mang 1 chieu chua cac gia tri danh cho viec bind data
         */
        $databind = array();
        for ($i = 0, $size = sizeof($datas); $i < $size; ++$i) {
            $databind = array_merge($databind, array_values($datas[$i]));
        }

        $begin = $this->getMicrotime();

        $this->prepare($sql);
        $this->_populateStmtValues($databind);
        $this->execute();

        $end = $this->getMicrotime();

        if (Flywheel_Config::get('debug')) {
            $this->log($sql, ($end - $begin));
        }
        return $this->lastInsertId();
    }

    /**
     * insert into on duplicate
     *
     * @param string $table DBTable name
     * @param array $data
     * @return int Last Insert Id
     * 
     * @throws SystemException
     */
    public function insertIntoOnDuplicateUpdateMulti($table, $datas, $quoted = false, $updateFields=false) {
        $sql = $this->_buildInsertIntoOnDuplicateUpdate($table, $datas, $quoted, $updateFields);

        /**
         * TODO tao ra mot mang 1 chieu chua cac gia tri danh cho viec bind data
         */
        $databind = array();
        for ($i = 0, $size = sizeof($datas); $i < $size; ++$i) {
            $databind = array_merge($databind, array_values($datas[$i]));
        }

        $begin = $this->getMicrotime();

        $this->prepare($sql);
        $this->_populateStmtValues($databind);
        $this->execute();

        $end = $this->getMicrotime();

        if (Flywheel_Config::get('debug')) {
            $this->log($sql, ($end - $begin));
        }
        return $this->lastInsertId();
    }

    /**
     * Update
     *
     * @param string $table Db Table Name
     * @param string $data Du lieu update voi key la ten truong va value la gia tri can update
     * @param string $where SQL query WHERE clause
     * 
     * @return affected rows
     * @throws SystemException
     */
    public function update($table, $data, $where) {
        $sql = $this->_buildUpdateSql($table, $data, $where);
        $begin = $this->getMicrotime();
        $this->prepare($sql);
        $this->_populateStmtValues(array_values($data));
        $this->execute();
        $end = $this->getMicrotime();

        if (Flywheel_Config::get('debug')) {
            $this->log($sql, ($end - $begin));
        }

        return $this->affectedRows();
    }

    /**
     * Build Update Sql
     * Ham lam nhiem vu gen ra cau lenh sql thuc hien viec update
     *
     * @param array $data, mang cac column va gia tri can thay doi
     * @param string $table, ten table
     * @param string $whereClause, mang dieu kieu
     * @return string sql
     */
    private function _buildUpdateSql($table, $data, $whereClause) {
        $sql = 'UPDATE ' . $this->nameQuote($table) . ' SET ';
        $modifiedColumns = array_keys($data);

        for ($i = 1, $size = sizeof($modifiedColumns); $i <= $size; $i++) {
            $sql .= $this->nameQuote($modifiedColumns[$i - 1]) . '= :p' . $i;

            if ($i !== $size) {
                $sql .= ', ';
            }
        }

        $sql .= ' WHERE ' . $whereClause;
        return $sql;
    }

    /**
     * Populate Stmt Values
     *
     * @param PDOStatement $stmt
     * @param array $data	 
     */
    private function _populateStmtValues($data) {
        for ($i = 0, $size = sizeof($data); $i < $size; ++$i) {
            if ($data[$i] === null) {
                $this->stmt->bindValue('p:' . ($i + 1), 'NULL', PDO::PARAM_NULL);
            } else {
                $this->stmt->bindValue(':p' . ($i + 1), $data[$i]);
            }
        }
    }

    /**
     * Delete
     *
     * @param string $table Db Table Name	 
     * @param string $where SQL query WHERE clause
     * @param string $limit SQL query LIMIT
     * 
     * @return affected rows
     * @throws SystemException
     */
    public function delete($table, $where, $limit = null) {
        $sql = 'DELETE FROM ' . $this->nameQuote($table) . ' WHERE ' . $where
                . (($limit !== null) ? ' LIMIT ' . $limit : '');
        $this->query($sql);
        return $this->affectedRows();
    }

    /**
     * Delete By Id
     * delete record by primary key
     *
     * @param string $table
     * @param int $id
     * @return int affectd rows
     * 
     * @throws SystemException
     */
    public function deleteById($table, $id) {
        return $this->delete($table, 'id=' . $id);
    }

    /**
     * Count number of records by key
     *
     * @param string $table DBTable Name
     * @param string $where SQL Query Where Clause
     * @param string $key column name to count
     * 
     * @return int result
     */
    public function count($table, $where = null, $key = 'id') {
        $key = $this->nameQuote($key);
        $table = $this->nameQuote($table);

        $sql = 'SELECT COUNT(' . $key . ') AS rows FROM ' . $table
                . (($where != null) ? ' WHERE ' . $where : '');
        $this->query($sql);
        $result = $this->fetch();
        return (int) $result['rows'];
    }

    /**
     * Begin Transaction
     * 
     * @return boolean
     */
    public function beginTransaction() {
        if (!$this->_transaction)
            return false;

        $this->pdo->beginTransaction();
        return true;
    }

    /**
     * Commit
     * 
     * @return boolean
     *
     */
    public function commit() {
        if (!$this->_transaction)
            return false;

        $this->pdo->commit();
        return true;
    }

    /**
     * Roll Back
     * 
     * @return boolean
     *
     */
    public function rollBack() {
        if (!$this->_transaction)
            return false;

        $this->pdo->rollBack();
        return true;
    }

    /**
     * Close
     * close connection
     *
     * @return boolean
     */
    public function close() {
        $this->pdo = null;

        return is_null($this->pdo);
    }

    /**
     * Destructor
     * close connection
     *
     */
    public function __destruct() {
        $this->pdo = null;
    }

    /**
     * selectPrepare 
     * Author:		longcaohoang@vccorp.vn
     * Date:		07-08-2010 
     * Description:	Execute select query string with prepare statement
     * 
     * @param statementsql 	ex: "select * from comments where id = ? and user_id = ?"
     * @param values 		ex: array(15, 48)
     * @param key 			ex: "comments"
     * @return ArrayObject 	ex: array(0, array(id => 15, user_id => 48))
     */
    public function selectPrepare($statementSql, $values, $key = '') {
        $this->prepare($statementSql);
        $i = 0;
        foreach ($values as $value) {
            $i++;
            $this->bindParam($i, $value);
            //$parameter = $value;
        }
        $this->execute();
        return $this->fetchAll($key);
    }

    /**
     * selectPrepareType
     * Author:		longcaohoang@vccorp.vn
     * Date:		07-08-2010 
     * Description:	Execute select query string with prepare statement
     * 
     * @param statementSql 	ex: "select * from comments where id = ? and user_id = ?"
     * @param values 		ex: array(
     * 								array('value'=>15, 'type'=>PDO::PARAM_INT), 
     * 								array('value'=>48, 'type'=>PDO::PARAM_INT),
     * 								)
     * @param key 			ex: "comments"
     * @return ArrayObject	ex: array(0, array(id => 15, user_id => 48))
     */
    public function selectPrepareType($statementSql, $values, $key = '') {
        $this->prepare($statementSql);
        $i = 0;
        foreach ($values as $value) {
            $i++;
            $this->bindParam($i, $value['value'], $value['type']);
            //$parameter = $value['value'];
        }
        $this->execute();
        return $this->fetchAll($key);
    }

    /**
     * selectOnePrepare 
     * Author:		longcaohoang@vccorp.vn
     * Date:		07-08-2010 
     * Description:	Execute select query string with prepare statement
     * 
     * @param statementsql 	ex: "select * from comments where id = ? and user_id = ?"
     * @param values 		ex: array(15, 48)
     * @param key 			ex: "comments"
     * @return ArrayObject 	ex: array(0, array(id => 15, user_id => 48))
     */
    public function selectOnePrepare($statementSql, $values, $key = '') {
        $this->prepare($statementSql);
        $i = 0;
        foreach ($values as $value) {
            $i++;
            $this->bindParam($i, $value);
            //$parameter = $value;
        }
        $this->execute();
        return $this->fetch($key);
    }

    /**
     * selectOnePrepareType
     * Author:		longcaohoang@vccorp.vn
     * Date:		07-08-2010 
     * Description:	Execute select query string with prepare statement
     * 
     * @param statementSql 	ex: "select * from comments where id = ? and user_id = ?"
     * @param values 		ex: array(
     * 								array('value'=>15, 'type'=>PDO::PARAM_INT), 
     * 								array('value'=>48, 'type'=>PDO::PARAM_INT),
     * 								)
     * @param key 			ex: "comments"
     * @return ArrayObject	ex: array(0, array(id => 15, user_id => 48))
     */
    public function selectOnePrepareType($statementSql, $values, $key = '') {
        $this->prepare($statementSql);
        $i = 0;
        foreach ($values as $value) {
            $i++;
            $this->bindParam($i, $value['value'], $value['type']);
            //$parameter = $value['value'];
        }
        $this->execute();
        return $this->fetch($key);
    }

}