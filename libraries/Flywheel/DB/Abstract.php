<?php
/**
 * Flywheel DB Abstract
 * 
 * @abstract
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Abstract.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Flywheel/DB
 *
 */
class Flywheel_DB_Abstract {
	const TYPE_MYSQL = 1;
	const TYPE_MYSQLI = 2;
	const TYPE_PDO = 3;
	
	protected $_log = array();
	
	/**
	 * Data Access object Id
	 * @var string
	 */
	protected $_id;
	
	/**
	 * Use Transaction
	 *
	 * @var boolean
	 */
	private $_transaction;
	
	protected function __construct($config) {
		$this->_transaction = isset($config['transaction'])? (boolean) $config['transaction']: false;		
	}
	
	public function getId() {
		return $this->_id;		
	}
	
	/**
	 * Log
	 *
	 * @param string $sql
	 * @param float $time
	 */
	public function log($sql, $time) {
		$this->_log[] = array(
				'sql' => $sql,
				'time'=> $time
			);
	}
	
	/**
	 * Get Log query
	 *
	 * @return Array
	 */
	public function getLog() {
		return $this->_log;		
	}
	
	/**
	 * Get Microtime
	 *
	 * @return float
	 */
	public function getMicrotime() {
		list( $usec, $sec ) = explode(' ', microtime());
		return ((float)$usec + (float)$sec);
	}
}