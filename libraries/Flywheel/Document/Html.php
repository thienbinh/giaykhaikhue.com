<?php

/**
 * Flywheel document html
 * object manager html element and data
 * 		element:	header element, javascript, css stylesheet
 * 		data: module (block) and component data
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Html.php 843 2010-12-23 07:04:04Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Document
 *
 */
define('JQUERY_OPEN', "\njQuery(document).ready(function () {\n");
define('JQUERY_CLOSE', "\n});\n");
defined('WEBSITE_NAME') or define('WEBSITE_NAME', '');

class Flywheel_Document_Html extends Flywheel_Document_Abstract {

    public $title = "";
    public $web_name = WEBSITE_NAME;
    public $count_visitor = 0;
    public $descriptions;
    public $keywork;
    public $meta;
    public $jsBaseUrl;
    public $cssBaseUrl;
    private $_baseUrl;
    private $_domain;
    private $_publicPath;
    private $_lang = 'vi-VN';
    private $_stylesheet = array();
    private $_cssText = '';
    private $_javascript = array();
    private $_jsText = array();
    public $header = array();
    private $_buffer;
    private $_blocks = array();

    public function __construct() {
        $router = Flywheel_Factory::getRouter();
        $this->_baseUrl = $router->getBaseUrl();
        $this->_domain = $router->getDomain();
        $this->_publicPath = $router->getFrontControllerPath();

        // Load config system
        $system_config = new System_Config();
        $_config_data = $system_config->getAllConfig();
        
        if(is_array($_config_data)) {
            // Name
            if (array_key_exists('name', $_config_data)) {
                $this->web_name = $_config_data['name']->value;
            }
            // Title
            if (array_key_exists('title', $_config_data)) {
                $this->title = $_config_data['title']->value;
            }
            // Description
            if (array_key_exists('descriptions', $_config_data)) {
                $this->descriptions = $_config_data['descriptions']->value;
            }
            // Keyword
            if (array_key_exists('keyword', $_config_data)) {
                $this->keywork = $_config_data['keyword']->value;
            }
            // Counter visitor
            if (array_key_exists('count_visitor', $_config_data)) {
                $this->count_visitor = $_config_data['count_visitor']->value;
            }
        }
        /* End loading system cache data */
    }

    /**
     * Get Base Url
     *
     * @return string
     */
    public function getBaseUrl() {
        return $this->_baseUrl;
    }

    /**
     * Get domain
     * 
     * @return string
     */
    public function getDomain() {
        return $this->_domain;
    }

    /**
     * Get public path (included domain) of script file
     * 
     * @return tring
     */
    public function getPublicPath() {
        return $this->_publicPath;
    }

    /**
     * Set buffer
     * 	Set document include data
     *
     * @param string $content	data
     * @param string $type		component|module
     * @param string $name
     * 
     * @return void
     */
    public function setBuffer($content, $type, $name = null) {
        if ($name != null) {
            $this->_buffer[$type][$name] = $content;
        } else {
            $this->_buffer[$type] = $content;
        }
    }

    /**
     * Get Buffer
     * 	get document include data
     *
     * @return array
     */
    public function getBuffer() {
        return $this->_buffer;
    }

    /**
     * set module to block
     * 
     * @param string	$pos
     * @param int		$ordering
     * @param string	$file
     * @param string	$name
     * @param string	$config
     */
    public function setBlock($pos, $ordering = 0, $file, $name, $config = array()) {
        if (!isset($this->_blocks[$pos])) {
            $this->_blocks[$pos] = array();
        }
        $this->_blocks[$pos][] = array('file' => $file, 'name' => $name, 'config' => $config);
    }

    /**
     * render block by position
     * @param string $position
     * 
     * @return html string
     */
    public function block($position) {
        if (!isset($this->_blocks[$position])) {
            return null;
        }
        $html = '';
        for ($i = 0, $size = sizeof($this->_blocks[$position]); $i < $size; ++$i) {
            $module = Flywheel_Factory::getModule($this->_blocks[$position][$i]['name'], $this->_blocks[$position][$i]['file'], $this->_blocks[$position][$i]['config']);
            if (false !== $module) {
                $html .= $module->render();
            }
        }

        return $html;
    }

    /**
     * count modules of postions
     * 
     * @param string	$condition
     * 
     * @return integer
     */
    public function countModules($condition) {
        $result = '';
        $words = explode(' ', $condition);
        for ($i = 0; $i < count($words); $i+=2) {
            $postion = strtolower($words[$i]);
            $words[$i] = (isset($this->_blocks[$postion]) || !is_array($this->_blocks[$postion])) ? 0 : sizeof($this->_blocks[$postion]);
        }

        $str = 'return ' . implode(' ', $words) . ';';
        return eval($str);
    }

    public function getComponentData() {
        return $this->_buffer['component'];
    }

    /**
     * Add Stylesheet
     *
     * @param string $file. Link to file
     * @param array $media. Stylesheet Media style
     */
    public function addCss($file, $media = array()) {
        $this->_stylesheet[$file] = $media;
    }

    /**
     * Add javascript file
     * 
     * @param string	$file
     * @param string	$position the position of js file, support TOP|BOTTOM 
     * @param array		$option
     */
    public function addJs($file, $position = 'BOTTOM', $option = array()) {
        $position = strtoupper($position);
        $this->_javascript[$position][$file] = $option;
    }

    /**
     * add Js code
     * @param string	$code js code
     * @param string	$position TOP|BOTTOm
     * @param string	$lib, js framework like 'jQuery', 'MooTool', 'Dojo' etc
     */
    public function addJsCode($code, $position = 'BOTTOM', $lib = 'jQuery') {
        $position = strtoupper($position);
        $this->_jsText[$position][$lib][] = $code;
    }

    /**
     * render css link
     * 
     * @return html string
     */
    public function css() {
        $css = '';
        $combine_js_css = Flywheel_Config::get('app_setting/combine_js_css');

        if (isset($combine_js_css) && intval($combine_js_css) > 0) {
            $file_list = array();
            $cache_path = PUBLIC_DIR . 'frontend/styles/';

            foreach ($this->_stylesheet as $file => $media) {
                $file_list[] = $cache_path . $file;
            }
            require_once LIBRARIES_DIR . 'MinifyStaticFile' . DS . 'combine.php';
            if (isset($_GET['change_static']) && intval($_GET['change_static']) == 1) {
                //xÃƒÂ³a cache vÃƒÂ  tÃ¡ÂºÂ¡o cache mÃ¡Â»â€ºi
                $CombineJsCss = new CombineJsCss('css', $file_list, $cache_path, true);
            } else {
                //lÃ¡ÂºÂ¥y cache cÃ…Â©
                $CombineJsCss = new CombineJsCss('css', $file_list, $cache_path, false);
            }

            $file = $CombineJsCss->getFileName();
            $time = $CombineJsCss->getFileTime();
            $css .= '<link rel="stylesheet" type="text/css" href="'
                    . $this->cssBaseUrl . 'css_cache/' . $file . '?time=' . $time . '"'
                    . '>' . "\n";
        } else {
            foreach ($this->_stylesheet as $file => $media) {
                $css .= '<link rel="stylesheet" type="text/css" href="'
                        . $this->cssBaseUrl . $file . '"'
                        . ((is_array($media)) ? ' media="' . implode(', ', $media) . '"' : '')
                        . '>' . "\n";
            }
        }

        return $css;
    }

    public function js($pos = 'BOTTOM') {
        $pos = strtoupper($pos);
        $jsCode = '';
        if (isset($this->_jsText[$pos])) {
            foreach ($this->_jsText[$pos] as $lib => $code) {
                if ('standard' == $lib) {
                    $jsCode .= implode("\n", $code) . "\n";
                } else {
                    $open = constant(strtoupper($lib) . '_OPEN');
                    $close = constant(strtoupper($lib) . '_CLOSE');
                    $jsCode .= $open . implode("\n", $code) . $close;
                }
            }
            $jsCode = "<script type=\"text/javascript\">\n" . $jsCode . "\n</script>";
        }
        $js = '';

        $combine_js_css = Flywheel_Config::get('app_setting/combine_js_css');

        if (isset($combine_js_css) && intval($combine_js_css) > 0) {
            $file_list = array();
            $cache_path = PUBLIC_DIR . 'js/';

            if (isset($this->_javascript[$pos])) {

                require_once LIBRARIES_DIR . 'MinifyStaticFile' . DS . 'combine.php';
                foreach ($this->_javascript[$pos] as $file => $option) {
                    $file_list[] = $cache_path . $file;
                }

                if (isset($_GET['change_static']) && intval($_GET['change_static']) == 1) {
                    //xÃƒÂ³a cache vÃƒÂ  tÃ¡ÂºÂ¡o cache mÃ¡Â»â€ºi
                    $CombineJsCss = new CombineJsCss('javascript', $file_list, $cache_path, true);
                } else {
                    //lÃ¡ÂºÂ¥y cache cÃ…Â©
                    $CombineJsCss = new CombineJsCss('javascript', $file_list, $cache_path, false);
                }

                $file = $CombineJsCss->getFileName();
                $time = $CombineJsCss->getFileTime();
                $js .= '<script type="text/javascript" src="'
                        . $this->jsBaseUrl . 'js_cache/' . $file . '?time=' . $time . '"></script>';
            }
        } else {
            if (isset($this->_javascript[$pos])) {
                foreach ($this->_javascript[$pos] as $file => $option) {
                    $js .= '<script type="text/javascript" src="'
                            . $this->jsBaseUrl . $file . '"></script>';
                }
            }
        }
        if ('TOP' == $pos) {
            $js .= $jsCode;
        } else {
            $js = $jsCode . $js;
        }

        return $js . "\n";
    }

}