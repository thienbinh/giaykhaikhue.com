<?php
/**
 * Flywheel Model
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Model.php 1118 2011-03-03 22:12:41Z mylifeisskidrow $
 * @package		Flywheel
 * @subpackage	Model
 *
 */
abstract class Flywheel_Model extends stdClass {
	/**
	 * status of new object. If object not store in database, this value is true
	 * @var $_new boolean
	 */
	private $_new = true;
	
	/**
	 * modified columns list
	 * 
	 * @access protected
	 * @var $_modifiedColumns array
	 */
	protected $_modifiedColumns = array();

	public function __construct($data = null) {
		if (is_array($data) || is_object($data)) {
			$this->_hydrate($data);
		}
		/*if (isset($data['id']) && ($data['id'] > 0)) {
			$this->setNew(false);
		}*/
	}
	/**
	 * is object did not store in database
	 * 
	 * @return boolean
	 */
	public function isNew() {
		return $this->_new;
	}
	
	/**
	 * Set New
	 * @param boolean $new
	 */
	public function setNew($isNew) {
		$this->_new = (boolean) $isNew;		
	}

	/**
	 * To Array
	 * 
	 * @return array
	 */
	final public function toArray($raw = false) {
		$data = get_object_vars($this);
		if (false === $raw) {
			foreach ($data as $property => $value) {
				if (strpos($property, '_') === 0) {
					unset($data[$property]);
				}
			}			
		}
		
		unset($data['_modifiedColumns']);	
		return $data;	
	}
	
	/**
	 * to array with sql name like `id`
	 * @param Array		$fields. table's fileds name
	 * @param Array		$columns
	 * @return Array
	 */
	final public function toArraySqlName($fields, $columns) {	    
		$data = array();
		for($i = 0; $i < sizeof($fields); ++$i) {
			$data[$fields[$i]] = $this->$columns[$i];			
		}
		
		return $data;
	}
	
	/**
	 * To Json
	 * 
	 * @return string JSon encode
	 */
	final public function toJSON() {
		return json_encode($this);		
	}
	
	/**
	 * hydrate object|array to this object's property
	 * 
	 * @param $data
	 */
        
	protected function _hydrate($data) {
		if (is_object($data)) {
			$data = get_object_vars($data);
		}
		if($data){
            foreach ($data as $property=>$value) {
                $a = trim($property);
                $this->$a = $value;
            }
        }
    }
    
    /**
     * is column has changed value
     * 
     * @param string	$column column name 
     * @return boolean
     */
    final public function isModifiedColumns($column) {
    	return (isset($this->_modifiedColumns[$column]) 
    				&& (true === $this->_modifiedColumns[$column]));    	
    }
    
    /**
     * check had modified columns
     * 	
     * @return boolean
     */
    final public function hasModifiedColumns() {
    	return (boolean) count($this->_modifiedColumns);   	
    }
    
    /**
     * get modified columns     
     * @return array;
     */
    final public function getModifiedColumns() {
    	return $this->_modifiedColumns;
    }
    
    public function __call($method, $params) {	
    	if (strpos($method, 'set') === 0) {
//    		if (!isset($params[0])) {
//                throw new Flywheel_DB_Exception('You must specify the value to ' . $method);
//            }
            if (isset($params[0]) && null !== $params[0]) {
	            $name = substr($method, 3, strlen($method));
	    		$name = Flywheel_Inflector::camelCaseToHungary($name);   		    		
	    		if (false === $this->_new) {
	    			if (is_array($this->$name) && is_array($params[0])) {
	    				if (serialize($this->$name) != serialize($params[0])) {
	    					$this->$name = $params[0];
	    					$this->_modifiedColumns[$name] = true;
	    				}  				
	    			} else if ($this->$name != $params[0]) {	    				
	    				$this->$name = $params[0];
	    				$this->_modifiedColumns[$name] = true;    				
	    			}				
	    		} else {
	    			$this->$name = $params[0];    			
	    		}            	
            }    		 		  		
    	}    	
    }
}