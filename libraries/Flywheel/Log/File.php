<?php
require_once FLYWHEEL_UTIL .'FolderUtil.php';
/**
 * Ming Log File
 * 	write log file action
 * 
 * @author		Volunteer_developer <hieuluutrong@vccorp.vn>
 * @version		$Id: File.php 629 2010-11-30 04:39:18Z mylifeisskidrow@gmail.com $
 * @package		Ming
 * @subpackage	Log
 */
class Flywheel_Log_File extends Flywheel_Log_Abstract {
	protected $_options = array(
		'format' 		=>  "%time%\t%message%%EOL%",
		'extension'     =>  '.log',
		'mode'			=>  'a',
	);	
	/**
	 * file resource hander
	 * 
	 * @var resource
	 */
	protected $_fp;
	
	/**
	 * Construct new Ming_Log_File
	 * 
	 * @param array $options
	 * 				'file' : file name to log
	 */
	public function __construct(array $options) {
		if (!isset($options['file'])) {	
			throw new Flywheel_Exception('You must provide a "file" parameter for this logger.');					
		}
		$this->_options = array_merge($this->_options, $options);
       
		$this->_options['file'] = $options['file'] .$this->_options['extension'];
		
		$_path = explode(DS, $this->_options['file']);
		$file = array_pop($_path);		
		$logDir = implode(DS, $_path);
				
		folder_create($logDir, 0777);		
		$fileExists = file_exists($this->_options['file']);
		if (false === is_writable($logDir) 
			|| ($fileExists && (false === is_writable($this->_options['file'])))) {
			throw new Flywheel_Exception(sprintf('Unable to open the log file "%s" for writing.'
												, $this->_options['file']));
		}
		@chmod($this->_options['file'], 0777);		
		register_shutdown_function(array($this, 'shutdown'));
	}
	
	/**
	 * open file log
	 */
	private function _open() {
		$this->_fp = fopen($this->_options['file'], $this->_options['mode']);				
	}
	
	/**
	 * log message
	 * @param string $message
	 * @param arrays $options
	 * 				'using_format': using format template for message
	 */
	public function log($message, $options = array()) {
		if (null == $this->_fp) {
			$this->_open();			
		}
		
		flock($this->_fp, LOCK_EX);
		if (!isset($options['using_format'])) {
			$options['using_format'] = true;
		}
		if ($options['using_format'] == true) {
			fwrite($this->_fp, strtr($this->_options['format'], array(
				'%time%'	=> date('Y-m-d H:i:s'),			
				'%message%'	=> $message,
				'%EOL%'		=> PHP_EOL
			)));
		} else {
			fwrite($this->_fp, $message.PHP_EOL);			
		}
		
		flock($this->_fp, LOCK_UN);		
	}

    /**
     * get log file
     * @return string the full path of log file
     */
    public function getLogFile() {
        return $this->_options['file'];
    }
	
	/**
	 * view log file
	 * 
	 * @return string | false if file has not been created
	 */
	public function viewLog($parseToArray = false) {        
		if (file_exists($this->_options['file'])) {            
			if (false == $parseToArray) {
				return file_get_contents($this->_options['file']);
			} else {                
				$buffer = array();
				$fp = fopen($this->_options['file'], 'r');                
				if (false === $fp) {
					return false;
				}
				while (!feof($fp)) {
					$_b = trim(fgets($fp));
					if (null != $_b && false != $_b) {
						$buffer[] = $_b;						
					}
				}
				fclose($fp);
				if (count($buffer) == 0) {
					return false;
				}
                return $buffer;
            }
		}
		return false;	
	}

    /**
     * set mode op file log hander
     * @param string $mode
     * @return boolean
     */
    public function setMode($mode) {
        if ($mode != $this->_options['mode']) {
            $this->_options['mode'] = $mode;
            if (null != $this->_fp) {
            	$this->_open();            
            }
            return true;
        }
        return false;
    }
	
	public function shutdown() {
		if (is_resource($this->_fp)) {
			fclose($this->_fp);
		}
	}
}