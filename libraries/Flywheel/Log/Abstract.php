<?php
/**
 * Flywheel Log
 * 	the abstract base class of logs class
 * 
 * @abstract
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Abstract.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Log
 */
abstract class Flywheel_Log_Abstract {
	/**
	 * log a message
	 * 
	 * @param string	$message
	 * @param array		$options
	 */
	abstract public function log($message, $options = array());
}