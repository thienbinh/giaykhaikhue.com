<?php
/**
 * Flywheel Inflector using inflecting text
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Inflector.php 398 2010-11-13 21:19:09Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Inflector
 */

class Flywheel_Inflector {
	/**
	 * Convert word from Camel Case to Hungary Notation. "ModelName" to "model_name"
	 * @param string $word camel case word
	 * @return string word by Hungary Notation
	 */
	public static function camelCaseToHungary($word) {
		return strtolower(preg_replace('~(?<=\\w)([A-Z])~', '_$1', $word));		
	}
	
	/**
	 * get method name from alias. make "alias_name" to "aliasName"
	 * @param string $word
	 * 
	 * @return string
	 */
	public static function getMethodNameFromAlias($word) {
		return ($word[0] . self::hungaryNotationToCamel(substr($word, 1, strlen($word))));		
	}
	
	/**
     * @desc Convert a word from Hungary Notation in to Camle Case format. Converts 'object_name' to 'ObjectName'
     *
     * @param string  $word  Hungary Notation word
     * @return string $word  by Camel Case
     */
	public static function hungaryNotationToCamel($word) {		
		$word = preg_replace('/[$]/', '', $word);
        return preg_replace_callback('~(_?)(_)([\w])~', array('Flywheel_Inflector', 'classifyCallback'), ucfirst(strtolower($word)));
	}
	
	/**
     * Callback function to classify a classname properly.
     *
     * @param  array  $matches  An array of matches from a pcre_replace call
     * @return string $string   A string with matches 1 and mathces 3 in upper case.
     */
    public static function classifyCallback($matches) {
        return $matches[1] . strtoupper($matches[3]);
    }
    
    /**
     * make slug string for friendly url, remove whitespace. 
     *  convert string 'nhÃ¡Â»Â¯ng viÃƒÂªn kÃ¡ÂºÂ¹o' to 'nhung-vien-keo'
     * @param string	$string
     * @param boolean	$lowerCase.
     * @return string
     */
    public static function slug($string, $lowerCase = false) {
    	require_once FLYWHEEL_UTIL .'SanitizeUtil.php';
    	$string = self::vi2en($string);    	
    	$string = sanitize_strip_whitespace($string);
    	$string = str_replace(' ','-', $string);
    	//remove non alpha numeric character, allow "-","_" and " "
    	$string = sanitize_paranoid($string, array('-', '_'));
    	//remove double "--"
    	$string = preg_replace('|-+|', '-', $string);
    	$string = trim($string, '-');

    	if (true === $lowerCase) {
    		$string = strtolower($string);
    	}
        
    	return $string;
    }
    
    /**
     * inflecting vietnamese character to english charater.
     * convert "ÃƒÂ¡, Ã†Â° , Ã†Â¡, Ã¡ÂºÂ¿, Ã¡Â»â€¹, ÃƒÂ½..." to "a, u, o, e, i , y ..."
     *  
     * @param string $str
     * 
     * @return string
     */
    public static function vi2en($str) {    
		$arrTViet=array("ÃƒÂ ","ÃƒÂ¡","Ã¡ÂºÂ¡","Ã¡ÂºÂ£","ÃƒÂ£","ÃƒÂ¢","Ã¡ÂºÂ§","Ã¡ÂºÂ¥","Ã¡ÂºÂ­","Ã¡ÂºÂ©","Ã¡ÂºÂ«","Ã„Æ’",
		"Ã¡ÂºÂ±","Ã¡ÂºÂ¯","Ã¡ÂºÂ·","Ã¡ÂºÂ³","Ã¡ÂºÂµ","ÃƒÂ¨","ÃƒÂ©","Ã¡ÂºÂ¹","Ã¡ÂºÂ»","Ã¡ÂºÂ½","ÃƒÂª","Ã¡Â»ï¿½"
		,"Ã¡ÂºÂ¿","Ã¡Â»â€¡","Ã¡Â»Æ’","Ã¡Â»â€¦",
		"ÃƒÂ¬","ÃƒÂ­","Ã¡Â»â€¹","Ã¡Â»â€°","Ã„Â©",
		"ÃƒÂ²","ÃƒÂ³","Ã¡Â»ï¿½","Ã¡Â»ï¿½","ÃƒÂµ","ÃƒÂ´","Ã¡Â»â€œ","Ã¡Â»â€˜","Ã¡Â»â„¢","Ã¡Â»â€¢","Ã¡Â»â€”","Ã†Â¡"
		,"Ã¡Â»ï¿½","Ã¡Â»â€º","Ã¡Â»Â£","Ã¡Â»Å¸","Ã¡Â»Â¡",
		"ÃƒÂ¹","ÃƒÂº","Ã¡Â»Â¥","Ã¡Â»Â§","Ã…Â©","Ã†Â°","Ã¡Â»Â«","Ã¡Â»Â©","Ã¡Â»Â±","Ã¡Â»Â­","Ã¡Â»Â¯",
		"Ã¡Â»Â³","ÃƒÂ½","Ã¡Â»Âµ","Ã¡Â»Â·","Ã¡Â»Â¹",
		"Ã„â€˜",
		"Ãƒâ‚¬","Ãƒï¿½","Ã¡ÂºÂ ","Ã¡ÂºÂ¢","ÃƒÆ’","Ãƒâ€š","Ã¡ÂºÂ¦","Ã¡ÂºÂ¤","Ã¡ÂºÂ¬","Ã¡ÂºÂ¨","Ã¡ÂºÂª","Ã„â€š"
		,"Ã¡ÂºÂ°","Ã¡ÂºÂ®","Ã¡ÂºÂ¶","Ã¡ÂºÂ²","Ã¡ÂºÂ´",
		"ÃƒË†","Ãƒâ€°","Ã¡ÂºÂ¸","Ã¡ÂºÂº","Ã¡ÂºÂ¼","ÃƒÅ ","Ã¡Â»â‚¬","Ã¡ÂºÂ¾","Ã¡Â»â€ ","Ã¡Â»â€š","Ã¡Â»â€ž",
		"ÃƒÅ’","Ãƒï¿½","Ã¡Â»Å ","Ã¡Â»Ë†","Ã„Â¨",
		"Ãƒâ€™","Ãƒâ€œ","Ã¡Â»Å’","Ã¡Â»Å½","Ãƒâ€¢","Ãƒâ€�","Ã¡Â»â€™","Ã¡Â»ï¿½","Ã¡Â»Ëœ","Ã¡Â»â€�","Ã¡Â»â€“","Ã†Â "
		,"Ã¡Â»Å“","Ã¡Â»Å¡","Ã¡Â»Â¢","Ã¡Â»Å¾","Ã¡Â»Â ",
		"Ãƒâ„¢","ÃƒÅ¡","Ã¡Â»Â¤","Ã¡Â»Â¦","Ã…Â¨","Ã†Â¯","Ã¡Â»Âª","Ã¡Â»Â¨","Ã¡Â»Â°","Ã¡Â»Â¬","Ã¡Â»Â®",
		"Ã¡Â»Â²","Ãƒï¿½","Ã¡Â»Â´","Ã¡Â»Â¶","Ã¡Â»Â¸",
		"Ã„ï¿½");
	
		$arrKoDau=array("a","a","a","a","a","a","a","a","a","a","a"
		,"a","a","a","a","a","a",
		"e","e","e","e","e","e","e","e","e","e","e",
		"i","i","i","i","i",
		"o","o","o","o","o","o","o","o","o","o","o","o"
		,"o","o","o","o","o",
		"u","u","u","u","u","u","u","u","u","u","u",
		"y","y","y","y","y",
		"d",
		"A","A","A","A","A","A","A","A","A","A","A","A"
		,"A","A","A","A","A",
		"E","E","E","E","E","E","E","E","E","E","E",
		"I","I","I","I","I",
		"O","O","O","O","O","O","O","O","O","O","O","O"
		,"O","O","O","O","O",
		"U","U","U","U","U","U","U","U","U","U","U",
		"Y","Y","Y","Y","Y",
		"D");
	
		return str_replace($arrTViet, $arrKoDau, $str);  	
    }
    
    public static function replaceVietnameseCharacters($str){
        $str = preg_replace("/[\x{00C0}-\x{00C3}\x{00E0}-\x{00E3}\x{0102}\x{0103}\x{1EA0}-\x{1EB7}]/u", "a", $str);
        $str = preg_replace("/[\x{00C8}-\x{00CA}\x{00E8}-\x{00EA}\x{1EB8}-\x{1EC7}]/u", "e", $str);
        $str = preg_replace("/[\x{00CC}\x{00CD}\x{00EC}\x{00ED}\x{0128}\x{0129}\x{1EC8}-\x{1ECB}]/u", "i", $str);
        $str = preg_replace("/[\x{00D2}-\x{00D5}\x{00F2}-\x{00F5}\x{01A0}\x{01A1}\x{1ECC}-\x{1EE3}]/u", "o", $str);
        $str = preg_replace("/[\x{00D9}-\x{00DA}\x{00F9}-\x{00FA}\x{0168}\x{0169}\x{01AF}\x{01B0}\x{1EE4}-\x{1EF1}]/u", "u", $str);
        $str = preg_replace("/[\x{00DD}\x{00FD}\x{1EF2}-\x{1EF9}]/u", "y", $str);
        $str = preg_replace("/[\x{0110}\x{0111}]/u", "d", $str);

        return $str;
    }

    /*
	 * Remove or Replace special symbols with spaces
	 */
    public static function removeSpecialCharacters($str, $remove=true) {
        // Remove or replace with spaces
        $substitute = $remove ? "": " ";
        $str = preg_replace("/[\x{0021}-\x{002D}\x{002F}\x{003A}-\x{0040}\x{005B}-\x{0060}\x{007B}-\x{007E}\x{00A1}-\x{00BF}]/u", $substitute, $str);
        return $str;
    }

    /*
	 * Remove 5 Vietnamese accent / tone marks if has Combining Unicode characters
	 * Tone marks: Grave (`), Acute(´), Tilde (~), Hook Above (?), Dot Bellow(.)
	 */
    public static function removeAccent($str) {
        $str = preg_replace("/[\x{0300}\x{0301}\x{0303}\x{0309}\x{0323}]/u", "", $str);
        return $str;
    }

    public static function filter($input, $lower = true, $maxLength = null) {
        $input = self::removeAccent($input);
        $input = self::removeSpecialCharacters($input);
        $input = self::replaceVietnameseCharacters($input);

        $input = preg_replace('/(\s|[^A-Za-z0-9\-])+/', '-', trim($input));

        if (null !== $maxLength) {
            $input = substr($input, 0, $maxLength);
        }

        $input = trim($input, '-');

        if ($lower) {
            $input = strtolower($input);
        }

        if (empty($input)) {
            $input = null; // should we return null or an empty string?
        }

        return $input;
    }
    
}