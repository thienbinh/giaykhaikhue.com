<?php
/**
 * Flywheel file image
 * Image Manipulation class
 * 
 * @author		Volunteer_developer <tronhieu.luu@gmail.com>
 * @version		$Id$
 * @package		Flywheel
 * @subpackage	File
 */
class Flywheel_File_Image {
	/**
	 * The prior image (before manipulation)
	 * 
	 * @var resource
	 */	
	protected $_source;
	
	/**
	 * The working image (used during manipulation)
	 * 
	 * @var resource
	 */
	protected $_workingImage;
	
	/**
	 * The name of the file we're manipulating
	 * 
	 * This must include the path to the file (absolute paths recommended)
	 * 
	 * @var string
	 */
	protected $_fileName;
	
	/**
	 * The current dimensions of the image
	 * 
	 * @var array
	 */
	protected $_currentDimensions = array();
	
	protected $_mine;
	
	/**
	 * The options for this class
	 * 
	 * This array contains various options that determine the behavior in
	 * various functions throughout the class.  Functions note which specific 
	 * option key / values are used in their documentation
	 * 
	 * @var array
	 */
	protected $_options = array('resize_up'				=> false,
				'jpeg_quality'			=> 100,
				'correct_permissions'	=> false,
				'preserve_alpha'			=> true,
				'alpha_mask_color'		=> array (255, 255, 255),
				'preserve_transparency'	=> true,
				'transparency_mask_color'	=> array (0, 0, 0));
	
	/**
	 * The last error message raised
	 * 
	 * @var string
	 */
	protected $_errorMessage = array();
	
	/**
	 * Class Constructor
	 * 
	 * @param string 	$fileName
	 * @param array		$options
	 */
	public function __construct($fileName, $options = array(), $isDataStream = false) {		
		$this->_fileName	= $fileName;
		
		if (true === $isDataStream) {
			$this->_source = imagecreatefromstring($fileName);					
		} else {
			$formatInfo 	= getimagesize($fileName);
			switch ($formatInfo['mime']) {
				case 'image/gif':
					$this->_source = imagecreatefromgif($fileName);
					break;
				case 'image/jpeg':
					$this->_source = imagecreatefromjpeg($fileName);
					break;
				case 'image/png':
					$this->_source = imagecreatefrompng($fileName);
					break;
				default:
					$this->_errorMessage[] = 'KhÃ´ng há»— trá»£ Ä‘á»‹nh dáº¡ng: ' .$formatInfo['mime']; 
			}
			$this->_mime = $formatInfo['mime'];			
		}
		
		$this->_currentDimensions = array (
			'width' 	=> imagesx($this->_source),
			'height'	=> imagesy($this->_source)
		);
		
		$this->setOptions($options);
		
		// TODO: Port gatherImageMeta to a separate function that can be called to extract exif data
	}
	
	/**
	 * Class Destructor
	 * 
	 */
	public function __destruct () {
		if (is_resource($this->_source)) {
			imagedestroy($this->_source);
		}
		
		if (is_resource($this->_workingImage)) {
			imagedestroy($this->_workingImage);
		}
	}
	
	/**
	 * Sets $this->options to $options
	 * 
	 * @param array $options
	 */
	public function setOptions($options = array()) {
		if (is_array($options)) {
			$this->_options = array_merge($this->_options, $options);
		}		
	}
	
	/**
	 * has error
	 * @return boolean
	 */
	public function hasError() {
		return (boolean) sizeof($this->_errorMessage);		
	}
	
	/**
	 * get error messsage
	 * 
	 * @return array
	 */
	public function getErrorMessage() {
		return $this->_errorMessage;
	}
	
	/**
	 * Resizes an image to be no larger than $maxWidth or $maxHeight
	 * 
	 * If either param is set to zero, then that dimension will not be considered as a part of the resize.
	 * Additionally, if $this->_options['resize_up'] is set to true (false by default), then this function will
	 * also scale the image up to the maximum dimensions provided.
	 * 
	 * @param int $maxWidth The maximum width of the image in pixels
	 * @param int $maxHeight The maximum height of the image in pixels
	 * @return boolean	 
	 */
	public function resize($maxWidth, $maxHeight) {
		$maxWidth	= Flywheel_Filter::clean($maxWidth, Flywheel_Filter::TYPE_INT);
		$maxHeight	= Flywheel_Filter::clean($maxHeight, Flywheel_Filter::TYPE_INT);
		
		if ($maxWidth == 0 && $maxHeight == 0) {
			$this->_error[] = 'Ã�t nháº¥t chiá»�u rá»™ng hoáº·c chiá»�u dÃ i > 0.';
			return false;
		}

		if (false === $this->_options['resize_up']) {
			$maxWidth	= ($maxWidth > $this->_currentDimensions['width'])? 
						$this->_currentDimensions['width']: $maxWidth;
			$maxHeight	= ($maxHeight > $this->_currentDimensions['height'])?
						$this->_currentDimensions['height'] : $maxHeight; 
		}
		
		$dimensions = $this->_calcImageSize($this->_currentDimensions['width'], $this->_currentDimensions['height'], $maxWidth, $maxHeight);
		// create the working image
		if (function_exists('imagecreatetruecolor')) {
			$this->_workingImage = imagecreatetruecolor($dimensions['newWidth'], $dimensions['newHeight']);
		}
		else {
			$this->_workingImage = imagecreate($dimensions['newWidth'], $dimensions['newHeight']);
		}
		
		$this->_preserveAlpha();
		
		// and create the newly sized image
		imagecopyresampled(
			$this->_workingImage,
			$this->_source,
			0, 0, 0, 0,
			$dimensions['newWidth'],
			$dimensions['newHeight'],
			$this->_currentDimensions['width'],
			$this->_currentDimensions['height']
		);
		
		$this->_source						= $this->_workingImage;
		$this->_currentDimensions['width'] 	= $dimensions['newWidth'];
		$this->_currentDimensions['height'] = $dimensions['newHeight'];
		return true;
	}
	
	/**
	 * Adaptively Resizes the Image
	 * 
	 * This function attempts to get the image to as close to the provided dimensions as possible, and then crops the 
	 * remaining overflow (from the center) to get the image to be the size specified
	 * 
	 * @param int $maxWidth
	 * @param int $maxHeight
	 * @return boolean
	 */
	public function adaptiveResize ($maxWidth, $maxHeight) {
		$maxWidth	= Flywheel_Filter::clean($maxWidth, Flywheel_Filter::TYPE_INT);
		$maxHeight	= Flywheel_Filter::clean($maxHeight, Flywheel_Filter::TYPE_INT);
		if ($maxWidth == 0 && $maxHeight == 0) {
			$this->_error[] = 'Ã�t nháº¥t chiá»�u rá»™ng hoáº·c chiá»�u dÃ i > 0.';
			return false;
		}
		
		if (false === $this->_options['resize_up']) {
			$maxWidth	= ($maxWidth > $this->_currentDimensions['width'])? 
						$this->_currentDimensions['width']: $maxWidth;
			$maxHeight	= ($maxHeight > $this->_currentDimensions['height'])?
						$this->_currentDimensions['height'] : $maxHeight; 
		}

		$dimensions = $this->_calcImageSizeStrict($this->_currentDimensions['width'], $this->_currentDimensions['height'], $maxWidth, $maxHeight);
		if (false === $this->resize($dimensions['newWidth'], $dimensions['newHeight'])) {
			return false;
		}
		
		$cropX 		= 0;
		$cropY 		= 0;
		
		// now, figure out how to crop the rest of the image...
		if ($this->_currentDimensions['width'] > $maxWidth) {
			$cropX = ceil(($this->_currentDimensions['width'] - $maxWidth) / 2);
		}
		elseif ($this->_currentDimensions['height'] > $maxHeight) {
			$cropY = ceil(($this->_currentDimensions['height'] - $maxHeight) / 2);
		}
		
		return $this->crop($cropX, $cropY, $maxWidth, $maxHeight);
	}
	
	/**
	 * Vanilla Cropping - Crops from x,y with specified width and height
	 * 
	 * @param int $startX
	 * @param int $startY
	 * @param int $cropWidth
	 * @param int $cropHeight
	 */
	public function crop($startX, $startY, $cropWidth, $cropHeight) {		
		// do some calculations
		$cropWidth	= ($this->_currentDimensions['width'] < $cropWidth)? 
					$this->_currentDimensions['width'] : $cropWidth;
		$cropHeight = ($this->_currentDimensions['height'] < $cropHeight)? 
					$this->_currentDimensions['height'] : $cropHeight;
		
		// ensure everything's in bounds
		if (($startX + $cropWidth) > $this->_currentDimensions['width']) {
			$startX = ($this->_currentDimensions['width'] - $cropWidth);
			
		}
		
		if (($startY + $cropHeight) > $this->_currentDimensions['height']) {
			$startY = ($this->_currentDimensions['height'] - $cropHeight);
		}
		
		if ($startX < 0) {
			$startX = 0;
		}
		
	    if ($startY < 0) {
			$startY = 0;
		}
		
		// create the working image
		if (function_exists('imagecreatetruecolor')) {
			$this->_workingImage = imagecreatetruecolor($cropWidth, $cropHeight);
		}
		else {
			$this->_workingImage = imagecreate($cropWidth, $cropHeight);
		}
		
		$this->_preserveAlpha();
		
		imagecopyresampled (
			$this->_workingImage,
			$this->_source,
			0, 0, $startX, $startY,
			$cropWidth, $cropHeight, $cropWidth, $cropHeight
		);
		
		$this->_source 						= $this->_workingImage;
		$this->_currentDimensions['width'] 	= $cropWidth;
		$this->_currentDimensions['height'] = $cropHeight;
	}
	
	/**
	 * Calculates the new image dimensions
	 * 
	 * These calculations are based on both the provided dimensions and $this->maxWidth and $this->maxHeight
	 * 
	 * @param int $width
	 * @param int $height
	 * 
	 * @return array
	 */
	protected function _calcImageSize($width, $height, $maxWidth, $maxHeight) {
		$newSize = array (
			'newWidth'	=> $width,
			'newHeight'	=> $height);
		
		if ($maxWidth > 0) {
			$newSize = $this->_calcWidth($width, $height, $maxWidth);			
			if ($maxHeight > 0 && $newSize['newHeight'] > $maxHeight) {
				$newSize = $this->_calcHeight($newSize['newWidth'], $newSize['newHeight'], $maxHeight);
			}			
		}
		
		if ($maxHeight > 0) {
			$newSize = $this->_calcHeight($width, $height, $maxHeight);
			
			if ($maxWidth > 0 && $newSize['newWidth'] > $maxWidth) {
				$newSize = $this->_calcWidth($newSize['newWidth'], $newSize['newHeight'], $maxHeight);
			}
		}
		return $newSize;
	}
	
	/**
	 * Calculates new image dimensions, not allowing the width and height to be less than either the max width or height 
	 * 
	 * @param int $width
	 * @param int $height
	 */
	protected function _calcImageSizeStrict($width, $height, $maxWidth, $maxHeight) {
		// first, we need to determine what the longest resize dimension is..
		if ($maxWidth >= $maxHeight) {
			// and determine the longest original dimension
			if ($width > $height) {
				$newDimensions = $this->_calcHeight($width, $height, $maxHeight);
				
				if ($newDimensions['newWidth'] < $maxWidth) {
					$newDimensions = $this->_calcWidth($width, $height, $maxWidth);
				}
			}
			elseif ($height >= $width) {
				$newDimensions = $this->_calcWidth($width, $height, $maxWidth);
				
				if ($newDimensions['newHeight'] < $maxHeight)
				{
					$newDimensions = $this->_calcHeight($width, $height, $maxHeight);
				}
			}
		}
		elseif ($maxHeight > $maxWidth) {
			if ($width >= $height) {
				$newDimensions = $this->_calcWidth($width, $height, $maxWidth);				
				if ($newDimensions['newHeight'] < $maxHeight) {
					$newDimensions = $this->_calcHeight($width, $height, $maxHeight);
				}
			}
			elseif ($height > $width) {
				$newDimensions = $this->_calcHeight($width, $height, $maxHeight);
				
				if ($newDimensions['newWidth'] < $maxWidth) {
					$newDimensions = $this->_calcWidth($width, $height, $maxWidth);
				}
			}
		}
		
		return $newDimensions;
	}
	
	/**
	 * Calculates a new width and height for the image based on $this->maxWidth and the provided dimensions
	 * 
	 * @return array 
	 * @param int $width
	 * @param int $height
	 */
	protected function _calcWidth($width, $height, $maxWidth) {
		$newWidthPercentage	= (100 * $maxWidth) / $width;
		$newHeight			= ($height * $newWidthPercentage) / 100;
		
		return array ('newWidth'	=> ceil($maxWidth),
			'newHeight'	=> ceil($newHeight));
	}
	
	/**
	 * Calculates a new width and height for the image based on $this->maxWidth and the provided dimensions
	 * 
	 * @return array 
	 * @param int $width
	 * @param int $height
	 */
	protected function _calcHeight($width, $height, $maxHeight) {
		$newHeightPercentage	= (100 * $maxHeight) / $height;
		$newWidth 				= ($width * $newHeightPercentage) / 100;
		
		return array ('newWidth'	=> ceil($newWidth),
			'newHeight'	=> ceil($maxHeight)
		);
	}
	
	/**
	 * Preserves the alpha or transparency for PNG and GIF files
	 * 
	 * Alpha / transparency will not be preserved if the appropriate options are set to false.
	 * Also, the GIF transparency is pretty skunky (the results aren't awesome), but it works like a 
	 * champ... that's the nature of GIFs tho, so no huge surprise.
	 * 
	 * This functionality was originally suggested by commenter Aimi (no links / site provided) - Thanks! :)
	 *   
	 */
	protected function _preserveAlpha()
	{
		if ($this->_mine == 'image/png' && $this->_options['preserveAlpha'] === true) {
			imagealphablending($this->_workingImage, false);
			
			$colorTransparent = imagecolorallocatealpha (
				$this->_workingImage, 
				$this->_options['alpha_mask_color'][0], 
				$this->_options['alpha_mask_color'][1], 
				$this->_options['alpha_mask_color'][2], 
				0);
			
			imagefill($this->_workingImage, 0, 0, $colorTransparent);
			imagesavealpha($this->_workingImage, true);
		}
		// preserve transparency in GIFs... this is usually pretty rough tho
		if ($this->_mine == 'image/gif' && $this->_options['preserveTransparency'] === true) {
			$colorTransparent = imagecolorallocate (
				$this->_workingImage, 
				$this->_options['transparency_mask_color'][0], 
				$this->_options['transparency_mask_color'][1], 
				$this->_options['transparency_mask_color'][2]);
			
			imagecolortransparent($this->_workingImage, $colorTransparent);
			imagetruecolortopalette($this->_workingImage, true, 256);
		}
	}
	
	/**
	 * Shows an image
	 * 
	 * This function will show the current image by first sending the appropriate header
	 * for the format, and then outputting the image data. If headers have already been sent, 
	 * a runtime exception will be thrown 
	 */
	public function show() {
		if (headers_sent()) {
			throw new RuntimeException('Cannot show image, headers have already been sent');
		}
		
		if (null == $this->_mime) {
			$this->_mime = 'image/png';
		}

		header("Content-Disposition: filename={$this->_fileName};");
		header("Content-Type: {$this->_mine}");
		header('Content-Transfer-Encoding: binary');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
		switch($this->_mime) {
			case 'image/gif':
				imagegif($this->_source);
				break;
			case 'image/jpeg':
				imagejpeg($this->_source, null, $this->_options['jpeg_quality']);
				break;
			case 'image/png':
				imagepng($this->_source);
				break;
		}
	}
	
	/**
	 * save image to disk as $file name
	 * @param string $file
	 * @param string $mime export file mime type
	 * 					default null mean export by filename extension
	 * 
	 * @return void save new file on disk
	 */
	public function save($file, $mime = null) {
		if (null == $mime) {
			$_t = explode('.', $file);
			$mime = end($_t);
		}
		
		if (null == $this->_mime) {
			$this->_mime = 'image/png';
		}
		
		switch ($mime) {
			case 'gif' :
				$mime = 'image/gif';
				break;
			case 'jpg':
			case 'jpeg' :
			case 'jpe' :
				$mime = 'image/jpeg';
				break;
			default :
				$mime = $this->_mime;
				break;
		}
		
		$dir = dirname($file);
		if (!is_dir($dir) || !is_writeable($dir)) {
			$this->_errorMessage[] = 'ThÆ° má»¥c khÃ´ng tá»“n táº¡i hoáº·c khÃ´ng cÃ³ quyá»�n ghi.';
			return false;
		}

		switch($mime) {
			case 'image/gif':
				@imagegif($this->_source, $file);
				break;
			case 'image/jpeg':
				@imagejpeg($this->_source, $file, $this->_options['jpeg_quality']);
				break;
			case 'image/png':
				@imagepng($this->_source, $file);
				break;			
		}
		
		return true;
	}
}