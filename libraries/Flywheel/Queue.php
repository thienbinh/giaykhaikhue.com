<?php
/**
 * Flywheel Queue 
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: Queue.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	Queue
 * @copyright	Flywheel Team (c) 2010
 */

class Flywheel_Queue {
	/**
	 * Name of queue
	 * 
	 * @var $_name string
	 */
	protected $_name;
	
	/**
	 * adapter
	 * Flywheel_Queue_Abstract
	 * 
	 * @var $_adapter
	 */
	protected $_adapter;
	
	public static $config;
	
	protected static $_instances = array();
	
	/**
	 * Get instance of Flywheel_Queue
	 * 
	 * @param string	$name. name of queue
	 * @param string 	$configKey. config queue
	 * 
	 * @return Flywheel_Queue_Redis
	 */
	public function getInstance($name, $configKey) {
		self::getConfigs();
		if (!isset(self::$config[$configKey])) {
			throw new Flywheel_Queue_Exception('Not found config ' .$configKey);
		}
		$config = self::$config[$configKey];
		
		if (!isset($config['adapter']) || null == $config['adapter']) {
			throw new Flywheel_Queue_Exception('Adapter not define in config');			
		}
		
		$key = $name .'_' .$configKey;

		if (!isset(self::$_instances[$key])) {
			$class = 'Flywheel_Queue_' .ucfirst($config['adapter']);
			self::$_instances[$key] = new $class($name, $config);			
		}
		
		return self::$_instances[$key];
	}
	
	/**
	 * get Configs
	 */
	public static function getConfigs() {
		if (self::$config == null || sizeof(self::$config) == 0) {
			self::$config = Flywheel_Config::load(CACHE_DIR .'queue.ini', 'queue', false);
			if (self::$config === false) {
				self::$config = Flywheel_Config::load(GLOBAL_CONFIGS_DIR .'queue.ini', 'queue');
			}
		}
	}
	
	
	/**
	 * Get name
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Get adapter
	 * 
	 * @return Flywheel_Queue_Abstract
	 */
	public function getAdapter() {
		return $this->_adapter;
	}	
}