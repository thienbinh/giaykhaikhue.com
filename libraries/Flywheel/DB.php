<?php
/**
 * Flywheel Database
 * 
 * @author		Volunteer_developer <mysteries.mine@gmail.com>
 * @version		$Id: DB.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Flywheel
 * @subpackage	DB
 *
 */
class Flywheel_DB {
	/**
	 * Config
	 * mang config
	 *
	 * @var Array
	 */	
	protected static $config = array();
	
	/**
	 * Instances
	 * mang cac doi tuong khoi Data Object
	 *
	 * @var Array
	 */
	protected static $instances = array();
	/**
	 * Get Database
	 *
	 * @param string $name
	 * @return Flywheel_DB_Driver_PDO
	 */
	public static function getConnection($name = null) {
		if (sizeof(self::$config) === 0) {
			self::initConfig();		
		}
		
		if ($name === null || !isset(self::$config['database'][$name])) {
			// Neu khong chi dinh ro ten hoac ten khong nam trong config thi se load ra doi tuong default
			$name = self::getDefault();			
		}
		
		if (!isset(self::$config['database'][$name])) {// Neu khong co config nao co ten nhu vay
			throw new Flywheel_DB_Exception('Config not found!');			
		}
		
		$dsn = self::$config['database'][$name]['username'] 
				. ':' . self::$config['database'][$name]['password']
				. '@' . self::$config['database'][$name]['host']
				. ';dbname=' .self::$config['database'][$name]['dbname'];
		
		$driver = self::$config['database'][$name]['driver'];
		
		if (!isset(self::$instances[$dsn])) {
			$class = 'Flywheel_DB_Driver_' .$driver;
			self::$instances[$dsn] = new $class(self::$config['database'][$name]);
		}
		else {
			self::$instances[$dsn]->selectDb(self::$config['database'][$name]['dbname']);
		}
				
		return self::$instances[$dsn];
	}
	
	/**
	 * Get Database Object
	 *
	 * @return Array
	 */
	public static function getInstances() {
		return self::$instances;		
	}
	
	/**
	 * Initialize Configuration
	 * 
	 * @return void
	 */
	public static function initConfig() {
		if (file_exists(CONFIG_DIR. DS .'db.conf.php')) {
			$configFile = CONFIG_DIR. DS .'db.conf.php';
		}
		else {
			$configFile = GLOBAL_CONFIGS_DIR .DS .'db.conf.php';
		}
		self::$config = include $configFile;					
	}
	
	/**
	 * Get Default Config
	 */
	public static function getDefault() {
		return self::$config['default'];
	}
}