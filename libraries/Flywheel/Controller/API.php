<?php
abstract class Flywheel_Controller_Api {
	/**
	 * Controller name
	 * @var $_name string
	 */
	protected $_name;
	
	public function __construct($name) {
		$this->_name = $name;
	}
	
	public function beforeExecute() {}
	
	final public function execute() {		
		//<-- begin debug code -->
		if (Flywheel_Config::get('debug')) {			
			Flywheel_Debug_Profiler::mark('Before execute action', get_class($this));			
		}
		//<-- /end debug code -->
		$this->beforeExecute();
		
		$action = str_replace(' ', '', ucwords(str_replace(array('-', '_'), ' ',
											Flywheel_Factory::getRouter()->getAction())));
		$action = strtolower(Flywheel_Factory::getRouter()->getMethodRequest()) . $action;
		$buffer = $this->$action();
		
		$this->afterExecute();

		//<-- begin debug code -->
		if (Flywheel_Config::get('debug')) {			
			Flywheel_Debug_Profiler::mark('After execute action', get_class($this));			
		}
		//<-- /end debug code -->
		
		return $buffer;
	}
	
	public function afterExecute() {}
}