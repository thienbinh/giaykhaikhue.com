<?php
abstract class Flywheel_Controller_Console {
	/**
	 * Controller name
	 * @var $_name string
	 */
	protected $_name;
	
	public function __construct($name) {
		$this->_name = $name;
	}
	
	/**
	 * Before execute action
	 */
	public function beforeExecute() {
	}
	
	final public function execute($action) {
		$action = 'execute' .Flywheel_Inflector::hungaryNotationToCamel($action);
		if (method_exists($this, $action)) {
			$this->beforeExecute();		
			$buffer = $this->$action();
			$this->afterExecute();
		} else {
			Flywheel_Application::end('ERROR: task ' .$this->name .':' .$action .' not existed!' .PHP_EOL);			
		}
	}
	
	/**
	 * after execute action
	 */
	public function afterExecute() {}
}