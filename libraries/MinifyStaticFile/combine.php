<?php

    /************************************************************************
     * CSS and Javascript Combinator 0.5
     * Copyright 2006 by Niels Leenheer
     *
     * Permission is hereby granted, free of charge, to any person obtaining
     * a copy of this software and associated documentation files (the
     * "Software"), to deal in the Software without restriction, including
     * without limitation the rights to use, copy, modify, merge, publish,
     * distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to
     * the following conditions:
     * 
     * The above copyright notice and this permission notice shall be
     * included in all copies or substantial portions of the Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
     * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
     * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
     * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
     * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
     * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
     * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */
require 'jsmin.php';
require 'cssmin.php'; 
class CombineJsCss
{
    public $re_create       = false;
    public $cachedir = '' ;
    public $type       = 'css';
    public $type_file       = 'css';
    public $files       = array();
    public $cachefile       = '';
    public $hash='';
   // public $cssdir   = PUBLIC_DIR .'/frontend/styles';
   // public $jsdir    = PUBLIC_DIR . '/js';
    function __construct($type='css',$files=array(),$cachedir=CACHE_DIR,$re_create=false)
    {
       $this->type= $type;
       if($this->type== 'javascript')
          $this->type_file='js';
       $this->files= $files;
       $this->cachedir=$cachedir.'/'.$this->type_file.'_cache';
       $this->cachedir = str_replace("//", "/", $this->cachedir);
       
       $this->re_create=$re_create;
       $this->hash = md5(implode(',',$this->files));
       $this->cachefile = 'cache-' . $this->hash . '.' . $this->type_file ;
     
       //neu khong ton tai thi tao thu muc
       if(!file_exists($this->cachedir))
       {
           $old = umask(0); 
           @mkdir($this->cachedir,0777,true);
           //share quyen
           umask($old);
           @chmod($this->cachedir,0777);
       }
    }
    function getFileTime()
    {
       $time=time();
       $time_path=$this->cachedir . '/' .'time_'.$this->hash.'.cache';
       if (!$this->re_create)
       {
          if(file_exists($time_path)) 
          $time=file_get_contents($time_path);
          else
          {
             if ($fp = fopen($time_path, 'wb')) {
                    fwrite($fp, $time);
                    fclose($fp);
                    return $time; 
             }; 
          }
       }else
       {
          if ($fp = fopen($time_path, 'wb')) {
                fwrite($fp, $time);
                fclose($fp);
                return $time; 
          }; 
       }
        
       return $time; 
    }
    function getFileName()
    {
        if (!$this->re_create) 
        {
            if (file_exists($this->cachedir . '/' . $this->cachefile))
            {
                return $this->cachefile;
            }
        }
        $type = $this->type;
        //$elements = explode(',', $_GET['files']);
        $elements =$this->files;
        
        // Determine last modification date of the files
        $lastmodified = 0;
    
        while (list(,$element) = each($elements)) {
           // $path = realpath($base . '/' . $element);
            $path = $element;
         
            if (($type == 'javascript' && substr($path, -3) != '.js') || 
                ($type == 'css' && substr($path, -4) != '.css')) {
                header ("HTTP/1.0 403 Forbidden");
                echo $path.' is not available';
                exit;    
            }
          
            //if (substr($path, 0, strlen($base)) != $base || !file_exists($path)) {
            if (!file_exists($path)) {
                header ("HTTP/1.0 404 Not Found");
                echo $path.' is not available';
                exit;
            }
            
            $lastmodified = max($lastmodified, filemtime($path));
        }
            
        // Get contents of the files
        $contents = '';
        reset($elements);

        while (list(,$element) = each($elements)) {

           $path = realpath($element);
           
           if($type=='javascript')
           {
               $contents .= "\n\n" . JSMin::minify(file_get_contents($path));
           }
           else
           {
               $contents .= "\n\n" . CssMin::minify(file_get_contents($path)); 
              //   $contents .= "\n\n" . file_get_contents($path); 
           } 
        }
        $contents=str_replace('../','../../',$contents);
        if ($fp = fopen($this->cachedir . '/' . $this->cachefile, 'wb')) {
            fwrite($fp, $contents);
            fclose($fp);
            return $this->cachefile; 
        }else return 'Error';
    }    
}   