<?
/**
 * Generate a random base code 64
 */
function randStr() {
  return base64_encode('vni_token='.microtime().'+'.rand());
}
require_once 'OAuth.php';
require_once 'LHOAuthDataStore.php';
require_once('Flywheeloauthserver.php');

class LHOAuthConnect{
  private $lh_ser;
  
  function __construct() {
	//create an oAuth server
	$this->lh_ser = new LHOAuthServer(new LHOAuthDataStore());
	
	//init signature method
	$hmac_method = new OAuthSignatureMethod_HMAC_SHA1();
	$plaintext_method = new OAuthSignatureMethod_PLAINTEXT();
	$rsa_method = new LHOAuthSignatureMethod_RSA_SHA1();
	
	//add signature method to oauth
	$this->lh_ser->add_signature_method($hmac_method);
	$this->lh_ser->add_signature_method($plaintext_method);
	$this->lh_ser->add_signature_method($rsa_method);
  }
  
  function fight_request_token(){
	try{
		
	  $req = OAuthRequest::from_request();	  
	  $arrRequestToken = $this->lh_ser->fetch_request_token($req);
	  
	  //print ($arrRequestToken);
	  return $arrRequestToken;
	}catch (OAuthException $e){
	  print($e->getMessage() . "\n<hr />\n<pre>");
	  //var_dump($req);
	  print '</pre>';
	  die();
	}
  }
  
  function fight_access_token(){
	try{
	
	  $req = OAuthRequest::from_request();
	  $arrAccessToken = $this->lh_ser->fetch_access_token($req);

	  //print $arrAccessToken;
	  return $arrAccessToken;
	}catch (OAuthException $e){
	  print($e->getMessage() . "\n<hr />\n<pre>");
	  //var_dump($req);
	  print '</pre>';
	  die();
	}
  }
  
  function fight_verifier_token($consumer_key){

  		$tmpDataStore = new LHOAuthDataStore();
		$result = $tmpDataStore->new_verifier($consumer_key);
  		return $result;
  }
  
  function getAppInfo($token = '', $token_type = 'request'){
	return $this->lh_ser->lookup_consumer_info($token, $token_type);
  }
  
  function getDataRequestToken($consumer_key, $request_token){
  	return $this->lh_ser->getDataRequestToken($consumer_key, $request_token);
  }
  
  function verify_request()
  {
		try
		{
			$req = OAuthRequest::from_request();
			$result = $this->lh_ser->verify_request($req);
			return $result;
			
		} catch ( OAuthException $e) {
			print($e->getMessage() . "\n<hr />\n<pre>");
			var_dump($req);
			print '</pre>';
			die();		
		}
  }
}
?>