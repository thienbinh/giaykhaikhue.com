<?php 
class LHOAuthDataStore extends OAuthDataStore {
	private $consumer;
	private $request_token;
	private $access_token;
	private $nonce;

	function __construct() {
	}

	function __destruct() {
	}
	
	/**
	* Check if consumer exists from a given consumer key.
	*
	* @param $consumer_key
	*   String. The consumer key.
	*/
	function lookup_consumer($consumer_key) {
		$object = $this->get_consumer($consumer_key);
		if ($object) {
		  	return new OAuthConsumer($object->app_key, $object->app_secret);
		}
		throw new OAuthException('Consumer not found');
	}
	
  /**
   * Check if the token exists.
   *
   * @param $consumer
   *   Object. The service consumer information.
   * @param $token_type
   *   Strint. The type of the token: 'request' or 'access'.
   * @param $token
   *   Strint. The token value.
   * @return
   *   String or NULL. The existing token or NULL in
   *   case it doesnt exist.
   */
	function lookup_token($consumer, $token_type, $token) {
		$object = $this->get_token($consumer->key, $token, $token_type);		
		if ($object) {
			return new OAuthToken($object->token_key, $object->token_secret);
		}

		return false;
	}
	
	/**
	* Check if the nonce value exists. If not, generate one.
	*
	* @param $consumer
	*   Object. The service consumer information with both key
	*   and secret values.
	* @param $token
	*   Strint. The current token.
	* @param $nonce
	*   Strint. A new nonce value, in case a one doesnt current exit.
	* @param $timestamp
	*   Number. The current time.
	* @return
	*   String or NULL. The existing nonce value or NULL in
	*   case it doesnt exist.
	*/
	function lookup_nonce($consumer, $token, $nonce, $timestamp) {
			$CacheOauth = Flywheel_Cache_Memcache::getInstance('oauth');
			$k = 'nonce_' . $token . '_' . $consumer->key;
			$value = array(
				'nonce'     => $nonce,
				'timestamp' => $timestamp,
				'token'     => $token,
				);
			$nonce_1 = $CacheOauth->get($k);
			if(!$nonce_1 || ($nonce_1->timestamp <= $timestamp)){
			$CacheOauth->set($k,(object)$value,8600);
			return NULL;
		}
		return $nonce_1;
	}

	/**
	* Generate a new request token.
	*
	* @param $consumer
	*   Object. The service consumer information.
	*/
	function new_request_token($consumer, $callback) {
		$object = $this->get_consumer($consumer->key);
		if(!$callback){
			$callback	=	$object->app_callback;
		}
		$token = new OAuthToken(randStr(), randStr());
		$value = array(
			'consumer_key'  => 	$consumer->key,
			'type'          => 	'request',
			'token_key'     => 	$token->key,
			'token_secret'	=> 	$token->secret,
			'callback'		=>	$callback		
		);
		$this->putTokenToMem((object)$value, $consumer->key, $token->key);
		return $token;
	}	

	/**
	* Generate a new verifier token.
	*
	* @param $consumer
	*   Object. The service consumer information.
	*/
	function new_verifier($consumer_key) {
		$object = $this->get_consumer($consumer_key);
		$token = new OAuthToken(randStr(), randStr());
		$value = array(
			'consumer_key'	=>	$consumer_key,
			'type'          => 	'verifier',
			'token_key'     => 	$token->key,
			'token_secret'  => 	$token->secret,
		);
		$this->putTokenToMem((object)$value, $consumer_key, $token->key, $value['type']);
		return $token->key;
	}
	
	function new_access_token($token_old, $consumer, $verifier = null) {
		$object_request = $this->get_token($consumer->key, $token_old->key);

		if ($object_request) {
			//Validate verifier
			$conf = array('mode' => 0600, 'timeFormat' => '%X %x');
			$logger = &Log::singleton('file', '../log/oauthserver.log', 'oauth', $conf);
			
			$check_verifer = $this->get_token($consumer->key, $verifier, 'verifier');
			if($check_verifer){
				//Check exist access token before revoke access
				$user_id = 1;
				$infoUser	=	$this->getInfoUser($consumer->key, $token_old->key);
				if (isset($infoUser->user_id))
					$user_id = $infoUser->user_id;
				$app_user	=	AppUsers::getByAppKeyAndUserid($consumer->key, $user_id);
				if(!$app_user){
					$token_new 	= 	new OAuthToken(randStr(), randStr());
					$app_user	=	new AppUsers();
					$app_user->app_key		=	$consumer->key;
					$app_user->token_key	=	$token_new->key;
					$app_user->token_secret	=	$token_new->secret;
					$app_user->user_id		=	$user_id;
					$app_user->type			=	'access';
					$app_user->allow		=	1;
					$app_user->created		=	time();
					$app_user->save();
					$this->putTokenToMem($app_user, $consumer->key, $token_new->key, 'access');
				}
				$result = array(
					'token_key' 	=> 	$app_user->token_key,
					'token_secret'	=>	$app_user->token_secret,
					'user_id' 		=> 	$app_user->user_id
				);
				return $result;
			}else{
				throw new OAuthException('Invalid verifier');
			}
		}
		throw new OAuthException('Invalid request token');
	}
	
		
	/**
	* Get consumer
	*/
	function get_consumer($consumer_key){
		return AppCustomer::findByAppkey($consumer_key);
	}

	/**
	* Get token using memcache
	*
	* @param $consumer_key
	*   String. The consumer_key
	* @param $token
	*   String. The token
	* @param $token_type
	*   String. The token type (access | request)
	*/
	function get_token($consumer_key='', $token='', $token_type='request'){
		$CacheOauth = Flywheel_Cache_Memcache::getInstance('oauth');
		$k = 'token_' . $consumer_key . '_' . $token_type . '_' . $token;
		$object = $CacheOauth->get($k);
		return $object;
	}
	
	/**
	* Get token using memcache
	*
	* @param $consumer_key
	*   String. The consumer_key
	* @param $token
	*   String. The token
	* @param $token_type
	*   String. The token type (access | request)
	*/
	function putTokenToMem($value, $consumer_key='', $token='', $token_type='request', $time = 1800){
		$CacheOauth = Flywheel_Cache_Memcache::getInstance('oauth');
		$k = 'token_' . $consumer_key . '_' . $token_type . '_' . $token;
		if('access' === $token_type){
			$CacheOauth->set($k, $value);
			//Need to find application id
			$CacheOauth->set($token, $consumer_key);
		}else{
			$CacheOauth->set($k, $value, false, $time);
			//Need to find application id
			$CacheOauth->set($token, $consumer_key, false, $time);
		}
		return true;	
	}
	
	/**
	* Get application infomation from token
	*
	* @param $token
	*   String. The token
	*/
	function lookup_consumer_info($token = '', $token_type = 'request'){
		$CacheOauth = Flywheel_Cache_Memcache::getInstance('oauth');
		$consumer_key = $CacheOauth->get($token);
		if($consumer_key && $consumer_key != ''){
			return $this->get_consumer($consumer_key);
		}
		return false;
	}
	
	/**
	* Get userid using memcache
	*
	* @app_key:
	* @request_token:
	*/
	function getInfoUser($app_key = '', $request_token = '')
	{
		$CacheOauth = Flywheel_Cache_Memcache::getInstance('oauth');
		$k = 'infouser_' . $app_key . '_' . $request_token;
		$object = $CacheOauth->get($k);
		return $object;
	}
	
	/**
	* Get DataRequestToken
	*
	* @param String @app_key:
	* @param String @request_token
	* @param String $token_type
	* 
	* return Object
	*/
	function getDataRequestToken($consumer_key = '', $token = '', $token_type = 'request'){
		return $this->get_token($consumer_key, $token, $token_type);
	}
	function new_access_for_big4($consumer_key, $user_id){
		$app_user	=	AppUsers::getByAppKeyAndUserid($consumer_key, $user_id);
		if(!$app_user){
			$token_new 	= 	new OAuthToken(randStr(), randStr());
			$app_user	=	new AppUsers();
			$app_user->app_key		=	$consumer_key;
			$app_user->token_key	=	$token_new->key;
			$app_user->token_secret	=	$token_new->secret;
			$app_user->user_id		=	$user_id;
			$app_user->type			=	'access';
			$app_user->allow		=	1;
			$app_user->created		=	time();
			$app_user->save();
			$this->putTokenToMem($app_user, $consumer_key, $token_new->key, 'access');
		}
		if($app_user){
			$result = array(
						'token_key' 	=> 	$app_user->token_key,
						'token_secret'	=>	$app_user->token_secret,
						'user_id' 		=> 	$app_user->user_id
					);
			return $result;
		}
		return false;
	}
}

?>