<?php

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

//define project directories
define('BASE_DIR', dirname(__FILE__));
define('APPS_DIR', BASE_DIR . DS . 'apps' . DS);
define('CACHE_DIR', BASE_DIR . DS . 'cache' . DS);
define('GLOBAL_DIR', BASE_DIR . DS . 'global' . DS);
define('LIBRARIES_DIR', BASE_DIR . DS . 'libraries' . DS);
define('LOG_DIR', BASE_DIR . DS . 'log' . DS);
define('MODELS_DIR', BASE_DIR . DS . 'models' . DS);
define('PLUGINS_DIR', BASE_DIR . DS . 'plugins' . DS);
define('PUBLIC_DIR', BASE_DIR . DS . 'public_html' . DS);
define('DATA_DIR', PUBLIC_DIR . 'data' . DS);
//Flywheel libraries directory
define('FLYWHEEL_DIR', LIBRARIES_DIR . 'Flywheel' . DS);
define('FLYWHEEL_UTIL', FLYWHEEL_DIR . 'Util' . DS);
//Project globals directory
define('GLOBAL_CONFIGS_DIR', GLOBAL_DIR . 'configs' . DS);
define('GLOBAL_CONTROLLERS_DIR', GLOBAL_DIR . 'controllers' . DS);
define('GLOBAL_MODULES_DIR', GLOBAL_DIR . 'modules' . DS);
define('GLOBAL_TEMPLATE_DIR', GLOBAL_DIR . 'templates' . DS);
define('GLOBAL_HELPER_DIR', GLOBAL_DIR . 'helper' . DS);
//project static resource
define('STATIC_DIR', PUBLIC_DIR . DS . 'medias' . DS);
define('TMP_DIR', PUBLIC_DIR . DS . 'medias' . DS . 'tmp' . DS);

define('WEBSITE_NAME', 'Bán lẻ 247');
define('ROOT_URL', 'http://localhost/tp_alimama/');

//Bat dau thong tin dang ky dich vu Ngan Luong
define('MERCHANT_ID', '25632');
define('MERCHANT_PASS', 'kt@alimama');
define('RECEIVER', 'hoangtm@alimama.vn');

define('MIN_MONEY_CHARGE', 50000);

define('URL_WS', 'https://www.nganluong.vn/micro_checkout_api.php?wsdl');
define('PASS_WS_NGAN_LUONG', 'kt@alimama');
//Ket thuc thong tin dang ky dich vu Ngan Luong
//Bat dau thong tin dang ky dich vu Bao Kim
define('BAOKIM_RECEIVER', 'hoangtm@alimama.vn');
//Ket thuc thong tin dang ky dich vu Ngan Luong
//khuyen mai khi nap tien qua vi dien tu
define('VDT_PROMOTION_START', '20/11/2012');
define('VDT_PROMOTION_END', '20/12/2012');
define('VDT_PROMOTION_NUM', 0.02);

define('VDT_NL_FEE', 0.005);
define('VDT_BK_FEE', 0.005);
define('PARTNER_FEE_WEIGHT_SUB', 30000);
define('ITEM_ID_FORPAYMENT', 144754);

//chodientu api
define('CDT_USERNAME', 'alimama');
define('CDT_TOKEN', '8aedd5cfda46fe20a28d74ae34591e82');
define('CDT_WS_URL', 'http://chodientu.vn:81/partner_webservice.php?wsdl');

define('SITES_LIST', serialize(array('mofayichu',
            '5taobao', 'chenxifushi', 'lelefushi', 'yilanfushi',
            'shmoyu', 'yiranmeifushi', 'yiwenfushi', 'rihanfushi',
            'chengzifs', '69shopfs', 'jj-fashion', 'fuzhuangpifa',
            'shanghai', 'eeshow', 'charm-dress', 'baobaopifa', 'xinshij',
            '1925'
        )));
require_once FLYWHEEL_DIR . 'Util' . DS . 'FolderUtil.php';
require_once GLOBAL_HELPER_DIR . 'UtilityHelper.php';

spl_autoload_register('autoload');

function autoload($class) {
    if (class_exists($class, false) || interface_exists($class, false)) {
        return true;
    }

    if (strpos($class, 'Flywheel') === 0) { // Flywheel
        $class = str_replace('Flywheel_', '', $class);
        $class = FLYWHEEL_DIR . str_replace('_', DS, $class) . '.php';
        if (file_exists($class)) {
            require $class;
            return true;
        }
    } elseif (file_exists($class = MODELS_DIR . str_replace('_', DS, $class) . '.php')) {
        require $class;
    }

    return false;
}
