<?php 
/**
 * Content
 *  This class has been auto-generated at 09/09/2010 17:42:35
 * @version		$Id: build_models.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Model
 * @subpackage		order
 * @copyright		Flywheel Team (c) 2010
 */
class Content extends Flywheel_Model {
    const _PUBLIC = 1;
    const UN_PUBLIC = 0;
    
    /**
     * id
     * primary
     * auto-increment
     * type : int(4) unsigned
     * @var integer $id
     */
    public $id = 0;

    /**
     * title
     * type : varchar(255)
     * maxlength : 255
     * @var string $title
     */
    public $title = '';

    /**
     * excerpt
     * type : text
     * maxlength : 
     * @var string $excerpt
     */
    public $excerpt = '';

    /**
     * content
     * type : text
     * maxlength : 
     * @var string $content
     */
    public $content = '';

    /**
     * source
     * type : text
     * maxlength : 
     * @var string $source
     */
    public $source = '';
    
    /**
     * image
     * type: varchar(255)
     * @var string $source
     */
    public $image = '';

    /**
     * ordering
     * type : int(11)
     * @var integer $pos
     */
    public $pos = 0;

    /**
     * config
     * type : text
     * maxlength : 
     * @var string $config
     */
    public $config = '';

    /**
     * created_by
     * type : int(4) unsigned
     * @var integer $created_by
     */
    public $created_by = 0;

    /**
     * created_time
     * type : int(11)
     * @var integer $created_time
     */
    public $created_time = 0;

    /**
     * modified_by
     * type : int(4) unsigned
     * @var integer $modified_by
     */
    public $modified_by = 0;

    /**
     * modified_time
     * type : int(11)
     * @var integer $modified_time
     */
    public $modified_time = 0;
    
    public $status = 0;

    /**
     * publish_from
     * type : int(11)
     * @var integer $publish_from
     */
    public $publish_from = 0;

    /**
     * publish_to
     * type : int(11)
     * @var integer $publish_to
     */
    public $publish_to = 0;

    /**
     * category_id
     * type : int(4) unsigned
     * @var integer $category_id
     */
    public $category_id = 0;

    /**
     * hits
     * type : int(4)
     * @var integer $hits
     */
    public $hits = 0;

    /**
     * database table name
     */
    const TABLE ='content';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
                    'fields' => array('content.`id`', 
                    'content.`title`', 'content.`excerpt`', 
                    'content.`content`', 'content.`source`', 
                    'content.`image`', 'content.`pos`', 
                    'content.`config`', 'content.`created_by`', 
                    'content.`created_time`', 
                    'content.`modified_by`', 
                    'content.`modified_time`', 
                    'content.`status`', 
                    'content.`publish_from`', 
                    'content.`publish_to`', 
                    'content.`category_id`',
                     'content.`hits`'),
                    'columns' => array('id','title','excerpt','content','source','image','pos','config','created_by','created_time','modified_by','modified_time', 'status','publish_from','publish_to','category_id','hits'));

    public static $instances = array();
    /**
     * Read object from database
     * @param int		$id	default null
     * @return Content
     * @throws Flywheel_DB_Exception
     */
    public function read($id = null) {
    	if (null == $id) {
    		$id = $this->id;
    	}
    	
    	$conn = Flywheel_DB::getConnection(self::TABLE);
    	$conn->prepare('SELECT ' .implode(', ', self::$schema['fields']) .' 
    						FROM ' .self::TABLE .' WHERE id=? LIMIT 1');
    	$conn->bindParam(1, $id, PDO::PARAM_INT);
    	$conn->execute();
    	$data = $conn->fetch();
    	if (false === $data) {
    		return false;
    	}
    	
    	$this->_hydrate($data);
    	$this->setNew(false);
    	self::addInstanceToPool($this);
    	return true;
    }

    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {
    	$conn = Flywheel_DB::getConnection(self::TABLE);
    	if (true === $this->isNew()) {
    		$this->modified_time = time();
    		$this->created_time = time();
    		$data = $this->toArray();
    		unset($data['id']);
    		$id = $conn->insert(self::TABLE, $data);
    		if (!$id) {
    			return false;
    		}
    		$this->id = $id;    		
    	} else {
    		//bind array to update
    		$data = array();
    		$columnModifed = 0;
    		unset($this->_modifiedColumns['modified_time']);
    		foreach ($this->_modifiedColumns as $modifiedColumn=>$value) {
    			++$columnModifed;
    			$data[$modifiedColumn] = $this->$modifiedColumn;    			    			    			    			    			    			    			
    		}
    		if ($columnModifed == 0) {
    			return false;   			
    		}

    		$data['modified_time'] = $this->modified_time = time();
    		$affectedRows = $conn->update(self::TABLE, $data, '`id`=' .$this->id .' LIMIT 1');
    		if (!$affectedRows) {
    			return false;
    		}
    		
    		$this->_modifiedColumns = array();	
    	}
    	if (true == $this->isNew()) { // increase one value of category's content amount
    		ContentCategory::retrieveByPk($this->category_id)->increaseContentAmount();    		
    	}
    	$this->setNew(false);	
    	self::addInstanceToPool($this);
    	return $this->id;
    }

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {
    	if (Flywheel_DB::getConnection(self::TABLE)->delete(self::TABLE, '`id`='.$this->id)) {
    		ContentCategory::retrieveByPk($this->category_id)->decreaseContentAmount();
    		self::deleteInstanceFromPool($this->id);
    		return true;    		   		    		
    	}
    	
    	return false;
    }
    
    /**
     * Get data by condition
     * @param type $where Condition query
     * @return type LIST (null if empty data)
     */
    public function selectList($where = '1=1', $order = null, $limit = null, $key = null)
    {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        $data = $conn->select(self::TABLE, "*", $where, $order, $limit, $key);
        return $data;
    }
    
    public function countItem($where = null) {
        $conn = Flywheel_DB::getConnection();
        return $conn->count(self::TABLE, $where);
    }
    
    public function isPublic() {
    	return ($this->publish_from == 0 || $this->publish_from <= time()) 
    				&& ($this->publish_to == 0 || $this->publish_to >= time());     	
    }

    /**
     * Retrieve content by primary key
     *
     * @author 		Nguyen Quang Phan
     * @param 		string $pk
     * @return 		Content false if pk not exists
     */
    public static function retrieveByPk($pk) {
    	if (false !== ($obj = self::getInstanceFromPool($pk))) {
    		return $obj;
    	}

    	$content = new self();
    	if ($content->read($pk)) {
    		self::addInstanceToPool($content);
    		return $content;
    	}
        
    	return false;

    }
    
    /**
     * get content's category
     * 
     * @return ContentCategory
     */
    public function getCategory() {
    	return ContentCategory::retrieveByPk($this->category_id);    	
    }

    /**
     * add instance to pool
     * 
     * @param Content $content
     */
    public static function addInstanceToPool(Content $content) {
    	self::$instances[$content->id] = $content;
    }
    
    /**
     * get instance from pool
     * @param integer $pk
     * 
     * @return Content
     * 			false if not found
     */
    public static function getInstanceFromPool($pk) {
    	return isset(self::$instances[$pk])? self::$instances[$pk]: false;    	
    }
    
    /**
     * delete instance from pool
     * @param integer $pk
     */
    public static function deleteInstanceFromPool($pk) {
    	unset(self::$instances[$pk]);    	
    }
    
    /**
     * clear pool
     */
    public static function clearPool() {
    	self::$instances = array();
    }
}