<?php 
/**
 * Users
 *  This class has been auto-generated at 25/08/2010 17:51:12
 * @version		$Id: Users.php 1292 2011-04-07 10:03:06Z mylifeisskidrow $
 * @package		Model
 * @subpackage		order
 * @copyright		Flywheel Team (c) 2010
 */
class Privilege extends Flywheel_Model {
    /**
     * id
     * primary
     * auto-increment
     * type : int(4) unsigned
     * @var integer $id
     */
    public $id = 0;

    /**
     * group_name
     * type : varchar(255)
     * maxlength : 255
     * @var string $group_name
     */
    public 	$privilege_name  = '';

    /**
     * privilege_code
     * type : text
     * @var string $user_ids
     */
    public $privilege_code = '';
    
    /**
     * status
     * type : int(11)
     * maxlength : 11
     * @var int status
     */
    public $status = 0;
    /**
     * database table name
     */
    const TABLE ='privilege';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
        'fields' => array('privilege.`id`', 'privilege.`privilege_name`', 'privilege.`privilege_code`', 'privilege.`status`'),
        'columns' => array('id','privilege_name','privilege_code','status')
    );

    protected static $instances = array();
    /**
     * Read object from database
     * @param int		$id	default null
     * @return Users
     * @throws Flywheel_DB_Exception
     */
    public $db          =   '';
    public $document    =   ''; 
    public $request     =   '';
    
    public function __construct()
    {
        $this->db = Flywheel_DB::getConnection(self::TABLE);
        $this->document = Flywheel_Factory::getDocument(); 
        $this->request = Flywheel_Factory::getRequest();     
    }
    
    public function read($id = null) {
    	if (null == $id) {
    		$id = $this->id;
    	}
    	
    	$db = Flywheel_DB::getConnection('privilege');
    	$db->prepare('SELECT ' .implode(', ', self::$schema['fields']) .' 
    					FROM ' .self::TABLE .' 
    					WHERE id = ? LIMIT 1');
    	
    	$db->bindParam(1, $id, PDO::PARAM_INT);
    	$db->execute();
    	$data = $db->fetch();
    	
    	if (false === $data) {
    		return false;
    	}
    	$this->_hydrate($data);
    	$this->setNew(false);    	
    	   	
    	return true;
    }
    
    public function selectOne($id = null) {
        if(!$id || $id == 0) return false;
        return $this->db->selectOne(self::TABLE,implode(',',self::$schema['fields']),'id='.$id);
    }
    
    public function selectOneCondition($condition = null) {
        if(!$condition) return false;
        return $this->db->selectOne(self::TABLE,implode(',',self::$schema['fields']),$condition);
    }
    
    public function selectList($condition = null,$order = null,$limit = null,$key = null) {
        if(!$condition) $condition = "1=1";
        return $this->db->select(self::TABLE,implode(',',self::$schema['fields']),$condition,$order,$limit,$key);
    }
    
    public function insert($arrayInfo)
    {
    	$conn = Flywheel_DB::getConnection(self::TABLE);
        $true = $conn->insert(self::TABLE, $arrayInfo);
        return $true;
    } 
    
    public function update($arrayInfo, $condition)
    {
    	$conn = Flywheel_DB::getConnection(self::TABLE);
        $affectedRows = $conn->update(self::TABLE, $arrayInfo, $condition);
        return $affectedRows;
    }
    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {    	
    	$db = Flywheel_DB::getConnection(self::TABLE);
    	if (true === $this->isNew()) 
        {
    		$this->modified_time = time();    		
    		$data = $this->toArray();
    		unset($data['id']);
    		$id = $db->insert(self::TABLE, $data);
    		if (!$id) {
    			return false;			
    		}   
    		$this->id = $id;
    		$this->setNew(false);    		 		
    	} 
        else 
        {
    		//bind array to update
    		$data = array();
    		$columnModifed = 0;
    		unset($this->_modifiedColumns['modified_time']);
    		foreach ($this->_modifiedColumns as $modifiedColumn=>$value) {
    			++$columnModifed;
    			$data[$modifiedColumn] = $this->$modifiedColumn;    			    			    			    			    			    			    			
    		}
    		if ($columnModifed == 0) {
    			return 0;   			
    		}

    		$data['modified_time'] = $this->modified_time = time();
    		$affectedRows = $db->update(self::TABLE, $data, 'id=' .$this->id);
    		if (0 == $affectedRows) {
    			return false;
    		}  		 		
    	}
    	self::addInstanceToPool($this);
    	return $this->id;
    }

    /**
     * Delete object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {
        $db = Flywheel_DB::getConnection(self::TABLE);
        return $db->delete(self::TABLE,'id='.$this->id);       
    }
    
    /**
     * Delete object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function deleteCondition($condition) {
        $db = Flywheel_DB::getConnection(self::TABLE);
        return $db->delete(self::TABLE,$condition);       
    }
    
    /**
     * Delete object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function deleteUP($condition) {
        $db = Flywheel_DB::getConnection('user_privilege');
        return $db->delete('user_privilege',$condition);       
    }
    
    public static function optionSelect($options = array(''),$title_colum = 'title', $value_colum = 'id', $selected='')
    {
        $html = "";
        if($options)
        {
            foreach($options as $row)
            {
                if($selected == $row[$value_colum])
                    $html .= "<option value='".$row[$value_colum]."' selected='selected'>".$row[$title_colum]."</option>";
                else
                    $html .= "<option value='".$row[$value_colum]."'>".$row[$title_colum]."</option>";
            }
        }
        return $html;
    }
    
    public function getUserPrivilege($privilege)
    {
        if(!$privilege) return false;
        $str_id = '';
        $arr_id = array();
        foreach($privilege as $row)
            $arr_id[] = $row['id'];
        if($arr_id) $str_id = implode(',',$arr_id);
        if($str_id)
        {
            $sql = "SELECT * FROM `user_privilege` WHERE privilege_id IN({$str_id}) ORDER BY privilege_id asc";
            $db = Flywheel_DB::getConnection('user_privilege');
            $db->prepare($sql);
            $db->execute();
            $data_up = $db->fetchAll();
            if($data_up)
            {
                $arr_result = array();
                foreach($data_up as $item)
                {
                    $arr_result[$item['privilege_id']]['id'] = $item['privilege_id'];
                    if($item['user_id']) $arr_result[$item['privilege_id']]['user_id'][] = $item['user_id'];
                    if($item['g_user_id']) $arr_result[$item['privilege_id']]['g_user_id'][] = $item['g_user_id'];
                }
                return ($arr_result) ? $arr_result:false;
            }
        }
        return false;
    }
    
    public function getStrUserPrivilege($privilege)
    {
        if(!$privilege) return false;
        $datas = self::getUserPrivilege($privilege);
        if($datas)
        {
            
        } 
    }
    public function getNamePrivilege($list_ids = array())
    {
        if(!$list_ids) return false;
        $sql = "SELECT * FROM `privilege` WHERE id IN({$list_ids})";
        $db = Flywheel_DB::getConnection('privilege');
        $db->prepare($sql);
        $db->execute();
        $data_up = $db->fetchAll();
        return ($data_up) ? $data_up : false; 
    }
    
    function getIDPrivilege($list_code)
    {
        if(!$list_code) return false;
        $arr_name = explode(',',$list_code);
        foreach($arr_name as $code)
           $list_code .= "'".$code."',";
        $list_code = substr($list_code,0,-1);
         
        $sql = "SELECT * FROM `privilege` WHERE privilege_code IN({$list_code})";
        $db = Flywheel_DB::getConnection('privilege');
        $db->prepare($sql);
        $db->execute();
        $data_up = $db->fetchAll();
        return ($data_up) ? $data_up : false;
    }
    
    function getPrivilegeCode($privilege_code)
    {
        if(!$privilege_code) return false;
        
        $sql = "SELECT * FROM `privilege` WHERE privilege_code = '".trim($privilege_code)."'";
        $db = Flywheel_DB::getConnection('privilege');
        $db->prepare($sql);
        $db->execute();
        $data_up = $db->fetch();
        return ($data_up) ? $data_up : false;
    }
    
    function getPrivilegeCodeList($privilege_codes)
    {
        if(!$privilege_codes) return false;
        $privilege_codes = explode(',',$privilege_codes);
        $str_list = '';
        foreach($privilege_codes as $code) $str_list = '"'.$code.'"';
        $condition = "privilege_code IN(".trim($str_list).")";
        $db = Flywheel_DB::getConnection('privilege');
        $data_up = $db->select(self::TABLE,'*',$condition);
        return ($data_up) ? $data_up : false;
    }
    
    function getPrivilegeIdList($privilege_ids)
    {
        if(!$privilege_ids) return false;
        $condition = "id IN(".trim($privilege_ids).")";
        $db = Flywheel_DB::getConnection(self::TABLE);
        $data_up = $db->select(self::TABLE,'*',$condition);
        return ($data_up) ? $data_up : false;
    }
    
    function getPrivilegeName($privilege_name)
    {
        if(!$privilege_name) return false;
        
        $sql = "SELECT * FROM `privilege` WHERE privilege_name = '".trim($privilege_name)."'";
        $db = Flywheel_DB::getConnection('privilege');
        $db->prepare($sql);
        $db->execute();
        $data_up = $db->fetch();
        return ($data_up) ? $data_up : false;
    }
    
    function getPrivilegeId($privilege_id)
    {
        if(!$privilege_id || !is_numeric($privilege_id)) return false;
        
        $sql = "SELECT * FROM `privilege` WHERE id = '".$privilege_id."'";
        $db = Flywheel_DB::getConnection('privilege');
        $db->prepare($sql);
        $db->execute();
        $data_up = $db->fetch();
        return ($data_up) ? $data_up : false;
    }
    
    /**
     * function check quyền 
     * @param $code: Mã quyền
     * @return TRUE | FALSE
     */
    public function checkPrivilegeUser($code)
    {
        $user = Flywheel_Authen::getInstance()->getUser()->toArray();  
        if(!isset($user) || !$user['username'] || !$code) return false;
        if($user['username'] == 'admin') return true;
        # Kiểm tra quyền có tồn tạ ko
        $dataPrivilege = $this->getPrivilegeCode($code);
        if(!$dataPrivilege) return false;
        $objUP = new UserPrivilege;
        
        #Lấy DL quyền theo user
        $dataUP = $objUP->getUserPrivilege('privilege_id='.$dataPrivilege['id'].' AND user_id='.$user['id']);
        if($dataUP && count($dataUP)) return true;
        
        #Kiểm tra thuộc nhóm nào không
        $objU = new UserGroup;
        $dataG = $objU->getGroupByUId($user['id']);
        if(!$dataG) return false;
        
        #Kiểm tra nhóm có quyền không
        $str_gid = implode(',',$dataG);
        $dataUP = $objUP->getUserPrivilege('privilege_id='.$dataPrivilege['id'].' AND g_user_id IN('.$str_gid.')');
        if(!$dataUP) return false;
        return true;
    }
    
    public function getPrivilegeUId($uid)
    {
        if(!$uid) return false;
        # Lấy dữ liệu từ Table user_privilege theo user
        $objUP = new UserPrivilege;
        $dataUP1 = $objUP->getUserPrivilege('user_id='.$uid);
        
        # Lấy nhóm của user
        $objUG = new UserGroup;
        $dataUG = $objUG->getGroupByUId($uid);
        
        $dataUP2 = '';
        if($dataUG)
        {
            $gid_list = '';
            foreach($dataUG as $gid)
                $gid_list[] = $gid;
            
            # Lấy dữ liệu từ Table user_privilege theo group
            $dataUP2 = $objUP->getUserPrivilege('g_user_id IN('.implode(',',$gid_list).')');
        }
        
        $idResult = array();
        if($dataUP1)
        foreach($dataUP1 as $row)
            $idResult[] = $row['privilege_id'];
        if($dataUP2)
        foreach($dataUP2 as $row)
        {
            if(!in_array($row['privilege_id'],$idResult))
                $idResult[] = $row['privilege_id'];
        }
        if(!$idResult) return false;
        
        #Lấy mã Quyền theo danh sách ID quyền
        $codeResult = array();
        $codes = $this->getPrivilegeIdList(implode(',',$idResult));
        $result = array();  
        foreach($codes as $code)
        {
            $result['Pid'][] = $code['id'];
            $result['Pcode'][] = $code['privilege_code'];
        }
        return $result;
    }
    
    public function adminPrivlege()
    {
        $db = Flywheel_DB::getConnection(self::TABLE);
        $data = $db->select(self::TABLE,'*',"1=1");
        $result = array();  
        foreach($data as $code)
        {
            $result['Pid'][] = $code['id'];
            $result['Pcode'][] = $code['privilege_code'];
        }
        return $result;
    }
}