<?php

/**
 * ContentCategory
 *  This class has been auto-generated at 09/09/2010 17:42:36
 * @version		$Id: build_models.php 43 2010-08-25 19:38:34Z mylifeisskidrow@gmail.com $
 * @package		Model
 * @subpackage		order
 * @copyright		Flywheel Team (c) 2010
 */
require_once FLYWHEEL_UTIL . 'StringUtil.php';

class ContentCategory extends Flywheel_Model {
    const STATUS_PUBLISHED = 1;
    const STATUS_UNPUBLISHED = 0;
    /**
     * id
     * primary
     * auto-increment
     * type : int(4) unsigned
     * @var integer $id
     */
    public $id = 0;
    /**
     * parrent_id
     * type : int(4)
     * @var integer $parrent_id
     */
    public $parent_id = 0;
    /**
     * name
     * type : varchar(255)
     * maxlength : 255
     * @var string $name
     */
    public $name = '';
    /**
     * description
     * type : text
     * maxlength : 
     * @var string $description
     */
    public $description = '';
    /**
     * config
     * type : text
     * maxlength : 
     * @var string $config
     */
    public $config = '';
    /**
     * published
     * type : tinyint(1)
     * @var integer $published
     */
    public $published = 1;
    /**
     * ordering
     * type: int(11)
     * @var integer
     */
    public $ordering = 0;
    /**
     * content_amount
     * type: int(4)
     * @var integer
     */
    public $content_amount = 0;
    /**
     * this category's childs
     * @var array
     */
    public $childs;

    /**
     * database name
     */
    /**
     * database table name
     */
    const TABLE ='content_category';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
        'fields' => array('content_category.`id`',
            'content_category.`parent_id`',
            'content_category.`ordering`',
            'content_category.`name`',
            'content_category.`description`',
            'content_category.`config`',
            'content_category.`published`',
            'content_category.`content_amount`'),
        'columns' => array('id', 'parent_id', 'ordering', 'name', 'description', 'config', 'published', 'content_amount'));
    /**
     * array of categories structures
     * 
     * @var array
     */
    protected static $_structures;
    public static $instances = array();
    private $_configs;

    /**
     * Read object from database
     * @param int		$id	default null
     * @return ContentCategory
     * @throws Flywheel_DB_Exception
     */
    public function read($id = null) {
        if (null == $id) {
            $this->id = $id;
        }

        $conn = Flywheel_DB::getConnection(self::TABLE);
        $conn->prepare('SELECT ' . implode(', ', self::$schema['fields']) . ' 
    						FROM ' . self::TABLE . ' WHERE id = ? LIMIT 1');
        $conn->bindValue(1, $id, PDO::PARAM_INT);
        $conn->execute();
        $data = $conn->fetch();
        if (false == $data) {
            return false;
        }

        $this->_hydrate($data);
        $this->setNew(false);
        self::addInstanceToPool($this);
        return true;
    }

    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        if (true == $this->isNew()) {
            $data = array();
            for ($i = 1, $size = sizeof(self::$schema['columns']); $i < $size; ++$i) {
                $column = self::$schema['columns'][$i];
                $data[$column] = $this->$column;
            }
            $id = $conn->insert(self::TABLE, $data);
            if (!$id) {
                return false;
            }
            $this->id = $id;
            self::_storeCategoriesStructureToCache();
        } else {
            //bind array to update
            $data = array();
            foreach ($this->_modifiedColumns as $modifiedColumn => $value) {
                $data[$modifiedColumn] = $this->$modifiedColumn;
            }
            if (sizeof($data) == 0) {
                return false;
            }

            $affectedRows = $conn->update(self::TABLE, $data, '`id`=' . $this->id . ' LIMIT 1');
            if (!$affectedRows) {
                return false;
            }

            if (isset($this->_modifiedColumns['parent_id'])
                    || isset($this->_modifiedColumns['ordering'])) {
                self::_storeCategoriesStructureToCache();
            }

            $this->_modifiedColumns = array();
        }
        $this->getChildsId();
        $this->setNew(false);
        $this->_storeCache();
        self::addInstanceToPool($this);
        return $this->id;
    }

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {
        if (Flywheel_DB::getConnection(self::TABLE)->delete(self::TABLE, '`id`=' . $this->id)) {
            $this->_removeFromCache();
            self::_storeCategoriesStructureToCache();
            self::deleteInstanceFromPool($this->id);
            return true;
        }

        return false;
    }

    public function increaseContentAmount($amount = 1) {
        $this->setContentAmount($this->content_amount + $amount);
        return $this->save();
    }

    public function decreaseContentAmount($amount = 1) {
        $this->setContentAmount($this->content_amount - $amount);
        return $this->save();
    }

    public function getChilds() {
        $childsId = $this->getChildsId();
        if (false === $childsId) {
            return false;
        }
        $childs = array();
        foreach ($childsId as $childId) {
            $_c = self::retrieveByPk($childId);
            if (false !== $_c) {
                $childs[] = $_c;
            }
        }

        return $childs;
    }

    /**
     * get category childs id
     * 
     * @return array | boolean false if this category is leaf
     */
    public function getChildsId() {
        return self::getStructureByNodeId($this->id);
    }

    /**
     * get number of category's childs
     * 
     * @return integer
     */
    public function getChildsNo() {
        return count($this->getChildsId());
    }

    /**
     * get parent category object
     * 
     * @return ContentCategory
     */
    public function getParentCategory() {
        if (0 == $this->parent_id) {
            return false;
        }

        return self::retrieveByPk($this->parent_id);
    }

    /**
     * get tree of categories by node id, if node id = 0, return all categories
     * @param int $nodeId
     * 
     * @return array of $category
     */
    public static function getTree($nodeId = 0) {
        if ($nodeId !== 0) {
            $node = self::retrieveByPk($nodeId);
            if (false === $node) {
                return false;
            }
        } else {
            $node = new self();
        }
        $tree = array();
        $childs = $node->getChilds();

        if (false !== $childs) {
            for ($i = 0, $size = sizeof($childs); $i < $size; ++$i) {
                $tree[] = self::getTree($childs[$i]->id);
            }
        }

        if ($nodeId == 0) {
            return $tree;
        } else {
            $node->childs = $tree;
            return $node;
        }
    }

    /**
     * get tree of noteId scalar to ordering array
     * 
     * @param integer	$nodeId
     * @param array		&$scalar
     * @param boolean	$onlyId
     * @param integer	$level starting level
     */
    public static function getTreeScalar($nodeId = 0, &$scalar, $onlyId = true, $includedLevel = true, $level = 0) {
        if (0 !== $nodeId) {
            $node = self::retrieveByPk($nodeId);
        } else {
            $node = new self();
        }

        if (0 !== $nodeId) {
            if (false === $onlyId) {
                $scalar[] = ($includedLevel) ?
                        array('category' => $node, 'level' => $level) : $node;
            } else {
                $scalar[] = ($includedLevel) ?
                        array('id' => $node->id, 'level' => $level) : $node->id;
            }
        }

        //if($node->id == 0) return;
        if(empty($node)) { return; }
        $childsId = $node->getChildsId();
        if (false !== $childsId) {
            foreach ($childsId as $childId) {
                self::getTreeScalar($childId, $scalar, $onlyId, $includedLevel, $level + 1);
            }
        }
    }

    /**
     * get category structure by node id
     * 
     * @param integer	$nodeId
     * 
     * @return array | false node category is leaf
     */
    public static function getStructureByNodeId($nodeId = 0) {
        self::loadStructureFromCache();
        if (isset(self::$_structures[$nodeId])) {
            return self::$_structures[$nodeId];
        }

        return false;
    }

    /**
     * load all structure from cache
     */
    public static function loadStructureFromCache() {
        if (null === self::$_structures) {
            if (!file_exists($file = CACHE_DIR . 'database' . DS . 'content_categories' . DS . 'structures.php')) {
                self::_storeCategoriesStructureToCache();
            } else {
                self::$_structures = require $file;
            }
        }
    }

    /**
     * store cactegory model n file cache 
     */
    private function _storeCache() {
        return Flywheel_Cache_File::getInstance(CACHE_DIR . 'database' . DS . 'content_categories')
                ->set($this->id, $this);
    }

    private function _removeFromCache() {
        return Flywheel_Cache_File::getInstance(CACHE_DIR . 'database' . DS . 'content_categories')
                ->delete($this->id);
    }

    /**
     * Get config param
     * 
     * @param string	$config name of config
     * @param integer	$filter	filter type {@see Flywheel_Filter}
     * @param mixed		$default the default value if $config not set
     * 
     * @return mixed
     */
    public function getConfigParam($config, $filter = Flywheel_Filter::TYPE_STRING, $default = null) {
        $this->getConfig();
        if (isset($this->_configs[$config])) {
            return Flywheel_Filter::clean($this->_configs[$config], $filter);
        }

        return $default;
    }

    /**
     * Get config of category
     * 
     * @return array
     */
    public function getConfig() {
        if (null == $this->_configs) {
            $this->_configs = json_decode($this->config, true);
        }

        return $this->_configs;
    }

    /**
     * get path from root category to this category
     */
    public static function getPath($nodeId, &$data = array()) {
        self::loadStructureFromCache();
        if (sizeof($data) == 0) {
            $data[] = $nodeId;
        }
        $category = self::retrieveByPk($nodeId);
        if (0 != $category->parent_id) {
            $data[] = $category->parent_id;
            self::getPath($category->parent_id, $data);
        }
    }

    /**
     * retrieve object by primary key
     * 
     * @param int $pk primary value
     * 
     * @return ContentCategory
     * 			false if not found
     */
    public static function retrieveByPk($pk) {
        if (false !== ($contentCategory = self::getInstanceFromPool($pk))) {
            return $contentCategory;
        }

        $cache = Flywheel_Cache_File::getInstance(CACHE_DIR . 'database' . DS . 'content_categories');
        $category = new self();
        $data = $cache->get($pk);
        if ($data === false) {
            if (false == ($category->read($pk))) {
                return false;
            }
            $category->_storeCache();
        } else {
            $category->_hydrate($data);
            self::addInstanceToPool($category);
        }

        return $category;
    }

    /**
     * store categories structure to cache
     */
    private static function _storeCategoriesStructureToCache() {
        $categories = Flywheel_DB::getConnection(self::TABLE)
                ->select(self::TABLE, '`id`, `parent_id`, `ordering`', null, '`parent_id` ASC, `ordering` ASC');
//    	if (false === $categories) {
//    		return false;
//    	}
        $tree = array();
        for ($i = 0, $size = sizeof($categories); $i < $size; ++$i) {
            if (!isset($tree[$categories[$i]['parent_id']])) {
                $tree[$categories[$i]['parent_id']] = array();
            }
            $tree[$categories[$i]['parent_id']][] = $categories[$i]['id'];
        }

        self::$_structures = $tree;

        $cacheDir = CACHE_DIR . 'database' . DS . 'content_categories' . DS;
        folder_create($cacheDir, 0777);
        $file = $cacheDir . 'structures.php';
        file_put_contents($file, sprintf("<?php\n" .
                        "// date: %s\nreturn %s;\n", date('Y/m/d H:i:s'), var_export($tree, true)));
        //<-- begin debug code -->
        if (true === Flywheel_Config::get('debug')) {
            Flywheel_Debug_Profiler::mark('Write content category structures to cache, never expire.', 'Model.ContentCategory');
        }
        //<-- /end debug code -->                
        chmod($file, 0777);
    }

    /**
     * add instance to pool
     * 
     * @param ContentCategory $contentCategory
     */
    public static function addInstanceToPool(self $contentCategory) {
        self::$instances[$contentCategory->id] = $contentCategory;
    }

    /**
     * get instance from pool
     * @param integer $pk
     * 
     * @return ContentCategory
     * 			false if not found
     */
    public static function getInstanceFromPool($pk) {
        return isset(self::$instances[$pk]) ? self::$instances[$pk] : false;
    }

    /**
     * delete instance from pool
     * @param integer $pk
     */
    public static function deleteInstanceFromPool($pk) {
        unset(self::$instances[$pk]);
    }

    /**
     * clear pool
     */
    public static function clearPool() {
        self::$instances = array();
    }

    /**
     * Get data by condition
     * @param type $where Condition query
     * @return type LIST (null if empty data)
     */
    public function selectList($where = '1=1', $order = null, $limit = null, $key = null) {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        $data = $conn->select(self::TABLE, "*", $where, $order, $limit, $key);
        return $data;
    }

    public function countItem($where = null) {
        $conn = Flywheel_DB::getConnection();
        return $conn->count(self::TABLE, $where);
    }

}