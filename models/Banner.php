<?php

class Banner extends Flywheel_Model
{

    /**
     * id
     * primary
     * auto-increment
     * type : int(10) unsigned
     * @var integer $id
     */
    public $id = 0;
    public $title = "";
    public $link = "";
    public $img = "";
    public $pos = 0;
    public $created_time = 0;
    public $status = 0;

    /**
     * database table name
     */
    const TABLE = 'banner';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
        'fields' => array(
            'banner.`id`', 'banner.`title`', 'banner.`link`', 'banner.`img`', 'banner.`pos`', 'banner.`created_time`', 'banner.`status`'), 
        'columns' => array(
            'id', 'title', 'link', 'img', 'pos', 'created_time', 'status')
        );

    public function retrieveByKey($id = null)
    {
        // Int object
        $item = new self();
        $item->read($id);
        return $item;
    }

    public function read($id = null)
    {
        if (null == $id) {
            $id = $this->id;
        }

        $conn = Flywheel_DB::getConnection(self::TABLE);
        $conn->prepare('SELECT ' . implode(', ', self::$schema['fields']) . ' 
    						FROM ' . self::TABLE . ' WHERE id=? LIMIT 1');
        $conn->bindParam(1, $id, PDO::PARAM_INT);
        $conn->execute();
        $data = $conn->fetch();
        $this->_hydrate($data);
        $this->setNew(false);

        return $data;
    }

    /**
     * Get data by condition
     * @param type $where Condition query
     * @return type LIST (null if empty data)
     */
    public function selectList($where = '1=1', $order = null, $limit = null, $key = null)
    {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        $data = $conn->select(self::TABLE, "*", $where, $order, $limit, $key);
        return $data;
    }
    
    public function countItem($where = null) {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        return $conn->count(self::TABLE, $where);
    }

    
    public function selectOne( $cond = "1=1" ) {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        $data = $conn->selectOne(self::TABLE, $cond);
        return $data;
    }

    public function save()
    {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        if (true === $this->isNew()) {
            $data = $this->toArraySqlName(self::$schema['fields'], self::$schema['columns']);
            $id = $conn->insert(self::TABLE, $data, true);
            if (!$id) {
                return false;
            }
            $this->setNew(false);
        } else { // Update
            $data = array();
            foreach ($this->_modifiedColumns as $modifiedColumn => $value) {
                $data[$modifiedColumn] = $this->$modifiedColumn;
            }
            if (sizeof($data) == 0) {
                return false;
            }
            $success = $conn->update(self::TABLE, $data, '`id`=' . $this->id . ' LIMIT 1');
            if (!$success) {
                return false;
            }
            $id = $this->id;
        }
        return $id;
    }

    public function delete()
    {
        if (Flywheel_DB::getConnection(self::TABLE)->deleteById(self::TABLE, $this->id)) {
            return true;
        }
        return false;
    }

}
