<?php 
/**
 * Users
 *  This class has been auto-generated at 25/08/2010 17:51:12
 * @version		$Id: Users.php 1292 2011-04-07 10:03:06Z mylifeisskidrow $
 * @package		Model
 * @subpackage		order
 * @copyright		Flywheel Team (c) 2010
 */
class AdminMenu extends Flywheel_Model {
    /**
     * id
     * primary
     * auto-increment
     * type : int(4) unsigned
     * @var integer $id
     */
    public $id = 0;

    /**
     * Name
     * type : varchar(255)
     * maxlength : 255
     * @var string $title
     */
    public 	$title = '';

    /**
     * comment
     * type : char(200)
     * maxlength : 200
     * @var string $comment
     */
    public $comment = '';
    
    /**
     * url
     * type : char(200)
     * maxlength : 200
     * @var string $url
     */
    public $url = '';
    
    /**
     * parent_id
     * type : int(11)
     * maxlength : 11
     * @var string $parent_id
     */
    public $parent_id = 0;
    
    /**
     * privilege_id
     * type : int(11)
     * maxlength : 11
     * @var string $privilege_id
     */
    public $privilege_id = 0;
    
    /**
     * privilege_code
     * type : int(11)
     * maxlength : 11
     * @var string $privilege_code
     */
    public $privilege_code = '';
    
    /**
     * status
     * type : int(11)
     * maxlength : 11
     * @var int status
     */
    public $status = 0;
    /**
     * database name
     */
    const DATABASE = 'fkids';
    /**
     * database table name
     */
    const TABLE ='admin_menu';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
        'fields' => array('admin_menu.`id`', 'admin_menu.`title`', 'admin_menu.`url`', 'admin_menu.`parent_id`', 'admin_menu.`comment`', 'admin_menu.`privilege_id`', 'admin_menu.`privilege_code`', 'admin_menu.`status`'),
        'columns' => array('id','title','url','parent_id','comment','privilege_id','privilege_code','status')
    );

    protected static $instances = array();
    /**
     * Read object from database
     * @param int		$id	default null
     * @return Users
     * @throws Flywheel_DB_Exception
     */
    public $db          =   '';
    public $document    =   ''; 
    public $request     =   '';
    public $user        =   '';
    
    public function __construct()
    {
        $this->db = Flywheel_DB::getConnection(self::TABLE);
        $this->document = Flywheel_Factory::getDocument(); 
        $this->request = Flywheel_Factory::getRequest();    
        $this->user       = (array)Flywheel_Authen::getInstance()->getUser(); 
    }
    
    public function read($id = null) {
    	if (null == $id) {
    		$id = $this->id;
    	}
    	
    	$db = Flywheel_DB::getConnection('user_group');
    	$db->prepare('SELECT ' .implode(', ', self::$schema['fields']) .' 
    					FROM ' .self::TABLE .' 
    					WHERE id = ? LIMIT 1');
    	
    	$db->bindParam(1, $id, PDO::PARAM_INT);
    	$db->execute();
    	$data = $db->fetch();
    	
    	if (false === $data) {
    		return false;
    	}
    	$this->_hydrate($data);
    	$this->setNew(false);    	
    	   	
    	return true;
    }
    
    public function selectOne($id = null) {
        if(!$id || $id == 0) return false;
        return $this->db->selectOne(self::TABLE,implode(',',self::$schema['fields']),'id='.$id);
    }
    
    public function selectOneCondition($condition = null) {
        if(!$condition) return false;
        return $this->db->selectOne(self::TABLE,implode(',',self::$schema['fields']),$condition);
    }
    
    public function selectList($condition = null,$order = null,$limit = null,$key = null) {
        if(!$condition) $condition = "1=1";
        return $this->db->select(self::TABLE,implode(',',self::$schema['fields']),$condition,$order,$limit,$key);
    }
    
    public function insert($arrayInfo)
    {
    	$conn = Flywheel_DB::getConnection(self::TABLE);
  		$true = $conn->insert(self::TABLE, $arrayInfo);
   		return $true;
    }
    
    public function update($arrayInfo, $condition)
    {
    	$conn = Flywheel_DB::getConnection(self::TABLE);
  		$affectedRows = $conn->update(self::TABLE, $arrayInfo, $condition);
   		return $affectedRows;
    }
    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {    	
    	$db = Flywheel_DB::getConnection(self::TABLE);
    	if (true === $this->isNew()) 
        {
    		$this->modified_time = time();    		
    		$data = $this->toArray();
    		unset($data['id']);
    		$id = $db->insert(self::TABLE, $data);
    		if (!$id) {
    			return false;			
    		}   
    		$this->id = $id;
    		$this->setNew(false);    		 		
    	} 
        else 
        {
    		//bind array to update
    		$data = array();
    		$columnModifed = 0;
    		unset($this->_modifiedColumns['modified_time']);
    		foreach ($this->_modifiedColumns as $modifiedColumn=>$value) {
    			++$columnModifed;
    			$data[$modifiedColumn] = $this->$modifiedColumn;    			    			    			    			    			    			    			
    		}
    		if ($columnModifed == 0) {
    			return 0;   			
    		}

    		$data['modified_time'] = $this->modified_time = time();
    		$affectedRows = $db->update(self::TABLE, $data, 'id=' .$this->id);
    		if (0 == $affectedRows) {
    			return false;
    		}  		 		
    	}
    	self::addInstanceToPool($this);
    	return $this->id;
    }

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {
        $db = Flywheel_DB::getConnection(self::TABLE);
        return $db->delete(self::TABLE,'id='.$this->id);       
    }
    
    public static function optionSelect($options = array(''),$title_colum = 'title', $value_colum = 'id', $selected='')
    {
        $html = "";
        if($options)
        {
            foreach($options as $row)
            {
                if($selected == $row[$value_colum])
                    $html .= "<option value='".$row[$value_colum]."' selected='selected'>".$row[$title_colum]."</option>";
                else
                    $html .= "<option value='".$row[$value_colum]."'>".$row[$title_colum]."</option>";
            }
        }
        return $html;
    }
    
    public function getListMenu($condition = '1=1')
    {
        if(!$condition) $condition = '1=1';
        $db = Flywheel_DB::getConnection(self::TABLE);
        return $db->select(self::TABLE,self::$schema['fields'],'status=1 AND '.$condition,'id asc');
    }
    
    public function getUserMenu()
    {
        $result = array();
        if(!isset($this->user) || !$this->user['username']) return $result;
        $objP = new Privilege;
        if($this->user['username'] == 'admin') 
            $listPrivilege = $objP->adminPrivlege();
        else 
            $listPrivilege = $objP->getPrivilegeUId($this->user['id']);
        if($listPrivilege)
        {
            $condition = '';
            $list_code = '';
            foreach($listPrivilege['Pcode'] as $item)
                $list_code .= "'".$item."',";
            $condition = 'privilege_code IN('. substr($list_code,0,-1) .')';
            
            $datas = $this->getListMenu($condition);
            if($datas) $result = $datas;
        }
        return $result;
    }
}