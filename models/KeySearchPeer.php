<?php

class KeySearchPeer {

    public static $arr_keywords = array();

    public static function getMulti($filter, $page, $pageSize, $order='id') {
        $keywords = array();
        $where = self::getCondition($filter);
        if ($where !== false) {
            $_c = Flywheel_DB::getConnection(KeySearch::TABLE)
                    ->select(KeySearch::TABLE, implode(', ', KeySearch::$schema['fields']), $where, '`' . $order . '` DESC', ($page - 1) * $pageSize . ', ' . $pageSize);

            if (false !== $_c) {
                for ($i = 0, $size = sizeof($_c); $i < $size; ++$i) {
                    $keywords[] = new KeySearch($_c[$i]);
                }
            }
        }
        return $keywords;
    }

    public static function countKeywords($filter) {

        $where = self::getCondition($filter);
        return Flywheel_DB::getConnection(KeySearch::TABLE)
                ->count(KeySearch::TABLE, $where);
    }

    public static function getCondition($filter) {
        $where = array();
        if (isset($filter['keyword_china'])) {
            $where[] = "`keyword_china` LIKE (\"%{$filter['keyword_china']}%\")";
        }
        if (isset($filter['keyword_vi'])) {
            $where[] = "`keyword_vi` LIKE (\"%{$filter['keyword_vi']}%\")";
        }
        if (isset($filter['is_translated']) && intval($filter['is_translated']) > -1) {
            $where[] = "`is_translated`=" . $filter['is_translated'];
        }
        if ($where)
            return implode(' AND ', $where);
        else
            return '';
    }

    /**
     * Xoa nhieu keyword theo id
     *
     * @param string $listId danh sach ma
     * @return
     */
    static function deleteMultiById($listId) {
        $cond = ' id in(' . $listId . ')';
        if (Flywheel_DB::getConnection(KeySearch::TABLE)->delete(KeySearch::TABLE, $cond)) {
            //$conn = Flywheel_DB::getConnection(KeySearch::TABLE);
            //	$conn->delete(KeySearch::TABLE, 'id in ('.$listId.')');
            return true;
        } else {
            return false;
        }
    }

    /**
     * dịch một đầu vào trung quốc
     *
     * @param string $str 
     * @return
     */
    public static function translate($str) {
        //lấy tất cả các keywordsearch
        $conn1 = Flywheel_DB::getConnection('keyword_search');

        $query = "Select id,keyword_china,keyword_vi,keyword_vi_sms from keyword_search order by weighted desc";

        $conn1->query($query);
        $keywords = $conn1->fetchAll();
        if (!empty($keywords)) {
            foreach ($keywords as $keyword) {
                if (stripos($str, $keyword['keyword_china']) !== false) {
                    //   echo 'tim thay:'.$keyword['keyword_china'];
                    $str = str_replace($keyword['keyword_china'], ' ' . $keyword['keyword_vi'] . ' ', $str);
                }
            }
        }
        // return mb_strtolower($str,'UTF-8'); 
        return $str;
    }

    /**
     * Translate option latin keyword
     * @param type $str string need translate
     * @param type $latin_translate true or false (translate latin or not)
     * @return type String
     */
    public static function translate_option($str, $latin_translate = true) {		
        //lấy tất cả các keywordsearch
        $conn1 = Flywheel_DB::getConnection('keyword_search');
        $query = "Select id,keyword_china,keyword_vi,keyword_vi_sms from keyword_search order by weighted desc";
        
        $conn1->query($query);
        $keywords = $conn1->fetchAll(); 
        
        if(!empty($keywords)) {
            foreach($keywords as $keyword) {
                if(!$latin_translate & !preg_match("/^\p{Han}+/u", $keyword['keyword_china'])) {
                    continue;
                }
                if(stripos($str,$keyword['keyword_china']) !== false ) {
                    $str = str_replace($keyword['keyword_china'],' '.$keyword['keyword_vi'].' ',$str);
                }
            }
        }
        return $str; 
    }
    
    public static function checkExist($china='', $id=0) {
        $where = 'keyword_china="' . $china . '"';
        if (intval($id) > 0)
            $where.=' and id<>' . $id;

        $count = Flywheel_DB::getConnection(KeySearch::TABLE)
                ->count(KeySearch::TABLE, $where);
        if (intval($count) > 0)
            return true;
        return false;
    }

    public static function translate_alias($str, $show_old=true) {
        // echo 'chua dich:'.$str    .'<br/>';
        //lấy tất cả các keywordsearch
        $old = $str;
        $is_translated = false;
        if (empty(self::$arr_keywords)) {
            $conn1 = Flywheel_DB::getConnection('keyword_search');

            $query = "Select * from keyword_search order by weighted desc";

            $conn1->query($query);
            self::$arr_keywords = $conn1->fetchAll();
        }
        $keywords = self::$arr_keywords;

        $title = array();
        $new_title = '';
        if (!empty($keywords)) {
            foreach ($keywords as $keyword) {
                if (stripos($str, $keyword['keyword_china']) !== false) {
                    //echo 'tim thay:'.$keyword['alias_vi'].' -- '.$keyword['weighted'].'<br>';
                    // $str=str_replace($keyword['alias_china'],' '.$keyword['alias_vi'].' ',$str);
                    $str = str_replace($keyword['keyword_china'], '', $str);
                    //sắp xếp lại theo các trọng số
                    $title[$keyword['weighted']][] = $keyword['keyword_vi'];
                    $is_translated = true;
                }
            }
            krsort($title);
            //buid tiêu đề mới :

            if (empty($title))
                return $old;
            foreach ($title as $weighted => $parses) {
                if (!empty($parses))
                    $new_title.=' ' . implode(' ', $parses);
            }
            $new_title.=' ' . $str;
            $new_title = ucfirst(trim($new_title));
        }
        // return mb_strtolower($str,'UTF-8'); 
        if ($is_translated) {
            //    echo $new_title;
            return $new_title . ($show_old == true ? ' <font style="color:red">(' . $old . ')</font> ' : '');
            //return $new_title.' ('.$old.') ';     
        } else {
            //   echo $str;   
            return $old;
        }
    }

    public static function getKeywordsByUser($user_name='') {
        $conn1 = Flywheel_DB::getConnection('keyword_search');

        $query = 'Select * from keyword_search where FROM_UNIXTIME(created_at,"%Y-%m-%d")="' . date('Y-m-d') . '" and created_by="' . $user_name . '" order by weighted desc';

        $conn1->query($query);
        return $conn1->fetchAll();
    }

}