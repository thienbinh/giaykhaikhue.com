<?php 
/**
 * Login
 * @author              trung
 * @package		Model
 * @copyright		tpc team (c)
 */
class Login extends Flywheel_Model {
	
   function __construct($data = null) {
      parent::__construct($data);
   }
   
   public function save(){}
   public function delete(){}
   
   public static function login($username,$password){
      $authen	= Flywheel_Authen::getInstance();
      $mes = "";
      if ('' == $username || '' == $password) {
         $mes = 'Tên đăng nhập hoặc mật khẩu trống';
      } elseif (false === $authen->authenticate($username, $password)) {
         $mes = 'Tên đăng nhập hoặc mật khẩu không đúng!';
      } else {
         $user = new Users(Users::findByUsername($username));
         if($user->checkLocked()==true){
            $mes = 'Tài khoản này đang bị khóa!';
         }
      }
      return $mes;
   }
    
   
   /**
    * todo        kiem tra xem da login chua,neu chua login thi quay lai trang dang nhap (co luu lai duong dan cu)
    * @author              trung
    * @package		Model
    * @copyright		tpc team (c)
    */
   public static function checkLogin(){
      $check = Flywheel_Authen::getInstance();
      $request = Flywheel_Factory::getRequest();
      if(!$check->isAuthenticated()){
         $url = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
         
         $url = base64_encode($url);
         $request->redirect(Flywheel_Factory::getDocument()->getPublicPath()."dang-nhap?url=".$url);
      }
   }
   
    public static function logout(){
       return Flywheel_Authen::getInstance()->logout();
    }
    
}
?>