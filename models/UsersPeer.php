<?php

class UsersPeer {

    public static function generateSalt($prefix = '') {
        return sha1($prefix . uniqid(time(), true));
    }

    /**
     * Compare password input password with encryted pass included salt string	 
     * @param string	$inputPass
     * @param string	$encrytedPass
     * 
     * @return boolean
     */
    public static function comparePassword($inputPass, $encrytedPass) {
        $salt = substr($encrytedPass, 0, 40);
        $md5Pass = substr($encrytedPass, 40, 32);

        return (md5($salt . $inputPass) == $md5Pass);
    }

    /**
     * check exists username
     * 
     * @param string	$username
     * @return boolean
     */
    public static function checkExistsUsername($username) {
        if (false !== Users::findByUsername($username)) {
            return true;
        }

        return false;
    }

    /**
     * check exists email by verified status
     * 
     * @param string	$email
     * @param int		$status. 1: active, 0: not active, 10 all
     * @return boolean
     */
    public static function checkExistsEmail($email, $user = null, $status = 1) {
        $userId = 0;
        if (null != $user) {
            if ($user instanceof Users) {
                $userId = $user->id;
            } else if (is_integer($user)) {
                $userId = $user;
            }
        }

        $where = array();
        $where[] = 'email="' . $email . '"';
        if (1 == $status || 0 == $status) {
            $where[] = 'status=' . $status;
        }
        if (0 != $userId) {
            $where[] = 'user_id=' . $userId;
        }
        $where = implode(' AND ', $where);
        return (boolean) Flywheel_DB::getConnection(UserEmail::TABLE)->count(UserEmail::TABLE, $where);
    }

    /**
     * Get list user
     */
    public static function getUserUtil($where=NULL, $limit=NULL) {
        $conn = Flywheel_DB::getConnection('users');
        $cond = "";
        $lm = "";

        if (is_array($where)) {
            $cond = implode(" ", $where);
        }
        if (NULL != $limit) {
            $lm = "LIMIT $limit";
        }
        $sql = "SELECT " . implode(', ', Users::$schema['fields']) . " 
                        FROM " . Users::TABLE . " 
                           WHERE `is_delete`=0 $cond $lm";
        $conn->prepare($sql);
        $conn->execute();
        $data = $conn->fetchAll();
        if (NULL != $data) {
            return $data;
        }
        else
            return FALSE;
    }

    public static function getTotalUser($where=NULL) {
        $conn = Flywheel_DB::getConnection('users');
        $cond = "";
        if ($where != NULL and is_array($where)) {
            $cond = implode(" ", $where);
        }
        $sql = "SELECT count(`id`) as `total` FROM " . Users::TABLE . " WHERE `is_delete`=0 $cond";
        $conn->prepare($sql);
        $conn->execute();

        $data = $conn->fetch();
        if ($data and !empty($data)) {
            return $data['total'];
        }
        return false;
    }

    public static function getTotalUserNew($where=NULL) {
        $conn = Flywheel_DB::getConnection('users');
        $cond = "";
        if ($where != NULL and is_array($where)) {
            $cond = implode(" and ", $where);
        }
        if ($cond) {
            $cond = ' and ' . $cond;
        }

        $sql = "SELECT count(`id`) as `total` FROM " . Users::TABLE . " WHERE `is_delete`=0 $cond";
        $conn->prepare($sql);
        $conn->execute();

        $data = $conn->fetch();
        if ($data and !empty($data)) {
            return $data['total'];
        }
        return false;
    }

    public static function setIsStaff($strIds) {
        $conn = Flywheel_DB::getConnection('users');
        $cond = "";
        $sql = "UPDATE " . Users::TABLE . " SET `is_staff`=1,`type`=3 WHERE `id` IN ($strIds)";
        if ($conn->query($sql)) {
            return true;
        }
        return false;
    }

    public static function getMulti($filter, $page, $pageSize, $order='id') {

        $where = self::getCondition($filter);

        $_c = Flywheel_DB::getConnection(Users::DATABASE)
                ->select(Users::TABLE, implode(', ', Users::$schema['fields']), $where, '`' . $order . '` DESC', ($page - 1) * $pageSize . ', ' . $pageSize);
        return $_c;
    }

    public static function getCondition($where) {
        return implode(' AND ', $where);
    }

}