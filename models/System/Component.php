<?php 
/**
 * System_Component
 *  This class has been auto-generated at 07/11/2010 03:17:06
 * @version		$Id: build_models.php 317 2010-11-05 19:07:57Z mylifeisskidrow@gmail.com $
 * @package		Model
 * @subpackage	System
 * @copyright		VCCorp (c) 2010
 */
class System_Component extends Flywheel_Model {
    /**
     * id
     * primary
     * auto-increment
     * type : int(4) unsigned
     * @var integer $id
     */
    public $id = 0;

    /**
     * component
     * type : varchar(255)
     * maxlength : 255
     * @var string $component
     */
    public $component = '';

    /**
     * controller
     * type : varchar(255)
     * maxlength : 255
     * @var string $controller
     */
    public $controller = '';

    /**
     * application
     * type : varchar(255)
     * maxlength : 255
     * @var string $application
     */
    public $application = '';

    /**
     * config
     * type : text
     * maxlength : 
     * @var string $config
     */
    public $config = '';

    /**
     * created_by
     * type : int(4) unsigned
     * @var integer $created_by
     */
    public $created_by = 0;

    /**
     * created_time
     * type : int(10) unsigned
     * @var integer $created_time
     */
    public $created_time = 0;

    /**
     * database name
     */
    const DATABASE = 'order';
    /**
     * database table name
     */
    const TABLE ='system_component';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
                    'fields' => array('system_component.`id`', 'system_component.`component`', 'system_component.`controller`', 'system_component.`application`', 'system_component.`config`', 'system_component.`created_by`', 'system_component.`created_time`'),
                    'columns' => array('id','component','controller','application','config','created_by','created_time'));

    /**
     * Read object from database
     * @param int		$id	default null
     * @return System_Component
     * @throws Flywheel_DB_Exception
     */
    public function read($id = null) {}

    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {}

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {}


}