<?php

/**
 * System_Config
 *  This class has been auto-generated at 07/11/2010 03:17:06
 * @version		$Id: build_models.php 317 2010-11-05 19:07:57Z mylifeisskidrow@gmail.com $
 * @package		Model
 * @subpackage	System
 * @copyright		VCCorp (c) 2010
 */
class System_Config extends Flywheel_Model {

    /**
     * id
     * type : int(11)
     * @var integer $id
     */
    public $id = 0;
    /**
     * key
     * type : varchar(255)
     * maxlength : 255
     * @var string $key
     */
    public $key = '';
    /**
     * value
     * type : text
     * maxlength : 
     * @var string $value
     */
    public $value = '';

    /**
     * database table name
     */
    const TABLE ='system_config';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
        'fields' => array('system_config.`id`', 'system_config.`key`', 'system_config.`value`'),
        'columns' => array('id', 'key', 'value'));
    public static $_instance = array();

    /**
     * Read object from database
     * @param int		$id	default null
     * @return System_Config
     * @throws Flywheel_DB_Exception
     */
    public function read($id = null) {
        if (null == $id) {
            $id = $this->id;
        }

        $db = Flywheel_DB::getConnection(self::TABLE);
        $db->prepare('SELECT ' . implode(', ', self::$schema['fields']) . ' 
    					FROM ' . self::TABLE . ' 
    					WHERE id = ? LIMIT 1');
        $db->bindParam(1, $id, PDO::PARAM_INT);
        $db->execute();
        $data = $db->fetch();

        if (false === $data) {
            return false;
        }

        $this->_hydrate($data);
        $this->setNew(false);
        return true;
    }

    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {
        $db = Flywheel_DB::getConnection(self::TABLE);
        if (true === $this->isNew()) {
            $data = $this->toArraySqlName(self::$schema['fields'], self::$schema['columns']);
            $id = $db->insert(self::TABLE, $data);
            if (!$id) {
                return false;
            }
            $this->id = $id;
            $this->setNew(false);
        } else {
            //bind array to update
            $data = $this->toArray();
            $affectedRows = $db->update(self::TABLE, $data, 'id=' . $this->id);
            if (0 == $affectedRows) {
                return false;
            }
        }
        return $this->id;
    }

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {
        
    }

    /**
     * Get config  from database
     * @param string 	$config
     * @param boolean	$needObj return object or value
     * 
     * @return System_Config|string
     */
    public static function setting($config, $needObj = false) {
        self::getAllConfig();
        if (!isset(self::$_instance[$config])) {
            if (true === $needObj) {
                $obj = new self();
                $obj->key = $config;
                self::$_instance[$config] = $obj;
            } else {
                return false;
            }
        }

        return (true === $needObj) ? self::$_instance[$config] :
                self::$_instance[$config]->value;
    }

    public static function getAllConfig() {
        if (self::$_instance == null) {
            $db = Flywheel_DB::getConnection(self::TABLE);
            $db->prepare('SELECT ' . implode(', ', self::$schema['fields']) . '
    					FROM ' . self::TABLE);
            $db->execute();
            $datas = $db->fetchAll('key');
            if (false == $datas) {
                return false;
            }
            $configs = array();
            foreach ($datas as $data) {
                $config = new self();
                $config->_hydrate($data);
                $config->setNew(false);
                $configs[$config->key] = $config;
            }
            self::$_instance = $configs;
        }

        return self::$_instance;
    }

    public function readConfigByKey($key = "", $value = "") {
        if (empty($key)) {
            $key = "id";
        }

        $db = Flywheel_DB::getConnection(self::TABLE);
        $db->prepare('SELECT ' . implode(', ', self::$schema['fields']) . ' 
    					FROM ' . self::TABLE . "
    					WHERE `{$key}` = ? LIMIT 1");
        $db->bindParam(1, $value, PDO::PARAM_INT);
        $db->execute();
        $data = $db->fetch();

        if (false === $data) {
            return false;
        }

        $this->_hydrate($data);
        $this->setNew(false);
        return true;
    }
}