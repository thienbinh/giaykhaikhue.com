<?php 
/**
 * System_ModuleComponent
 *  This class has been auto-generated at 07/11/2010 03:17:06
 * @version		$Id: build_models.php 317 2010-11-05 19:07:57Z mylifeisskidrow@gmail.com $
 * @package		Model
 * @subpackage	System
 * @copyright		VCCorp (c) 2010
 */
class System_ModuleComponent extends Flywheel_Model {
    /**
     * id
     * primary
     * type : int(4)
     * @var integer $id
     */
    public $id = 0;

    /**
     * module_id
     * type : int(4) unsigned
     * @var integer $module_id
     */
    public $module_id = 0;

    /**
     * component_name
     * type : varchar(255)
     * maxlength : 255
     * @var string $component_name
     */
    public $component_name = '';

    /**
     * position
     * type : varchar(255)
     * maxlength : 255
     * @var string $position
     */
    public $position = '';

    /**
     * ordering
     * type : int(11)
     * @var integer $ordering
     */
    public $ordering = 0;

    /**
     * database name
     */
    const DATABASE = 'order';
    /**
     * database table name
     */
    const TABLE ='system_module_component';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
                    'fields' => array('system_module_component.`id`', 'system_module_component.`module_id`', 'system_module_component.`component_name`', 'system_module_component.`position`', 'system_module_component.`ordering`'),
                    'columns' => array('id','module_id','component_name','position','ordering'));

    /**
     * Read object from database
     * @param int		$id	default null
     * @return System_ModuleComponent
     * @throws Flywheel_DB_Exception
     */
    public function read($id = null) {}

    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {}

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {}


}