<?php 
/**
 * System_Module
 *  This class has been auto-generated at 07/11/2010 03:17:06
 * @version		$Id: build_models.php 317 2010-11-05 19:07:57Z mylifeisskidrow@gmail.com $
 * @package		Model
 * @subpackage	System
 * @copyright		VCCorp (c) 2010
 */
class System_Module extends Flywheel_Model {
    /**
     * id
     * primary
     * auto-increment
     * type : int(4) unsigned
     * @var integer $id
     */
    public $id = 0;

    /**
     * name
     * type : varchar(255)
     * maxlength : 255
     * @var string $name
     */
    public $name = '';

    /**
     * file
     * type : varchar(255)
     * maxlength : 255
     * @var string $file
     */
    public $file = '';

    /**
     * status
     * type : tinyint(1)
     * @var integer $status
     */
    public $status = 1;

    /**
     * application
     * type : varchar(255)
     * maxlength : 255
     * @var string $application
     */
    public $application = '';

    /**
     * config
     * type : text
     * maxlength : 
     * @var string $config
     */
    public $config = '';

    /**
     * created_time
     * type : int(11)
     * @var integer $created_time
     */
    public $created_time = 0;

    /**
     * modified_time
     * type : int(11)
     * @var integer $modified_time
     */
    public $modified_time = 0;

    /**
     * database name
     */
    const DATABASE = 'order';
    /**
     * database table name
     */
    const TABLE ='system_module';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
                    'fields' => array('system_module.`id`', 'system_module.`name`', 'system_module.`file`', 'system_module.`status`', 'system_module.`application`', 'system_module.`config`', 'system_module.`created_time`', 'system_module.`modified_time`'),
                    'columns' => array('id','name','file','status','application','config','created_time','modified_time'));

    /**
     * Read object from database
     * @param int		$id	default null
     * @return System_Module
     * @throws Flywheel_DB_Exception
     */
    public function read($id = null) {}

    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {}

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {}


}