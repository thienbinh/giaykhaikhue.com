<?php
class Paging
{
/**
* @desc  Ham khoi tao
*/
    public $pub_path = '';
    public $base_url = '';
    function __construct()
    {
        $this->pub_path = Flywheel_Factory::getDocument()->getPublicPath();
        $this->base_url = Flywheel_Factory::getDocument()->getBaseUrl();
    }
    
    public static function pagingAjax($total, $perPage, $currentpage, $numPageShow = 5, $page_name='page')
    {
    	$content='<div class="paginator"><ul class="paging" id="ajax-paging">';
        //tong trang
        $totalpage = ceil($total/$perPage);
        if ($totalpage < 2) { // Return neu chi co 1 trang
        	return '';
        }
        //trang hien tai
        $currentpage = round($currentpage);
        if($currentpage <= 0 || $currentpage > $totalpage)
        {
            $currentpage = 1;
        }
        
        if($currentpage > ($numPageShow/2))
        {
            $startpage = $currentpage - floor($numPageShow/2);
            if($totalpage - $startpage < $numPageShow)
            {
                $startpage = $totalpage - $numPageShow + 1;
            }
        }
        else
        {
            $startpage = 1;
        }
        if($startpage < 1)
        {
            $startpage = 1;
        }        
        //Link den trang truoc
        if($currentpage > 1)
        {
            $content.= '<LI><A rel="'.($currentpage-1).'" title="Trang trước" HREF = "javascript:;" >&laquo; </A></LI>';
        }
        //Danh sach cac trang        
        if($startpage > 1)
        {
        	$content.= '<li><a rel="1" title="Trang 1" href= "javascript:;">1</a></li>';            
        }
        for($i = $startpage; $i <= $startpage + $numPageShow - 1 && $i <= $totalpage; $i++)
        {
            if($i == $currentpage)
            {
                $content.= '<li><span>'.$i.'</span></li>';
            }
            else 
            {
                $content .= '<li><a rel="'.$i.'" title="Trang '.$i.'" href= "javascript:;">'.$i.'</a></li>';
            }
        }
        if($i == $totalpage)
        {
            $content .= '<li><a rel="'.$totalpage.'" title="Trang '.$totalpage.'" href= "javascript:;">'.$totalpage.'</a></li>';
        }
        else
        if($i < $totalpage)
        {
            $content .= '...<li><a rel="'.$totalpage.'" title="Trang '.$totalpage.'" href= "javascript:;">'.$totalpage.'</a></li>';
        }        
        //Trang sau
        if($currentpage < $totalpage)
        {
            $content .= '<LI><A rel="'.($currentpage+1).'" title="Trang sau" HREF = "javascript:;">&raquo;</A></LI>';
        }         
        return $content.'</ul></div>';        
    }
    public static function paging($total, $perPage, $currentpage, $numPageShow = 5, $action, $char = '?')
    {
        $base_url = $action;
    	$content='<ul class="paging" style="margin-top:0;">';
        //tong trang
        $totalpage = ceil($total/$perPage);
        if ($totalpage < 2) { // Return neu chi co 1 trang
        	return '';
        }
        //trang hien tai
        $currentpage = round($currentpage);
        if($currentpage <= 0 || $currentpage > $totalpage)
        {
            $currentpage = 1;
        }
        
        if($currentpage > ($numPageShow/2))
        {
            $startpage = $currentpage - floor($numPageShow/2);
            if($totalpage - $startpage < $numPageShow)
            {
                $startpage = $totalpage - $numPageShow + 1;
            }
        }
        else
        {
            $startpage = 1;
        }
        if($startpage < 1)
        {
            $startpage = 1;
        }        
        //Link den trang truoc
        if($currentpage > 1) {
            $content.= '<li><a rel="'.($currentpage-1).'" title="Trang trước" href = "'.$base_url.$char.'p='.($currentpage-1).'" >Trước </A></LI>';
        }
        //Danh sach cac trang        
        if($startpage > 1)
        {
        	$content.= '<li><a rel="1" title="Trang 1" href= "'.$base_url.'1">1</a></li>';            
        }
        for($i = $startpage; $i <= $startpage + $numPageShow - 1 && $i <= $totalpage; $i++)
        {
            if($i == $currentpage){
                $content.= '<li><span class="active">'.$i.'</span></li>';
            }
            else{
                $content .= '<li><a rel="'.$i.'" title="Trang '.$i.'" href= "'.$base_url.$char.'p='.$i.'">'.$i.'</a></li>';
            }
        }
        if($i == $totalpage){
            $content .= '<li><a rel="'.$totalpage.'" title="Trang '.$totalpage.'" href= "javascript:;">'.$totalpage.'</a></li>';
        } else if ($i < $totalpage) {
            $content .= '...<li><a rel="'.$totalpage.'" title="Trang '.$totalpage.'" href= "javascript:;">'.$totalpage.'</a></li>';
        }        
        //Trang sau
        if($currentpage < $totalpage) {
            $content .= '<li><a rel="'.($currentpage+1).'" title="Trang sau" href = "'.$base_url.$char.'p='.($currentpage+1).'">Sau </A></LI>';
        }         
        return $content.'</ul>';        
    }
	
    static function sub_string($url)
    {
        $index = strpos($url,'.htm');
        return substr($url,0,$index+4);
    }
}