<?php

class ContentPeer {

    public static function getContents($filter, $page, $pageSize, $order='modified_time') {
        $contents = array();
        $where = array();
        if (isset($filter['keyword'])) {
            $where[] = "(`title` LIKE \"%{$filter['keyword']}%\" OR `content` LIKE \"%{$filter['keyword']}%\")";
        }
        if (isset($filter['state'])) {
            if (1 == $filter['state']) {
                $where[] = '`status`=1';
            } else {
                $where[] = '`status`=0';
            }
        }
        if (isset($filter['cat_id'])) {
            $cats = array();
            ContentCategory::getTreeScalar($filter['cat_id'], $cats, true, false);
            if (sizeof($cats) == 1) {
                $where[] = '`category_id` = ' . $filter['cat_id'];
            } else {
                $where[] = '`category_id` IN (' . implode(', ', $cats) . ')';
            }
        }
        $_c = Flywheel_DB::getConnection(Content::TABLE)
                ->select(Content::TABLE, implode(', ', Content::$schema['fields']), implode(' AND ', $where), '`' . $order . '` DESC', ($page - 1) * $pageSize . ', ' . $pageSize);

        if (false !== $_c) {
            for ($i = 0, $size = sizeof($_c); $i < $size; ++$i) {
                $contents[] = new Content($_c[$i]);
            }
        }
        return $contents;
    }

    public static function countContents($filter) {
        $where = array();
        if (isset($filter['keyword'])) {
            $where[] = "(`title` LIKE \"%{$filter['keyword']}%\" OR `content` LIKE \"%{$filter['keyword']}%\")";
        }
        if (isset($filter['state'])) {
            if (1 == $filter['state']) {
                $where[] = '`status`=1';
            } else {
                $where[] = '`status`=0';
            }
        }
        if (isset($filter['cat_id'])) {
            $cats = array();
            ContentCategory::getTreeScalar($filter['cat_id'], $cats, true, false);
            if (sizeof($cats) == 1) {
                $where[] = '`category_id` = ' . $filter['cat_id'];
            } else {
                $where[] = '`category_id` IN (' . implode(', ', $cats) . ')';
            }
        }
        return Flywheel_DB::getConnection(Content::TABLE)
                ->count(Content::TABLE, implode(' AND ', $where));
    }

    public static function getContents1($filter) {
        $contents = array();
        $where = array();
        if (isset($filter['keyword'])) {
            $where[] = "`title` LIKE (\"%{$filter['keyword']}%\")";
        }
        if (isset($filter['state'])) {
            if (1 == $filter['state']) {
                $where[] = '(`publish_from` = 0 OR `publish_from` <= ' . time() . ')
								AND (`publish_to` = 0 OR `publish_to` >= ' . time() . ')';
            } else {
                $where[] = '(`publish_from` > ' . time() . ')
								OR (`publish_to` > 0 AND `publish_to` < ' . time() . ')';
            }
        }
        if (isset($filter['cat_id'])) {
            $cats = array();
            ContentCategory::getTreeScalar($filter['cat_id'], $cats, true, false);
            if (sizeof($cats) == 1) {
                $where[] = '`category_id` = ' . $filter['cat_id'];
            } else {
                $where[] = '`category_id` IN (' . implode(', ', $cats) . ')';
            }
        }
        $_c = Flywheel_DB::getConnection(Content::TABLE)
                ->select(Content::TABLE, implode(', ', Content::$schema['fields']), implode(' AND ', $where), '`ordering`');

        if (false !== $_c) {
            for ($i = 0, $size = sizeof($_c); $i < $size; ++$i) {
                $contents[] = new Content($_c[$i]);
            }
        }
        return $contents;
    }

    public static function getNewContents() {
        $contents = array();
        $where = array();
        $cats = array();
        ContentCategory::getTreeScalar(5, $cats, true, false);
        if (sizeof($cats) == 1) {
            $where[] = '`category_id` = 5';
        } else {
            $where[] = '`category_id` IN (' . implode(', ', $cats) . ')';
        }
        $_c = Flywheel_DB::getConnection(Content::TABLE)
                ->select(Content::TABLE, implode(', ', Content::$schema['fields']), implode(' AND ', $where), '`modified_time` DESC', 6);

        if (false !== $_c) {
            for ($i = 0, $size = sizeof($_c); $i < $size; ++$i) {
                $contents[] = new Content($_c[$i]);
            }
        }
        return $contents;
    }

}