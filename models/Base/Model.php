<?php
class Model{
    protected $db;
    protected $database;
    protected $table;
    protected $schema = array();
    protected $primary_key = '';
    function __construct($configs){
        if(!isset($configs['database'])){
            $configs['database'] = 'alimama';
        }
        $this->database = $configs['database'];
        $this->table = $configs['table'];
        $this->db = Flywheel_DB::getConnection($this->database);
    }
    public function count($condition){
        return $this->db->count($this->table, $condition);
    }
    public function selectList($condition = null,$order = null,$limit = null,$key = null) {
        if($condition==null)$condition= "1=1";

        return $this->db->select($this->table,$this->schema['fields'],$condition,$order,$limit,$key);
    }
    public function selectOne($id = null) {
        if(!$id || $id == 0) return false;
        return $this->db->selectOne($this->table,$this->schema['fields'],'id='.$id);
    }
    public function insert($array){
        return $this->db->insert($this->table, $array);
    }

    public function insertMutil($array){
        $this->db->insertMulti($this->table,$array);
        return true;
    }

    public function update($array, $condition){
        return $this->db->update($this->table, $array, $condition);
    }

    public function delete() {
        return $this->db->delete($this->table,'id='.$this->id);
    }
}
?>