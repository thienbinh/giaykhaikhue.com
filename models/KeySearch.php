<?php

require_once FLYWHEEL_UTIL .'StringUtil.php';

class KeySearch extends Flywheel_Model{
	/**
	 * Primary key. Auto increment
	 * @var integer $id
	 * @param int(4) unsigned
	 */
	public $id =0;
	
	/**
	 * Keyword china
	 * @var integer $keyword_china
	 * @param string
	 */
	public $keyword_china  = '';
	public $keyword_vi     = '';
    public $keyword_vi_sms = '';
    public $full_text_search = '';
    public $weighted = -1;
    public $is_translated = 0;
    public $created_by = '';
    public $created_at = '';
    
    /**
     * database name
     */
    const DATABASE = 'order';
    /**
     * database table name
     */
    const TABLE ='keyword_search';
    
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
		
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
         'fields' => array('keyword_search.`id`', 
                            'keyword_search.`keyword_china`',
                            'keyword_search.`keyword_vi`',
                            'keyword_search.`keyword_vi_sms`',
                            'keyword_search.`created_by`',
                            'keyword_search.`created_at`',
                            'keyword_search.`full_text_search`','keyword_search.`is_translated`','keyword_search.`weighted`'),
         'columns' => array('id', 'keyword_china', 'keyword_vi','keyword_vi_sms','full_text_search','is_translated','weighted','created_by','created_at'));
    
    public function getKeyBySuggestion($key) {
        $sql = "SELECT ".implode(', ', KeySearch::$schema['fields']) 
                ." FROM ".KeySearch::TABLE." WHERE full_text_search LIKE '$key%' LIMIT 1, 10";
                
        $conn = Flywheel_DB::getConnection(self::TABLE);
        $conn->prepare($sql);
        $conn->execute();
        $data = $conn->fetchAll();
        if (NULL!= $data) {
            return $data;
        }
        else return false;
    }
    public function read($id = null) {
        if (null == $id) {
            $this->id = $id;
        }
        
        $conn = Flywheel_DB::getConnection(self::TABLE);
        $conn->prepare('SELECT ' . implode(', ', self::$schema['fields']) .' 
                            FROM ' .self::TABLE .' WHERE id = ? LIMIT 1');
        $conn->bindValue(1, $id, PDO::PARAM_INT);
        $conn->execute();
        $data = $conn->fetch();
        if (false == $data) {
            return false;            
        }
              
        $this->_hydrate($data);
        $this->setNew(false); 
        return true;    
    }
    public function save() {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        if (true == $this->isNew()) {
            $data = array();
            for ($i = 1, $size = sizeof(self::$schema['columns']); $i < $size; ++$i) {
                $column = self::$schema['columns'][$i];
                $data[$column] = $this->$column;                                
            }  
            $id = $conn->insert(self::TABLE, $data);
            if (!$id) {
                return false;
            }
            $this->id = $id;
                
        } else {
            //bind array to update
            $data = array();            
            foreach ($this->_modifiedColumns as $modifiedColumn=>$value) {  
                   $data[$modifiedColumn] = $this->$modifiedColumn;                                                                                                                
            }
            if (sizeof($data) == 0) {
                return false;                
            }
            
            $affectedRows = $conn->update(self::TABLE, $data, '`id`=' .$this->id .' LIMIT 1');
            if (!$affectedRows) {
                return false;
            }
      
            $this->_modifiedColumns = array();
        }
        $this->setNew(false);
        return $this->id;
    }
    
    public function delete() {
        if (Flywheel_DB::getConnection(self::TABLE)->deleteById(self::TABLE, $this->id)) {
            $conn = Flywheel_DB::getConnection(self::TABLE_CONNECT);
            $conn->delete(self::TABLE_CONNECT, 'id = '.$this->id);
            return true;                                   
        }        
        return false;
    }
	/**
     * database table schema
     * @static
     * @var array $schema
     */
	  public function getKeyTranslate($title) {
        $sql = "SELECT keyword_china,keyword_vi,keyword_vi_sms FROM ".KeySearch::TABLE;
                
        $conn = Flywheel_DB::getConnection(self::TABLE);
        $conn->prepare($sql);
        $conn->execute();
        $data = $conn->fetchAll();
        if (NULL!= $data) {           
            foreach($data as $item)
			{				
				if(stripos($title,$data['keyword_china'])!== false)
				{					
					$title = str_replace($data['keyword_china'],$data['keyword_vi'],$title);                
				}
				
			}
			return $title;
        }
        else return false;
    }
    
    /**
     * Translate Chinese to Vietnamese
    */
    public function translate_vi($title) {
        
        $conn = Flywheel_DB::getConnection('keyword_search');
        $query = "Select keyword_china,keyword_vi,keyword_vi_sms from keyword_search";       
        $conn->query($query);
        $items = $conn->fetchAll();        
        if($items)
        foreach($items as $item)
        {
            if(stripos($title,$item['keyword_china'])!== false)
            {
                //echo "C? t? ".$item['keyword_china'].'<br>'; 
                $title = str_replace($item['keyword_china'],$item['keyword_vi'],$title);                
            }
        }
        return $title;
    }
    
    static public function ChinaToVietNammess($title)
    {
        $title = trim($title);
        if(!$title) return false;
        $conn = Flywheel_DB::getConnection('keyword_search');
        $query = "Select keyword_china,keyword_vi,keyword_vi_sms from keyword_search";       
        $conn->query($query);
        $items = $conn->fetchAll();        
        if($items)
        foreach($items as $item)
        {
            if(stripos($title,$item['keyword_china'])!== false)
            {
                //echo "C? t? ".$item['keyword_china'].'<br>'; 
                $title = str_replace($item['keyword_china'],' '.$item['keyword_vi'],$title);                
            }
        }
        return $title;
    }
}