<?php 
/**
 * UserAcl
 *  This class has been auto-generated at 07/10/2010 12:04:57
 * @version		$Id: build_models.php 4555 2010-09-27 04:59:33Z hieult $
 * @package		Model
 * @subpackage	order
 * @copyright		VCCorp (c) 2010
 */
class UserAcl extends Flywheel_Model {
    /**
     * id
     * primary
     * auto-increment
     * type : int(4) unsigned
     * @var integer $id
     */
    public $id = 0;

    /**
     * user_id
     * type : int(4) unsigned
     * @var integer $user_id
     */
    public $user_id = 0;

    /**
     * value
     * type: tinyint(1) unsigned
     * $var integer $value
     */
    public $value = 0;

    /**
     * acl_alias
     * type : varchar(255)
     * maxlength : 255
     * @var string $acl_alias
     */
    public $acl_alias = '';

    /**
     * database name
     */
    const DATABASE = 'order';
    /**
     * database table name
     */
    const TABLE ='user_acl';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
                    'fields' => array('user_acl.`id`','user_acl.`value`', 'user_acl.`user_id`', 'user_acl.`acl_alias`'),
                    'columns' => array('id','value', 'user_id','acl_alias'));

    public static $aclInstance = array();

        /**
     * Read object from database
     * @param int		$id	default null
     * @return UserAcl
     * @throws Flywheel_DB_Exception
     */
    public function read($id = null) {
        if (null == $id) {
    		$id = $this->id;
    	}
    	
    	$db = Flywheel_DB::getConnection('user_acl');
    	$db->prepare('SELECT ' .implode(', ', self::$schema['fields']) .' 
    					FROM ' .self::TABLE .' 
    					WHERE id = ? LIMIT 1');
    	$db->bindParam(1, $id, PDO::PARAM_INT);
    	$db->execute();
    	$data = $db->fetch();
    	
    	if (false === $data) {
    		return false;
    	}
    	
    	$this->_hydrate($data);
    	$this->setNew(false);    	  	
    	return true;
    }

    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {
        $db = Flywheel_DB::getConnection('user_acl');
    	if (true === $this->isNew()) {   		
    		$data = $this->toArray();
    		unset($data['id']);
    		$id = $db->insert(self::TABLE, $data);
    		if (!$id) {
    			return false;			
    		}   
    		$this->id = $id;
    		$this->setNew(false);    		 		
    	} else {
    		//bind array to update
    		$data = $this->toArray();
    		$affectedRows = $db->update(self::TABLE, $data, 'id=' .$this->id);
    		if (0 == $affectedRows) {
    			return false;
    		}  		 		
    	}
    	return $this->id;
    }

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {}


	/**
     * Retrieve user_acl by primary key
     *
     * @param 		string $pk
     * @return 		user_acl false if pk not exists
     */
    public static function retrieveByPk($pk) {
    	$user_acl = new self();
    	if ($user_acl->read($pk)) {
    		return $user_acl;
    	}
    	return false;
    }

    /**
     * find by acl_alias
     *
     * @param int	$aclAlias
     * @return	UserAcl
     * 				false if not found
     */
    public static function findUserAclByAclAlias($aclAlias) {
    	if (null == $aclAlias) {
    		return false;
    	}
    	$conn = Flywheel_DB::getConnection('user_acl');
    	$conn->prepare('SELECT ' .implode(', ', self::$schema['fields']) .'
    						FROM '. self::TABLE .' WHERE acl_alias=?');
    	$conn->bindValue(1, $aclAlias, PDO::PARAM_STR);
    	$conn->execute();

    	$datas = $conn->fetchAll();
    	if (false === $datas) {
    		return false;
    	}

        $userAcls = array();
        foreach ($datas as $data){
            $userAcl = new self();
            $userAcl->_hydrate($data);
            $userAcl->setNew(FALSE);
            $userAcls[$userAcl->id] = $userAcl;
        }
        return $userAcls;
    }

    /**
     * find by user Id
     * @param int $userId
     * @return Acl false if not found
     */

    public static function findUserAclByUserId($userId){
    	if (null == $userId) {
    		return false;
    	}
        if (!isset (self::$aclInstance[$userId])){
            $conn = Flywheel_DB::getConnection('user_acl');
            $conn->prepare('SELECT ' .implode(', ', self::$schema['fields']) .'
    						FROM '. self::TABLE .' WHERE user_id=?');
            $conn->bindValue(1, $userId, PDO::PARAM_STR);
            $conn->execute();

            $datas = $conn->fetchAll('acl_alias');
            self::$aclInstance[$userId] = $datas;
            return $datas;
        }
        else{
            return self::$aclInstance[$userId];
        }

    }

    /**
     * Find Acl by $userId
     * @param $userId
     * @return array object Acl false if not found or value = 0
     */
    public static function findAclByUserId($userId){
        if (NULL == $userId){
            return FALSE;
        }

        $acls = array();
        $userAcls = self::findUserAclByUserId($userId);
        var_dump($userAcls);
        if ($userAcls != false){
            foreach ($userAcls as $userAcl){
                if($userAcl['value'] == 1){
                    $acl = Acl::findByAlias($userAcl['acl_alias']);
                    $acls[$acl->id] = $acl;
                }
            }
        }
        if ($acls == null){
            return FALSE;
        }
        return $acls;

    }

    /**
     * check if user has acl
     * @param $userId, $alias
     * @return boolean
     */
    public static function checkUserAcl($userId, $alias){
        if($userId == null){
            return false;
        }
        if ($alias == ""){
            return false;
        }
        $userAcls = UserAcl::findUserAclByUserId($userId);
        if ($userAcls == false){
            return false;
        }
        if ((isset ($userAcls[$alias])) && ($userAcls[$alias]['value'] == 1)){
            return true;
        }else{
            return false;
        }
        return false;
    }

    /**
     *  find UserAcl by userId and alias
     *  @param $userId, $aclAlias
     *  @return UserAcl
     *          false if not found
     */
    public static function findValuebyUserId($userId, $aclAlias){
        $userAcls = UserAcl::findUserAclByAclAlias($aclAlias);
        if ($userAcls == false){
            return false;
        }
        foreach($userAcls as $userAcl){
            if ($userAcl->user_id == $userId){
                return $userAcl;
            }
        }
        return false;
    }



}