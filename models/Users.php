<?php

class Users extends Flywheel_Model {

    public $id = 0;
    public $is_delete = 0;
    public $username = '';
    public $password = '';
    public $type = ''; //0:Người dùng thường, 1:Kho hàng,2:nguoi ban trung quoc
    public $code = ''; //ma nguoi dung
    public $fullname = '';
    public $gender = 1;
    public $birthday = '';
    public $phone = '';
    public $mobile = '';
    public $avatar = '';
    public $active = '';
    public $status = '';
    public $status_time = '';
    public $email = 0;
    public $detail_address = '';
    public $qh_id = '';
    public $qh_address = '';
    public $tt_id = '';
    public $tt_address = '';
    public $time_join = 0;
    public $lock_status = 0;
    public $lock_time_begin = 0;
    public $lock_time_end = 0;
    public $lock_reason = '';
    public $bank_account = '';
    public $bank_self = '';
    public $bank_name = '';
    public $bank_brand = '';
    public $yahoo = '';
    public $acc_nl = '';
    public $acc_bk = '';
    public $skype = '';
    public $qq = '';
    public $alioangoang = '';
    public $aliwangwang_ali = '';
    public $rate = '';
    public $cmtnd = '';
    public $ip = '';
    public $is_staff = '';
    public $is_auto_create = 0;
    public $is_make_promotion = 0;
    public $is_translate_item = 0;
    public $is_crawler = 0;
    public $is_shop_marketer = 0;
    public $order_code = '';
    public $is_edit_order_code = 0;

    const LEVEL_MODERATOR = 7;
    const TABLE ='users';
    const PRIMARY_FIELD = 'id';

    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
        'fields' => array(
            'users.`id`',
            'users.`is_delete`',
            'users.`username`',
            'users.`password`',
            'users.`type`',
            'users.`code`',
            'users.`fullname`',
            'users.`gender`',
            'users.`birthday`',
            'users.`phone`',
            'users.`mobile`',
            'users.`bank_name`',
            'users.`active`',
            'users.`avatar`',
            'users.`status`',
            'users.`status_time`',
            'users.`email`',
            'users.`detail_address`',
            'users.`qh_address`',
            'users.`qh_id`',
            'users.`tt_id`',
            'users.`tt_address`',
            'users.`time_join`',
            'users.`lock_status`',
            'users.`lock_time_begin`',
            'users.`lock_time_end`',
            'users.`lock_reason`',
            'users.`bank_account`',
            'users.`bank_self`',
            'users.`bank_name`',
            'users.`bank_brand`',
            'users.`yahoo`',
            'users.`qq`',
            'users.`alioangoang`',
            'users.`aliwangwang_ali`',
            'users.`acc_nl`',
            'users.`acc_bk`',
            'users.`cmtnd`',
            'users.`skype`',
            'users.`rate`',
            'users.`ip`',
            'users.`is_auto_create`',
            'users.`is_make_promotion`',
            'users.`is_translate_item`',
            'users.`is_crawler`',
            'users.`is_shop_marketer`',
            'users.`is_staff`',
            'users.`order_code`',
            'users.`is_edit_order_code`'
        ),
        'columns' => array(
            'id',
            'is_delete',
            'username',
            'password',
            'type',
            'code',
            'fullname',
            'gender',
            'birthday',
            'phone',
            'mobile',
            'active',
            'avatar',
            'status',
            'status_time',
            'email',
            'detail_address',
            'qh_id',
            'qh_address',
            'tt_id',
            'tt_address',
            'time_join',
            'lock_status',
            'lock_time_begin',
            'lock_time_end',
            'lock_reason',
            'bank_account',
            'bank_self',
            'bank_name',
            'bank_brand',
            'cmtnd',
            'acc_nl',
            'acc_bk',
            'yahoo',
            'skype',
            'qq',
            'alioangoang',
            'aliwangwang_ali',
            'rate',
            'ip',
            'is_auto_create',
            'is_make_promotion',
            'is_translate_item',
            'is_crawler',
            'is_shop_marketer',
            'is_staff',
            'order_code',
            'is_edit_order_code'
        )
    );
    protected static $instances = array();

    /**
     * Read object from database
     * @param int		$id	default null
     * @return Users
     * @throws Flywheel_DB_Exception
     */
    public function read($id = null) {
        if (null == $id) {
            $id = $this->id;
        }

        $db = Flywheel_DB::getConnection('users');
        $db->prepare('SELECT ' . implode(', ', self::$schema['fields']) . ' 
    					FROM ' . self::TABLE . ' 
    					WHERE id = ? LIMIT 1');

        $db->bindParam(1, $id, PDO::PARAM_INT);
        $db->execute();
        $data = $db->fetch();

        if (false === $data) {
            return false;
        }
        $this->_hydrate($data);
        $this->setNew(false);
        return true;
    }

    public function retrieveByPk($pk) {

        $user = new self();
        if ($user->read($pk)) {
            return $user;
        }
        return false;
    }

    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {
        $db = Flywheel_DB::getConnection('users');
        if (true === $this->isNew()) {
            $data = $this->toArray();
            unset($data['id']);
            $id = $db->insert(self::TABLE, $data);
            if (!$id) {
                return false;
            }
            $this->id = $id;
            $this->setNew(false);
        } else {
            //bind array to update
            $data = array();
            $columnModifed = 0;

            foreach ($this->_modifiedColumns as $modifiedColumn => $value) {
                ++$columnModifed;
                $data[$modifiedColumn] = $this->$modifiedColumn;
            }
            if ($columnModifed == 0) {
                return 0;
            }
            $affectedRows = $db->update(self::TABLE, $data, 'id=' . $this->id);
            if (0 == $affectedRows) {
                return false;
            }
        }
        return $this->id;
    }

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {
        $db = Flywheel_DB::getConnection('users');
        return $db->delete(self::TABLE, '`id`=' . $this->id);
    }

    public static function findByUsername($username) {
        if (null == $username) {
            return false;
        }

        $conn = Flywheel_DB::getConnection('users');
        $conn->prepare('SELECT ' . implode(', ', self::$schema['fields']) . ' 
    						FROM ' . self::TABLE . ' WHERE username=? LIMIT 1');
        $conn->bindValue(1, $username, PDO::PARAM_STR);
        $conn->execute();
        $data = $conn->fetch();

        if (false === $data) {
            return false;
        }

        $user = new self();
        $user->_hydrate($data);
        $user->setNew(false);

        return $user;
    }

    /**
     * Return rows has been affect by query with condition
     * @param type $condition Filter
     * @param type $order Order by keyword
     * @param type $limit limit rows return
     * @param type $key Array key will be return
     * @return type Users data
     */
    public function selectList($condition = "1=1", $order = null, $limit = null, $key = null) {
        $conn = Flywheel_DB::getConnection();
        return $conn->select(self::TABLE, implode(',', self::$schema['fields']), $condition, $order, $limit, $key);
    }
    
    /**
     * Return row with condition
     * @param type $condition Condition to query
     * @param type $field custom field or all fields if null
     * @return type User data
     */
    public function selectOne( $condition = "1=1", $field = null) {
        if($field == null) {
            $field = self::$schema['fields'];
        }
        $conn = Flywheel_DB::getConnection();
        return $conn->selectOne(self::TABLE, $field, $condition);
    }

    public static function checkExistEmail($email) {
        $user = new self;
        $data = array();
        $data = $user->getUserByEmail($email);
        if (!empty($data))
            return true;
        return false;
    }

}