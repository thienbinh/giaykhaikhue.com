<?php

class Item extends Flywheel_Model
{

    /**
     * id
     * primary
     * auto-increment
     * type : int(10) unsigned
     * @var integer $id
     */
    public $id = 0;
    public $title = "";
    public $title_origin = "";
    public $cid = 0;
    public $img = "";
    public $code = "";
    public $price = 0;
    public $pro_price = 0;
    public $pro_des = "";
    public $site_price = 0;
    public $brief = "";
    public $des= "";
    public $is_front= 0;
    public $is_new = 0;
    public $pos = 0;
    public $instock = 0;
    public $tags = "";
    public $created_time = 0;
    public $status = 0;
    public $link_crawl = "";
    public $id_craw = 0;
    public $type = 0;

    /**
     * database table name
     */
    const TABLE = 'item';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array('fields' => array(
            'item.`id`', 'item.`title`', 'item.`title_origin`', 'item.`cid`', 'item.`img`', 'item.`code`', 'item.`price`',
            'item.`pro_price`', 'item.`pro_des`', 'item.`site_price`',
            'item.`brief`', 'item.`des`', 'item.`is_front`',
            'item.`is_new`', 'item.`pos`', 'item.`instock`', 'item.`tags`', 
            'item.`created_time`', 'item.`status`', 'item.`link_crawl`', 'item.`id_craw`', 'item.`type`'), 
        'columns' => array(
            'id', 'title', 'title_origin', 'cid', 'img', 'code', 'price',
            'pro_price', 'des', 'site_price', 'brief', 'des',
            'is_front', 'is_new', 'pos', 'instock', 'tags', 'created_time', 'status', 'link_crawl', 'id_craw', 'type'));

    public function retrieveByKey($id = null)
    {
        // Int object
        $item = new self();
        $item->read($id);
        return $item;
    }

    public function read($id = null)
    {
        if (null == $id) {
            $id = $this->id;
        }

        $conn = Flywheel_DB::getConnection(self::TABLE);
        $conn->prepare('SELECT ' . implode(', ', self::$schema['fields']) . ' 
    						FROM ' . self::TABLE . ' WHERE id=? LIMIT 1');
        $conn->bindParam(1, $id, PDO::PARAM_INT);
        $conn->execute();
        $data = $conn->fetch();
        $this->_hydrate($data);
        $this->setNew(false);

        return $data;
    }

    /**
     * Get data by condition
     * @param type $where Condition query
     * @return type LIST (null if empty data)
     */
    public function selectList($where = '1=1', $order = null, $limit = null, $key = null)
    {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        $data = $conn->select(self::TABLE, "*", $where, $order, $limit, $key);
        return $data;
    }
    
    public function countItem($where = null) {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        return $conn->count(self::TABLE, $where);
    }

    
    public function selectOne( $cond = "1=1" ) {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        $data = $conn->selectOne(self::TABLE, self::$schema['fields'] , $cond);
        return $data;
    }

    public function save()
    {
        $conn = Flywheel_DB::getConnection(self::TABLE);
        if (true === $this->isNew()) {
            $data = $this->toArraySqlName(self::$schema['fields'], self::$schema['columns']);
            $id = $conn->insert(self::TABLE, $data, true);
            if (!$id) {
                return false;
            }
            $this->setNew(false);
        } else { // Update
            $data = array();
            foreach ($this->_modifiedColumns as $modifiedColumn => $value) {
                $data[$modifiedColumn] = $this->$modifiedColumn;
            }
            if (sizeof($data) == 0) {
                return false;
            }
            $success = $conn->update(self::TABLE, $data, '`id`=' . $this->id . ' LIMIT 1');
            if (!$success) {
                return false;
            }
            $id = $this->id;
        }
        return $id;
    }

    public function delete()
    {
        if (Flywheel_DB::getConnection(self::TABLE)->deleteById(self::TABLE, $this->id)) {
            return true;
        }
        return false;
    }

}
