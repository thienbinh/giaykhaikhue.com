<?php 
class Lang extends Flywheel_Model {
    
    public $id                  = 0;
    public $key      = '';
    public $vi               = '';
    public $cn            = '';
    const DATABASE = 'alimama';
    const TABLE ='lang';
    const PRIMARY_FIELD = 'id';
    
    public static $schema = array(
        'fields' => array(
            '`lang`.`id`',
            '`lang`.`key`',
            '`lang`.`vi`',
            '`lang`.`cn`',
        ),
        'columns' => array(
            'id',
            'key',
            'vi',
            'cn',
        )
    );

    public $db = '';
    function __construct(){
        parent::__construct();
        $this->db = Flywheel_DB::getConnection(self::DATABASE);
    }
    protected static $instances = array();
    
    public function read($id = null) {
    	$db = Flywheel_DB::getConnection(self::DATABASE);
    	$db->prepare('SELECT ' .implode(', ', self::$schema['fields']) .' 
                            FROM `' .self::TABLE .'` 
                            WHERE `'.self::TABLE.'`.`id` = '.$id.' LIMIT 1');
    	$db->execute();
    	$data = $db->fetch();
        if(!$data){
            return false;
        }
    	return $data;
    }

    
    public function selectList($condition = null,$order = null,$limit = null,$key = null) {
        if(!$condition) $condition = "1=1";
        return $this->db->select(self::TABLE,implode(',',self::$schema['fields']),$condition,$order,$limit,$key);
    }
    
    public function insert($array){
        return $this->db->insert(self::TABLE, $array);
    }
    
    public function update($array, $condition){
        return $this->db->update(self::TABLE, $array, $condition);
    }

    public function save() {}
    
    public function delete() {
        return $this->db->delete(self::TABLE,'id='.$this->id);       
    }
    
}