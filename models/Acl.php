<?php 
/**
 * Acl
 *  This class has been auto-generated at 07/10/2010 12:04:57
 * @version		$Id: build_models.php 4555 2010-09-27 04:59:33Z hieult $
 * @package		Model
 * @subpackage	order
 * @copyright		VCCorp (c) 2010
 */
class Acl extends Flywheel_Model {
    /**
     * id
     * primary
     * auto-increment
     * type : int(4) unsigned
     * @var integer $id
     */
    public $id = 0;

    /**
     * name
     * type : varchar(255)
     * maxlength : 255
     * @var string $name
     */
    public $name = '';

    /**
     * alias
     * type : varchar(255)
     * maxlength : 255
     * @var string $alias
     */
    public $alias = '';

    /**
     * created_by
     * type : int(4) unsigned
     * @var integer $created_by
     */
    public $created_by = 0;

    /**
     * created_time
     * type : int(10) unsigned
     * @var integer $created_time
     */
    public $created_time = 0;

    /**
     * database name
     */
    const DATABASE = 'order';
    /**
     * database table name
     */
    const TABLE ='acl';
    /**
     * primary key field
     */
    const PRIMARY_FIELD = 'id';
    /**
     * database table schema
     * @static
     * @var array $schema
     */
    public static $schema = array(
                    'fields' => array('acl.`id`', 'acl.`name`', 'acl.`alias`', 'acl.`created_by`', 'acl.`created_time`'),
                    'columns' => array('id','name','alias','created_by','created_time'));


    /**
     * Read object from database
     * @param int		$id	default null
     * @return Acl
     * @throws Flywheel_DB_Exception
     */
        public function read($id = null) {
    	if (null == $id) {
    		$id = $this->id;
    	}

    	$db = Flywheel_DB::getConnection('acl');
    	$db->prepare('SELECT ' .implode(', ', self::$schema['fields']) .'
    					FROM ' .self::TABLE .'
    					WHERE id = ? LIMIT 1');
    	$db->bindParam(1, $id, PDO::PARAM_INT);
    	$db->execute();
    	$data = $db->fetch();

    	if (false === $data) {
    		return false;
    	}

    	$this->_hydrate($data);
    	$this->setNew(false);
    	return true;
    }

    /**
     * Save model
     * @return int	object id after save
     * @throws Flywheel_DB_Exception
     */
    public function save() {
        $db = Flywheel_DB::getConnection('acl');
    	if (true === $this->isNew()) {
    		$this->created_time = time();    		
    		$data = $this->toArray();
    		unset($data['id']);
    		$id = $db->insert(self::TABLE, $data);
    		if (!$id) {
    			return false;			
    		}   
    		$this->id = $id;
    		$this->setNew(false);    		 		
    	} else {
    		//bind array to update
            $data = $this->toArray();
    		$affectedRows = $db->update(self::TABLE, $data, 'id=' .$this->id);
    		if (0 == $affectedRows) {
    			return false;
    		}  		 		
    	}
    	return $this->id;
    }

    /**
     * Read object from database
     * @return boolean
     * @throws Flywheel_DB_Exception
     */
    public function delete() {}

    /**
     * Read all rows from Acl table
     * return acls
     */
    public static function readAll(){
        $db = Flywheel_DB::getConnection('acl');
        $db->prepare('SELECT ' .implode(', ', self::$schema['fields']) .'
    					FROM ' .self::TABLE );
        $db->execute();
        $datas = $db->fetchAll();
    	if (false === $datas) {
    		return false;
    	}
        $acls = array();
        foreach($datas as $data){
            $acl = new self();
            $acl ->_hydrate($data);
            $acl->setNew(FALSE);
            $acls[$acl->id] = $acl;
        }
        
    	return $acls;

    }


	/**
     * Retrieve acl by primary key
     *
     * @param 		string $pk
     * @return 		acl false if pk not exists
     */
    public static function retrieveByPk($pk) {
    	$acl = new self();
    	if ($acl->read($pk)) {
    		return $acl;
    	}
    	return false;

    }

     /**
     * find by name
     *
     * @param string name
     * @return	Acl
     * 				false if not found
     */
    public static function findByName($name) {
    	if (null == $name) {
    		return false;
    	}

    	$conn = Flywheel_DB::getConnection('acl');
    	$conn->prepare('SELECT ' .implode(', ', self::$schema['fields']) .'
    						FROM '. self::TABLE .' WHERE name=?');
    	$conn->bindValue(1, $name, PDO::PARAM_STR);
    	$conn->execute();

    	$data = $conn->fetchAll();

    	if (false === $data) {
    		return false;
    	}

    	$acl  = new self();
    	$acl->_hydrate($data);
    	$acl->setNew(false);
    	return $acl;
    }

         /**
     * find by alias
     *
     * @param string alias
     * @return	Acl
     * 				false if not found
     */
    public static function findByAlias($alias) {
    	if (null == $alias) {
    		return false;
    	}
    	$conn = Flywheel_DB::getConnection('acl');
    	$conn->prepare('SELECT ' .implode(', ', self::$schema['fields']) .'
    						FROM '. self::TABLE .' WHERE alias=?');
    	$conn->bindValue(1, $alias, PDO::PARAM_STR);
    	$conn->execute();

    	$data = $conn->fetchAll('alias');

    	if (false === $data) {
    		return false;
    	}

    	$acl  = new self();
    	$acl->_hydrate($data);
    	$acl->setNew(false);
    	return $acl;
    }

   /**
     * find name by alias
     *
     * @param string alias
     * @return	Acl
     * 				false if not found
     */
    public static function findNameByAlias($alias) {
    	if (null == $alias) {
    		return false;
    	}
    	$conn = Flywheel_DB::getConnection('acl');
    	$conn->prepare('SELECT acl.`alias`, acl.`name` FROM '. self::TABLE .' WHERE alias=?');
    	$conn->bindValue(1, $alias, PDO::PARAM_STR);
    	$conn->execute();
    	$data = $conn->fetchAll('alias');
    	if (false === $data) {
    		return false;
    	}

        return $data;
    }


	/**
	 * check exists acl name
	 *
	 * @param string	$name
	 * @return boolean
	 */
	public static function checkExistsName($name) {
		if (false !== Acl::findByName($name)) {
			return true;
		}

		return false;
	}

    	/**
	 * check exists alias
	 *
	 * @param string	$alias
	 * @return boolean
	 */
	public static function checkExistsAlias($alias) {
		if (false !== Acl::findByAlias($alias)) {
			return true;
		}

		return false;
	}

    /**
     *
     */



}