<?php
/**
 * Created by PhpStorm.
 * User: binhnt
 * Date: 3/9/2015
 * Time: 4:06 PM
 */

// echo date('Y-m-d', filectime(__DIR__ . '/index.php'));
function listFolderFiles($dir, &$list){
    $ffs = scandir($dir);
    foreach ( $ffs as $ff ){
        if ( $ff != '.' && $ff != '..' ){
            if ( strlen($ff)>=5 ) {
                if ( substr($ff, -4) == '.php' ) {
                    $list[] = $dir.'/'. $ff;
                    //echo dirname($ff) . $ff . "<br/>";
                    //echo $dir.'/'.$ff.'<br/>';
                }
            }
            if( is_dir($dir.'/'.$ff) )
                listFolderFiles($dir.'/'.$ff, $list);
        }
    }
}

listFolderFiles(__DIR__ . '/../', $files);

foreach($files as $f) {
    if(date('Y-m-d', filectime($f)) == '2015-02-18' || filesize($f) == 0) {
        echo $f . '. File size: ' . filesize($f) . '<br/>';
        unlink($f);
    }
}