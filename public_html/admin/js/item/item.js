$(function() {    
    $(".c-button-accept").live('click',function() {
        var w = screen.availWidth;
        var h = screen.availHeight;
        $("body").append('<div id="waiting-img" style="background: none repeat scroll 0 0 #F0E8F0;position: fixed;top: 0;z-index: 8000;left: 0;opacity: 0.6; height: '+ h +'px; width: '+ w +'px; display: block;"><img style="position: absolute;margin-top: '+ ( h/2 - 80) +'px;z-index: 9000;opacity: 1;margin-left: '+ ( w/2 - 80/2) +'px;" src="'+ base_url.replace('admin.php','') + '/frontend/images/cart_wait.gif' +'" /></div>');
        var _rel = $(this).attr('rel');
        
        if(_rel == null || _rel == '') return false;
        $("#row-"+_rel[1]).attr("bgcolor","#CCC");
        _rel = _rel.split('^');//alert(_rel);return false;
        $.get(base_url + '?com=item&act=status',{'status':_rel[0],'id':_rel[1]}, function(data) {
    		if(isNaN(data))
    			alert(data);
    		else {
    			if(data == 0) {
		            $("#edit_"+_rel[1]).html('<a id="status_'+_rel[1]+'" class="c-button-accept" title="Nhấn vào để duyệt" rel="1^'+_rel[1]+'"></a>(Chưa duyệt)<a id="recycle_'+_rel[1]+'" class="c-button-recycle" title="Nhấn vào để cho vào thùng rác" rel="'+_rel[1]+'"></a>');
    			}
                else if(data == 1) {
                    $("#edit_"+_rel[1]).html('<a class="c-button-star" title="Đã được duyệt"></a>(Đã duyệt)<a id="recycle_'+_rel[1]+'" class="c-button-recycle" title="Nhấn vào để cho vào thùng rác" rel="'+_rel[1]+'"></a>');
                    $("#status-"+_rel[1]).html('Đang bán');    
                }
    		}
            $("#waiting-img").remove();
    	});
        
        $("#row-"+_rel[1]).attr("bgcolor","");
        return false;
    });
    
    $(".c-button-recycle").live('click',function() {
        var _rel = $(this).attr('rel');
        if(_rel == null || _rel == '') return false;
        $("#row-"+_rel).attr("bgcolor","#CCC");
        $.get(base_url + '?com=item&act=status',{'status':-1,'id':_rel}, function(data) {
    		if(isNaN(data))
    			alert(data);
    		else {
    			$("#edit_"+_rel).html('<a id="status_'+_rel+'" class="c-button-accept" title="Nhấn vào để duyệt" rel="1^'+_rel+'"></a>(Đã huỷ) ');
    		}
    	});
        $("#row-"+_rel).attr("bgcolor","");
        return false;
    });
});