$(document).ready(function() {
        $('.box-cate').dialog({
            title: 'Thông tin danh mục',
            modal: true,
            autoOpen: false,
            width: 500
        });
       $('.bt-open-dlg').click(function(){
           resetForm();
           
           $('.box-cate').dialog('open');
           $('input[name=catId]').val('');
           $('.bt-add-cate').val('Thêm');
           
       });
       
       // Edit action
       $('.edit').click(function(e) {
           
           $('.bt-add-cate').val('Cập nhật');
           $('input[name=catId]').val($(this).attr('rel'));
            var id_wholesale = $(this).attr('id_wholesale')  ;
            
           var id = $(this).attr('rel');
           $.ajax({
               url: '?com=item_home&act=edit&type=cate',
               type: 'POST',
               data: {
                   'cid':id
               },
               success: function(d) {
                   d = $.parseJSON(d);
                   
                   $('input[name=tb_category]').val(d.name);
                   $('input[name=chb_type]').attr('checked', d.type == '1' ? true: false);
                   $('input[name=tb_pos]').val(d.pos);
                   $('.cb-cate-sys').val(d.refer);
                   $('input[name=link]').val(d.link);
                   $('input[name=tb_note]').val(d.note);
                   $('input[name=chb_status]').attr('checked', d.status == '1' ? true: false);
                   var img = $('#category_img_'+id).attr('src');
                   $('#view_img','#box-cate').attr('src',img);
                   var parent_id = $('input[name="parent_id"]').val();
                   $("select.parent_id option[value='"+parent_id+"']").attr('selected',true);
                   $("#c_img").val( $('#category_img_'+id).attr('rel') );
                   //dung cho ban theo day
                 
                   if( id_wholesale  ){
                       $('#shopcommit').show();
                       $('input[name=category_type]').attr('checked',true);
                       $("select.shopcommit option[value='"+id_wholesale+"']").attr('selected',true);
                   }else{
                        $('#shopcommit').hide();
                       $('input[name=category_type]').attr('checked',false);
                   }
                   
                   $('.box-cate').dialog('open');
               }, error: function(d) {
                   
               }
           });
       });
       
       // Save action 
       $('.bt-add-cate').click(function(e) {
          
           $.ajax({
               url: '?com=item_home&act=save&type=cate',
               type: 'POST',
               data: {
                   'cid':$('input[name=catId]').val(),
                   'name':$('input[name=tb_category]').val(),
                   'image':$('input[name=c_img]').val(),
                   'parent_id': $('.parent_id').val(),
                   'type':$('input[name=chb_type]').attr('checked') == 'checked'? 1:0,
                   'refer': $('.cb-cate-sys').val(),
                   'link':$('input[name=link]').val(),
                   'pos':$('input[name=tb_pos]').val(),
                   'status':$('input[name=chb_status]').attr('checked') == 'checked'? 1:0,
                   'note':$('input[name=tb_note]').val(),
                   'category_type': $('input[name=category_type]').attr('checked') == 'checked'? 1:0,
                   'id_wholesale': $('.shopcommit').val(),
                   'title_wholesale': $.trim( $('.shopcommit option:selected').text() )
               },
               success: function(d) {
                   d = $.parseJSON(d);
                   if(!d.status) {
                       alert(d.msg);
                       return;
                   }
                   window.location.reload();
               }, error: function(d) {
                   
               }
           });
       });
       
       // Delete action
       $('.del').click(function(e) {
            if(confirm('Xóa danh mục?')) {
                $.ajax({
                    url: '?com=item_home&act=del&type=cate&cid=' + $(this).attr('rel'),
                    type: 'GET',
                    success: function(d) {
                        d = $.parseJSON(d);
                        if(d.status) {
                            window.location.reload();
                        } else {
                            alert('Xóa có lỗi xảy ra! Xin thử lại.');
                        }
                    }, error: function(err) {
                        
                    }
                });
            }
       });
       
       $('.bt-cancel').click(function(e) {
            resetForm();
       });
       //
       $('input[name="category_type"]').click(function(){
            var thisCheck = $(this);
            if ($('input[name="category_type"]').is (':checked'))
            {
                $('#shopcommit').show();
            }else{
                $('#shopcommit').hide();
            }
       });
       
        new AjaxUpload("#upload_img", {
                action: base_url + '?com=item_home&act=upload_image_cate',
                autoSubmit: true,
                onSubmit: function(file, extension) {
                },
                onComplete: function(file, data) { 
                    var arr = $.parseJSON(data); 
                    if(arr['errorMess'] != ''){ 
                        alert(arr['errorMess']);
                    } else {
                        $("#c_img").val(arr['image_name']);
                        $(".data-add img").attr('src', arr['image_url']);
                    }	
                }
            });
            
    });
    
    function resetForm() {
        $('#box-cate').find('input[type=text]').val('');
        $('#box-cate').find('input[type=checkbox]').attr('checked', false);
    }
   
	
           
       