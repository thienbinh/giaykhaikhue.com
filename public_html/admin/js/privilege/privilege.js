$(function() {
    $('input.deleteListPrivi').click(function () {
		var listId = getListCheck('id[]');
		if(listId == 0 || !listId) {
			alert('Bạn chưa chọn sản phẩm muốn xóa');
			return;
		}
		$.get(base_url + '?com=privilege&act=delete',{'list_id':listId}, function(data) {
			if(isNaN(data))
				alert(data);
			else {
				alert('Bạn đã xóa thành công');
				window.location.reload();
			}
		});
	});	
    
    $('#status-click').click(function() {
        var _rel = $(this).attr('rel');
        _rel = _rel.split('-');
        $.ajax({
           url: 'ajax/privilege.process.php?cmd=u-status&id=' + _rel[0] + '&status=' + _rel[1],
           data:'',
           type: 'GET',
           success: function(data) {
                alert('Bạn đã xóa cache của sản phẩm thành công!');
           }, error: function(err) { } 
        });
   });
   
   // Show tool edit & delete category when hover
   $('.alt').hover(function(){
       $(this).find('.tool-hover').css('visibility', 'visible');	    
   }, function(){
       $(this).find('.tool-hover').css('visibility', 'hidden');	    
   });
});
function getListCheck(name) {
	var listId = '';
	$("input[type=checkbox][name = '"+name+"']:checked").each( 
	    function(i, checked) { 
	    	if(i == 0)
	    		listId += $(checked).val();
	    	else listId += ',' + $(checked).val();
	    }
	);
	return listId;
}

function addUserPrivilege(pid) {
    var _uname = prompt("Nhập user name","","Thêm người dùng");
    if(_uname != '' && _uname != null) {
        $.get(base_url + '?com=privilege&act=adduser',{'username':_uname,'pid':pid}, function(data) {
    		if(isNaN(data) || data == 0)
    			alert("Không thể cập nhật có thể do không tồn tại người dùng này");
    		else {
    			alert('Bạn đã thêm thành công');
    			window.location.reload();
    		}
    	});
    }
    return false;
}

function addGroupUserPrivilege(pid) {
    var gname = prompt("Nhập nhóm user","","Thêm nhóm người dùng");
    if(gname != '' && gname != null) {
        $.get(base_url + '?com=privilege&act=addgroupuser',{'gname':gname,'pid':pid}, function(data) {
    		if(isNaN(data) || data == 0)
    			alert("Không thể cập nhật có thể do không tồn tại nhóm này");
    		else {
    			alert('Bạn đã thêm thành công');
    			window.location.reload();
    		}
    	});
     }
     return false;
}

function addGroupUser(id) {
    var _uname = prompt("Nhập user","","Thêm người dùng");
    if(_uname != '' && _uname != null) {
        $.get(base_url + '?com=usergroup&act=adduser',{'uname':_uname,'id':id}, function(data) {
    		if(isNaN(data) || data == 0)
    			alert("Không thể cập nhật có thể do không tồn tại người dùng này");
    		else {
    			alert('Bạn đã thêm thành công');
    			window.location.reload();
    		}
    	});
     }
     return false;
}

function addPrToGroup(id) {
     var _uname = prompt("Nhập mã quyền","","Thêm quyền cho nhóm");
     if(_uname != '' && _uname != null) {
        $.get(base_url + '?com=user_group&act=addprivilege',{'pcode':_uname,'id':id}, function(data) {
    		if(isNaN(data) || data == 0)
    			alert("Không thể cập nhật có thể do không tồn tại quyền này");
    		else {
    			alert('Bạn đã thêm thành công');
    			window.location.reload();
    		}
    	});
     }
     return false;
}
function addPrivilegeGroup() {
    var privilege_name = $("#privilege_name").val();
    var privilege_code = $("#privilege_code").val();
    if(privilege_name == '' || privilege_name == null) {
        alert("Tên quyền không được để trống");
        return false;
    }
    
    if(privilege_code == '' || privilege_code == null) {
        alert("Mã quyền không được để trống");
        return false;
    }
    
    $.get(base_url + '?com=privilege&act=newajax',{'privilege_name':privilege_name,'privilege_code':privilege_code}, function(data) {
		if(isNaN(data))
			alert(data);
		else {
			alert('Bạn đã thêm thành công');
			window.location.reload();
		}
	});
    return false;
}

function addNewMenu() {
    var title = $("#title").val();
    var parent_id = $("#parent_id option:selected").val();
    var url = $("#url").val();
    var privilege = $("#privilege").val();
    var comment = $("#comment").val();
    
    if(title == '' || title == null) {
        alert("Tên menu không được để trống");
        return false;
    }
    
    if(url == '' || url == null) {
        alert("Đường dẫn không được để trống");
        return false;
    }
    
    if(privilege == '' || privilege == null) {
        alert("Mã quyền không được để trống");
        return false;
    }
    
    if(comment == '' || comment == null) {
        comment = title;
    }
    
    $.get(base_url + '?com=admin_menu&act=newajax', {
        'title'         :   title,
        'parent_id'     :   parent_id,
        'url'           :   url,
        'privilege'     :   privilege,
        'comment'       :   comment
    },function(data) {
    		if(isNaN(data))
    			alert(data);
    		else {
    			alert('Bạn đã thêm thành công');
    			window.location.reload();
    		}
	    }
    );
    return false;
}