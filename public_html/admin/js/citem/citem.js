$(function() { 
    
    $(".c-button-add").click(function() {
        var _rel = $(this).attr('rel');
        //if(_rel == 0) return false;
        var html_option = '';
        $("#level").val(_rel);
        if(_rel>0) {
            var pid = $("#option-cat"+(_rel-1)+" option:selected").val();
            $.get(base_url + '?com=category&act=getcat',{'level':_rel-1,'id':pid}, function(data) {
                if(data != '')
        		html_option = data.toString();
                $("#parent_id").html(html_option);
        	});
        }
        if($("#add-category-box").hide())
            $("#add-category-box").show('slow');
        else
            $("#add-category-box").hide('fast');
        $("#title-view").html("Thêm mới danh mục");
        $("input[type=text]").val('');
        $("textarea").val('');
        $("#edit").val(0);
        $("#title-cat").html('');
        $('#files').html('');
        $("#save-img").val("");
    });
    
    $(".c-button-edit").click(function() {
        var _rel = $(this).attr('rel');
        if(_rel == 0) return false;
        $("#action").val(0);
        $("#id").val($("#option-cat"+_rel+" option:selected").val());
        editCategory();
        if($("#add-category-box").hide())
            $("#add-category-box").show('slow');
        else
            $("#add-category-box").hide('fast');
        $("#title-view").html("Chỉnh sửa danh mục:");
        $("#edit").val(1);
        $("#action").val(1);
        $("#title-cat").html($("#option-cat"+_rel+" option:selected").text());
        $("#c-title").html($("#p_title").val().length);
        $("#c-description").html($("#p_description").val().length);
    });
    
    $(".c-button-del").click(function() {
        var _rel = $(this).attr('rel');
        if(_rel == 0) return false;
        var _id = $("#option-cat"+_rel+" option:selected").val();
        if(confirm("Bạn muốn xoá danh mục này, các danh mục con sẽ bị xoá nếu có")) {
            $.get(base_url + '?com=category&act=delajax',{'id':_id}, function(data) {
        		onclickOption(parseInt(data)-1);
        	});
        }
        return false;
    });
    $(".c-button-prt").click(function() {
        var _rel = $(this).attr('rel');
        var _id = $("#option-cat"+_rel+" option:selected").val();
        getPrt(_id);
        return false;
    });
    
    $(".show-zproperty").live('click',function() { 
        var _rel = $(this).attr('rel');
        _rel = _rel.split('-');
        getChildPrt(_rel[0],_rel[1]);
    });
    
    $(".c-button-search").click(function() {
        var _rel = $(this).attr('rel');
        var _title = $("#input-cat"+_rel).val();
        if(_title == '' || _title == null) {
            //return false;
        }
        $.get(base_url + '?com=category&act=getlist',{'level':_rel,'title':_title}, function(data) {
            data = $.parseJSON(data);
    		$("#option-cat"+_rel).html(data);
    	});
    });
    
    $("input#button-content-yell").live('click', function() {
        $("input#button-content-yell").jAlert('Đã cập nhật thành công!', "success");
    });
    
    $(".msg-btn").live('click', function() {
        $("#add-category-box").fadeOut('fast');
    });
    
    $(".item-is-check").click(function() {
        var check = $(this).attr('checked');
        var id = $(this).val();
        var is_check = 0;
        if(check == 'checked') {
            is_check = 1;
        }
        $.get(base_url + '?com=property&act=ischeck',{'id':id,'is_check':is_check}, function(data) {
            $("#is_check_"+id).jAlert("Cập nhật thành công","success");
    	});
    });
    
	var btnUpload=$('#upload');
    if(document.getElementById('upload')) 
	new AjaxUpload(btnUpload, {
		action: base_url + '?com=category&act=upload',
		name: 'uploadfile',
		onSubmit: function(file, ext){
			 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                // extension is not allowed 
				status.text('Only JPG, PNG or GIF files are allowed');
				return false;
			}
			//status.text('Đang tải...');
            $("waiting-up").show();
		},
		onComplete: function(file, response) {
			//On completion clear the status
			$("waiting-up").css('display','none');
			//Add uploaded file to list
			if(response !== "0"){
                var _file = response.lastIndexOf('/');
			    $("#save-img").val(response.substring(_file+1,response.length)); 
				$('#files').html('<img src="'+response+'" alt="" />').addClass('success');
			} else{
				$('#files').text("Có lỗi! Không lưu được ảnh").addClass('error');
			}
		}
	});
});

function readCategory(level,pid) {
    $.get(base_url + '?com=category&act=catlevel',{'level':level,'pid':pid}, function(data) {
		return data;
	});
}

function addCategory() {
    var edit = $("#edit").val(); 
    if(edit == 1) {
        editCategory();
        return false;    
    }
    
    var name = $("#title").val();
    var slug = $("#slug").val();
    var position = $("#position").val();
    var parent_id = $("#parent_id option:selected").val();
    var home_1 = $("#home-1");
    var home_0 = $("#home-0");
    var status_1 = $("#status-1");
    var status_0 = $("#status-0");
    var comment = $("#comment").val();
    var level = $("#level").val();
    var img = $("#save-img").val();
    var p_title = $("#p_title").val();
    var p_description = $("#p_description").val();
    $(".d-tags").remove();
    var keywords = $("#tags").html();//$("#keywords").val();
    var home = 0;
    if(home_1.attr('checked') == 'checked') home = 1;
    
    var status = 0;
    if(status_1.attr('checked') == 'checked') status = 1;
    
    if(name == '' || name == null) {
        alert("Tên danh mục không được để trống");
        return false;
    } 
    
    //if(comment == '' || comment == null) {
//        alert("Mô tả danh mục không được để trống");
//        return false;
//    }
    
    $.get(base_url + '?com=category&act=newajax',{'name':name,'slug':slug,'level':level,'parent_id':parent_id,'position':position,'home':home,'status':status,'comment':comment,'img_icon':img,'p_title':p_title,'p_description':p_description,'keywords':keywords}, function(data) {
		if(isNaN(data))
			alert(data);
		else {
			var l = (level > 0)?level-1:1;
            onclickOption(l);
		}
	});
    return false;
}

function editCategory() {
    var action = $("#action").val();
    var id = $("#id").val();
    if(parseInt(action) == 1) {
        var name = $("#title").val();
        var slug = $("#slug").val();
        var position = $("#position").val();
        var parent_id = $("#parent_id option:selected").val();
        var home_1 = $("#home-1");
        var home_0 = $("#home-0");
        var status_1 = $("#status-1");
        var status_0 = $("#status-0");
        var comment = $("#comment").val();
        var level = $("#level").val();
        var img = $("#save-img").val();
        
        var p_title = $("#p_title").val();
        var p_description = $("#p_description").val();
        $(".d-tags").remove();
        var keywords = $("#tags").html();//$("#keywords").val();
        
        var home = 0;
        if(home_1.attr('checked') == 'checked') home = 1;
        
        var status = 0;
        if(status_1.attr('checked') == 'checked') status = 1;
        
        if(name == '' || name == null) {
            alert("Tên danh mục không được để trống");
            return false;
        } 
        
        //if(comment == '' || comment == null) {
//            alert("Mô tả danh mục không được để trống");
//            return false;
//        }
        
        $.get(base_url + '?com=category&act=editajax',{'action':action,'id':id,'name':name,'slug':slug,'level':level,'parent_id':parent_id,'position':position,'home':home,'status':status,'comment':comment,'img_icon':img,'p_title':p_title,'p_description':p_description,'keywords':keywords}, function(data) {
   			//window.location.reload();
            var l = (level-1 > 0)?level-1:1;
            onclickOption(l);
    	});
    }
    else {
        $.get(base_url + '?com=category&act=editajax',{'action':action,'id':id}, function(data) {
            if(data == 0) return false;
            else {
    			data = $.parseJSON(data);
                
                $("#title").val(data[1]);
                $("#position").val(data[10]);
                $("#comment").val(data[9]);
                $("#home-"+data[6]).attr('checked',true);
                $("#status-"+data[11]).attr('checked',true);
                $('#files').html('<img src="'+data[8]+'" alt="" />').addClass('success');
                $("#save-img").val(data[12]);
                if(data[3] == '')
                    $("#parent_id").html('<option value="0">Là cấp cha</option>');
                else
                    $("#parent_id").html(data[3]);
                $("#level").val(data[4]);
                
                $("#slug").val(data[16]);
                $("#tags").html(data[15]);
                $("#p_description").val(data[14]);
                $("#p_title").val(data[13]);
            }
    	});
    }
    return false;
}

function onclickOption(index) {
    var _id = $("#option-cat"+index+" option:selected").val();
    $.get(base_url + '?com=category&act=catlevel',{'level':(index+1),'pid':_id}, function(data) {
		$("#option-cat"+(index+1)).html('<option value="0">Danh mục cấp '+(index+1)+'</option>'+data);
        if(data == '<option value="0">Chưa có danh mục cha</option>') $("#property-cat-"+index).css('display','');
        else $("#property-cat-"+index).css('display','none');
	});
    if(index < 2) $(".admin-from table").attr('width','60%');
    else $(".admin-from table").attr('width','98%');
    $("#cat-level"+(index+1)).show();
}

function updatePath() {
    $("#update-path").jAlert("Đang cập nhật, đợi trong giây lát...","info", 'infoboxid');
    $.get(base_url + '?com=category&act=updatepath', function(data) {
        $(".close-info").click();
        $("#update-path").jAlert("Đã cập nhật thành công","success");
		getChildPrt(cid,pid);
	});
}

//id danh mục
function getPrt(id) {
    $.get(base_url + '?com=category&act=properties',{'id':id}, function(data) {
		$("#p-properties-box").html(data);
        $("#p-properties-box").show('normal');
	});
}

function getChildPrt(id,prtid) {
    $.get(base_url + '?com=category&act=childproperties',{'id':id,'pid':prtid}, function(data) {
		$("#float-properties-box").html(data);
        $("#float-properties-box").show('normal');
	});
}

function delPrt(id) {
    if(confirm("Bạn có muốn xoá nó không")) {
        $.get(base_url + '?com=property&act=delajax',{'id':id}, function(data) {
            $("#p-row-"+id).attr('bgcolor','#CCC');
    	    $("#p-row-"+id).hide('slow');
    	});
    }
}

function delChildPrt(id) {
    if(confirm("Bạn có muốn xoá nó không")) {
        $.get(base_url + '?com=property_value&act=delajax',{'id':id}, function(data) {
    		$("#pv-row-"+id).attr('bgcolor','#CCC');
    	    $("#pv-row-"+id).hide('slow');
    	});
    }
}

/***/
function addProperty() {
    var title = $("#title").val();
    var category = $("#category_id option:selected").val();
    var is_check = $("#is_check option:selected").val();
    
    if(title == '' || title == null) {
        alert('Nhập tên thuộc tính.');
        return false;
    }
    
    if(category == '' || category == null || category == 0) {
        alert('Chọn danh mục.');
        return false;
    }
    
    if(is_check < 0) {
        alert("Bạn chưa chọn dạng hiển thị.");
        return false;
    }
    $.get(base_url + '?com=property&act=newajax',{'title':title,'cid':category,'is_check':is_check}, function(data) {
		window.location.reload();
	});
}

function editProperty(id,edit) {
    if(edit == 0) {
        $.get(base_url + '?com=property&act=editajax&edit=0',{'id':id}, function(data) {
           data = $.parseJSON(data);
           $("#title-edit").val(data[0]);
           $("#category_id-edit").html(data[1]);
           $("#id-edit").val(id); 
           $("#status-edit option[value="+data[3]+"]").attr('selected',true);
        });    
        $(".form-dialog").dialog({
            title:'SỬA THUỘC TÍNH',
            width:'400px',
            buttons:{
               'Cập nhật':function(){
                    //call function here
                    var title = $("#title-edit").val();
                    var category = $("#category_id-edit option:selected").val();
                    var status = $("#status-edit option:selected").val();
                    
                    if(title == '' || title == null) {
                        alert('Nhập tên thuộc tính');
                        return false;
                    }
                    
                    if(category == '' || category == null || category == 0) {
                        alert('Chọn danh mục');
                        return false;
                    }
                    
                    $.get(base_url + '?com=property&act=editajax&edit=1',{'id':id,'title':title,'cid':category,'status':status}, function(data) {
                    	window.location.reload();
                    });
               },
               'Đóng lại':function(){
                  $(this).dialog("close");
               }
            }
        });
        return false;
    }
}

/***/
function addPropertyValue() {
    var title = $("#title").val();
    var property = $("#property_id option:selected").val();
    var category = $("#category_id option:selected").val();
    var color    = $("#color").val();
    
    if(title == '' || title == null) {
        alert('Nhập tên thuộc tính');
        return false;
    }
    
    if(category == '' || category == null || category == 0) {
        alert('Chọn danh mục');
        return false;
    }
    
    if(property == '' || property == null || property == 0) {
        alert('Chọn thuộc tính');
        return false;
    }
    
    $.get(base_url + '?com=property_value&act=newajax',{'title':title,'cid':category,'pid':property,'color':color}, function(data) {
		window.location.reload();
	});
}

function editPropertyValue(id,edit) {
    if(edit == 0) {
        $.get(base_url + '?com=property_value&act=editajax&edit=0',{'id':id}, function(data) {
           data = $.parseJSON(data);
           $("#title-edit").val(data[0]);
           $("#category_id-edit").html(data[1]);
           $("#property_id-edit").html(data[2]);
           $("#id-edit").val(id); 
           $("#status-edit option[value="+data[4]+"]").attr('selected',true);
           $("#color-edit").val(data[5]);
        });    
        $(".form-dialog-value").dialog({
            title:'SỬA THUỘC TÍNH',
            width:'500px',
            buttons:{
               'Cập nhật':function(){
                    //call function here
                    var title = $("#title-edit").val();
                    var category = $("#category_id-edit option:selected").val();
                    var property = $("#property_id-edit option:selected").val();
                    var status = $("#status-edit option:selected").val();
                    var color = $("#color-edit").val();
                    
                    if(title == '' || title == null) {
                        alert('Nhập tên thuộc tính');
                        return false;
                    }
                    
                    if(category == '' || category == null || category == 0) {
                        alert('Chọn danh mục');
                        return false;
                    }
                    
                    if(property == '' || property == null || property == 0) {
                        alert('Chọn thuộc tính');
                        return false;
                    }
                    
                    $.get(base_url + '?com=property_value&act=editajax&edit=1',{'id':id,'title':title,'cid':category,'pid':property,'status':status, 'color':color}, function(data) {
                    	window.location.reload();
                    });
               },
               'Đóng lại':function(){
                  $(this).dialog("close");
               }
            }
        });
        return false;
    }
}

function editPropertyValueCat(id,edit) {
    if(edit == 0) {
        $.get(base_url + '?com=property_value&act=editajax&edit=0',{'id':id}, function(data) {
           data = $.parseJSON(data);
           $("#title-value-edit").val(data[0]);
           $("#category_id-value-edit").html(data[1]);
           $("#property_id-value-edit").html(data[2]);
           $("#id-edit").val(id); 
           $("#status-value-edit option[value="+data[4]+"]").attr('selected',true);
           $("#color-edit").val(data[5]);
        });    
        $(".form-dialog-value").dialog({
            title:'SỬA THUỘC TÍNH',
            width:'500px',
            buttons:{
               'Cập nhật':function(){
                    //call function here
                    var title = $("#title-value-edit").val();
                    var category = $("#category_id-value-edit option:selected").val();
                    var property = $("#property_id-value-edit option:selected").val();
                    var status = $("#status-value-edit option:selected").val();
                    var color = $("#color-edit").val();
                    
                    if(title == '' || title == null) {
                        alert('Nhập tên thuộc tính');
                        return false;
                    }
                    
                    if(category == '' || category == null || category == 0) {
                        alert('Chọn danh mục');
                        return false;
                    }
                    
                    if(property == '' || property == null || property == 0) {
                        alert('Chọn thuộc tính');
                        return false;
                    }
                    
                    $.get(base_url + '?com=property_value&act=editajax&edit=1',{'id':id,'title':title,'cid':category,'pid':property,'status':status, 'color':color}, function(data) {
                    	//$("#pv-title-"+id).html(title);
                        getChildPrt(category,property);
                        $(this).dialog("close");
                    });
               },
               'Đóng lại':function(){
                  $(this).dialog("close");
               }
            }
        });
        return false;
    }
}

function editCatProperty(id,edit) {
    if(edit == 0) {
        $.get(base_url + '?com=property&act=editajax&edit=0',{'id':id}, function(data) {
           data = $.parseJSON(data);
           $("#title-edit").val(data[0]);
           $("#category_id-edit").html(data[1]);
           $("#id-edit").val(id); 
           $("#status-edit option[value="+data[3]+"]").attr('selected',true);
        });    
        $(".form-dialog").dialog({
            title:'SỬA THUỘC TÍNH',
            width:'400px',
            buttons:{
               'Cập nhật':function(){
                    //call function here
                    var title = $("#title-edit").val();
                    var category = $("#category_id-edit option:selected").val();
                    var status = $("#status-edit option:selected").val();
                    
                    if(title == '' || title == null) {
                        alert('Nhập tên thuộc tính');
                        return false;
                    }
                    
                    if(category == '' || category == null || category == 0) {
                        alert('Chọn danh mục');
                        return false;
                    }
                    
                    $.get(base_url + '?com=property&act=editajax&edit=1',{'id':id,'title':title,'cid':category,'status':status}, function(data) {
                    	$("#show-zproperty-"+id).html(title);
                    });
               },
               'Đóng lại':function(){
                  $(this).dialog("close");
               }
            }
        });
        return false;
    }
}
//--------------------------------------------------------//
function addPropertyFCat(category_id) {
    $("#category-p").val($("#p-catname strong").text());
    $(".form-dialog-add").dialog({
        title:'SỬA THUỘC TÍNH',
        width:'400px',
        buttons:{
           'Cập nhật':function(){
                //call function here
                var title = $("#title-p").val();
                var status = $("#status-p option:selected").val();
                
                if(title == '' || title == null) {
                    alert('Nhập tên thuộc tính');
                    return false;
                }
                
                $.get(base_url + '?com=property&act=newajax',{'title':title,'cid':category_id}, function(data) {
            		getPrt(category_id);
            	});
           },
           'Đóng lại':function(){
              $(this).dialog("close");
           }
        }
    });
    return false;
}

function addPropertyVFCat(cid,pid) {
    $("#category-pv").val($("#gpv-catname").text());
    $("#property-pv").val($("#pv-catname").text());
    $(".form-dialog-padd").dialog({
        title:'THÊM GiÁ TRỊ THUỘC TÍNH',
        width:'400px',
        buttons:{
           'Cập nhật':function(){
                //call function here
                var title = $("#pvalue-pv").val();
                var color = $("#color").val();
                var status = $("#status-pv option:selected").val();
                
                if(title == '' || title == null) {
                    alert('Nhập tên giá trị thuộc tính');
                    return false;
                }
                
                $.get(base_url + '?com=property_value&act=newajax',{'title':title,'cid':cid,'pid':pid,'color':color}, function(data) {
            		getChildPrt(cid,pid);
            	});
           },
           'Đóng lại':function(){
              $(this).dialog("close");
           }
        }
    });
    return false;
}