$(document).ready(function() {
        $('.box-cate').dialog({
            title: 'Thông tin danh mục',
            modal: true,
            autoOpen: false,
            width: 400
        });
       $('.bt-open-dlg').click(function(){
           resetForm();
           
           $('.box-cate').dialog('open');
           $('input[name=catId]').val('');
           $('.bt-add-shop').val('Thêm');
           
       });
       
       // Edit action
       $('.edit').click(function(e) {
           
           $('.bt-add-shop').val('Cập nhật');
           $('input[name=catId]').val($(this).attr('rel'));
         
           var id = $(this).attr('rel');
           var shopid = $(this).attr('shopid');
           var checkeds = $(this).attr('checkeds');
           $.ajax({
               url: '?com=item_home&act=edit&type=cate',
               type: 'POST',
               data: {
                   'cid':id
               },
               success: function(d) {
                   d = $.parseJSON(d);
                   
                   $('input[name=tb_category]').val(d.name);
                   $('input[name=chb_type]').attr('checked', d.type == '1' ? true: false);
                   $('input[name=tb_pos]').val(d.pos);
                   $('.cb-cate-sys').val(d.refer);
                   $('input[name=tb_note]').val(d.note);
                   $('input[name=chb_status]').attr('checked', d.status == '1' ? true: false);
                   var img = $('#category_img_'+id).attr('src');
                   $('#view_img','#box-cate').attr('src',img);
                   var parent_id = $('input[name="parent_id"]').val();
                   $("select.parent_id option[value='"+parent_id+"']").attr('selected',true);
                   $("#c_img").val( $('#category_img_'+id).attr('rel') );
                   
                   $("select.cb-cate-sys option[value='"+shopid+"']").attr('selected',true);
                
                   $('input[name=chb_status]').attr('checked', checkeds == '1' ? true: false);
                   //dung cho ban theo day
                 
                 
                   
                   $('.box-cate').dialog('open');
               }, error: function(d) {
                   
               }
           });
       });
       
       // Save action 
       $('.bt-add-shop').click(function(e) {
          var shopid = $('.cb-cate-sys').val();
          //var shopname =  $('input[name=tb_name]').val();
          var c_img =  $('input[name=c_img]').val();
          
          if(!shopid ){
              alert('Bạn chưa chọn shop');
              return false;
          }
           
          if(!c_img){
               alert('Bạn chưa có ảnh shop');
              return false;
          } 
          console.log(shopid);
       
           $.ajax({
               url: '?com=item_home_shop&act=save&type=cate',
               type: 'POST',
               data: {
                   'cid':$('input[name=catId]').val(),
                   'name':$('input[name=tb_category]').val(),
                   'image':c_img,
                   'parent_id': $('.parent_id').val(),
                   'type':$('input[name=type]').val(),
                   'shopid': shopid,
                   //'shopname': shopname,
                   'pos':$('input[name=tb_pos]').val(),
                   'status':$('input[name=chb_status]').attr('checked') == 'checked'? 1:0,
                   'note':$('input[name=tb_note]').val()
                   
                  
               },
               success: function(d) {
                   d = $.parseJSON(d);
                   if(!d.status) {
                       alert(d.msg);
                       return;
                   }
                   window.location.reload();
               }, error: function(d) {
                   
               }
           });
       });
       
       // Delete action
       $('.del').click(function(e) {
            if(confirm('Xóa danh mục?')) {
                $.ajax({
                    url: '?com=item_home&act=del&type=cate&cid=' + $(this).attr('rel'),
                    type: 'GET',
                    success: function(d) {
                        d = $.parseJSON(d);
                        if(d.status) {
                            window.location.reload();
                        } else {
                            alert('Xóa có lỗi xảy ra! Xin thử lại.');
                        }
                    }, error: function(err) {
                        
                    }
                });
            }
       });
       
       $('.bt-cancel').click(function(e) {
            resetForm();
       });
       //
       $('input[name="category_type"]').click(function(){
            var thisCheck = $(this);
            if ($('input[name="category_type"]').is (':checked'))
            {
                $('#shopcommit').show();
            }else{
                $('#shopcommit').hide();
            }
       });
       
        new AjaxUpload("#upload_img", {
                action: base_url + '?com=item_home&act=upload_image_cate',
                autoSubmit: true,
                onSubmit: function(file, extension) {
                },
                onComplete: function(file, data) { 
                    var arr = $.parseJSON(data); 
                    if(arr['errorMess'] != ''){ 
                        alert(arr['errorMess']);
                    } else {
                        $("#c_img").val(arr['image_name']);
                        $(".data-add img").attr('src', arr['image_url']);
                    }	
                }
            });
            
    });
    
    function resetForm() {
        $('#box-cate').find('input[type=text]').val('');
        $('#box-cate').find('input[type=checkbox]').attr('checked', false);
    }
   
	
           
       