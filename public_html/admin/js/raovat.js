$(function() { 
     $(".raovat-button-add").click(function() {
        var _rel = $(this).attr('rel');
        //if(_rel == 0) return false;
        var html_option = '';
        $("#level").val(_rel);
        if(_rel>0) {
            var pid = $("#option-cat"+(_rel-1)+" option:selected").val();
            $.get(base_url + '?com=raovat_category&act=getcat',{'level':_rel-1,'id':pid}, function(data) {
                if(data != '')
        		html_option = data.toString();
                $("#parent").html(html_option);
        	});
        }
        if($("#add-category-box").hide())
            $("#add-category-box").show('slow');
        else
            $("#add-category-box").hide('fast');
        $("#title-view").html("Thêm mới danh mục");
        $("input[type=text]").val('');
        $("textarea").val('');
        $("#edit").val(0);
        $("#title-cat").html('');
        $('#files').html('');
        $("#save-img").val("");
    });
    $(".raovat-button-del").click(function() {
        var _rel = $(this).attr('rel');
        if(_rel == 0) return false;
        var _id = $("#option-cat"+_rel+" option:selected").val();
        if(confirm("Bạn muốn xoá danh mục này, các danh mục con sẽ bị xoá nếu có")) {
            $.get(base_url + '?com=raovat_category&act=delajax',{'id':_id}, function(data) {
        		//onclickOptionRaovat(parseInt(data)-1);
                window.location.reload();
        	});
        }
        return false;
    });
    $(".raovat-button-edit").click(function() {
        var _rel = $(this).attr('rel');
        if(_rel == 0) return false;
        $("#action").val(0);
        $("#id").val($("#option-cat"+_rel+" option:selected").val());
        editCategoryRaovat();
        if($("#add-category-box").hide())
            $("#add-category-box").show('slow');
        else
            $("#add-category-box").hide('fast');
        $("#title-view").html("Chỉnh sửa danh mục:");
        $("#edit").val(1);
        $("#action").val(1);
        $("#title-cat").html($("#option-cat"+_rel+" option:selected").text());
        $("#c-title").html($("#p_title").val().length);
        $("#c-description").html($("#p_description").val().length);
    });
});

function readCategory(level,pid) {
    $.get(base_url + '?com=category&act=catlevel',{'level':level,'pid':pid}, function(data) {
		return data;
	});
}

function addCategoryRaovat() {
    var edit = $("#edit").val(); 
    if(edit == 1) {
        editCategoryRaovat();
        return false;    
    }
    
    var name = $("#title").val();
  
    var parent = $("#parent option:selected").val();
  
    
    if(name == '' || name == null) {
        alert("Tên danh mục không được để trống");
        return false;
    } 
    

    
    $.get(base_url + '?com=raovat_category&act=newajax',{'name':name,'parent':parent}, function(data) {
		if(isNaN(data))
			alert(data);
		else {
			//var l = (level > 0)?level-1:1;
            //onclickOptionRaovat(l);
            window.location.reload();
		}
	});
    return false;
}

function editCategoryRaovat() {
    var action = $("#action").val();
    var id = $("#id").val();
    
   if(parseInt(action) == 1) {//dung cho edit
        var name = $("#title").val();
      
        var parent = $("#parent option:selected",'#add-category-box').val();
        var type = $("#type option:selected",'#add-category-box').val();
        var status = $("#status option:selected",'#add-category-box').val();
       
        if(name == '' || name == null) {
            alert("Tên danh mục không được để trống");
            return false;
        } 
        
        $.get(base_url + '?com=raovat_category&act=editajax',{'action':action,'id':id,'name':name,'parent':parent,'status':status,'type':type}, function(data) {
   			
            //var l = (level-1 > 0)?level-1:1;
            //onclickOptionRaovat(l);
            window.location.reload();
    	});
    }else{
        $.get(base_url + '?com=raovat_category&act=editajax',{'action':action,'id':id}, function(data) {
            if(data == 0) return false;
            else {
    			data = $.parseJSON(data);
                $("#title").val(data.title);
                $("#type option[value="+data.type+"]").attr('selected','selected');
                $("#status option[value="+data.status+"]").attr('selected','selected');
                $("#parent option[value="+data.parent+"]").attr('selected','selected');
            }
    	});
   
    return false;
    }
}
function onclickOptionRaovat(index) {
    var _id = $("#option-cat"+index+" option:selected").val();
    $.get(base_url + '?com=raovat_category&act=catlevel',{'level':(index+1),'pid':_id}, function(data) {
		$("#option-cat"+(index+1)).html('<option value="0">Danh mục cấp '+(index+1)+'</option>'+data);
        if(data == '<option value="0">Chưa có danh mục cha</option>') $("#property-cat-"+index).css('display','');
        else $("#property-cat-"+index).css('display','none');
	});
    if(index < 2) $(".admin-from table").attr('width','60%');
    else $(".admin-from table").attr('width','98%');
    $("#cat-level"+(index+1)).show();
}

