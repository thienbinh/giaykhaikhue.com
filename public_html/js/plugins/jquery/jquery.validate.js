var validate = {
    'construct' : _error = true,
    
    $("label.error").remove();
    var fullname = $("#fullname").val();
    if($.trim(fullname).length <= 0) {
        $("#fullname").parent('td').append('<label class="error">Bạn chưa nhập đúng tên đầy đủ.</label>');
        _error = false;
    }
    
    var email = $("#email").val();
    var emailEx = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
    if(emailEx.test($.trim(email)) == false) {
        $("#email").parent('td').append('<label class="error">Bạn chưa nhập đúng email.</label>');
        _error = false;
    }

    var phone = $("#phone").val();
    if(/^\d+$/.test($.trim(phone))) {
        $("#phone").parent('td').append('<label class="error">Bạn chưa nhập đúng số điện thoại.</label>');
        _error = false;
    }
    var city = $("#city").val();
    if(city <= 0) {
        $("#city").parent('td').append('<label class="error">Bạn chưa chọn đúng tên tỉnh thành.</label>');
        _error = false;
    }
    var district = $("#district").val();
    if(district <= 0) {
        $("#district").parent('td').append('<label class="error">Bạn chưa chọn đúng tên quận huyện(thành phố).</label>');
        _error = false;
    }
    var address = $("#address").val();
    if($.trim(address).length <= 0) {
        $("#address").parent('td').append('<label class="error">Bạn chưa nhập đúng địa chỉ.</label>');
        _error = false;
    }
    if(_error == false) return false;
}