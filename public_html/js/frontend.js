/**
 * Banner slide
 */
var currentImage;
var currentIndex = -1;
var interval;
var myTimer;
function showImage(index){
    if(index < $('#bigPic img').length){
        var indexImage = $('#bigPic img')[index];
        if(currentImage) {   
            if(currentImage != indexImage ) {
                $(currentImage).css('z-index',2);
                clearTimeout(myTimer);
                $(currentImage).fadeOut(250, function() {
                    myTimer = setTimeout("showNext()", 3000);
                    $(this).css({
                        'display':'none',
                        'z-index':1
                    });
                });
            }
        }
        $(indexImage).css({
            'opacity':1
        });
        $(indexImage).fadeIn('slow');
        currentImage = indexImage;
        currentIndex = index;
        $('#thumbs li').removeClass('active');
        $($('#thumbs li')[index]).addClass('active');
    }
} 
function showNext(){
    var len = $('#bigPic img').length;
    var next = currentIndex < (len-1) ? currentIndex + 1 : 0;
    showImage(next);
}
    
/**
 * Run on load
 */
$(document).ready(function() {
    // Check element thumbs
    if($('#thumbs').length > 0) {
        myTimer = setTimeout("showNext()", 3000);
        showNext(); //loads first image
        $('#thumbs li').bind('click',function(e){
            var count = $(this).attr('rel');
            showImage(parseInt(count)-1);
        });
    }
    // Auto run next slide new items
    if($('.items.new').length > 0) {
        auto_run("next_slide($('.items.new').find('.next'))", 3000);
        // Stop or start auto run slide
        $('.items.new').hover(function() {
            clearTimeout(slide_timer);
        }).mouseleave(function() {
            auto_run("next_slide($('.items.new').find('.next'))", 3000);
        });
    }
   
    // fancy box
    $("a.fancybox").fancybox();
});
/* --------------------- */

var slide_timer = null;
function auto_run(func, delay) {
    eval(func);
    slide_timer = setTimeout("auto_run(\"" + func + "\", " + delay + ")", delay);
}

// slide item new
function next_slide(obj) {
    obj = $(obj).parent().parent().parent();
    var space = 192;
    // Width of slide
    var wslider = $(obj).find('ul').css('width').replace('px', '');
    // Current position
    var lefts  = parseInt( $(obj).find('ul').css('left').replace('px','').replace('-','') );
    if((lefts + space) >= wslider){
        $(obj).find("ul").css('left', space + 'px');
        $(obj).find("ul").animate({"left": "-=" + space + "px"}, "slow");
        return false;
    } else {
        $(obj).find("ul").animate({"left": "-=" + space + "px"}, "slow");
    }
    return true;
}
function preview_slide(obj) {
    obj = $(obj).parent().parent().parent();
    var space = 192;
    // Width of slide
    var wslider = $(obj).find('ul').css('width').replace('px', '');
    // Current position
    var lefts  = parseInt( $(obj).find('ul').css('left').replace('px',''));
    if(lefts >= 0){
        $(obj).find("ul").css('left', '-' + (wslider) + 'px');
        $(obj).find("ul").animate({"left": "+=" + space + "px"}, "slow");
        return false;
    } else {
        $(obj).find("ul").animate({"left": "+=" + space + "px"}, "slow");
    }
    return true;
}

/**
 * Link refer
 */ 
function go_link() {
    var url = $('#link_element option:selected').val();
    if(isNaN(url)) {
        window.open(url, "_blank");
    }
}