<?php

/**
 * class helper utility
 * Enter description here ...
 *
 */
function currency_format($value, $add_currency=false, $symbol='đ') {
    if (strlen($value) > 2) {
        if ($value != '' and is_numeric($value)) {
            $value = number_format($value, 2, ',', '.');
            $value = str_replace(',00', '', $value);
        }
    }
    if ($add_currency)
        $value.=' ' . $symbol;
    return $value;
}

/**
 * Ham cat xau tra ve du tu
 *
 * @param string $str - xau can cat
 * @param int $len - so ky tu can cat
 * @param string $pad - chuoi can noi them vao cuoi, defaul '...'
 * @param boolean $strip - yeu cau trip_tags truoc khi cat hay khong
 * @return string
 */
function strLeft($str, $len, $pad='...', $strip=FALSE) {
    if (strlen($str) <= $len)
        return $str;
    if ($strip)
        $str = strip_tags($str);
    $txt = substr($str, 0, $len);
    $pos = strrpos($txt, ' ');
    return substr_replace($txt, $pad, $pos + 1);
}

//funtion get Lang
function _getLang() {
    $lang = new Lang;
    $langs = array();
    $langs = $lang->selectList(NULL, NULL, NULL, 'key');
    return $langs;
}

function _e($key, $l='vi', $def=null, $langs = NULL) {
    echo _gL($key, $l, $def, $langs);
}

function _gL($key, $l='vi', $def=null, $langs=null) {

    $lb = '';
    if ($langs == NULL) {
        $langs = _getLang();
    }
    if (isset($langs[$key])) {

        if ($l == 'vi') {
            if ($langs[$key]['vi'] != '') {
                $lb = $langs[$key]['vi'];
            } else {
                $lb = $def;
            }
        } else if ($l == 'cn') {

            if ($langs[$key]['cn'] == '') {
                $lb = $def;
            } else {
                $lb = $langs[$key]['cn'];
            }
        } else {
            $lb = $def;
        }
    } else {
        $lb = $def;
    }
    return $lb;
}

//ham chuyen dinh dang ngay thang nam ra so de so sanh trong tim kiem
function _strtotime($time) {
    $tmp = array();
    $tmp = explode("/", $time);
    $new_str = $tmp[2] . '/' . $tmp[1] . '/' . $tmp[0];
    return strtotime($new_str);
}

function isUTF8($string) {
    return (utf8_encode(utf8_decode($string)) == $string);
}

//chekc utf-8
function is_utf8($str) {
    $c = 0;
    $b = 0;
    $bits = 0;
    $len = strlen($str);
    for ($i = 0; $i < $len; $i++) {
        $c = ord($str[$i]);
        if ($c > 128) {
            if (($c >= 254))
                return false;
            elseif ($c >= 252)
                $bits = 6;
            elseif ($c >= 248)
                $bits = 5;
            elseif ($c >= 240)
                $bits = 4;
            elseif ($c >= 224)
                $bits = 3;
            elseif ($c >= 192)
                $bits = 2;
            else
                return false;
            if (($i + $bits) > $len)
                return false;
            while ($bits > 1) {
                $i++;
                $b = ord($str[$i]);
                if ($b < 128 || $b > 191)
                    return false;
                $bits--;
            }
        }
    }
    return true;
}

function detectUTF8($string) {
    return preg_match('%(?:
        [\xC2-\xDF][\x80-\xBF]        # non-overlong 2-byte
        |\xE0[\xA0-\xBF][\x80-\xBF]               # excluding overlongs
        |[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}      # straight 3-byte
        |\xED[\x80-\x9F][\x80-\xBF]               # excluding surrogates
        |\xF0[\x90-\xBF][\x80-\xBF]{2}    # planes 1-3
        |[\xF1-\xF3][\x80-\xBF]{3}                  # planes 4-15
        |\xF4[\x80-\x8F][\x80-\xBF]{2}    # plane 16
        )+%xs', $string);
}

/**
 * remove tag ko cần thiết
 */
function removeTagNotFound($content, $searchs) {
    $standContent = '';
    /* $src_regex = '/\s*src\s*=\s*["\'](.*?)\s*["\']/s';
      preg_match_all($src_regex, $content, $srcs); */
    foreach ($searchs as $img) {
        $standContent = str_replace($img, '', $content);
    }

    return $standContent;
}

/*
 * 
 */
function cut_str_left($str, $len, $pad='...', $strip=FALSE) {
    //$str_strip = html_entity_decode( strip_tags($str,'<br><p><img><table><tr><td>') );    
    $str_strip = html_entity_decode(strip_tags($str, '<div><p><img>'));
    $str_strip = preg_replace('/<[^>]*>/', '', $str_strip);

    if (strlen($str_strip) <= $len) {
        return json_encode(array('msg' => 'false', 'str' => $str_strip));
    }
    $txt = substr($str_strip, 0, $len);
    return json_encode(array('msg' => 'done', 'str' => $txt . $pad));
}

/**
 * Auto Generate String
 * @param type $length: Length of string want return
 * @return type STRING
 */
function genRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';

    $string = '';

    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return strtoupper($string);
}
/**
 * data serilize
 * @param type $data
 * @return type Array
 */
function data_from_serilize($data) {
    $data_array = explode("amp;", $data);
    $result = array();
    foreach ($data_array as $a) {
        $key_values = explode("=", $a);
        $result[$key_values[0]] = str_replace("&", "", $key_values[1]);
    }
    return $result;
}

//lấy danh sách file bên trong 1 forder
function getDirectoryList($directory) {
    $results = array();
    $handler = opendir($directory);
    while ($file = readdir($handler)) {
        if ($file != "." && $file != "..") {
            $results[] = $file;
        }
    }
    closedir($handler);
    return $results;
}

?>
