<?php

/**
 * tien ich ngoai front
 * @author       trung
 */
class FrontendUtil {

    public static $routing = array();

    //not found a page
    public static function page404() {
        $document = Flywheel_Factory::getDocument();
        $url = $document->getPublicPath() . 'page404';

        return $url;
    }

    //not permisson
    public static function page403() {
        $document = Flywheel_Factory::getDocument();
        $url = $document->getPublicPath() . 'page403';

        return $url;
    }

    /**
     * Hàm tạo link url
     * @version         $Id$
     * @since           Ff v2.0
     * @package         Flywheel
     *
     */
    public static function url_format($controller, $action = 'default', $params = array()) {
        
        $document   = Flywheel_Factory::getDocument();
        $url        = $document->getPublicPath();
        
        if (!empty(self::$routing)) {
            $config = self::$routing;
        } else {
            $configFile = APPS_DIR . 'frontend' . DS . 'configs' . DS . 'routing.ini';
            $config = parse_ini_file($configFile, true);
            self::$routing = $config;
        }
        
        if (intval($config['friendly_url']) == 1) {
            $str = '';
            // tra ve url da rewrite    
            if (!empty($params)) {
                foreach ($params as $param => $val) {
                    if ($str == '')
                        $str.= $param . '/' . $val;
                    else
                        $str.= '/' . $param . '/' . $val;
                }
            }
            $url .= $config['reflect'][$controller] . '/' . $action;
            if ($str != '')
                $url.='/' . $str;
        } else {
            //tra ve url chua rewrite
            $str = '';
            if (!empty($params)) {
                foreach ($params as $param => $val) {
                    if ($str == '')
                        $str.= $param . '=' . $val;
                    else
                        $str.= '&' . $param . '=' . $val;
                }
            }
            $url.='index.php?com=' . $controller . '&act=' . $action;
            if ($str != '')
                $url.='&' . $str;
        }
        return $url;
    }

    public static function currencyFormat($value) {
        if (@strlen($value) > 2) {
            if ($value != '' and is_numeric($value)) {
                $value = number_format($value, 2, ',', '.');
                $value = str_replace(',00', '', $value);
            }
        }
        return $value;
    }

    /**
     * url product cua san
     * @param type $controller
     * @param type $action
     * @param type $params
     * @return string 
     */
    public static function urlProduct($route, $params=array()) {
        $document = Flywheel_Factory::getDocument();
        $url = $document->getPublicPath();

        //tra ve url da rewrite    

        if (isset($params['title']))
            $params['title'] = BuildLink::slugify($params['title']);
        switch ($route) {
            case 'shop':
                $url_suffix = '';

                $url.='gian-hang/san-pham/' . $params['user_id'] . '' . (isset($params['cat_id']) ? ('?cat_id=' . $params['cat_id']) : '' );


                break;
            case 'product-detail':
                $url_suffix = '.html';
                if (!isset($params['title']))
                    $params['title'] = 'no-title';

//                    $url.='gian-hang/san-pham/'.$params['user_id'].'/';
//                    $url.= $params['title'].'-'.$params['item_id'];
                $url.='san-pham/';
                $url.= $params['title'] . '-' . $params['item_id'];
                $url.= $url_suffix;
                break;
        }
        //phan trang 
        if (!empty($params['page'])) {
            $url.='/trang-' . $params['page'];
        }
        if (!empty($params['sort'])) {
            $url.='/loc-' . $params['sort'];
        }


        return $url;
    }

}