<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// Load categories
$cateObj_left = new ItemCategory();
$_cateList_left = $cateObj_left->selectList("`status`=1", "`pos` ASC", null, "id");

if (!empty($_cateList_left)) {
    // Filter childs to parent
    $_trees = array();
    foreach ($_cateList_left as $k => $v) {
        foreach ($_cateList_left as $k_child => $v_child) {
            if ($v_child['pid'] == $k) {
                if(!isset($_trees[$k]))
                    $_trees[$k] = $v;
                
                $_trees[$k]['childs'][] = $v_child;
                unset($_cateList_left[$k_child]);
            }
        }
    }
    $_cateList_left = $_trees;
}
//echo '<pre>';
//print_r($_cateList_left);
//die();

$router = Flywheel_Factory::getRouter();
$selected = false;
if(strtolower($router->getController()) == 'browse') {
    $selected = true;
}

$req = Flywheel_Factory::getRequest();
$id = $req->get('id', Flywheel_Filter::TYPE_INT, 0);

?>
<div class="browse-left fl">
    <h2>Sản phẩm</h2>
    <?php
    
    if (!empty($_cateList_left)) {
        foreach ($_cateList_left as $k => $v) {
            ?>
            <div class="widget">
                <h3 lass="title"><a href=""><?php echo $v['title'] ?></a></h3>
                <?php if (array_key_exists('childs', $v)) {
                    ?>
                    <ul>
                        <?php
                        foreach ($v['childs'] as $k1 => $v1) {
                            ?>
                            <li class="<?php echo $selected & $v1['id'] == $id ? 'active' : '' ?>">
                                <a href="<?php echo FrontendUtil::url_format('browse', 'default', array('title' => Flywheel_Inflector::filter($v['title'], true), 'id' => $v1['id'])) ?>">
                                    <?php echo $v1['title'] ?>
                                </a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                <?php } ?>
            </div>
            <?php
        }
    }
    ?>

    <h2>Dịch vụ</h2>     
    <div class="widget">
        <h3 lass="title"><a href="">Bán hàng tận nơi</a></h3>
        <h3 lass="title"><a href="">Vận chuyển miễn phí</a></h3>
    </div>

    <?php
    $_session_views = $_session->get('current_views');
    if (!empty($_session_views)) {
        ?>
        <h2>Sản phẩm đã xem</h2>
        <div class="widget items">
            <ul>
                <?php
                foreach ($_session->get('current_views') as $k => $v) {
                    ?>
                    <li>
                        <a href="<?php echo FrontendUtil::url_format('item', 'default', array('title' => Flywheel_Inflector::filter($v['title'], true), 'id' => $v['id'])) ?>" title="<?php echo $v['title'] ?>">
                            <?php $img = !preg_match('/http:\/\//', $v['img']) ? $url_path . 'medias/items/'.$v['img'] 
                                            : $v['img'] ?>
                            <img src="<?php echo $img ?>" alt="<?php echo $v['title'] ?>" />
                        </a>
                        <h2>
                            <a href="<?php echo FrontendUtil::url_format('item', 'default', array('title' => Flywheel_Inflector::filter($v['title']), 'id' => $v['id'])) ?>" title="<?php echo $v['title'] ?>"><?php echo $v['title'] ?></a>
                        </h2>
                        <span class="price">Giá: <span><?php echo FrontendUtil::currencyFormat($v['price']) ?></span> VNĐ</span>
                        <span class="remain">Còn <span><?php echo $v['instock'] ?></span> sp</span>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    <?php } ?>
</div>