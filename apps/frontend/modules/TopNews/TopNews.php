<?php
class TopNews extends Flywheel_Module {
    public function  __construct() {
    }
	public function render() {
        $view = Flywheel_Factory::getView();
        $this->_template = 'modules/TopNews';
        $view->assign('newContents', ContentPeer::getNewContents());
		return $view->render($this->_template);
	}	
}