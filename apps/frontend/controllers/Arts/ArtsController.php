<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once CONTROLLERS_DIR.'FrontendController.php';
require_once APPS_DIR .'frontend'.DS.'libraries'.DS.'FrontendUtil.php';

class ArtsController extends FrontendController {
    
    public function beforeExecute() {
        parent::beforeExecute();
    }
    
    /**
     * // -----------------------------------------------
     * Articles
     */
    public function executeDefault() {
        
        $req    = Flywheel_Factory::getRequest();
        $view   = Flywheel_Factory::getView();
        $doc    = Flywheel_Factory::getDocument();
        $_cateObj   = new ContentCategory();
        $_artObj    = new Content();
        
        $cid    = $req->get('cid', Flywheel_Filter::TYPE_INT, 0);
        
        // Get category information
        $_category = $_cateObj->retrieveByPk($cid);
        // Get list arts
        $_arts      = $_artObj->selectList("`category_id`={$cid} AND `status`=1", "`created_time` DESC");
        
        $view->assign('_category', $_category);
        $view->assign('_arts', $_arts);
        $this->setView('default');
    }
    
    /**
     * // -----------------------------------------------
     * trang chi tiet
     */
    public function executeDetail() {
        $req    = Flywheel_Factory::getRequest();
        $view   = Flywheel_Factory::getView();
        $doc    = Flywheel_Factory::getDocument();
        
        $id = $req->get('id', Flywheel_Filter::TYPE_INT, 0);
        $_artObj = new Content();
        $_art = $_artObj->retrieveByPk($id);
        
        // Read more other arts
        $_arts = $_artObj->selectList("`category_id` = {$_art->category_id} AND `status` = 1 AND `id` != {$id}", "`created_time` DESC", "0, 10");
        
        $view->assign('_art', $_art);
        $view->assign('_arts', $_arts);
        
        $this->setView('detail');
        
    } 
    
    /**
     * View contact detail
     */
    public function executeContact() {
        
        $this->setView('contact');
        
    }
}
?>