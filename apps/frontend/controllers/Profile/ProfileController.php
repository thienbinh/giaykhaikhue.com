<?php

require_once CONTROLLERS_DIR . 'FrontendController.php';
require_once GLOBAL_DIR . 'helper/mail_helper.php';
require_once APPS_DIR . 'shop/libraries/ShopUtil.php';
require_once GLOBAL_HELPER_DIR . 'UtilityHelper.php';
require_once GLOBAL_DIR . 'helper/mail_helper.php';
require FLYWHEEL_UTIL . 'Browser.php';

class ProfileController extends FrontendController {

    //put your code here
    public $img = '';
    public $request = '';
    public $document = '';
    public $view = '';
    public $user = '';
    public $url_path = '';

    public function beforeExecute() {
        parent::beforeExecute();
        $this->img = new Images();
        $this->request = Flywheel_Factory::getRequest();
        $this->document = Flywheel_Factory::getDocument();
        $this->view = Flywheel_Factory::getView();
        $this->user = Flywheel_Authen::getInstance()->getUser();
        $this->url_path = Flywheel_Factory::getDocument()->getPublicPath();

        $this->document->addCss('profile.css');
        $this->document->addCss('jquery.alerts.css');
        $this->document->addJs('profile.js');

        $this->document->addJs('UIBlock.js', 'TOP');
        $this->document->addJs('plugins/jquery/jquery.alerts.js');
    }

    public function executeDefault() {
        $this->document->title = "Tài khoản cá nhân";
        Login::checkLogin();
        $this->setLayout('profile_user_2_col');
        $this->setView('default');
    }

    public function executeGalary() {
        $this->document->title = "Thư viện ảnh cá nhân";
        $this->setLayout('profile_user_3_col');
        $this->setView('galary');
        $router = Flywheel_Factory::getRouter();
        $absoluteUri = "thu-vien-anh";
        Login::checkLogin();
        $prs = $router->getParams();
        if ($prs)
            $page = $prs[0];
        else
            $page = 1;
        $perpage = 16;
        $limit = $perpage * ($page - 1) . ',' . $perpage;
        $this->view->assign('user', $this->user);
        $imgs = $this->img->selectList("`username`='{$this->user->username}'", 'name desc', $limit);
        $countAll = $this->img->count("`username`='{$this->user->username}'");
        $paging = new Paging();
        $strpage = $paging->paging($countAll, $perpage, $page, 5, $absoluteUri);

        $this->view->assign('strpage', $strpage);
        $this->view->assign('imgs', $imgs);
    }

    public function executeUpdateStatus() {
        $request = Flywheel_Factory::getRequest();
        $status = trim($request->post('status'));

        $_user = new Users($this->user);
        $_user->setStatus($status);
        $_user->setStatusTime(time());
        $_user->setNew(false);
        $_user->save();
        echo _usingEmo($status);
        die;
    }

    public function executeSave() {

        $request = $this->request;
        $user_id = $this->user->id;
        
        $user = new Users();
        $user->read($user_id);
        $user->setPhone($request->get("phone"));
        $user->setFullname($request->get("fullname"));
        $user->setGender($request->get("gender"));
        $user->setBirthday($request->get("year_born") . "/" . $request->get("month_born") . "/" . $request->get("date_born"));

        $user->setDetailAddress($request->get("detail_address"));
        $user->setQhAddress($request->get("qh_address"));
        $user->setTtAddress($request->get("tt_address"));

        $user->setBankAccount($request->get("bank_account"));
        $user->setBankSelf($request->get("bank_self"));
        $user->setBankName($request->get("bank_name"));
        $user->setBankBrand($request->get("bank_brand"));
        $user->setAccNl($request->get("acc_nl"));
        $user->setAccBk($request->get("acc_bk"));
        $user->setCmtnd($request->get("cmtnd"));
        $user->setMobile($request->get("mobile"));
        $user->setEmail($request->get("email")); //cho cap nhat email
        $user->setYahoo($request->get("yahoo"));
        $user->setSkype($request->get("skype"));
        $user->setQq($request->get("qq"));
        $user->setAlioangoang($request->get("alioangoang"));
        $user->setNew(false);
        if ($user->save()) {
            Flywheel_Authen::getInstance()->resetSessionUser($user);
            echo '1';
        } else {
            echo '0';
        }
        die;
    }

    public function executeUploadImage() {

        if (!is_dir(DATA_DIR . $this->user->username))
            @mkdir(DATA_DIR . $this->user->username, 0777);
        require_once LIBRARIES_DIR . 'ImageSD' . DS . 'Upload.php';
        $upl = 'upload_img';
        $title_img = $_POST['title_img'];
        $upload = new UploadSD;
        $image = $upload->uploadImageMultySize($upl, $this->user->username, Item::$arrThunb);

        $arrayInsert = array(
            'name' => $image['image'],
            'comment' => '',
            'username' => $this->user->username,
            'item_id' => 0,
            'status' => 1,
            'position' => 1,
            'title_img' => $title_img,
            'folder_path' => $image['url_org'],
            'created_at' => time(),
            'created_by' => $this->user->username,
        );
        $this->img->insert($arrayInsert);
        $this->request->redirect($this->url_path . "thu-vien-anh");
    }

    public function executeChangeAvatar() {

        if (!is_dir(DATA_DIR . $this->user->username))
            @mkdir(DATA_DIR . $this->user->username, 0777);
        require_once LIBRARIES_DIR . 'ImageSD' . DS . 'Upload.php';
        $upl = 'uploadfile';
        $upload = new UploadSD;
        $image = $upload->uploadImageMultySize($upl, $this->user->username, Item::$arrThunb);
        
        $arrayInsert = array(
            'name' => $image['image'],
            'comment' => '',
            'username' => $this->user->username,
            'item_id' => 0,
            'status' => 1,
            'position' => 1,
            'title_img' => '',
            'folder_path' => $image['url_org'],
            'created_at' => time(),
            'created_by' => $this->user->username,
        );
        $this->img->insert($arrayInsert);

        $_user = new Users();
        $_user->read($this->user->id);
        $_user->setAvatar($image['image']);
        $_user->setNew(false);
        $_user->save();
        //set lai avatar cho session hien tai
        $user = Flywheel_Authen::getInstance()->getUser();
        $img = new Images();
        $_SESSION['user']['avatar'] = $image['image'];
        echo $img->getImageUrl($image['image'], $this->user->username, Item::SIZE_120);
        exit;
    }

    public function executeDeleteImg() {

        $request = Flywheel_Factory::getRequest();
        $list = $request->post('list');
        $arr_list = explode(",", $list);
        foreach ($arr_list as $_list) {
            $this->img->read($_list);
            $this->img->deleteFile();
        }
        echo 1;
        die;
    }

    public function executeSetupAvatar() {
        $user = Flywheel_Authen::getInstance()->getUser();
        $request = Flywheel_Factory::getRequest();
        $img_name = $request->post('img_name');

        $_user = new Users;
        $_user->read($user->id);
        $_user->setAvatar($img_name);
        $_user->setNew(false);
        $_user->save();

        die(Users::getAvatar($_user, null, 'avatar'));
    }

    public function executeChangePass() {
        $user = Flywheel_Authen::getInstance()->getUser();
        $request = Flywheel_Factory::getRequest();
        $old_pass = $request->post('old_pass');
        $new_pass = $request->post('new_pass');
        $renew_pass = $request->post('renew_pass');
        $u = new Users();
        $u->read($user->id);

        $msg = "";
        if ($old_pass == "") {
            $msg.="Chưa nhập mật khẩu cũ";
        } else if (UsersPeer::comparePassword($old_pass, $u->password) === false) {
            $msg.="Nhập mật khẩu cũ chưa đúng";
        } else if ($new_pass == "") {
            $msg.="Chưa nhập mật khẩu mới";
        } else if (strlen($new_pass) < 8 or strlen($new_pass) > 32) {
            $msg.="Mật khẩu tối thiểu 8 ký tự,tối đa 32 ký tự";
        } else if ($renew_pass == "") {
            $msg.="Chưa nhập lại mật khẩu mới";
        } else {
            if ($new_pass != $renew_pass) {
                $msg.="Mật khẩu nhập lại không đúng";
            }
        }
        $type = 0;
        if ($msg == "") {
            $salt = UsersPeer::generateSalt($u->username);
            $u->setPassword($salt . md5($salt . $new_pass));
            $u->setNew(false);
            $u->save();

            $type = 1;
            $msg = "Đổi mật khẩu thành công";
        }
        print_r(json_encode(array('type' => $type, 'msg' => $msg)));
        exit;
    }

    //change order code
    public function executeChangeOrderCode() {
        $user = Flywheel_Authen::getInstance()->getUser();
        $request = Flywheel_Factory::getRequest();
        $order_code = $request->get('order_code');

        $u = new Users();
        $u->read($user->id);

        if ($order_code == $u->order_code) {
            echo json_encode(array('error' => 0, 'msg' => 'Chưa nhập mã order mới'));
            exit;
        }

        $order_code = strtoupper($order_code);
        $u->setCode($order_code);
        $u->setIsEditOrderCode(1);
        $u->setNew(false);
        if ($u->save()) {
            echo json_encode(array('error' => 1, 'msg' => 'done'));
            exit;
        } else {
            echo json_encode(array('error' => 0, 'msg' => 'error'));
            exit;
        }
    }

    public function executeResendMailActive() {
        $user = Flywheel_Authen::getInstance()->getUser();

        $e_a = new EmailActive();
        $d = $e_a->_get(" AND `user_id`=$user->id");
        $data = array(
            'link_active' => Flywheel_Factory::getDocument()->getPublicPath() . "registry/active?code=" . $d['code'],
            'user_name' => $user->username
        );
        $recipients = $user->email;
        $subject = '[ALIMAMA.VN] Thông tin yêu cầu kích hoạt tài khoản';
        $body = get_mail_template('active_account', $data);
        send_mail($recipients, $subject, $body);
        echo 1;
        exit;
    }

    public function executeGetListemo() {
        $emos = _getListEmo();
        if ($emos and !empty($emos)) {
            $a = '<table style="position: absolute;background: #F9F9F9;margin-left: 179px;margin-top: 27px;">';
            $a.= '<tr>';
            $i = 0;
            foreach ($emos as $emo) {
                $i++;
                $a.= '<td style="text-align:center;padding:5px;">' . $emo . '</td>';
                if ($i == 7) {
                    $a.='</tr><tr>';
                    $i = 0;
                }
            }
            $a.='</table>';
        }
        echo $a;
        die();
    }

    /*     * **************Begin Dung lv********************************************************* */

    public function executePaymentPassword() {
        $result = array();
        
        $request = $this->request;
        if ($request->get("payment_password")) {
            $check_string = preg_match('/^(?=.*\d)(?=.*[A-Za-z])[A-Za-z0-9]*$/', $request->get("payment_password"));
            if (!$check_string) {
                $result = array('error' => 1, 'msg' => 'error_string');
            } else {
                $user_id = $this->user->id;
                $_token = genRandomString(5);
                $pay = new UserPaymentPassword();
                $check = $pay->read($user_id);

                $_browser = new Browser();
                $_agentBrowser = $_browser->getBrowser();
                $pay->setBrowser(serialize(array($_agentBrowser)));
                $pay->setUserId($user_id);
                $pay->setPassword($request->get("payment_password"));
                $pay->setKeyVerify($_token);
                $pay->setCreatetime(time());
                $pay->setUpdatetime(time());

                $pay->setNew($check ? false : true);

                if ($pay->save() > 0) {
                    $result = array('error' => 0, 'msg' => '');
                    
                    // Send mail
                    $baseUrl = Flywheel_Factory::getRouter()->getFrontControllerPath();
                    $recipients = $this->user->email;
                    $subject = '[ALIMAMA.VN] xác minh mật khẩu thanh toán';
                    $data = array(
                        'link_verify' => Flywheel_Factory::getDocument()->getPublicPath() . "xac-minh-thanh-toan?email=" . $recipients . "&createtime=" . time() . "&secure_code=" . $_token,
                        'username' => $this->user->username,
                        'email' => $this->user->email,
                        'secure_code' => $_token,
                        'date' => date('d/m/Y', time()),
                    );

                    $body = get_mail_template('user_verify_payment', $data);
                    send_mail($recipients, $subject, $body);
                } else {
                    $result = array('error' => 1, 'msg' => 'Tạo mật khẩu không thành công! Xin thử lại.');
                }
            }
            return $this->renderText(json_encode($result));
        } else {
            $this->document->title = "Mật khẩu thanh toán";
            Login::checkLogin();
            $this->setLayout('profile_user_2_col');
            $this->setView('payment_password');
        }
    }

    public function executeChangePaymentPassword() {
        
        $uid = $this->user->id;
        if ($uid == 0) {
            echo json_encode(array('error' => 1, 'msg' => 'LOGIN_REQUIRE'));
            die();
        }
        $request = $this->request;
        $new_password = $request->get("new_password");

        $check_string = preg_match('/^(?=.*\d)(?=.*[A-Za-z])[A-Za-z0-9]*$/', $new_password);
        if (!$check_string) {
            echo json_encode(array('error' => 1, 'msg' => 'STRING_ERROR'));
        } else {
            //update payment
            $_token = genRandomString(5);
            $_pay = new UserPaymentPassword;
            $pay = $_pay->read($this->user->id);
            $pay->setPassword($new_password);
            $pay->setKeyVerify($_token);
            $pay->setBrowser('');
            $pay->setUpdatetime($pay->updatetime.','.time());
            $pay->save();
            
            Flywheel_DB::getConnection(
                            UserSecurity::TABLE
                        )->delete(UserSecurity::TABLE, "uid=$uid");
            echo json_encode(array('error' => 0, 'msg' => 'SAVE_SUCCESS'));
        }

        die;
    }

    /**
     * Verify payment password
     * @return type Array
     */
    public function executeVerifyPayment() {

        $request = $this->request;
        $_pay = new UserPaymentPassword();
        $result = array();
        
        if (!$this->user->id) {
            echo json_encode(array('error' => 1, 'msg' => 'error'));
            die();
        }

        try {
            $pay = $_pay->read($this->user->id);
            if ($pay->key_verify != $request->post("secure_code")) {
                $result = array('error' => 1, 'msg' => 'secure_code');
            } else {
                
                // Create session
                $_browser = new Browser();
                $_agentBrowser = $_browser->getBrowser();
                $_token = md5('SEC_COOK_' . time());
                setcookie('SEC_COOK', $_token, 7 * 24 * 3600 + time(), '/');
                
                // Security record
                $security = new UserSecurity();
                // Check user has token with current browser or not
                $uid = $this->user->id;
                $user_security = $security->utility(" WHERE browser='$_agentBrowser' AND uid=$uid");
                
                if(!empty($user_security)) { // Exist, maybe lost session or 
                    $_token = $_token.','.$user_security[0]['token'];
                    $security->read($user_security[0]['id']);
                }
                
                $security->setUid($uid);
                $security->setBrowser($_agentBrowser);
                $security->setToken($_token);
                
                if($security->save()) {
                    $result = array('status' => true, 'msg' => 'VERIFIED_SUCCESS');
                } else {
                    $result = array('status' => true, 'msg' => 'VERIFIED_FALSE');
                }
                
            }
            echo json_encode($result);
        } catch (Exception $ex) {
            
        }
        die();
    }

    public function executeVerifySuccess() {
        $this->document->title = "Xác minh mã thanh toán thành công";
        Login::checkLogin();
        $this->setLayout('profile_user_2_col');
        $this->setView('verify_success');
    }

    //ham su dung lay ma theo may
    public function executeIsSecurity() {
        $request = $this->request;
        $_pay = new UserPaymentPassword;
        $_security = new UserSecurity();
        $_browser = new Browser();
        $_token = md5(time());

        $array_acc_update = array(
            'is_security' => $request->post('is_s'),
            'update_time' => time()
        );

        $rs = $_security->update($array_acc_update, 'uid=' . $this->user->id);
        if ($rs) {
            echo json_encode(array('error' => '1', 'msg' => 'done'));
            exit;
        } else {
            echo json_encode(array('error' => '0', 'msg' => 'error2'));
            exit;
        }
    }

    /*     * **************End Dung lv********************************************************* */

    public function executeResentEmail() {
        $request = Flywheel_Factory::getRequest();
        // Update token
        $payment = new UserPaymentPassword();
        $user = Flywheel_Authen::getInstance()->getUser();
        if (empty($user)) {
            return json_encode(array('status' => false, 'msg' => 'LOGIN_REQUIRE'));
        }
        
        $paymentObj = $payment->read($user->id);
        $_token = $paymentObj->key_verify;

        // Check browse
        $_browser = new Browser();
        $_agentBrowser = $_browser->getBrowser();
        $list_browser = unserialize($paymentObj->browser);
        
        // if Type is change password, then update time will be change
        if ($request->get('option', Flywheel_Filter::TYPE_STRING) == 'change_pwd') {
            $_token = genRandomString(5);
            $paymentObj->setKeyVerify($_token);
            $paymentObj->setUpdatetime($paymentObj->updatetime . ',' . time());
        }
        
        if(!empty($list_browser) ) {
            if(!in_array($_agentBrowser, $list_browser)) {
                $list_browser[] = $_agentBrowser;
                $_token = genRandomString(5);
                $paymentObj->setKeyVerify($_token);
                $paymentObj->setBrowser(serialize($list_browser));
            }
        } else {
            $paymentObj->setBrowser(serialize(array($_agentBrowser)));
        }
        
        $paymentObj->save();

        /**
         * Send mail
         */
        $baseUrl = Flywheel_Factory::getRouter()->getFrontControllerPath();
        $recipients = $this->user->email;
        $subject = '[ALIMAMA.VN] xác minh mật khẩu thanh toán';
        $data = array(
            'link_verify' => Flywheel_Factory::getDocument()->getPublicPath() . "xac-minh-thanh-toan?email=" . $recipients . "&createtime=" . time() . "&secure_code=" . $_token,
            'username' => $this->user->username,
            'email' => $this->user->email,
            'secure_code' => $_token,
            'date' => date('d/m/Y', time()),
        );

        $body = get_mail_template('user_verify_payment', $data);
        send_mail($recipients, $subject, $body);
        return json_encode(1);
    }

    /* Password payment post form */
    public function executeCreatePassPayment() {
        $this->document->title = "Mật khẩu thanh toán";
        Login::checkLogin();
        
        $password_payment   = new UserPaymentPassword();
        $payment            = $password_payment->read($this->user->id);
        
        // Result display message
        $result = array('status' => true, 'msg' => '', 'pos' => 0, 'email' => '');
        
        if($this->request->isPost()) {
            // Check has pass send
            $old_pwd    = $this->request->post('pwd_old');
            $new_pwd    = $this->request->post('pwd_new');
            $code       = $this->request->post('code');
            $pwd        = $this->request->post('pwd');
            
            if(empty($code)) {
                if(!empty($payment)) {
                    /* Send security code to authen changed password */
                    // Verify old password
                    $_pay_pwd       = $password_payment->selectList("`user_id`={$this->user->id} AND `password`='{$old_pwd}'");
                    if(!$_pay_pwd) {
                        // Mật khẩu không đúng
                        $result['msg'] = 'Mật khẩu cũ không đúng!';
                        $result['status'] = false;
                        $result['pos'] = 1;
                    } else {
                        $verify_pwd = preg_match('/^(?=.*\d)(?=.*[A-Za-z])[A-Za-z0-9]*$/', $new_pwd);
                        if(empty($new_pwd) || !$verify_pwd) {
                            // Mật khẩu mới không hợp lệ
                            $result['msg'] = 'Mật khẩu mới không hợp lệ!';
                            $result['status'] = false;
                            $result['pos'] = 2;
                        } else {
                            // Đổi mật khẩu
                            $_token = genRandomString(5);
                            $payment->setTmpPwd($new_pwd);
                            $payment->setKeyVerify($_token);
                            
                            /* Send key verify to mail of user */
                            // Check user has email
                            if(empty($this->user->email)) {
                                $url_profile = Flywheel_Factory::getDocument()->getPublicPath() . "ca-nhan";
                                $result['msg'] = "Bạn chưa tạo email! Hãy <a href='{$url_profile}'>click vào đây</a> để điền email của bạn.";
                                $result['status'] = false;
                                $result['pos'] = 0;
                            } else {
                                // Send email
                                $baseUrl    = Flywheel_Factory::getRouter()->getFrontControllerPath();
                                $recipients = $this->user->email;
                                $subject = '[ALIMAMA.VN] xác minh mật khẩu thanh toán';
                                $data = array(
                                    'username' => $this->user->username,
                                    'date' => date('d/m/Y', time()),
                                    'email' => $this->user->email,
                                    'secure_code' => $_token
                                );

                                $body = get_mail_template('user_change_payment', $data);
                                send_mail($recipients, $subject, $body);
                                
                                // Save temp password
                                $payment->save();
                                $result['email'] = $this->user->email;
                                
                            }
                            
                        }
                    }
                } else {
                    // Create new pwd
                    $verify_pwd = preg_match('/^(?=.*\d)(?=.*[A-Za-z])[A-Za-z0-9]*$/', $pwd);
                    if(!$verify_pwd) {
                        $result['msg'] = 'Mật khẩu không hợp lệ!';
                        $result['status'] = false;
                        $result['pos'] = 1;
                    } else {
                        $payment = new UserPaymentPassword();
                        $payment->setUserId($this->user->id);
                        $payment->setPassword($pwd);
                        $payment->setStatusVerify(1);
                        
                        $payment->save();
                    }
                    // Redirect form
                    $this->request->redirect(Flywheel_Factory::getDocument()->getPublicPath() .'mat-khau-thanh-toan');
                }
            } else {
                // Xác thực mã
                if(!empty($payment)) {
                    if($payment->key_verify == $code) {
                        $payment->setPassword($payment->tmp_pwd);
                        $payment->setStatusVerify(1);
                        $result['status'] = true;
                        $payment->save();
                    } else {
                        $result['msg'] = 'Mã không hợp lệ!';
                        $result['email'] = $this->user->email;
                        $result['status'] = true;
                        $result['pos'] = 1;
                    }
                }
            }
            
        }
        
        $this->view->assign('request', $this->request);
        $this->view->assign('result', $result);
        $this->view->assign('payment', $payment);
        
        $this->setLayout('profile_user_2_col');
        $this->setView('pass_payment');
    }

    public function executeForgotPassPayment() {
        $this->document->title = "Quên mật khẩu thanh toán";
        Login::checkLogin();
        
        $password_payment   = new UserPaymentPassword();
        $payment            = $password_payment->read($this->user->id);
        if(empty($payment)) {
            $this->request->redirect(Flywheel_Factory::getDocument()->getPublicPath() . 'mat-khau-thanh-toan');
        }
        
        $result = array('status' => false, 'msg' => '', 'pos' => 0);
        
        if($this->request->isPost()) {
            $action = $this->request->post('action');
            $code   = $this->request->post('code');
            if(!empty ($code)) {
                // Verify code
                if($payment->key_verify != $code) {
                    // Key not associated
                    $result['msg'] = 'Mã xác thực không hợp lệ!';
                    $result['status'] = false;
                    $result['pos'] = 1;
                } else {
                    // Response old password payment
                    $result['status'] = true;
                    $result['pwd'] = $payment->password;
                }
            } else {
                // Send mail
                $_token = genRandomString(5);
                $payment->setKeyVerify($_token);
                $baseUrl    = Flywheel_Factory::getRouter()->getFrontControllerPath();
                $recipients = $this->user->email;
                $subject = '[ALIMAMA.VN] Yêu cầu lấy lại mật khẩu';
                $data = array(
                    'username' => $this->user->username,
                    'date' => date('d/m/Y', time()),
                    'email' => $this->user->email,
                    'secure_code' => $_token
                );

                $body = get_mail_template('user_forgot_payment', $data);
                send_mail($recipients, $subject, $body);

                // Save temp password
                $payment->save();
            }
        }
        
        $this->view->assign('user', $this->user);
        $this->view->assign('result', $result);
        $this->view->assign('request', $this->request);
        $this->setLayout('profile_user_2_col');
        $this->setView('payment_forgot_pwd');
    }
}