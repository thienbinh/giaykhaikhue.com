<?php

require_once FLYWHEEL_DIR . 'Util' . DS . 'ValidateUtil.php';
require_once FLYWHEEL_DIR . 'Util' . DS . 'FormUtil.php';
require_once FLYWHEEL_DIR . 'Util' . DS . 'MessageUtil.php';
require_once APP_LIB_DIR . 'Decorator' . DS . 'Message.php';
require_once APP_LIB_DIR . 'FrontendUtil.php';

abstract class FrontendController extends Flywheel_Controller {

    public function beforeExecute() {
        $document   = Flywheel_Factory::getDocument();
        $view       = Flywheel_Factory::getView();
        $req        = Flywheel_Factory::getRequest();
        $session    = Flywheel_Factory::getSession();
        
        $document->jsBaseUrl    = $document->getPublicPath() . 'js/';
        $document->cssBaseUrl   = $document->getPublicPath() . 'frontend/css/';
        
        $document->addJs('plugins/jquery/jquery-1.7.1.min.js', 'TOP');
        $document->addJs('plugins/fancybox/jquery.fancybox.js', 'TOP');
        $document->addJs('frontend.js', 'TOP');
        $document->addJsCode('var base_url = "' . trim($document->getPublicPath(), '/') . '";', 'TOP', 'standard');
        $document->addCss('sys.css', 'TOP');
        $document->addCss('style.css', 'TOP');
        $document->addCss('fancybox/jquery.fancybox.css', 'TOP');
        
        $view->assign('document', $document);
        $view->assign('url_path', $document->getPublicPath());
        
        /**
         * -------------------------------------------
         */
        // Count user visited
        if(!array_key_exists('visited', $_SESSION)) {
            $_SESSION['visited'] = true;
            // Increament counter
            $_sysObj = new System_Config(); $_sysObj->readConfigByKey('key', 'count_visitor'); $_sysObj->setValue($_sysObj->value + 1); $_sysObj->save();
        }
        
        // Assign sesion key
        $view->assign('_session', $session);
    }

}