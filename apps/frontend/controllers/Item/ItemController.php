<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once CONTROLLERS_DIR . 'FrontendController.php';
include_once LIBRARIES_DIR . "Openid/openid.php";

class ItemController extends FrontendController {

    public function beforeExecute() {
        parent::beforeExecute();
        
    }

    /**
     * View item detail
     * @return type STRING
     */
    public function executeDefault() {
        $doc        = Flywheel_Factory::getDocument();
        $req        = Flywheel_Factory::getRequest();
        $item_id    = $req->get('id', Flywheel_Filter::TYPE_INT, 0);
        $view       = Flywheel_Factory::getView();
        $_itemObj   = new Item();
        $_itemCategory = new ItemCategory();
        
        $item_data  = $_itemObj->read($item_id);
        $itemCategory = $_itemCategory->retrieveByKey($item_data['cid']);
        // The other items
        $_items = $_itemObj->selectList("`cid`={$item_data['cid']} AND `id`!= {$item_id} AND `status`=1", "`created_time` DESC");
        
        // Create to viewed item by session
        $this->createSessionItemViewed($item_data, $item_id);

        $view->assign('_item', $item_data);
        $view->assign('_items', $_items);
        $view->assign('_category', $itemCategory);
        $this->setView('default');
        $doc->title = $item_data['title'];
        
        return $this->renderComponent();
    }

    /**
     * Add to session current item have viewed
     * @param type $item_data Item data
     * @param type $item_id ID of item
     */
    private function createSessionItemViewed($item_data = "", $item_id = "") {
        $session = Flywheel_Factory::getSession();
        // Find last item current user selected: Max 10 items
        if ($item_id != 0) { // View new item
            $_current_views = $session->get('current_views');
            if(empty($_current_views)) {
                // Init temporary session array
                $_tmp = array($item_id => $item_data);
                $session->set('current_views', $_tmp);
                // Assign to init array process
                $_current_views = $session->get('current_views');
            } else {
                // Add current item to session
                $_current_views[$item_id] = $item_data;
                // Reset array items if has full
                if(count($_current_views) > 10) {
                    // Unset first key - value in array
                    foreach($_current_views as $k => $v) {
                        unset($_current_views[$k]); break;
                    }
                }
            }
            // Reset session
            $session->set('current_views', $_current_views);
        }
    }
}

?>

