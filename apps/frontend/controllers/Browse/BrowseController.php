<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once CONTROLLERS_DIR . 'FrontendController.php';
include_once LIBRARIES_DIR . "Openid/openid.php";

class BrowseController extends FrontendController {

    public function beforeExecute() {
        parent::beforeExecute();
    }

    public function executeDefault() {
        $doc        = Flywheel_Factory::getDocument();
        $req        = Flywheel_Factory::getRequest();
        $view       = Flywheel_Factory::getView();
        $_itemObj   = new Item();
        $_cateObj   = new ItemCategory();
        // $limit      = "0, 10";
        
        $p = $req->get('p', Flywheel_Filter::TYPE_INT, 1);

        // Load cate
        $cid = $req->get('id', Flywheel_Filter::TYPE_INT, 0);
        
        $_cate = $_cateObj->retrieveByKey($cid);
        $_cids = $cid;
        // Load childs categories
        $comma = "";
        if($_cate->pid == 0) { // Level 1
            $_cateLists = $_cateObj->selectList("`pid`={$_cate->id}");
            if(!empty($_cateLists)) {
                foreach($_cateLists as $k => $v) {
                    $_cids .= $comma . $v['id'];
                    $comma = ",";
                }
            }
        }
        
        // Load items in cate
        if($cid == 0) { // Load all
            $_items = $_itemObj->selectList("", "`pos` ASC");
        } else {
            $_items = $_itemObj->selectList("`cid` IN ({$_cids})", "`pos` ASC");
        }
        
        $this->setView('default');
        $view->assign('_cate', $_cate);
        $view->assign("_items", $_items);
        
        return $this->renderComponent();
    }

}

?>

