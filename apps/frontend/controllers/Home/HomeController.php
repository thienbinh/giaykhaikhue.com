<?php

require_once CONTROLLERS_DIR . 'FrontendController.php';

class HomeController extends FrontendController {

    public function beforeExecute() {
        parent::beforeExecute();
    }

    public function executeDefault() {
        $view       = Flywheel_Factory::getView();

        // Load banner
        $_bannerObj = new Banner();
        $_banners = $_bannerObj->selectList("`status`=1", "`pos` ASC", "0,5");
        
        // Load content
        $itemObj = new Item();
        $_items = $itemObj->selectList("`status`=1 AND (`is_new`=1 OR `is_front`=1)", "`pos` ASC");
        $_items_special = $_items_new = array();
        if(!empty($_items)) {
            foreach($_items as $k => $v) {
                if($v['is_front'] == 1) {
                    $_items_special[] = $v;
                }
                if($v['is_new'] == 1) {
                    $_items_new[] = $v;
                }
            }
        }
        
        $this->setView('default');
        $view->assign('_items_special', $_items_special);
        $view->assign('_items_new', $_items_new);
        $view->assign('_banners', $_banners);
        
        return $this->renderComponent();
    }

}