<?php
require_once CONTROLLERS_DIR.'FrontendController.php';
require_once FLYWHEEL_DIR .'Util' .DS .'MessageUtil.php';
require_once FLYWHEEL_DIR.'Util'.DS.'ValidateUtil.php';
require_once FLYWHEEL_UTIL.'securimage.php';
require_once GLOBAL_DIR.'helper/mail_helper.php';

class RegistryController extends FrontendController {
    //put your code here
    private $b_url = "";
    public function  beforeExecute() {
        parent::beforeExecute();
        $doc = Flywheel_Factory::getDocument();
        $doc->addCss('login.css');
        $doc->addJs('login.js');
        $this->b_url = Flywheel_Factory::getDocument()->getPublicPath();
    }
    public function executeDefault() {
        $this->executeRegistry();
    }
    
    public function executeValidFocus(){
       $request 	= Flywheel_Factory::getRequest();
       $k = $request->post("k");
       $val = $request->post("val");
       switch ($k){
          case 'username':
             $username = $val;
             $valid = true;
             if ('' == $username) {
               $valid = false;
               $mes['msg'] = "Chưa điền tên đăng nhập";
             } else if ( strlen( $username ) < 4 ){
               $valid = false;
               $mes['msg'] = "Tên đăng nhập này đã tồn tại";
             } else if ($this->checkSpecialChar($username)==false){
               $valid = false;
               $mes['msg'] = "Không được bao gồm ký tự lạ";
             } else {
               $_user = Users::findByUsername($username);
               if($_user){
                  $valid = false;
                  $mes['msg'] = 'Tên đăng nhập này đã tồn tại';
               }
             }
             if($valid == true){
                $mes['t'] = 1;
                $mes['msg'] = "Tên đăng nhập hợp lệ";
             }else{
                $mes['t'] = 0;
             }
             break;
          
          case 'password':
             $valid = true;
             $password = $val;
             if ('' == $password) {
                $valid = false;
                $mes['msg'] = 'Chưa nhập mật khẩu';
             }else if (strlen($password) < 8 or strlen($password) > 32 ){
                $valid = false;
                $mes['msg'] = 'Mật khẩu tối thiểu 8 ký tự,tối đa 32 ký tự';
             }
             if($valid == true){
                $mes['t'] = 1;
                $mes['msg'] = "Mật khẩu hợp lệ";
             }else{
                $mes['t'] = 0;
             }
             break;
             
          case 'repassword':
             $to_check = $val;
             $_a = explode("_", $to_check);
             $valid = true;
             if($_a[0]==''){ 
                $valid = false;
                $mes['msg'] = "Chưa nhập lại mật khẩu";
             } else if ($_a[0]!=$_a[1]){
                $valid = false;
                $mes['msg'] = "Mật khẩu nhập lại không đúng";
             }
             
             if($valid == true){
                $mes['t'] = 1;
                $mes['msg'] = "Mật khẩu hợp lệ";
             }else{
                $mes['t'] = 0;
             }
             break;
           
           case 'email':
              $email = $val;
              $valid = true;
              if(''== $email){
                  $valid = false;
                  $mes['msg'] = "Chưa nhập email";
                  
               }else if(Users::isValidEmail($email)==false){
                  $valid = false;
                  $mes['msg'] = 'Email không đúng định dạng' ;  
               }else if(Users::checkExistEmail($email)==true){
                  $valid = false;
                  $mes['msg'] = 'Email này đã đăng ký trước đó';
               }
              if($valid == true){
                $mes['t'] = 1;
                $mes['msg'] = "Email hợp lệ";
              }else{
                $mes['t'] = 0;
              } 
                
              break; 
       }
       print(json_encode($mes));exit;
    }
    
    public function executeRegistry() {
             
             $this->setLayout('login');
             $view      = Flywheel_Factory::getView();
             $this->setView('registry');
             $authen		= Flywheel_Authen::getInstance();
             $document 	= Flywheel_Factory::getDocument();
             $request 	= Flywheel_Factory::getRequest();
             if($authen->isAuthenticated()){
                $request->redirect(Flywheel_Factory::getDocument()->getPublicPath());
             }
             $email = $request->get('mail');
             if($email){
             	$view->assign('email',$email);
             }			
             $message 	= MessageUtil::getMessage('login');
             
             $document->title = 'Đăng ký';

             if (Flywheel_Application_Request::POST == $request->getMethod()) {
                     $username = $request->post('username');
                     $password = $request->post('password');
                     $repassword = $request->post('repassword');
                     $email = $request->post('email');
                     $captcha = $request->post('captcha');
                     
                     
                     $view->assign('username',$username );
                     $view->assign('password',$password);
                     $view->assign('repassword',$repassword);
                     $view->assign('email',$email);
                     $view->assign('captcha', $captcha);
                     //$view->assign('order_code', @Transaction::renderRandomString(3) );
                     $valid = true;
                     
                     $_user = Users::findByUsername($username);
                
                     if ('' == $username) {

                        $valid = false;
                        $view->assign('err_username','Chưa điền tên đăng nhập' ); 

                     } else if (strlen($username)< 4 || strlen($username) > 16 ){
                        $valid = false;
                        $view->assign('err_username','Tên đăng nhập tối thiểu 4 ký tự và tối đa 16 ký tự' );
                     } else if ($this->checkUserNameRule($username)== 1  ){
                        $valid = false;
                        $view->assign('err_username','Tên đăng nhập không được có khoảng trống hoặc ký tự lạ' );
                     } else if($_user){
                        $valid = false;
                        $view->assign('err_username','Tên đăng nhập này đã tồn tại' );
                     }else{
                        
                        $view->assign('sus_username','Tên đăng nhập hợp lệ' );
                     }

                     if ('' == $password) {
                        $valid = false;
                        $view->assign('err_password','Chưa nhập mật khẩu' );
                     } else if (strlen($password) < 8 or strlen($password) > 32 ){
                         $valid = false;
                         $view->assign('err_password','Mật khẩu tối thiểu 8 ký tự,tối đa 32 ký tự' ); 
                     }else{
                        
                        $view->assign('sus_password','Mật khẩu hợp lệ' );
                     }
                     
                     if($repassword==''){
                           $valid = false;
                           $view->assign('err_repassword','Chưa nhập lại mật khẩu' );
                     }else if($password != $repassword){
                           $valid = false;
                           $view->assign('err_repassword','Nhập lại mật khẩu không đúng' );  
                     }else {
                         
                         $view->assign('sus_repassword','Mật khẩu nhập lại hợp lệ' );
                     }
                     
                     if(''== $email){
                           $valid = false;
                           $view->assign('err_email','Chưa nhập email' );  
                     }else if( Users::isValidEmail($email) == false ){
                           $valid = false;
                           $view->assign('err_email','Email không đúng định dạng' );  
                     }else if(Users::checkExistEmail($email)==true){
                           $valid = false;
                           $view->assign('err_email','Email đã tồn tại' );
                     } else {
                           
                           $view->assign('sus_email','Email hợp lệ' );  
                     }
                     $img_sec = new Securimage();
                     if(''==$captcha){
                           $valid = false;
                           $view->assign('err_code','Chưa nhập mã bảo mật' );
                     }else if($img_sec->check($captcha) == false){
                           $valid = false;
                           $view->assign('err_code','Mã bảo mật không đúng' );
                     }else{
                           
                           $view->assign('sus_code','Mã bảo mật hợp lệ' );  
                     }

                     if($valid == true){
                        //tao moi nguoi dung
                        $user = new Users();
                        $user->setUsername($username);
                        $salt = UsersPeer::generateSalt($user->username);
                        $user->setPassword($salt .md5($salt .$password));
                        $user->setEmail($email);
                        $user->setTimeJoin(time());
                        //$user->setOrderCode(@Transaction::renderRandomString(3));
                        //$user->setCode(@Transaction::renderRandomString(3));
                        $user->setNew(true);
                        $lastuser_id = $user->save();
                        
                        if($lastuser_id){
                           $this->send_mail_active($user);
                           $request->redirect(Flywheel_Factory::getDocument()->getPublicPath().'dang-ky-thanh-cong/'.$lastuser_id.'/'.$email);
                        }else{
                           $message->add('Không tạo được tài khoản.Vui lòng liên hệ để được hỗ trợ', '', MessageUtil::TYPE_ERROR);
                        }
                     }
                     $view->assign('username', $username);
                     $view->assign('email', $email);
             }

     }
    function checkUserNameRule($string)
    {
        return preg_match("/[^a-z0-9A-Z_.]+/", $string);//Nếu có kí tự đặc biệt
    }
   public function executeBActive(){
       $this->setLayout('login');
       $view = Flywheel_Factory::getView();
       $this->setView('b_active');
       $router = Flywheel_Factory::getRouter();
       $email = "";
       $code = "";
       
       $user_id = $router->getParams(1);
       $email = $router->getParams(2);
       
       $email_active = new EmailActive();
       $data = $email_active->_get(" AND `user_id`=$user_id");
      //var_dump($data);die;
       if($data!=false){
          $code = $data['code'];
       }
       $view->assign('user_id',$user_id);
       $view->assign('code',$code);
       $view->assign('email',$email);
   }
   
   public function send_mail_active($user){
          //tao moi 1 ban ghi cho email active
          $email_active = new EmailActive;
          $email_active->setUserId($user->id);
          $email_active->setExpireTime(time()+24*60*60);
          $email_active->setCreatedTime(time());
          $email_active->setNew(true);
          $lastid = $email_active->save();
          
          $email_active = new EmailActive();
          $email_active->read($lastid);
          //tao ra mang du lieu
          $data = array(
              'link_active'=>Flywheel_Factory::getDocument()->getPublicPath() . "registry/active?code=".$email_active->code,
              'user_name'=>$user->username
          );
          $recipients  = $user->email;
          $subject = '[ALIMAMA.VN] Thông tin yêu cầu kích hoạt tài khoản';
          $body    =  get_mail_template('active_account',$data);
          send_mail($recipients, $subject, $body);
          return true;
   }
   
   public function executeActive(){
          $this->setLayout('login');
          $this->setView("active");
          $view 	= Flywheel_Factory::getView();
          $request 	= Flywheel_Factory::getRequest();
          $code         = $request->get('code');
          
          
          $e_a = new EmailActive;
          $data = $e_a->_get(" AND `code`='$code'");
          
          $email_active = new EmailActive($data);
          
            $_user = new Users(Users::retrieveByPk($email_active->user_id));
            if($_user->active!=1){
                if($email_active->finished==1){
                    $request->redirect($this->b_url."page404");
                }else{
                    $_user->setActive(1);
                    $_user->setNew(false);
                    $_user->save();


                    $email_active->setFinished(1);
                    $email_active->setFinishedTime(time());
                    $email_active->setNew(false);
                    if(!$email_active->save()){
                        $mes = 'Có lỗi.Vui lòng kích hoạt lại.';
                    }else{
                        $mes = 'Kích hoạt tài khoản thành công';
                    }
                }
            }else{
                $request->redirect($this->b_url."page404");
            }
          
          
          $view->assign('status', $mes);
       }
    
    public function checkSpecialChar($username){
       $specialChar="~`!@#$%^&*()-+=|\{}[]:;><,?/".'"';
       for ($i=0; $i<=strlen($specialChar); $i++){ 
          if (@strpos($username, $specialChar[$i]) !==false){ 
              return false;
          } 
       }  
       return true;
    }
    /**
     * gửi lại mật khẩu vào email
     * 
     */
    public function executeReSendMail(){
          
          $request 	= Flywheel_Factory::getRequest();
          $user_id  = $request->post('user_id');
          $email_active = new EmailActive;
          $email_active->read($user_id);
          $user = new Users;
          $user->read($user_id);
          
          //tao ra mang du lieu
          $data = array(
              'link_active'=>Flywheel_Factory::getDocument()->getPublicPath() . "registry/active?code=".$email_active->code,
              'user_name'=>$user->username
          );
          $recipients  = $user->email;
          $subject = '[ALIMAMA.VN] gửi lại mật khẩu xác minh';
          $body    =  get_mail_template('active_account',$data);
          $rs = send_mail($recipients, $subject, $body);
          if($rs){
              echo json_encode(array('error'=>'0', 'msg'=>'DONE' ) );
          }else{
              echo json_encode(array('error'=>'1', 'msg'=>'ERROR' ) );
          }
              exit;
   }
   
}