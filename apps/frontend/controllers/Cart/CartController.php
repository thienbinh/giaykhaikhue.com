<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once CONTROLLERS_DIR . 'FrontendController.php';

class CartController extends FrontendController {

    public $result = array('msg' => '', 'status' => true, 'order_id' => 0);
    public function beforeExecute() {
        parent::beforeExecute();
    }

    public function executeDefault() {
        
        $doc        = Flywheel_Factory::getDocument();
        $view       = Flywheel_Factory::getView();
        $req        = Flywheel_Factory::getRequest();
        $session    = Flywheel_Factory::getSession();
        $_cartObj = new Cart();
        $itemObj = new Item();
        
        // Get items in cart
        $session_id = $session->id();
        
        if($req->getMethod() == Flywheel_Application_Request::POST) {
            
            $ajax_post = false;
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $ajax_post = true;
            }
            
            if($ajax_post) {
                // Find item
                $item_id = $req->post('item_id', Flywheel_Filter::TYPE_INT, 0);
                $amount = $req->post('amount', Flywheel_Filter::TYPE_INT, 1);
                $type = $req->post('type', Flywheel_Filter::TYPE_STRING, "");

                $_item = $itemObj->retrieveByKey($item_id);
                // Check has been exist item
                $_find_in_cart = $_cartObj->selectOne("`item_id`={$item_id} AND `user_id`='{$session_id}'");
                if(!empty($_find_in_cart)) {
                    $_cartObj->read($_find_in_cart['id']);
                    $amount = $type == 'update' ? $amount : $_find_in_cart['amount'] + $amount;
                    $_cartObj->setAmount($amount);
                } else {
                    $_cartObj->setItemId($item_id);
                    $_cartObj->setPrice(intval($_item->pro_price) != 0 ? $_item->pro_price : $_item->price);
                    $_cartObj->setUserId($session_id);
                    $_cartObj->setAmount($amount);
                    $_cartObj->setCreatedTime(time());
                }
                $_cartObj->save();

                // Return json string
                return $this->renderText(json_encode(array('status' => true, 'msg' => "")));
            } else {
                // Save order
                $this->_saveOrder();
                if($this->result['status']) { // Create order success
                    $req->redirect(FrontendUtil::url_format('cart', 'detail', array('id' => $this->result['order_id'])));
                }
            }
        }

        $_itemsCart = $_cartObj->selectList("`user_id`='{$session_id}'", "`id` ASC", null, "item_id");
        // Load items data
        $_itemsData = array();
        if(!empty($_itemsCart)) {
            $_item_ids = $comma = "";
            foreach($_itemsCart as $k => $v) {
                $_item_ids .= $comma . $k;
                $comma = ",";
            }
            $_itemsData = $itemObj->selectList("`id` IN ({$_item_ids})", null, null, "id");
        }
        
        $view->assign('_itemsCart', $_itemsCart);
        $view->assign('_itemsData', $_itemsData);
        $view->assign('_result', $this->result);
        
        $this->setView('default');
    }
    
    public function executeDel() {
        $req = Flywheel_Factory::getRequest();
        
        $id = $req->post('id', Flywheel_Filter::TYPE_INT, 0);
        $itemCart = new Cart();
        $itemCart->read($id);
        
        $result = $itemCart->delete();
        return $this->renderText(json_encode(array('status' => $result, 'msg' => ($result ? "Xóa thành công":"Xóa lỗi."))));
    }

    /**
     * View detail order
     */
    public function executeDetail() {
        $req = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        $order = new Order();
        
        $order->read($req->get('id', Flywheel_Filter::TYPE_INT, 0));
        if($order->is_delete == 1) {
            $order->read(0);
        }
        $total_money = 0;
        if($order->id != 0) {
            // Save custommer info
            $session = Flywheel_Factory::getSession();
            $custom_info = array(
                'name' => $order->name,
                'address' => $order->address,
                'phone' => $order->phone,
                'fax' => $order->fax,
                'email' => $order->email
            );
            $session->set('custom_info', $custom_info);
        }
        
        // Get list items special
        $itemObj = new Item();
        $_items = $itemObj->selectList("`status`=1 AND `is_front`=1", "`pos` ASC");
        
        $view->assign('_order', $order);
        $view->assign('_items_special', $_items);
        $this->setView('detail');
        
    }
    
    /**
     * Save order
     */
    private function _saveOrder() {
        
        $req = Flywheel_Factory::getRequest();
        $session = Flywheel_Factory::getSession();
        $session_id = $session->id();
        // Get items cart
        $cart       = new Cart();
        $_itemsCart = $cart->selectList("`user_id`='{$session_id}'", "`id` ASC", null, "item_id");
        if(empty($_itemsCart)) {
            $this->result['status'] = false;
            $this->result['msg'] = "Chưa có sản phẩm nào trong giỏ hàng của bạn.";
            return;
        }
        
        $username = $req->post('name');
        $address = $req->post('address');
        $phone = $req->post('phone');
        $fax = $req->post('fax');
        $email = $req->post('email');
        $note = $req->post('note');
        
        if(preg_replace('/\s+/', '', $username) == "") {
            $this->result['status'] = false;
            $this->result['msg'] = 'Chưa điền tên khách hàng.';
            return;
        }
        if(preg_replace('/\s+/', '', $address) == "") {
            $this->result['status'] = false;
            $this->result['msg'] = 'Chưa điền địa chỉ hàng.';
            return;
        }
        if(preg_replace('/\s+/', '', $phone) == "") {
            $this->result['status'] = false;
            $this->result['msg'] = 'Chưa điền số điện thoại.';
            return;
        }
        
        $order = new Order();
        $order->setUsername($username);
        $order->setAddress($address);
        $order->setPhone($phone);
        $order->setFax($fax);
        $order->setEmail($email);
        $order->setNote($note);
        $order->setCreatedTime(time());

        $order_id = $order->save();
        if($order_id != 0) {
            $itemObj    = new Item();
            $_total_money = 0;
            if(!empty($_itemsCart)) {
                $_item_ids = $comma = "";
                foreach($_itemsCart as $k => $v) {
                    $_item_ids .= $comma . $k;
                    $comma      = ",";
                }
                $_itemsData = $itemObj->selectList("`id` IN ({$_item_ids})", null, null, "id");
                foreach($_itemsCart as $k => $v) {
                    $_total_money += $v['price'] * $v['amount'];
                    
                    $order_items = new OrderItems();
                    $order_items->setOrderId($order_id);
                    $order_items->setTitle($_itemsData[$k]['title']);
                    $order_items->setItemCode($_itemsData[$k]['code']);
                    $order_items->setItemId($k);
                    $order_items->setPrice($v['price']);
                    $order_items->setAmount($v['amount']);
                    if($order_items->save()) {
                        // Delete items in cart
                        $cart->read($v['id']);
                        $cart->delete();
                    }
                }
                // Update total money
                $order->read($order_id);
                $order->setTotalMoney($_total_money);
                $order->save();
            }
            // End if
            $this->result['status'] = true;
            $this->result['order_id'] = $order_id;
        } else {
            $this->result['status'] = false;
            $this->result['msg'] = "Tạo hóa đơn lỗi! Hệ thống đang nâng cấp.";
        }
    }
}

?>
