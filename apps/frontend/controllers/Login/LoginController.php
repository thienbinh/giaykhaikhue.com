<?php

require_once CONTROLLERS_DIR . 'FrontendController.php';
require_once FLYWHEEL_UTIL . 'securimage.php';
require_once GLOBAL_DIR . 'helper/mail_helper.php';

class LoginController extends FrontendController {

    public $_user = '';
    private $b_url = "";

    public function beforeExecute() {
        parent::beforeExecute();
        $doc = Flywheel_Factory::getDocument();
        $doc->addJs('login.js');
        $doc->addJs('user.js');
        $doc->addJs('UIBlock.js', 'TOP');

        $doc->addCss('login.css');
        $doc->addCss('message.css');

        $this->_user = new Users;
        $this->b_url = Flywheel_Factory::getDocument()->getPublicPath();
    }

    public function executeDefault() {
        $this->executeLogin();
    }

    public function executeLogin() {
        $this->setLayout('login');
        $view = Flywheel_Factory::getView();
        $authen = Flywheel_Authen::getInstance();
        $document = Flywheel_Factory::getDocument();
        $document->title = 'Đăng nhập';
        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('login');
        $sessionId = Flywheel_Factory::getSession()->id();
        $url = $request->get('url');
        $view->assign('url', $url);
        if ($authen->isAuthenticated())
            $request->redirect($url ? $url : 'home');

        if (Flywheel_Application_Request::POST == $request->getMethod()) {
            $username = $request->post('username');
            $password = $request->post('password');
            $remember = $request->post('remember');

            $url = $request->post('url');
            $mes = Login::login($username, $password);

            if (!$mes) {
                $user = Flywheel_Authen::getInstance()->getUser();
                Cart::updateUserIdCartItemBySession($user->id, $sessionId);

                if ($remember and $remember == 1) {
                    $expire = time() + 60 * 60 * 24 * 30;
                    @setcookie("username", $username, $expire);
                    @setcookie("password", $password, $expire);
                }
                if (!$url or $url == '')
                    $url = 'home';
                else
                    $url = base64_decode($url);
                $request->redirect($url);
            } else {
                $message->add($mes, '', MessageUtil::TYPE_ERROR);
            }
        }
        $view->assign('message', MessageUtil::render($message));
    }

    public function executeLogout() {
        Login::logout();
        Flywheel_Factory::getRequest()->redirect('home');
    }

    public function executePassForget() {

        $this->setLayout('login');
        $this->setView("pass_forget");

        $document = Flywheel_Factory::getDocument();
        $request = Flywheel_Factory::getRequest();
        $document->title = "Quên mật khẩu";
        $mes = array();
        if (Flywheel_Application_Request::POST == $request->getMethod()) {
            
            $img_sec = new Securimage();
            $email = $request->post('email');
            $captcha = $request->post('code');

            if ($email == "") {
                $mes['status'] = 0;
                $mes['error'] = "Chưa nhập Email";
            }
            
            if ('' == preg_replace('/\s+/', '', $captcha)) {
                $mes['status'] = 0;
                $mes['error'] = "Chưa nhập mã bảo mật";
            }

            if ('' != $captcha & $img_sec->check($captcha) != false) {
                
                $user = new Users($this->_user->getUserByEmail($email));
                //check xem co duoc phep gui them yeu cau khong
                $email_forgetpass = new EmailForgetpass;
                $data = $email_forgetpass->_get(" AND `user_id`=$user->id");

                if (time() < $data['expire_time']) {
                    $mes['status'] = 0;
                    $mes['error'] = "Bạn chỉ được tiếp tục yêu cầu lấy lại mật khẩu lúc " . date("H:i:s d/m/Y", $data['expire_time']);
                    print(json_encode($mes));
                    exit;
                }

                $email_forgetpass = new EmailForgetpass;
                $email_forgetpass->setUserId($user->id);
                $email_forgetpass->setExpireTime(time() + 24 * 60 * 60);
                $email_forgetpass->setNew(true);
                $l_id = $email_forgetpass->save();

                if ($l_id) {
                    $email_forgetpass = new EmailForgetpass;
                    $email_forgetpass->read($l_id);

                    $recipients = $email;
                    $subject = '[SẾUĐỎ.VN] Thông tin yêu cầu lấy lại mật khẩu';
                    $data = array(
                        'link_confirm' => Flywheel_Factory::getDocument()->getPublicPath() . "login/new_pass?code=" . $email_forgetpass->code,
                        'user_name' => $user->username
                    );
                    $body = get_mail_template('forgot_pass', $data);

                    if (send_mail($recipients, $subject, $body) == true) {
                        $mes['status'] = 1;
                        $mes['error'] = "Yêu cầu lấy lại mật khẩu thành công.Vui lòng kiểm tra email đê tiếp tục.";
                    }
                }
            } else {
                $mes['status'] = 0;
                $mes['error'] = "Mã bảo mật không đúng!";
            }

            print(json_encode($mes));
            exit;
        }
    }

    public function executeNewPass() {
        $document = Flywheel_Factory::getDocument();
        $document->title = "Quên mật khẩu";
        $this->setLayout('login');
        $this->setView("new_pass");
        $request = Flywheel_Factory::getRequest();
        $request = Flywheel_Factory::getRequest();
        $code = trim($request->get('code'));
        //check xem code nay` con hieu luc hay khong
        $email_forgetpass = new EmailForgetpass;
        $data = $email_forgetpass->_get(" AND `code`='$code'");

        if ($data['finished'] == 1) {
            $request->redirect($this->b_url . "/page404");
        }

        if (Flywheel_Application_Request::POST == $request->getMethod()) {
            $img_sec = new Securimage();
            $code = $request->post('code');
            $pass = $request->post('pass');
            $repass = $request->post('repass');
            $captcha = $request->post('captcha');

            $email_forgetpass = new EmailForgetpass($email_forgetpass->_get(" AND `code`='$code'"));
            $user_id = $email_forgetpass->user_id;


            $valid = true;
            $mes = array();
            if ('' == $pass) {
                $valid = false;
                $mes['status'] = 0;
                $mes['error'] = 'Chưa nhập mật khẩu';
            }
            if ($repass == '') {
                $valid = false;
                $mes['status'] = 0;
                $mes['error'] = 'Chưa nhập lại mật khẩu';
            } else if ($pass != $repass) {
                $valid = false;
                $mes['status'] = 0;
                $mes['error'] = 'Nhập lại mật khẩu không đúng';
            }

            if ('' == $captcha or $img_sec->check($captcha) == false) {
                $valid = false;
                $mes['status'] = 0;
                $mes['error'] = 'Mã bảo mật không đúng';
            }

            if ($valid != false) {
                //$email_forgetpass
                //cap nhat thanh cong cho quen mat khau

                $email_forgetpass->setFinished(1);
                $email_forgetpass->setFinishedTime(time());
                $email_forgetpass->setNew(false);
                $email_forgetpass->save();
                //cap nhat lai pass word cho nguoi dung
                $user = Users::retrieveByPk($user_id);
                $salt = UsersPeer::generateSalt($user->username);
                $user->setPassword($salt . md5($salt . $pass));

                $user->setNew(false);
                if ($user->save()) {

                    $mes['status'] = 1;
                    $mes['error'] = 'Thành công';
                }
            }

            print(json_encode($mes));
            exit;
        }
    }

}

?>
