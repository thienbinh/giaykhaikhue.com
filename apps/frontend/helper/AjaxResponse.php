<?php
class AjaxResponse extends stdClass {
	const TYPE_SUCCESS	= 1;
	const TYPE_WARNING	= 2;
	const TYPE_ERROR	= 0;

	public $type;

	public $message;
	
	public function toString() {
		return json_encode($this);
	}
	
	public static function setResponseType($format = 'xhtml') {
		if ('json' == $format) {
			Flywheel_Factory::getResponse()->setHeader('Content-type', 'application/json', true);		
		}
	}
}