<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Backend_Utility {

    /**
     * Return string with length inputed
     * @param type $len int
     * @param type $format: default, up, low
     * @return type String
     */
    public static function renderRandomString($len = 8, $format = "") {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ';
        $str = '';
        for ($p = 0; $p < $length; $p++) {
            $str .= $characters[mt_rand(0, strlen($characters))];
        }
        switch($format) {
            case "up":
                $str = strtoupper($str);
                break;
            case "low":
                $str = strtolower($str);
                break;
            default:
                break;
        }
        return $str;
    }

    /**
     * Validate email format
     * @param type $email string
     * @return type Boolean
     */
    public static function isValidEmail($email) {
        if (@eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email) == true) {
            return true;
        }
        return false;
    }

    /**
     * class helper utility
     * Enter description here ...
     *
     */
    static function currency_format($value, $add_currency=false, $symbol='đ') {
        if (strlen($value) > 2) {
            if ($value != '' and is_numeric($value)) {
                $value = number_format($value, 2, ',', '.');
                $value = str_replace(',00', '', $value);
            }
        }
        if ($add_currency)
            $value.=' ' . $symbol;
        return $value;
    }
}

?>
