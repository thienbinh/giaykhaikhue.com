<?php

$router     = Flywheel_Factory::getRouter();
$baseUrl    = $router->getBaseUrl();
$controller = $router->getController();
$action     = $router->getAction();

$menuStructures = array(
    'home' => array(
        'datas' => array(
            'title' => 'Hệ thống', 'com' => 'home', 'act' => 'default', 'show' => true
        ),
        'childs' => array(
            'intro' => array(
                'title' => 'Hệ thống', 'com' => 'intro', 'act' => 'default', 'show' => true
            ),
            'banner' => array(
                'title' => 'Banner', 'com' => 'banner', 'act' => 'default', 'show' => true
            ),
            'config' => array(
                'title' => 'Cấu hình', 'com' => 'system_config', 'act' => 'default', 'show' => true
            )
        )
    ),
    'user' => array(
        'datas' => array(
            'title' => 'Người dùng', 'com' => 'user', 'act' => 'default', 'show' => false
        ),
        'childs' => array(
            'user_list' => array(
                'title' => 'Danh sách', 'com' => 'user', 'act' => 'default', 'show' => true
            ),
            'user_add' => array(
                'title' => 'Thêm mới', 'com' => 'user', 'act' => 'add', 'show' => true
            )
        ),
    ),
    'order' => array(
        'datas' => array(
            'title' => 'Đơn hàng', 'com' => 'order', 'act' => 'default', 'show' => true
        ),
        'childs' => array(
            'order' => array(
                'title' => 'Đơn hàng', 'com' => 'order', 'act' => 'default', 'show' => true
            )
        ),
    ),
    'item' => array(
        'datas' => array(
            'title' => 'Sản phẩm', 'com' => 'item_category', 'act' => 'default', 'show' => true
        ),
        'childs' => array(
            'item_category' => array(
                'title' => 'Danh mục sản phẩm', 'com' => 'item_category', 'act' => 'default', 'show' => true
            ),
            'item_s' => array(
                'title' => 'Sản phẩm', 'com' => 'item', 'act' => 'default', 'show' => true
            ),
            'item_crawl' => array(
                'title' => 'Crawl sản phẩm', 'com' => 'item', 'act' => 'crawl', 'show' => true
            ),
            'item_add' => array(
                'title' => 'Thêm mới sản phẩm', 'com' => 'item', 'act' => 'edit', 'show' => false
            )
        ),
    ),
    'content' => array(
        'datas' => array(
            'title' => 'Bài viết', 'com' => 'content_category', 'act' => 'default', 'show' => true
        ),
        'childs' => array(
            'content_cate' => array(
                'title' => 'Danh mục bài viết', 'com' => 'content_category', 'act' => 'default', 'show' => true
            ),
            'content' => array(
                'title' => 'Bài viết', 'com' => 'content', 'act' => 'default', 'show' => true
            ),
            'introduct' => array(
                'title' => 'Giới thiệu', 'com' => 'content', 'act' => 'contact', 'show' => true
            )
        )
    )
);

//draw child menu
function am_draw_childs_menu($childs = array(), $baseUrl = "", $cur_controller = "", $cur_act = "") {
    $html = '<ul id="menu-main-sub">';
    if (!empty($childs)) {
        foreach ($childs as $child) {
            if($child['show'] == false) {
                continue;
            }
            $active = $cur_controller == $child['com'] ? true : false; // & ($cur_act == $child['act'])
            $html .= '<li><a href="' . $baseUrl . '?com=' . $child['com'] . '&act=' . $child['act'] . '">';
            $html .=  $active ? '<b>' . $child['title'] . '</b>' : $child['title'];
            $html .= '</a></li>';
        }
    }
    $html .= '</ul>';
    return $html;
}
?>
<div id="menu-main" class="container_24">
    <ul id="menu-main-f">
        <?php
        $subMenu = "";
        foreach ($menuStructures as $mainMenu => $structures) {
            $menu = $structures['datas'];
            if($menu['show'] == false) {
                continue;
            }
            $active = false;
            if(strtolower($controller) == strtolower($menu['com'])) {
                $subMenu = am_draw_childs_menu($structures['childs'], $baseUrl, $controller, $action);
                $active = true;
            } else {
                if(!empty($structures['childs'])) {
                    foreach($structures['childs'] as $k => $v) {
                        if(strtolower($controller) == strtolower($v['com'])) {
                            $active = true;
                            $subMenu = am_draw_childs_menu($structures['childs'], $baseUrl, $controller, $action);
                            break;
                        }
                    }
                }
            }
            ?>
        <li class="<?php echo $active ? 'active' : '' ?>">
            <a href="<?php echo $baseUrl ?>?com=<?php echo $menu['com']; ?>&act=<?php echo $menu['act']; ?>">
                <span><?php echo $menu['title']; ?></span>
            </a>
        </li>
            <?php
        }
        ?>
    </ul>
    <?php echo $subMenu; ?>
</div>