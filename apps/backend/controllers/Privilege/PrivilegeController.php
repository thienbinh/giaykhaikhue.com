<?php
require_once CONTROLLERS_DIR .'BackendController.php';
class PrivilegeController extends BackendController 
{
    public $view     = '';
    public $message  = '';
    public $request  = '';
    public $user     = '';
    public $document = '';
    public $conn     = '';
    public $obj      = '';
    
	public function  beforeExecute() 
    { 
		parent::beforeExecute();
		$this->view       = Flywheel_Factory::getView();
		$this->message    = MessageUtil::getMessage('privilege');
		$this->request    = Flywheel_Factory::getRequest();
		$this->user       = Flywheel_Authen::getInstance()->getUser();
        $this->document   = Flywheel_Factory::getDocument();
        $this->conn       = Flywheel_DB::getConnection(Privilege::TABLE);
        $this->obj        = new Privilege;
	}

	public function executeDefault() 
    {   
        $this->setView('default');
		$this->message    = MessageUtil::getMessage('privilege');
		$this->document->title = "Quản trị Phân quyền - Admin Privilege - ".$this->document->title;
        
		$valid      = true;
		$sql        = "";
        $condition  = "1=1";
        
        $title = $this->request->get('privilege_name',Flywheel_Filter::TYPE_STRING,'');
        if($title != '')
        {
            $condition .= ' AND privilege_name LIKE "%'.$title.'%"';
        }
        $this->view->assign('privilege_name',$title);
        
        $pcode = $this->request->get('privilege_code',Flywheel_Filter::TYPE_STRING,'');
        if($pcode != '')
        {
            $condition .= ' AND privilege_code LIKE "%'.$pcode.'%"';
        }
        $this->view->assign('privilege_code',$pcode);
        
        $datas = array('');
        
        $sql = "SELECT ".implode(',',privilege::$schema['fields']).' FROM '.privilege::TABLE.' WHERE '.$condition.' ORDER BY id desc';
        $this->conn->prepare($sql);
        $this->conn->execute(); 
        $datas = $this->conn->fetchAll();
        
        $this->view->assign('com','privilege');
        $this->view->assign('datas',$datas);
        $count = $this->conn->count(privilege::TABLE,$condition);
        $this->view->assign('count',$count);
        
        $data_privilege = $this->obj->getUserPrivilege($datas);
        //var_dump($data_privilege);
        $array_up = $array_gup = array();
        $str_up = $str_gup = '';
        if($data_privilege)
        {
            $objUG = new UserGroup;
            foreach($data_privilege as $row)
            {
                $str_up = $str_gup = '';
                $arr_up = $arr_gup = array();
                if(isset($row['user_id']))
                {
                    $arr_up = $objUG->getNameUser(implode(',',$row['user_id']));     
                }
                
                if(isset($row['g_user_id']))
                {
                    $arr_gup = $objUG->getGroupNameUser(implode(',',$row['g_user_id']));     
                }
                if($arr_up)
                {
                    $enter = 0;
                    foreach($arr_up as $i)
                    {
                        $str_up .= $i['username'].'(<a style="color:red;font-weight:bold;" href="?com=privilege&act=deloneuser&uid='.$i['id'].'&id='.$row['id'].'">x</a>),&nbsp;';
                        if(++$enter % 5 == 0) $str_up .= '<br />';
                    }
                }
                if($str_up) $array_up[$row['id']] = $str_up;
                
                if($arr_gup)
                {
                    foreach($arr_gup as $i)
                        $str_gup .= $i['group_name'].'(<a href="?com=privilege&act=deloneguser&gid='.$i['id'].'&id='.$row['id'].'">x</a>),';
                }
                if($str_gup) $array_gup[$row['id']] = $str_gup;
            }
        }
        $this->view->assign('array_up',$array_up);
        $this->view->assign('array_gup',$array_gup);

        $obj_menu = new AdminMenu;
        $dataMenu = $obj_menu->selectList('1=1',null,null,'privilege_code');
        //print_r($dataMenu);
        $this->view->assign('dataMenu',$dataMenu);
        //Link sửa xoá
        $link_edit = Flywheel_Factory::getDocument()->getPublicPath().'admin.php?com=privilege&act=edit&id=';
        $link_del = Flywheel_Factory::getDocument()->getPublicPath().'admin.php?com=privilege&act=delete&id=';
        $this->view->assign('link_edit',$link_edit);
        $this->view->assign('link_del',$link_del);
	}
    
	public function executeEdit() 
    {
		$this->message            = MessageUtil::getMessage('privilege');
		$this->document->title    = "Sửa Nhóm người dùng - Admin CP -  ".$this->document->title;
        
		$id = $this->request->get('id', Flywheel_Filter::TYPE_INT, 0);
		$ok = $this->request->post('ok');  
        $this->setView('edit');
        $obj = new Privilege;
        if($id > 0)
        {
            if($ok)
            {
                $privilege_name = trim($this->request->post('privilege_name'));
                $privilege_code = trim($this->request->post('privilege_code'));
                $valid = true;
                
                if(!$privilege_name)
                {
                    $this->message->add('Tên quyền không được để trống','CẢNH BÁO',MessageUtil::TYPE_WARNING);
                    $valid = false;
                }
                
                if(!$privilege_code)
                {
                    $this->message->add('Mã quyền không được để trống','CẢNH BÁO',MessageUtil::TYPE_WARNING);
                    $valid = false;
                }
                
                if($valid)
                {
                    $array_update = array('privilege_name' => $privilege_name, 'privilege_code' => $privilege_code, 'status' => 1);
                    
                    $this->obj->update($array_update,'id='.$id);
                    $this->request->redirect('?com=privilege&act=default&privilege_code='.urlencode($privilege_code));
                }
                else
                {
                    $this->view->assign('message',MessageUtil::render($message));
                }
            }
            else
            {
                $data = array('');
                $this->conn->prepare("SELECT ".implode(',',privilege::$schema['fields']).' FROM '.Privilege::TABLE.' WHERE id='.$id );
                $this->conn->execute();
                $data = $this->conn->fetch();
                $this->view->assign('data',$data);//var_dump($data);
            }
        }
		$this->view->assign('com','privilege');
		Flywheel_Factory::getView()->assign('message', MessageUtil::render($this->message));
	}
	/**
	 * Add new user in admin with multiform of emails & address
	 * @param
	 * @return user _ id
	 * @throws Flywheel_Exception
	 */
	public function executeNew() 
    {
		$this->message      = MessageUtil::getMessage('privilege');
		$this->document->title    = "Thêm Quyền - Admin Privilege -  ".$this->document->title;
        $this->setView('new');
		$ok = $this->request->post('ok');
        if($ok)
        {
            $array_insert = array(
                'privilege_name'        =>  trim($this->request->post('privilege_name')),
                'privilege_code'        =>  strtoupper(trim($this->request->post('privilege_code'))),
                'status'                =>  1,
            );
            
            $valid = true;
            if(!$array_insert['privilege_name'])
            {
                $this->message->add('Tên quyền không được để trống','CẢNH BÁO',MessageUtil::TYPE_WARNING);
                $valid = false;
            }
            
            if(!$array_insert['privilege_code'])
            {
                $this->message->add('Mã quyền không được để trống','CẢNH BÁO',MessageUtil::TYPE_WARNING);
                $valid = false;
            }
            
            if($valid)
            {
                $this->obj->insert($array_insert);
                $this->request->redirect('?com=privilege');
            }
        }
        $this->view->assign('com','privilege');
		Flywheel_Factory::getView()->assign('message', MessageUtil::render($this->message));
	}
    
    public function executeNewAjax() 
    {
        $array_insert = array(
            'privilege_name'        =>  trim($this->request->get('privilege_name')),
            'privilege_code'        =>  strtoupper(trim($this->request->get('privilege_code'))),
            'status'                =>  1,
        );
        if($this->obj->insert($array_insert))
        {
            echo 1;die;
        }
        else
        {
            echo null;die;
        }    
	}
    
    public function executeDelete()
    {
        $id = $this->request->get('id');
        
        if(is_numeric($id))
        {        
            $this->obj->id = $id;
            $this->obj->delete();
        }    
        $this->request->redirect('?com=privilege');
    }
    
    public function executeDelOneUser()
    {
        $id = $this->request->get('id');
        $uid = $this->request->get('uid');
        $condition = "user_id=$uid AND privilege_id=$id";
        $this->obj->deleteUP($condition);
        $this->request->redirect('?com=privilege');
    }
    
    public function executeDelOneGUser()
    {
        $id = $this->request->get('id');
        $gid = $this->request->get('gid');
        $condition = "g_user_id=$gid AND privilege_id=$id";
        $this->obj->deleteUP($condition);
        $this->request->redirect('?com=privilege');
    }
    
    public function executeAddUser()
    {
        $username = $this->request->get('username');
        
        if(!$username) $this->request->redirect('?com=privilege');
        
        $objUG = new UserGroup;
        $arr = array();
        $arr_id = $objUG->getIdUser($username);
        if($arr_id)
        {
            foreach($arr_id as $row)
            {
                $arr[] = $row['id'];
            }
        }
        else
        {
            echo 0;exit;
        }
        $privilege = $this->request->get('pid');
        $objMN = new AdminMenu;
        $data = $objMN->selectOneCondition('privilege_id='.$privilege);
        $parent_code_id = 0;
        if($data['parent_id'] > 0) 
        {
            $data = $objMN->selectOne($data['parent_id']);//var_dump($data);die;
            if($data)
                $parent_code_id = $data['privilege_id'];
        } 
        $objUP = new UserPrivilege;
        if($arr)
        foreach($arr as $a)
        {
            $arr_insert = array(
                'user_id'       =>  $a,
                'g_user_id'     =>  0,
                'privilege_id'  =>  $privilege,
            );
            $objUP->insert($arr_insert);
            if($parent_code_id > 0)
            {
                $arr_insert = array(
                    'user_id'       =>  $a,
                    'g_user_id'     =>  0,
                    'privilege_id'  =>  $parent_code_id,
                );
                $objUP->insert($arr_insert);
            }
        }
        echo 1; exit;
    }
    
    public function executeAddGroupUser()
    {
        $gname = $this->request->get('gname');
        if(!$gname) $this->request->redirect('?com=privilege');
        
        $objUG = new UserGroup;
        $arr_id = $objUG->getGroupIdUser($gname);
        if($arr_id)
        {
            foreach($arr_id as $row)
            {
                $arr[] = $row['id'];
            }
        }
        else
        {
            echo 0;exit;
        }
        
        $privilege = $this->request->get('pid');
        $objUP = new UserPrivilege;
        foreach($arr as $a)
        {
            $arr_insert = array(
                'user_id'       =>  0,
                'g_user_id'     =>  $a,
                'privilege_id'  =>  $privilege,
            );
            
            $objUP->insert($arr_insert);
        }
        echo 1; exit;
    }

    public function executeDelPrivilegeUser()
    {
        //Check sự tồn tại của user
        $user_name = $this->request->get('user_name');
        $conn = Flywheel_DB::getConnection(Users::TABLE);
        $user = $conn->selectOne(Users::TABLE,'id,username','username="'.$user_name.'"');
        //print_r($user);die;
        //Lấy Quyền của user
        if($user)
        {
            //Xoá quyền
            $obj = new UserPrivilege();
            echo $obj->deleteCondition('user_id='.$user['id']);//die;
            //Xoá khỏi Group
            $objUG = new UserGroup();
            $datas = $objUG->selectList();//print_r($datas);die;
            if($datas)
            {
                foreach($datas as $row)
                {
                    $list_uid = $row['user_ids'];
                    $list_uid = explode(',',$list_uid);
                    $arr = array();
                    $true = false;
                    foreach($list_uid as $uid)
                        if($uid != $user['id']) {
                            $arr[] = $uid;
                        } else $true = true;
                    if($true)
                        echo $objUG->update(array('user_ids' => implode(',',$arr)),'id='.$row['id']);
                }
            }
        }
        die;
    }
}