<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SystemConfigController
 *
 * @author roking
 */
require_once CONTROLLERS_DIR . 'BackendController.php';

class SystemConfigController extends BackendController {

    public function executeDefault() {
        $document = Flywheel_Factory::getDocument();
        $document->title = 'Cấu hình website';
        $request = Flywheel_Factory::getRequest();

        if (Flywheel_Application_Request::POST == $request->getMethod()) {
            $settings = $request->post('setting', Flywheel_Filter::TYPE_ARRAY, array());
            foreach ($settings as $key => $value) {
                $siteSetting = System_Config::setting($key, true);
                $siteSetting->setValue($value);
                $siteSetting->save();
            }
        }
    }

}