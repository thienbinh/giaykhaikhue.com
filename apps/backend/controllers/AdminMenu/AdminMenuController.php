<?php

require_once CONTROLLERS_DIR . 'BackendController.php';

class AdminMenuController extends BackendController {

    public function beforeExecute() {
        parent::beforeExecute();
        $view = Flywheel_Factory::getView();
        $message = MessageUtil::getMessage('common');
        $request = Flywheel_Factory::getRequest();
        $user = Flywheel_Authen::getInstance()->getUser();
        /* if (!$user->hasRule('staff_management')){
          $message->add('Bạn không có quyền truy cập khu vực này!', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
          $request->redirect('/');
          } */
        $view->assign('message', MessageUtil::render($message));
    }

    public function executeDefault() {
        $objP = new Privilege;
        $dataP = $objP->selectList('1=1');
        $arrCode = array();
        if ($dataP)
            foreach ($dataP as $row)
                $arrCode[] = '"' . $row['privilege_code'] . '"';
        $obj = new AdminMenu;
        if ($arrCode)
            $dataMN = $obj->selectList('privilege_code NOT IN(' . implode(',', $arrCode) . ')');
        else
            $dataMN = $obj->selectList('1=1');
        if ($dataMN) {
            foreach ($dataMN as $row) {
                $objP->insert(array('privilege_name' => $row['title'], 'privilege_code' => $row['privilege_code']));
            }
        }
        unset($obj);
        unset($objP);

        $message = MessageUtil::getMessage('menu');
        $view = Flywheel_Factory::getView();
        $request = Flywheel_Factory::getRequest();
        $document = Flywheel_Factory::getDocument();
        $document->title = "Quản Menu - Admin Menu - " . $document->title;
        $conn = Flywheel_DB::getConnection(AdminMenu::TABLE);
        $valid = true;
        $sql = "";
        $condition = "1=1";

        $title = $request->get('title', Flywheel_Filter::TYPE_STRING, '');
        if ($title != '') {
            $condition .= ' AND title LIKE "%' . $title . '%"';
        }
        $view->assign('title', $title);

        $parent_id = $request->get('parent_id', Flywheel_Filter::TYPE_INT, 0);
        if ($parent_id != 0) {
            $condition .= ' AND parent_id = ' . $parent_id;
        }
        $privilege = array(0 => 'Chọn cấp cha');
        $sql_parent = "SELECT " . implode(', ', AdminMenu::$schema['fields']) . " FROM " . AdminMenu::TABLE . " WHERE parent_id=0 AND status=1";
        $conn->prepare($sql_parent);
        $conn->execute();
        $data_parent = $conn->fetchAll();
        $view->assign('selectParent', AdminMenu::optionSelect($data_parent, 'title', 'id', $request->get('parent_id')));

        $url = $request->get('url', Flywheel_Filter::TYPE_STRING, '');
        if ($url) {
            $condition .= ' AND url LIKE "%' . $url . '%"';
        }
        $view->assign('url', $url);

        $title_privilege = $request->get('privilege', Flywheel_Filter::TYPE_STRING, '');
        if ($title_privilege) {
            $condition .= ' AND privilege_code = "' . $title_privilege . '"';
        }
        $view->assign('privilege', $title_privilege);

        $comment = $request->get('comment', Flywheel_Filter::TYPE_STRING, '');
        if ($comment) {
            $condition .= ' AND comment LIKE "%' . trim($comment) . '%"';
        }
        $view->assign('comment', $comment);

        $datas = array('');

        $sql = "SELECT " . implode(',', AdminMenu::$schema['fields']) . ' FROM ' . AdminMenu::TABLE . ' WHERE ' . $condition;
        $conn->prepare($sql);
        $conn->execute();
        $datas = $conn->fetchAll();
        $view->assign('com', 'admin_menu');
        $view->assign('datas', $datas);
        $count = $conn->count('admin_menu', $condition);
        $view->assign('count', $count);
        #Cấp cha
        $sql_parent = "SELECT " . implode(', ', AdminMenu::$schema['fields']) . " FROM " . AdminMenu::TABLE . " WHERE parent_id=0 AND status=1";
        $conn->prepare($sql_parent);
        $conn->execute();
        $title_parent = $conn->fetchAll('id');
        //var_dump($title_parent);
        $view->assign('title_parent', $title_parent);

        //Link sửa xoá
        $link_edit = Flywheel_Factory::getDocument()->getPublicPath() . 'admin.php?com=admin_menu&act=edit&id=';
        $link_del = Flywheel_Factory::getDocument()->getPublicPath() . 'admin.php?com=admin_menu&act=delete&id=';
        $view->assign('link_edit', $link_edit);
        $view->assign('link_del', $link_del);
    }

    public function executeEdit() {
        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('menu');
        $document = Flywheel_Factory::getDocument();
        $document->title = "Sửa menu - Admin CP -  " . $document->title;
        $conn = Flywheel_DB::getConnection(AdminMenu::TABLE);
        $view = Flywheel_Factory::getView();
        $id = $request->get('id', Flywheel_Filter::TYPE_INT, 0);
        $ok = $request->get('ok');

        $this->setView('edit');
        $admin_menu = new AdminMenu;
        if ($id > 0) {
            if ($ok) {
                $title = $request->get('title');
                $parent_id = $request->get('parent_id');
                $comment = $request->get('comment');
                $url = $request->get('url');
                $privilege = trim($request->get('privilege'));
                $valid = true;

                if (!$title) {
                    $message->add('Bạn chưa nhập tên Menu', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
                    $valid = false;
                }
                if (!$url) {
                    $message->add('Bạn chưa nhập url cho Menu', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
                    $valid = false;
                }
                if (!$privilege) {
                    $message->add('Bạn chưa nhập Quyền truy cập cho Menu', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
                    $valid = false;
                }

                if ($valid) {
                    $array_update = array('title' => $title, 'comment' => $comment, 'url' => $url, 'parent_id' => $parent_id, 'privilege_id' => 0, 'privilege_code' => $privilege, 'status' => 1);
                    //var_dump($array_update);die;
                    /*
                      $admin_menu->setTitle($title);
                      $admin_menu->setComment($comment);
                      $admin_menu->setUrl($url);
                      $admin_menu->setParentId($parent_id);
                      $admin_menu->setPrivilegeId($privilege_id);
                      $admin_menu->setPrivilegeCode($privilege_code);
                      $admin_menu->setStatus($status);
                     */
                    //var_dump($array_update);die;
                    $admin_menu->update($array_update, 'id=' . $id);
                    $request->redirect('?com=admin_menu&act=default&title=' . urlencode($title));
                } else {
                    $view->assign('message', MessageUtil::render($message));
                }
            } else {
                $data = array('');
                $conn->prepare("SELECT " . implode(',', AdminMenu::$schema['fields']) . ' FROM ' . AdminMenu::TABLE . ' WHERE id=' . $id);
                $conn->execute();
                $data = $conn->fetch();
                //var_dump($data);
                $view->assign('data', $data);
                $sql_parent = "SELECT " . implode(', ', AdminMenu::$schema['fields']) . " FROM " . AdminMenu::TABLE . " WHERE parent_id=0 AND status=1";
                $conn->prepare($sql_parent);
                $conn->execute();
                $data_parent = $conn->fetchAll();
                $view->assign('selectParent', AdminMenu::optionSelect($data_parent, 'title', 'id', $data['parent_id']));
            }
        }
        $view->assign('com', 'admin_menu');
        Flywheel_Factory::getView()->assign('message', MessageUtil::render($message));
    }

    /**
     * Add new user in admin with multiform of emails & address
     * @param
     * @return user _ id
     * @throws Flywheel_Exception
     */
    public function executeNew() {
        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('menu');
        $document = Flywheel_Factory::getDocument();
        $document->title = "Thêm menu - Admin CP -  " . $document->title;
        $view = Flywheel_Factory::getView();
        $conn = Flywheel_DB::getConnection(AdminMenu::TABLE);
        $this->setView('new');
        $obj = new AdminMenu;
        $ok = $request->get('ok');
        if ($ok) {
            $array_insert = array(
                'title' => $request->get('title'),
                'comment' => $request->get('comment'),
                'url' => $request->get('url'),
                'parent_id' => $request->get('parent_id'),
                'privilege_id' => 0,
                'privilege_code' => $request->get('privilege'),
                'status' => 1,
            );

            $valid = true;
            if (!$array_insert['title']) {
                $message->add('Bạn chưa nhập tên Menu', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
                $valid = false;
            }

            if (!$array_insert['comment']) {
                $message->add('Bạn chưa nhập thông tin thêm Menu', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
                $valid = false;
            }

            if (!$array_insert['url']) {
                $message->add('Bạn chưa nhập link Menu', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
                $valid = false;
            }

            if (!$array_insert['privilege_code']) {
                $message->add('Bạn chưa nhập Mã quyền Menu', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
                $valid = false;
            }

            if ($valid) {
                $obj->insert($array_insert);
                $request->redirect('?com=admin_menu');
            }
        }
        $view->assign('com', 'admin_menu');
        $sql_parent = "SELECT " . implode(', ', AdminMenu::$schema['fields']) . " FROM " . AdminMenu::TABLE . " WHERE parent_id=0 AND status=1";
        $conn->prepare($sql_parent);
        $conn->execute();
        $data_parent = $conn->fetchAll();
        $view->assign('selectParent', AdminMenu::optionSelect($data_parent, 'title', 'id', $request->get('parent')));

        Flywheel_Factory::getView()->assign('message', MessageUtil::render($message));
    }

    public function executeNewAjax() {
        $request = Flywheel_Factory::getRequest();
        $obj = new AdminMenu;
        $privilege = $request->get('privilege');
        $data = $obj->selectOneCondition('privilege_code="' . $privilege . '"');
        if ($data) {
            echo -2;
            die;
        }
        $array_insert = array(
            'title' => $request->get('title'),
            'comment' => $request->get('comment'),
            'url' => $request->get('url'),
            'parent_id' => $request->get('parent_id'),
            'privilege_id' => 0,
            'privilege_code' => $request->get('privilege'),
            'status' => 1
        );
        $array_p = array(
            'privilege_name' => $request->get('title'),
            'privilege_code' => $request->get('privilege'),
            'status' => 1,
        );
        $objPrivilege = new Privilege;
        $pri_id = $objPrivilege->insert($array_p);
        $array_insert['privilege_id'] = $pri_id;
        if ($obj->insert($array_insert))
            echo 1;
        else
            echo null;
        exit;
    }

    public function executeDelete() {
        $request = Flywheel_Factory::getRequest();
        $id = $request->get('id');
        if (is_numeric($id)) {
            $obj = new AdminMenu;
            $obj->id = $id;
            $obj->delete();
        }
        $request->redirect('?com=admin_menu');
    }

    public function executeDelCache() {
        $this->setView('del_cache');
    }

    public function executeSlugCode() {
        $request = Flywheel_Factory::getRequest();
        $text = $request->get('text'); //echo $text;
        echo strtoupper(BuildLink::slugify($text, '_'));
        die;
    }

}