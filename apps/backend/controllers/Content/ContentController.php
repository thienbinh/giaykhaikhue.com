<?php

require_once CONTROLLERS_DIR . 'BackendController.php';

class ContentController extends BackendController {

    public function executeDefault() {
        $view = Flywheel_Factory::getView();
        $document = Flywheel_Factory::getDocument();
        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('content');
        $pageSize = 20; // 20 content per page

        $document->title = 'Quản lý nội dung - Admin CP -  ' . $document->title;

        $contents = new Content();
        $categoryTree = ContentCategory::getTree();
        $view->assign('categoryTree', $categoryTree);

        //get filter from request
        $filter = array();
        $filterState = $request->get('state', Flywheel_Filter::TYPE_INT, -1);
        if ($filterState >= 0) {
            $filter['state'] = $filterState;
        }
        $filterCat = $request->get('cat_id', Flywheel_Filter::TYPE_INT, 0);
        if ($filterCat > 0) {
            $filter['cat_id'] = $filterCat;
        }
        $keyword = $request->get('k');
        if (null != $keyword) {
            $filter['keyword'] = $keyword;
        }
        $view->assign('keyword', $keyword);

        $state = array(
            'Trạng thái' => 'all',
            'Mở' => '1',
            'Khóa' => '0'
        );
        $view->assign('state_select', render_form_select('state', $state, $filterState, 'filter-by-states'));

        // Get categories tree
        $tree = array(0 => array('id' => 0, 'level' => 0));
        ContentCategory::getTreeScalar(0, $tree);
        for ($i = 0, $size = sizeof($tree); $i < $size; ++$i) {
            $option = ($tree[$i]['level'] > 0 ) ? '|' . str_repeat('--', $tree[$i]['level']) . ' ' : '';
            if ($tree[$i]['id'] != 0) {
                $option .= ContentCategory::retrieveByPk($tree[$i]['id'])->name;
            } else {
                $option = 'Tất cả';
            }
            $optCat[$option] = $tree[$i]['id'];
        }
        $view->assign('filter_cats', render_form_select('cat_id', $optCat, $filterCat, 'filter-by-categories'));

        $total = ContentPeer::countContents($filter);

        $pagination = new Flywheel_Pagination();
        $urlStructure = '';
        foreach ($filter as $control => $value) {
            $urlStructure .= '&' . $control . '=' . $value;
        }
        $pagination->urlStructure = '?com=content&page=<page>'
                . ((null != $urlStructure) ? '&' . $urlStructure : '');

        // Set pagination        
        $currentPage = $request->get('page', Flywheel_Filter::TYPE_INT, 1);
        $pagination->pageSize = $pageSize;
        $pagination->currentPage = $currentPage;
        $pagination->template = TEMPLATE_DIR . 'modules' . DS . 'pagination';
        $pagination->total = $total;
        $pagination->initialize();
        $view->assign('pagination', $pagination->render());

        $start = $pagination->pageSize * ($pagination->currentPage - 1);
        $view->assign('current', $start + 1);
        $view->assign('contents', ContentPeer::getContents($filter, $currentPage, $pageSize));

        //echo $pagination->total;
        $view->assign('total', $total);
        $view->assign('message', MessageUtil::render($message));
    }

    public function executeNew() {
        $message = MessageUtil::getMessage('content');
        $view = Flywheel_Factory::getView();
        $document = Flywheel_Factory::getDocument();
        $request = Flywheel_Factory::getRequest();

        $document->title = "Thêm bài mới - Admin CP -  " . $document->title;
        $this->setView('form');

        $content = new Content();
        if (Flywheel_Application_Request::POST == $request->getMethod()) {
            if (true === $this->_save($content)) {
                $message->add('Tạo mới bài viết ' . $content->title . ' thành công!'
                        , 'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
                $request->redirect('?com=content');
            }
        }
        $document->title = 'Thêm bài mới - Admin CP - ' . $document->title;
        $this->_drawContentForm($content);
        $view->assign('message', $message);
    }

    public function executeEdit() {
        $view = Flywheel_Factory::getView();
        $document = Flywheel_Factory::getDocument();
        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('content');
        $document->title = "Sửa bài viết - Admin CP -  " . $document->title;
        $this->setView('form');
        $id = $request->get('id', Flywheel_Filter::TYPE_INT, 0);
        $content = new Content();

        if (0 == $id || true !== $content->read($id)) {
            $message->add('Bài viết [id=' . $id . '] không tồn tại', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
            $request->redirect('?com=content');
        }

        if (Flywheel_Application_Request::POST == $request->getMethod()) {
            if (true === $this->_save($content)) {
                $message->add('Lưu thành công!', 'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
            }
        }

        $this->_drawContentForm($content);
        $view->assign('message', $message);
    }

    private function _save(Content &$content) {
        $message = MessageUtil::getMessage('content');
        $request = Flywheel_Factory::getRequest();
        $session = Flywheel_Authen::getInstance()->getUser();

        $content->setTitle(trim($request->post('title', Flywheel_Filter::TYPE_STRING)));
        $content->setCategoryId($request->post('category_id', Flywheel_Filter::TYPE_INT, 0));
        $content->setExcerpt($request->post('excerpt', Flywheel_Filter::TYPE_STRING, null, 2));
        $content->setContent($request->post('content', Flywheel_Filter::TYPE_STRING, null, 2));
        $content->setPos($request->post('ordering', Flywheel_Filter::TYPE_INT, 0));

        $_public_f =
                Flywheel_DateTime::dateToUnixTimestamp($request->post('published_f', Flywheel_Filter::TYPE_STRING, '') . ' 0:0:0', 'D/M/Y H:I:S');
        $content->setPublishFrom($_public_f);

        $_public_t =
                Flywheel_DateTime::dateToUnixTimestamp($request->post('published_t', Flywheel_Filter::TYPE_STRING, '') . ' 0:0:0', 'D/M/Y H:I:S');
        $content->setPublishTo($_public_t);

        $content->setHits($request->post('hits', Flywheel_Filter::TYPE_INT, 0));
        $content->setSource(nl2br(trim($request->post('source', Flywheel_Filter::TYPE_STRING))), true);
        $content->setStatus($request->post('cb_public', Flywheel_Filter::TYPE_STRING, '') ? 1 : 0);
        //$content->setCreatedBy($session->id);

        if (true == $content->isNew()) {
            $content->setCreatedBy($session->id);
        } else {
            $content->setModifiedBy($session->id);
        }

        $valid = true;

        if (null == $content->title) {
            $message->displayMessageOnField('title', 'Tiêu đề bài viết để trống', 'LỖI', MessageUtil::TYPE_ERROR);
            $valid = false;
        }

        if (0 == $content->category_id) {
            $message->displayMessageOnField('category_id', 'Chưa chọn nhóm nội dung', 'LỖI', MessageUtil::TYPE_ERROR);
            $valid = false;
        }

        if (true !== $valid) {
            return false;
        }

        if ($content->save()) {
            return true;
        }

        return false;
    }

    public function _drawContentForm(Content &$content) {
        $view = Flywheel_Factory::getView();
        $this->setView('form');
        $view->assign('content', $content);

        $tree = array(0 => array('id' => 0, 'level' => 0));
        ContentCategory::getTreeScalar(0, $tree);
        for ($i = 0, $size = sizeof($tree); $i < $size; ++$i) {
            $option = ($tree[$i]['level'] > 0 ) ? '|' . str_repeat('--', $tree[$i]['level']) . ' ' : '';
            if ($tree[$i]['id'] != 0) {
                $option .= ContentCategory::retrieveByPk($tree[$i]['id'])->name;
            } else {
                $option = 'Chọn danh mục';
            }
            $optCat[$option] = $tree[$i]['id'];
        }
        $view->assign('select_category', render_form_select('category_id', $optCat, $content->category_id));
    }

    public function executePublic() {
        $request = Flywheel_Factory::getRequest();

        $_cid = $request->post('cid', Flywheel_Filter::TYPE_STRING, '');
        $selectContents = explode(',', $_cid);

        if (!empty($selectContents)) {
            foreach ($selectContents as $id) {
                $content = Content::retrieveByPk($id);
                $content->setStatus(1);
                $content->setModifiedTime(time());
                $content->save();
            }
            return $this->renderText(json_encode(array('status' => true)));
        }

        return $this->renderText(json_encode(array('status' => false)));
    }

    public function executeUnpublic() {
        $request = Flywheel_Factory::getRequest();

        $_cid = $request->post('cid', Flywheel_Filter::TYPE_STRING, '');
        $selectContents = explode(',', $_cid);

        if (!empty($selectContents)) {
            foreach ($selectContents as $id) {
                $content = Content::retrieveByPk($id);
                $content->setStatus(0);
                $content->setModifiedTime(time());
                $content->save();
            }
            return $this->renderText(json_encode(array('status' => true)));
        }

        return $this->renderText(json_encode(array('status' => false)));
    }

    /*
      private function _publishContent($selectContents) {
      $view = Flywheel_Factory::getView();
      $message = MessageUtil::getMessage('content');

      if(0 == sizeof($selectContents)) {
      $message->add('Phải chọn bài viết mới có thể thực hiện thao tác này !',
      'CẢNH BÁO', MessageUtil::TYPE_WARNING);
      $view->assign('message', MessageUtil::render($message));
      return false;
      }
      else {
      $content = new Content();
      $currentTime= time();
      foreach ($selectContents as $id) {
      //$content->getContents($_where)
      $content->setId($id);
      $content->setPublishFrom($currentTime);
      $content->setPublishTo(0);
      $content->save();
      }
      $message->add('Đã công khai các bài viết được chọn !',
      'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
      $view->assign('message', MessageUtil::render($message));
      return true;
      }
      }
      private function _unPublishContent($selectContents) {

      $view = Flywheel_Factory::getView();
      $message = MessageUtil::getMessage('content');

      if(0 == sizeof($selectContents)) {
      $message->add('Phải chọn bài viết mới có thể thực hiện thao tác này !',
      'CẢNH BÁO', MessageUtil::TYPE_WARNING);
      $view->assign('message', MessageUtil::render($message));
      return false;
      }
      else {
      $content = new Content();
      $currentTime= time();
      foreach ($selectContents as $id) {
      //$content->getContents($_where)
      $content->setId($id);
      $content->setPublishTo($currentTime);
      $content->save();
      //   echo $content->id;
      }
      $message->add('Đã khóa các bài viết được chọn !',
      'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
      $view->assign('message', MessageUtil::render($message));
      return true;
      }

      }
     */

    private function _deleteContent($selectContents) {

        $view = Flywheel_Factory::getView();
        $message = MessageUtil::getMessage('content');

        if (0 == sizeof($selectContents)) {
            $message->add('Phải chọn bài viết mới có thể thực hiện thao tác này !', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
            $view->assign('message', MessageUtil::render($message));
            return false;
        } else {
            $content = new Content();
            foreach ($selectContents as $id) {
                $content->setId($id);
                $content->save();
                $content->delete();
            }
            $message->add('Đã xóa các bài viết được chọn !', 'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
            $view->assign('message', MessageUtil::render($message));
            return true;
        }
    }

    public function executeDel() {

        $request = Flywheel_Factory::getRequest();
        $user = Flywheel_Authen::getInstance()->getUser();

        $id = $request->post('id', Flywheel_Filter::TYPE_STRING, '');
        $status = true;
        if ($user) {
            if ($user->type >= 5) {
                $arrId = explode(',', $id);
                for ($i = 0; $i < count($arrId); $i++) {
                    $content = Content::retrieveByPk($arrId[$i]);
                    if (!$content->delete()) {
                        $status = false;
                        break;
                    }
                }
            }
        }
        return $this->renderText($status);
    }

    /**
     * View contact template
     */
    public function executeContact() {
        $content = "";
        $req = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();

        if ($req->getMethod() == Flywheel_Application_Request::POST) {
            // Save contact content
            $content = $req->post('content', Flywheel_Filter::TYPE_STRING, 0);
            
            $file = CACHE_DIR . 'contact_temp.html';
            file_put_contents($file, $content);
            chmod($file, 0777);
        }

        $this->setView('contact');
    }

    public function executeNotify() {
        
    }
    
}