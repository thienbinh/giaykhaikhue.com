<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once CONTROLLERS_DIR . 'BackendController.php';
require_once APP_DIR . 'helpers' . DS . 'Utility.php';

class OrderController extends BackendController {
    public function beforeExecute() {
        parent::beforeExecute();
    }
    
    public function executeDefault() {
        $view = Flywheel_Factory::getView();
        $user = Flywheel_Authen::getInstance()->getUser();
        $req = Flywheel_Factory::getRequest();
        // Load list order
        $_orderObj = new Order();
        $page = $req->get('p', Flywheel_Filter::TYPE_INT, 1);
        $pageSize = 20;
        $filter = array();
        $structure = "";
        $condition = "`is_delete`=0";
        if($req->get('name') != "") {
            $condition .= " AND `username` LIKE '%{$req->get('name')}%'";
            $structure .= "&name={$req->get('name')}";
        }
        if($req->get('status', Flywheel_Filter, -1) > -1 & $req->get('status', Flywheel_Filter, -1) < 2) {
            $condition .= " AND `status` = {$req->get('status')}";
            $structure .= "&status={$req->get('status')}";
        }
        if($req->get('df') != "") {
            $df = Flywheel_DateTime::dateToUnixTimestamp($req->get('df'), "D/M/Y H:I:S");
            $condition .= " AND `created_time` >= {$df}";
            $structure .= "&df={$req->get('df')}";
        }
        if($req->get('dt') != "") {
            $dt = Flywheel_DateTime::dateToUnixTimestamp($req->get('dt'), "D/M/Y H:I:S");
            $condition .= " AND `created_time` <= {$dt}";
            $structure .= "&dt={$req->get('dt')}";
        }
        
        $pagination = new Flywheel_Pagination();
        $pagination->pageSize = $pageSize;
        $pagination->currentPage = $page;
        $pagination->template = TEMPLATE_DIR . 'modules' . DS . 'pagination';
        $pagination->total = $_orderObj->countItem();
        $pagination->urlStructure = "?com=order&page=<page>";
        $pagination->initialize();
        $view->assign('pagination', $pagination->render());
        
        // Load list items
        $limit = $pageSize * ($page - 1) . "," . $pageSize;
        $_orders = $_orderObj->selectList($condition, "`created_time` DESC", $limit);
        
        $view->assign('_orders', $_orders);
        $this->setView('default');
    }

    public function executeDetail() {
        $req = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        
        $id = $req->get('id', Flywheel_Filter::TYPE_INT, 0);
        // Read order info
        $order = new Order();
        $order->read($id);
        
        // Read items in order
        $order_items = new OrderItems();
        $_items = $order_items->selectList("`order_id` = {$id}");
        
        $view->assign('_order', $order);
        $view->assign('_items', $_items);
        
        $this->setView('detail');
    }

    public function executeDeliver() {
        $req = Flywheel_Factory::getRequest();
        $result = array('status' => true, 'msg' => '');
        $id = $req->post('id', Flywheel_Filter::TYPE_INT, 0);
        
        $order = new Order();
        $order->read($id);
        if($order->id == 0) {
            $result['status'] = false;
            $result['msg'] = "Đơn hàng không tồn tại!";
        }
        
        if($result['status']) {
            $order->setStatus(1);
            if(!$order->save()) {
               $result['status'] = false;
               $result['msg'] = "Chuyển trạng thái đơn hàng lỗi!";
            }
        }
        return $this->renderText(json_encode($result));
    }
    
    public function executeDel() {
        $req = Flywheel_Factory::getRequest();
        $result = array('status' => true, 'msg' => '');
        $id = $req->post('id', Flywheel_Filter::TYPE_INT, 0);
        
        $order = new Order();
        $order->read($id);
        if($order->id == 0) {
            $result['status'] = false;
            $result['msg'] = "Đơn hàng không tồn tại!";
        }
        
        if($result['status']) {
            $order->setIsDelete(1);
            if(!$order->save()) {
               $result['status'] = false;
               $result['msg'] = "Xóa đơn hàng hàng lỗi!";
            }
        }
        return $this->renderText(json_encode($result));
    }
    
}
?>
