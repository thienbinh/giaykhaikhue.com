<?php
require_once CONTROLLERS_DIR .'BackendController.php';
require_once FLYWHEEL_DIR .'Util' .DS .'MessageUtil.php';
class LoginController extends BackendController {
	public function executeDefault() {
		$this->executeLogin();
	}
	
	public function executeLogin() {		
		$this->setLayout('login');
		$view 		= Flywheel_Factory::getView();
		$authen		= Flywheel_Authen::getInstance();
		$document 	= Flywheel_Factory::getDocument();
		$request 	= Flywheel_Factory::getRequest();				
		$message 	= MessageUtil::getMessage('login');
		
		if ($authen->isAuthenticated()) {			
			$request->redirect(($request->get('redirect'))? $request->get('redirect'): '');			
		}
        
		$view->assign('action', Flywheel_Factory::getRouter()->getAbsoluteUri());
		
		$document->title = 'Log in | ' .$document->title .': Admin CP';
		
		if (Flywheel_Application_Request::POST == $request->getMethod()) {
			$username = $request->post('username');
			$password = $request->post('password');
                        
			if ('' == $username || '' == $password) {
                              $message->add('Tên đăng nhập hoặc mật khẩu trống!', 'LỖI', MessageUtil::TYPE_ERROR);			
			} elseif (false === $authen->authenticate($username, $password)) {
                              $message->add('Tên đăng nhập hoặc mật khẩu không đúng!', 'LỖI', MessageUtil::TYPE_ERROR);				
			} else {
                              $user = new Users(Users::findByUsername($username));
                             
                              if($user->checkLocked()==true){
                                 $message->add('Tài khoản này đang bị khóa!', 'THÔNG BÁO', MessageUtil::TYPE_ERROR);				
                              }else{
                                 $request->redirect('');
                              }
                        }
		}
		$view->assign('message', MessageUtil::render($message));
	}
	
	public function executeLogout() {
		Flywheel_Authen::getInstance()->logout();
		MessageUtil::getMessage('login')->add('Thoát thành công!', 'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
		Flywheel_Factory::getRequest()->redirect('?com=login');
	}
	
	public function executeAccessDenied() {
		$this->setView('access_denied');	
	}
    
    
}