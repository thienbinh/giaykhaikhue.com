<?php

require_once CONTROLLERS_DIR . 'BackendController.php';

class UserController extends BackendController {

    public $public_html = '';
    public $request = '';
    public $user = '';

    public function beforeExecute() {
        parent::beforeExecute();
        $view = Flywheel_Factory::getView();
        $message = MessageUtil::getMessage('common');
        $view->assign('message', MessageUtil::render($message));
        $this->public_html = Flywheel_Factory::getDocument()->getPublicPath();
        $this->request = Flywheel_Factory::getRequest();
        $this->user = new Users;
    }

    public function executeDefault() {
        $message = MessageUtil::getMessage('user');
        $view = Flywheel_Factory::getView();
        $request = Flywheel_Factory::getRequest();
        $document = Flywheel_Factory::getDocument();
        $document->title = "Quản lý nhân viên - Admin CP -  " . $document->title;
        $pageSize = 15;

        $filter_paging = array();
        //get filter from request
        $filter = array();
        if ($_GET) {
            $username = $request->get("username");
            $fullname = $request->get("fullname");
            $is_staff = $request->get("is_staff");
            $is_staff = $request->get("is_staff");
            $df     = $request->get("time_join_from");
            $dt     = $request->get("time_join_to");
            if ($username) {
                $filter[] = " `username` LIKE '%" . $username . "%'";
                $filter_paging['username'] = $username;
            }
            if ($fullname) {

                $filter[] = " `fullname` LIKE '%" . $fullname . "%'";
                $filter_paging['fullname'] = $fullname;
            }
            if ($is_staff) {
                $filter[] = " `is_staff` = " . $is_staff . "";
                $filter_paging['is_staff'] = $is_staff;
            }
            if(!empty($df)) {
                $filter[] = " `time_join` >= " . Flywheel_DateTime::dateToUnixTimestamp($df . " 0:0:1", "D/M/Y H:I:S") . "";
                $filter_paging['time_join_from'] = $df;
            }
            if(!empty($dt)) {
                $filter[] = " `time_join` <= " . Flywheel_DateTime::dateToUnixTimestamp($dt . " 23:59:59", "D/M/Y H:I:S") . "";
                $filter_paging['time_join_to'] = $dt;
            }
        }
        $total = UsersPeer::getTotalUserNew($filter);
        $pagination = new Flywheel_Pagination();
        $urlStructure = '';
        if (!empty($filter_paging)) {
            foreach ($filter_paging as $control => $value) {
                $urlStructure .= '&' . $control . '=' . $value;
            }
        }

        $pagination->urlStructure = '?com=user&page=<page>'
                . ((null != $urlStructure) ? '&' . $urlStructure : '');

        $currentPage = $request->get('page', Flywheel_Filter::TYPE_INT, 1);
        $pagination->pageSize = $pageSize;
        $pagination->currentPage = $currentPage;
        $pagination->template = TEMPLATE_DIR . 'modules' . DS . 'pagination';
        $pagination->total = $total;
        $pagination->initialize();
        $view->assign('pagination', $pagination->render());

        $start = $pagination->pageSize * ($pagination->currentPage - 1);
        $view->assign('current', $start + 1);
        $users = UsersPeer::getMulti($filter, $currentPage, $pageSize);

        $view->assign('users', $users);
        $view->assign('user_type', Users::userType());

        //echo $pagination->total;
        $view->assign('total', $total);
        $view->assign('page', $currentPage);
        $view->assign('message', MessageUtil::render($message));
    }

    /**
     * Add new user in admin with multiform of emails & address
     * @param
     * @return user _ id
     * @throws Flywheel_Exception
     */
    public function executeNew() {
        $user = new Users();
        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('user');
        $document = Flywheel_Factory::getDocument();
        $document->title = "Thêm nhân viên - Admin";
        $this->setView('form');
        if (Flywheel_Application_Request::POST == $request->getMethod()) {
            if ((true === $this->_save($user))) {

                $message->add('Tạo mới tài khoản ' . $user->full_name . ' thành công!'
                        , 'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
                $request->redirect('?com=user');
            }
        }
        //$view->assign('user', $user);
        Flywheel_Factory::getView()->assign('message', MessageUtil::render($message));
    }

    /**
     * Save user
     *
     * @param Users		$user
     * @return boolean
     *
     * @throws Flywheel_DB_Excpetion
     */
    private function _save(Users &$user) {
        $request = Flywheel_Factory::getRequest();

        $valid = true;
        $message = MessageUtil::getMessage('user');

        $username = $request->post('username');
        $password = $request->post('password');
        $repassword = $request->post('repassword');

        if ($user->isNew()) {
            $user->setUsername($username);
            $user->setTimeJoin(time());
        }


        if ((true == $user->isNew()) && (null == $password)) {
            $message->add('Mật khẩu không được để trống', 'LỖI', MessageUtil::TYPE_ERROR);
        }
        if (null != $password) {
            if (strlen($password) < 6) {
                $valid = false;
                $message->add('Password quá ngắn!', 'LỖI', MessageUtil::TYPE_ERROR);
            } else if ($password != $repassword) {
                $valid = false;
                $message->add('Xác nhận password không chính xác!', 'LỖI', MessageUtil::TYPE_ERROR);
            } else {
                $salt = UsersPeer::generateSalt($user->username);
                $user->setPassword($salt . md5($salt . $password));
                //$user->setPassword(md5($password));
            }
        }

        $user->setFullname($request->post('fullname'));

        //$user->setGender($request->post('gender'));
        $user->setEmail($request->post('email'));

        //$user->setMobile($request->post('mobile'));
        //$user->setPhone($request->post('phone'));
        //$address_detail = $request->post('address');
        //$state = $request->post('state');
        //$disctrict = $request->post('disctric');
        //$address = $address_detail.", ".$disctrict.", ".$state;
        //$user->setAddress($address);
        //$user->setStatus($request->post('status', 	Flywheel_Filter::TYPE_INT), 1);
        //$user->setBirthday($request->post('birth_year') .'-' . $request->post('birth_month') .'-' .$request->post('birth_day'));

        if (true == $user->isNew()) {
            $valid = is_valid_username($user->username);
            if (false == $valid) {
                $message->add('Username không đúng định dang!', 'LỖI', MessageUtil::TYPE_ERROR);
            }
        }

        if (null == $user->fullname) {
            $valid = false;
            $message->add('Tên thật không được để trống!', 'LỖI', MessageUtil::TYPE_ERROR);
        }
        if (true === $user->isNew() && UsersPeer::checkExistsUsername($user->username)) {
            $valid = false;
            $message->add('Tài khoản đã tồn tại!', 'LỖI', MessageUtil::TYPE_ERROR);
        }
        if (false != $valid) {
            if ($user->save()) {
                return true;
            }
            return false;
        }
        return false;
    }

    public function executeDelete() {
        $request = Flywheel_Factory::getRequest();
        $user_id = $request->post('user_id');
        $user = new Users(Users::retrieveByPk($user_id));
        if ($user->active == 0) {
            $user->setIsDelete(1);
            if ($user->save()) {
                echo 'SUCCESS';
            } else {
                echo 'ERROR';
            }
        } else {
            echo 'Không thể xóa người dùng này';
        }
        exit;
    }

    public function executeLock() {
        $request = Flywheel_Factory::getRequest();

        $user_id = $request->post('user_id');
        $lock_type = $request->post('lock_type');
        $from = $request->post('from');
        $to = $request->post('to');
        $reson = $request->post('reson');

        $success = 0;
        $mes = "";
        $result = array();

        $user = new Users(Users::retrieveByPk($user_id));

        $user->setLockStatus($lock_type);
        if ($lock_type != 2) {
            $arr_from = explode("/", $from);

            $user->setLockTimeBegin(strtotime($arr_from[2] . "/" . $arr_from[1] . "/" . $arr_from[0]));

            $arr_to = explode("/", $to);
            $user->setLockTimeEnd(strtotime($arr_to[2] . "/" . $arr_to[1] . "/" . $arr_to[0]));
        }
        $user->setLockReason($reson);
        $user->setNew(false);
        if ($user->save()) {
            $success = 1;
            $status = $user->lock_status;
            $mes = "Khóa tài khoản thành công";
        } else {
            $success = 0;
            $status = $user->lock_status;
            $mes = "Có lỗi xảy ra";
        }
        $result = array("success" => $success, "mes" => $mes, "status" => $status);
        print(json_encode($result));
        exit;
    }

    public function executeUnlock() {
        $request = Flywheel_Factory::getRequest();
        $user_id = $request->post('user_id');
        $user = new Users(Users::retrieveByPk($user_id));
        $user->setLockStatus(0);
        $user->setNew(false);
        if ($user->save()) {
            $success = 1;
            $mes = "Mở khóa thành công.";
        } else {
            $success = 0;
            $mes = "Có lỗi xảy ra";
        }
        $result = array("success" => $success, "mes" => $mes);
        print(json_encode($result));
        exit;
    }

    public function executeActive() {
        $request = Flywheel_Factory::getRequest();
        $user_id = $request->post('user_id');
        $user = new Users(Users::retrieveByPk($user_id));
        $mes = "Có lỗi xảy ra";
        $type = 0;
        if ($user->active == 0) {
            $user->setActive(1);
            $user->setNew(false);
            if ($user->save()) {
                $type = 1;
                $mes = "Kích hoạt mail thành công";
            } else {
                $type = 0;
                $mes = "Có lỗi xảy ra";
            }
        }
        $arr = array("type" => $type, "mes" => $mes);
        print_r(json_encode($arr));
        exit;
    }

    public function executeEdit() {
        $view = Flywheel_Factory::getView();
        $req = $this->request;
        $user_id = $req->get('id');
        $this->setView('edit');

        $this->user->read($user_id);

        $view->assign('_user', $this->user);
        $view->assign('url_post', $this->public_html . 'admin.php?com=user&act=edit');
        if ($_POST) {
            $user_id = $req->post('user_id');
            $this->user->read($user_id);
            $this->user->setFullname($req->post('fullname'));
            $this->user->setMobile($req->post('mobile'));
            $this->user->setPhone($req->post('phone'));
            $this->user->setDetailAddress($req->post('address'));
            $this->user->setQhAddress($req->post('disctric'));
            $this->user->setTtAddress($req->post('state'));
            $this->user->setGender($req->post('gender'));
            $this->user->setBirthday($req->post('birthday'));
            $this->user->setCmtnd($req->post('cmtnd'));
            $this->user->setBankAccount($req->post('bank_account'));
            $this->user->setBankName($req->post('bank_name'));
            $this->user->setBankBrand($req->post('bank_brand'));
            $this->user->setBankSelf($req->post('bank_self'));
            $this->user->setAccNl($req->post('acc_nl'));
            $this->user->setAccBk($req->post('acc_bk'));
            $this->user->setYahoo($req->post('yahoo'));
            $this->user->setSkype($req->post('skype'));
            $this->user->setQq($req->post('qq'));
            $this->user->setAlioangoang($req->post('alioangoang'));
            $this->user->setAliwangwangAli($req->post('aliwangwang_ali'));

            $this->user->setNew(false);
            $this->user->save();
            $this->request->redirect('?com=user&act=edit&id=' . $user_id);
        }
    }

    //la nhan vien cong ty
    public function executeSetIsStaff() {
        $request = Flywheel_Factory::getRequest();
        $userIds = $request->post('userIds');
        $userIds = trim($userIds, ",");
        if (true == UsersPeer::setIsStaff($userIds)) {
            $type = 1;
            $mes = "Cập nhật thành công";
        } else {
            $type = 0;
            $mes = "Có lỗi xảy ra";
        }
        $arr = array("type" => $type, "mes" => $mes);
        print_r(json_encode($arr));
        exit;
    }

    public function executeResetPass() {
        $request = Flywheel_Factory::getRequest();
        $user_id = $request->post('user_id');
        $str_pass = Users::renderRandomString();
        $user = new Users();
        $user->read($user_id);
        $salt = UsersPeer::generateSalt($user->username);
        $user->setPassword($salt . md5($salt . $str_pass));
        $user->setNew(false);
        $user->save();
        echo $str_pass;
        die;
    }

    public function executeAssignShop() {
        $request = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        $user_id = $request->post('user_id');
        $user_name = $request->post('user_name');
        $this->setView('assign_shop');
        //lay danh sach cac shop da duoc assign 
        $assigned_shops = EditorShopPeer::getAllRecords();
        $arr = array();
        if (!empty($assigned_shops)) {
            foreach ($assigned_shops as $assigned_shop) {
                $arr[] = $assigned_shop['shop_id'];
            }
        }
        $shops = ShopInfoPeer::getShopsYetAssign('id,url,name', $arr);
        $view->assign('shop_list', $shops);
        $view->assign('user_id', $user_id);

        return $this->renderPartial();
    }

    public function executeAddShop() {

        $request = Flywheel_Factory::getRequest();

        $list_shop_id = $_POST['list_id'];
        $user_id = $request->post('user_id');
        $user = new Users();
        $user->read($user_id);

        if (!empty($list_shop_id)) {
            $ids = $list_shop_id;

            if (!empty($ids)) {
                $tmp_shops = ShopInfoPeer::getShopsByIds('id,url,user_id,username', $ids);
                $shops = array();
                if (!empty($tmp_shops)) {
                    foreach ($tmp_shops as $tmp_shop) {
                        $shops[$tmp_shop['id']] = $tmp_shop;
                    }
                }

                foreach ($ids as $shop_id) {
                    $EditorShop = new EditorShop();
                    $EditorShop->read(0);
                    $EditorShop->setUserId($user_id);
                    $EditorShop->setUsername($user->username);
                    $EditorShop->setShopUserId((isset($shops[$shop_id]) ? $shops[$shop_id]['user_id'] : 0));
                    $EditorShop->setShopUsername((isset($shops[$shop_id]) ? $shops[$shop_id]['username'] : ''));
                    $EditorShop->setShopId($shop_id);
                    $EditorShop->setShopUrl((isset($shops[$shop_id]) ? $shops[$shop_id]['url'] : ''));
                    $EditorShop->setNew(true);
                    $EditorShop->save();
                }
            }
        }

        echo 'SUCCESS';
        die;
    }

    public function executeItemManager() {

        $message = MessageUtil::getMessage('user');
        $view = Flywheel_Factory::getView();
        $request = Flywheel_Factory::getRequest();
        $document = Flywheel_Factory::getDocument();
        $document->title = "Quản lý Nhân viên làm nội dung sản phẩm - Admin CP -  " . $document->title;
        $this->setView('ctv_shop');

        $time = $request->get('time', Flywheel_Filter::TYPE_STRING, '');
        $view->assign('time', $time);

        $time_start_search = $request->get('time_start_search', Flywheel_Filter::TYPE_STRING, '');

        $str_time_start_search = $time_start_search;
        $view->assign('time_start_search', $time_start_search);

        $time_end_search = $request->get('time_end_search', Flywheel_Filter::TYPE_STRING, '');
        $str_time_end_search = $time_end_search;
        $view->assign('time_end_search', $time_end_search);

        if ($time != '') {
            $time = explode('/', $time);
            $time = mktime(1, 1, 1, $time[1], $time[0], $time[2]);
        }

        if ($time_start_search != '') {
            $time_start_search = explode('/', $time_start_search);
            $time_start_search = mktime(1, 1, 1, $time_start_search[1], $time_start_search[0], $time_start_search[2]);
        }

        if ($time_end_search != '') {
            $time_end_search = explode('/', $time_end_search);
            $time_end_search = mktime(1, 1, 1, $time_end_search[1], $time_end_search[0], $time_end_search[2]);
        }

        //dan sach cac ban ghi cua editor va user
        $user_shops = EditorShopPeer::getAllRecords();
        $user_ids = array();
        $user_shop_ids = array();
        $user_user_ids = array();

        $shop_ids = array();

        $users = array();
        $shops = array();
        $promotion_amounts = array();
        $items_user = array();
        $tmp_user_translate = Users::listContentStaff();

        if (!empty($tmp_user_translate)) {
            foreach ($tmp_user_translate as $tmp_user_trans) {
                $user_ids[] = $tmp_user_trans['id'];
            }
        }

        if (!empty($user_shops)) {
            foreach ($user_shops as $user_shop) {
                //danh sach cac user da tham gia quan ly noi dung 
                //  if(!in_array($user_shop['user_id'],$user_ids)) 
                //    $user_ids[]= $user_shop['user_id'];       
                //danh sach cac user chu shop
                if (!in_array($user_shop['shop_user_id'], $user_user_ids))
                    $user_user_ids[] = $user_shop['shop_user_id'];

                //danh sach cac shop duoc gan cho tung user
                if (!isset($user_shop_ids[$user_shop['user_id']]))
                    $user_shop_ids[$user_shop['user_id']] = array();

                $user_shop_ids[$user_shop['user_id']][] = $user_shop['shop_id'];

                //danh sach cac shop
                if (!in_array($user_shop['shop_id'], $shop_ids))
                    $shop_ids[] = $user_shop['shop_id'];
            }
            //  $users=Users::getUsersInfo('id,username,fullname,time_join',$user_ids); 
            $users = Users::listContentStaff();
            $tmp_shops = ShopInfoPeer::getShopsByIds('id,url,user_id,username', $shop_ids);
            if (!empty($tmp_shops)) {
                foreach ($tmp_shops as $tmp_shop) {
                    $shops[$tmp_shop['id']] = $tmp_shop;
                }
            }
            //danh sach sp da duet cua cac chu shop
            $amounts = ItemPeer::getApprovedAmountOfUsers($user_user_ids, $time);
            //danh sach sp promotion da duyet cua cac user
            $promotion_amounts = ItemPeer::getApprovedPromotionsOfUsers($user_ids, $time);

            //   echo 'ngay hien tai:'.time().'<::>'.date('d/m/Y').':::ngay truyen:'.$time.':::sau xu ly:'.date('d/m/Y',$time);
            if ($str_time_start_search != '') {
                if (!is_array($time))
                    $time = array();
                $time['start_time'] = $time_start_search;
            }

            if ($str_time_end_search != '') {
                if (!is_array($time))
                    $time = array();
                $time['end_time'] = $time_end_search;
            }

            $item_amounts = ItemPeer::getApprovedItemsOfUsers($user_ids, $time);
            //dem sp dang ban theo shop
            $items_user = ItemPeer::getItemsOfUsers($user_user_ids);
        }
        $view->assign('num_items_user', $items_user);
        $view->assign('amounts', $amounts);
        $view->assign('item_amounts', $item_amounts);
        $view->assign('promotion_amounts', $promotion_amounts);
        $view->assign('users', $users);
        $view->assign('shops', $shops);
        $view->assign('user_shop_ids', $user_shop_ids);
        $view->assign('message', MessageUtil::render($message));
    }

    public function executeRemoveShop() {

        $request = Flywheel_Factory::getRequest();

        $user_id = $request->post('user_id');
        $shop_id = $request->post('shop_id');

        EditorShopPeer::deleteByUserIdAndShopId($user_id, $shop_id);
        //lay lai danh sach shop cua user
        $filter = array('user_id' => $user_id);
        $records = EditorShopPeer::getMulti($filter, 1, 50);
        if (!empty($records)) {
            foreach ($records as $record) {
                echo $record->shop_url . ' (<a user-id="' . $user_id . '" shop-url="' . $record->shop_url . '" shop-id="' . $record->shop_id . '" class="remove_shop" href="javascript:void()">x</a>)<br/>';
            }
        }
        //echo 'SUCCESS';
        die;
    }

    public function executeCrawlerManager() {

        $message = MessageUtil::getMessage('user');
        $view = Flywheel_Factory::getView();
        $request = Flywheel_Factory::getRequest();
        $document = Flywheel_Factory::getDocument();
        $document->title = "Quản lý Nhân viên lấy sản phẩm - Admin CP -  " . $document->title;
        $this->setView('crawler_shop');

        $time = $request->get('time', Flywheel_Filter::TYPE_STRING, '');
        $view->assign('time', $time);

        if ($time != '') {
            $time = explode('/', $time);
            $time = mktime(1, 1, 1, $time[1], $time[0], $time[2]);
        }
        //dan sach cac ban ghi cua crawler va user
        $user_shops = CrawlerShopPeer::getAllRecords();
        $user_ids = array();
        $user_shop_ids = array();
        $user_user_ids = array();

        $shop_ids = array();

        $users = array();
        $shops = array();

        $items_user = array();
        $amounts = array();

        if (!empty($user_shops)) {
            foreach ($user_shops as $user_shop) {
                //danh sach cac user da tham gia lay sp 
                if (!in_array($user_shop['user_id'], $user_ids))
                    $user_ids[] = $user_shop['user_id'];

                //danh sach cac user chu shop
                if (!in_array($user_shop['shop_user_id'], $user_user_ids))
                    $user_user_ids[] = $user_shop['shop_user_id'];

                //danh sach cac shop duoc gan cho tung user
                if (!isset($user_shop_ids[$user_shop['user_id']]))
                    $user_shop_ids[$user_shop['user_id']] = array();

                $user_shop_ids[$user_shop['user_id']][] = $user_shop['shop_id'];

                //danh sach cac shop
                if (!in_array($user_shop['shop_id'], $shop_ids))
                    $shop_ids[] = $user_shop['shop_id'];
            }
            //    $users=Users::getUsersInfo('id,username,fullname,time_join',$user_ids); 

            $tmp_shops = ShopInfoPeer::getShopsByIds('id,url,user_id,username', $shop_ids);
            if (!empty($tmp_shops)) {
                foreach ($tmp_shops as $tmp_shop) {
                    $shops[$tmp_shop['id']] = $tmp_shop;
                }
            }
            //danh sach sp da lay cac chu shop
            $amounts = ItemPeer::getCrawledAmountOfUsers($user_ids, $time);

            //dem sp dang ban theo shop
            $items_user = ItemPeer::getItemsOfUsers($user_user_ids);
        }
        //neu trong thi lay danh sach nhan vien lam noi dung

        if (empty($users)) {
            $users = Users::listCrawlerStaff();
        }
        //sinh addon cho nhan vien

        if (Flywheel_Application_Request::POST == $request->getMethod()) {

            $crawler_username = $request->post('crawler_username', Flywheel_Filter::TYPE_STRING, '');

            require_once APPS_DIR . 'frontend' . DS . 'libraries' . DS . 'generate_addon.php';
            $file = GLOBAL_DIR . 'configs' . DS . 'template_getitem_addon.js';

            $crawler = Users::findByUsername($crawler_username);
            if ($crawler->id > 0)
                genAddon(array('name_addon' => 'alimama_for_staff_' . $crawler_username . '_' . date('d_m_Y'), 'crawler_username' => $crawler_username, 'crawler_user_id' => $crawler->id), $file);
        }
        $view->assign('num_items_user', $items_user);
        $view->assign('amounts', $amounts);

        $view->assign('users', $users);
        $view->assign('shops', $shops);
        $view->assign('user_shop_ids', $user_shop_ids);
        $view->assign('message', MessageUtil::render($message));
    }

    public function executeAssignShopForCrawler() {
        $request = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        $user_id = $request->post('user_id');
        $user_name = $request->post('user_name');
        $this->setView('assign_shop_for_crawler');
        //lay danh sach cac shop da duoc assign 
        $assigned_shops = CrawlerShopPeer::getAllRecords();
        $arr = array();
        if (!empty($assigned_shops)) {
            foreach ($assigned_shops as $assigned_shop) {
                $arr[] = $assigned_shop['shop_id'];
            }
        }
        $shops = ShopInfoPeer::getShopsYetCrawl('id,url,name', $arr);
        $view->assign('shop_list', $shops);
        $view->assign('user_id', $user_id);

        return $this->renderPartial();
    }

    public function executeAddShopForCrawler() {

        $request = Flywheel_Factory::getRequest();

        $list_shop_id = $_POST['list_id'];
        $user_id = $request->post('user_id');
        $user = new Users();
        $user->read($user_id);

        if (!empty($list_shop_id)) {
            $ids = $list_shop_id;

            if (!empty($ids)) {
                $tmp_shops = ShopInfoPeer::getShopsByIds('id,url,user_id,username', $ids);
                $shops = array();
                if (!empty($tmp_shops)) {
                    foreach ($tmp_shops as $tmp_shop) {
                        $shops[$tmp_shop['id']] = $tmp_shop;
                    }
                }

                foreach ($ids as $shop_id) {
                    $CrawlerShop = new CrawlerShop();
                    $CrawlerShop->read(0);
                    $CrawlerShop->setUserId($user_id);
                    $CrawlerShop->setUsername($user->username);
                    $CrawlerShop->setShopUserId((isset($shops[$shop_id]) ? $shops[$shop_id]['user_id'] : 0));
                    $CrawlerShop->setShopUsername((isset($shops[$shop_id]) ? $shops[$shop_id]['username'] : ''));
                    $CrawlerShop->setShopId($shop_id);
                    $CrawlerShop->setShopUrl((isset($shops[$shop_id]) ? $shops[$shop_id]['url'] : ''));
                    $CrawlerShop->setNew(true);
                    $CrawlerShop->save();
                }
            }
        }

        echo 'SUCCESS';
        die;
    }

    public function executeRemoveShopForCrawler() {

        $request = Flywheel_Factory::getRequest();

        $user_id = $request->post('user_id');
        $shop_id = $request->post('shop_id');

        CrawlerShopPeer::deleteByUserIdAndShopId($user_id, $shop_id);
        //lay lai danh sach shop cua user
        $filter = array('user_id' => $user_id);
        $records = CrawlerShopPeer::getMulti($filter, 1, 50);
        if (!empty($records)) {
            foreach ($records as $record) {
                echo $record->shop_url . ' (<a user-id="' . $user_id . '" shop-url="' . $record->shop_url . '" shop-id="' . $record->shop_id . '" class="remove_shop" href="javascript:void()">x</a>)<br/>';
            }
        }
        //echo 'SUCCESS';
        die;
    }

    public function executeMakePromotion() {
        $request = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        $user_id = $request->post('user_id');
        $user_name = $request->post('user_name');

        //lay danh sach cac shop da duoc assign 
        $user = new Users(Users::retrieveByPk($user_id));
        if ($user->is_make_promotion == 1) {
            $user->setIsMakePromotion(0);
        }else
            $user->setIsMakePromotion(1);
        $user->setNew(false);
        $user->save();

        echo 'OK';
        exit;
    }

    public function executeMakeTranslateItem() {
        $request = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        $user_id = $request->post('user_id');
        $user_name = $request->post('user_name');

        //lay danh sach cac shop da duoc assign 
        $user = new Users(Users::retrieveByPk($user_id));
        if ($user->is_translate_item == 1) {
            $user->setIsTranslateItem(0);
        }else
            $user->setIsTranslateItem(1);
        $user->setNew(false);
        $user->save();

        echo 'OK';
        exit;
    }

    public function executeMakeCrawler() {
        $request = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        $user_id = $request->post('user_id');
        $user_name = $request->post('user_name');

        //lay danh sach cac shop da duoc assign 
        $user = new Users(Users::retrieveByPk($user_id));
        if ($user->is_crawler == 1) {
            $user->setIsCrawler(0);
        }else
            $user->setIsCrawler(1);
        $user->setNew(false);
        $user->save();

        echo 'OK';
        exit;
    }

    public function executeMakeShopMarketer() {
        $request = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        $user_id = $request->post('user_id');
        $user_name = $request->post('user_name');

        //lay danh sach cac shop da duoc assign 
        $user = new Users(Users::retrieveByPk($user_id));
        if ($user->is_shop_marketer == 1) {
            $user->setIsShopMarketer(0);
        }else
            $user->setIsShopMarketer(1);
        $user->setNew(false);
        $user->save();

        echo 'OK';
        exit;
    }

    public function executeUserType() {
        $request = Flywheel_Factory::getRequest();
        $user_id = $request->post('uid');
        $type = $request->post('user_type');
        $is_staff = $request->post('is_staff');

        $user = new Users(Users::retrieveByPk($user_id));
        if (!$user->id) {
            $result = array("error" => 0, "mes" => 'User không tồn tại');
            print(json_encode($result));
            exit;
        }
        if ($type == Users::STAFFER) {
            $user->setType($type);
            $user->setIsStaff(1);
        } else {
            $user->setType($type);
            $user->setIsStaff(0);
        }

        $user->setNew(false);

        if ($user->save()) {
            $success = 1;
            $mes = "Thay đổi kiểu user thành công";
        } else {
            $success = 0;
            $mes = "Có lỗi xảy ra";
        }
        $result = array("error" => $success, "mes" => $mes, "type" => $type);
        print(json_encode($result));
        exit;
    }

    public function executeTest() {
        var_dump(Users::listQqAli('9,6,7'));
        die;
    }

    public function executeMarketerShop() {

        $message = MessageUtil::getMessage('user');
        $view = Flywheel_Factory::getView();
        $request = Flywheel_Factory::getRequest();
        $document = Flywheel_Factory::getDocument();
        $document->title = "Quản lý Nhân viên lấy sản phẩm - Admin CP -  " . $document->title;
        $this->setView('marketer_shop');

        $time = $request->get('time', Flywheel_Filter::TYPE_STRING, '');
        $view->assign('time', $time);

        if ($time != '') {
            $time = explode('/', $time);
            $time = mktime(1, 1, 1, $time[1], $time[0], $time[2]);
        }
        //danh sach cac ban ghi cua crawler va user
        $user_shops = MarketerShopPeer::getAllRecords();
        $user_ids = array();
        $user_shop_ids = array();
        $user_user_ids = array();

        $shop_ids = array();

        $users = array();
        $shops = array();

        $items_user = array();
        $amounts = array();

        if (!empty($user_shops)) {
            foreach ($user_shops as $user_shop) {
                //danh sach cac user da tham gia marketing
                if (!in_array($user_shop['user_id'], $user_ids))
                    $user_ids[] = $user_shop['user_id'];

                //danh sach cac user chu shop
                if (!in_array($user_shop['shop_user_id'], $user_user_ids))
                    $user_user_ids[] = $user_shop['shop_user_id'];

                //danh sach cac shop duoc gan cho tung user
                if (!isset($user_shop_ids[$user_shop['user_id']]))
                    $user_shop_ids[$user_shop['user_id']] = array();

                $user_shop_ids[$user_shop['user_id']][] = $user_shop['shop_id'];

                //danh sach cac shop
                if (!in_array($user_shop['shop_id'], $shop_ids))
                    $shop_ids[] = $user_shop['shop_id'];
            }

            $tmp_shops = ShopInfoPeer::getShopsByIds('id,url,user_id,username', $shop_ids);
            if (!empty($tmp_shops)) {
                foreach ($tmp_shops as $tmp_shop) {
                    $shops[$tmp_shop['id']] = $tmp_shop;
                }
            }
            //danh sach sp da lay cac chu shop
            //   $amounts= ItemPeer::getCrawledAmountOfUsers($user_ids,$time);
            //dem sp dang ban theo shop
            //   $items_user=ItemPeer::getItemsOfUsers($user_user_ids);
        }
        //neu trong thi lay danh sach nhan vien lam noi dung

        if (empty($users)) {
            $users = Users::listMarketerStaff();
        }
        // $view->assign('num_items_user', $items_user);                        
        //  $view->assign('amounts', $amounts);

        $view->assign('users', $users);
        $view->assign('shops', $shops);
        $view->assign('user_shop_ids', $user_shop_ids);
        $view->assign('message', MessageUtil::render($message));
    }

    public function executeAddShopForMarketer() {

        $request = Flywheel_Factory::getRequest();

        $list_shop_id = $_POST['list_id'];
        $user_id = $request->post('user_id');
        $user = new Users();
        $user->read($user_id);

        if (!empty($list_shop_id)) {
            $ids = $list_shop_id;

            if (!empty($ids)) {
                $tmp_shops = ShopInfoPeer::getShopsByIds('id,url,user_id,username', $ids);
                $shops = array();
                if (!empty($tmp_shops)) {
                    foreach ($tmp_shops as $tmp_shop) {
                        $shops[$tmp_shop['id']] = $tmp_shop;
                    }
                }

                foreach ($ids as $shop_id) {
                    $MarketerShop = new MarketerShop();
                    $MarketerShop->read(0);
                    $MarketerShop->setUserId($user_id);
                    $MarketerShop->setUsername($user->username);
                    $MarketerShop->setShopUserId((isset($shops[$shop_id]) ? $shops[$shop_id]['user_id'] : 0));
                    $MarketerShop->setShopUsername((isset($shops[$shop_id]) ? $shops[$shop_id]['username'] : ''));
                    $MarketerShop->setShopId($shop_id);
                    $MarketerShop->setShopUrl((isset($shops[$shop_id]) ? $shops[$shop_id]['url'] : ''));
                    $MarketerShop->setNew(true);
                    $MarketerShop->save();
                }
            }
        }

        echo 'SUCCESS';
        die;
    }

    public function executeRemoveShopForMarketer() {

        $request = Flywheel_Factory::getRequest();

        $user_id = $request->post('user_id');
        $shop_id = $request->post('shop_id');

        MarketerShopPeer::deleteByUserIdAndShopId($user_id, $shop_id);
        //lay lai danh sach shop cua user
        $filter = array('user_id' => $user_id);
        $records = MarketerShopPeer::getMulti($filter, 1, 50);
        if (!empty($records)) {
            foreach ($records as $record) {
                echo $record->shop_url . ' (<a user-id="' . $user_id . '" shop-url="' . $record->shop_url . '" shop-id="' . $record->shop_id . '" class="remove_shop" href="javascript:void()">x</a>)<br/>';
            }
        }
        //echo 'SUCCESS';
        die;
    }

    public function executeOrderCode() {
        $request = Flywheel_Factory::getRequest();
        $user_id = $request->post('user_id');
        $code = $request->post('code');

        $user = new Users();
        $checkcode = $user->selectList("code='" . $code . "' and id !=" . $user_id);
        if ($checkcode) {
            echo json_encode(array('error' => '1', 'msg' => 'EXIST'));
            exit;
        }
        $user->read($user_id);
        $user->setCode($code);
        $user->setNew(false);
        $rs = $user->save();
        if ($rs) {
            echo json_encode(array('error' => '1', 'msg' => 'DONE'));
            exit;
        } else {
            echo json_encode(array('error' => '1', 'msg' => 'ERROR'));
            exit;
        }

        die;
    }

    public function executeEditorPayment() {

        $message = MessageUtil::getMessage('user');
        $view = Flywheel_Factory::getView();
        $request = Flywheel_Factory::getRequest();
        $document = Flywheel_Factory::getDocument();
        $document->title = "Quản lý thanh toán nhân viên làm nội dung - Admin CP -  " . $document->title;
        $this->setView('editor_payment');

        $month = $request->get('month', Flywheel_Filter::TYPE_INT, date('m'));
        $view->assign('month', $month);

        $year = $request->get('year', Flywheel_Filter::TYPE_INT, date('Y'));
        $view->assign('year', $year);

        //danh sach cac ban ghi cua editor va user

        $users = array();
        $tmp_user_translate = Users::listContentStaff();

        $users_last_time = array();
        if (!empty($tmp_user_translate)) {
            foreach ($tmp_user_translate as $tmp_user_trans) {
                $users[$tmp_user_trans['id']] = $tmp_user_trans;

                //thoi gian chot lan cuoi cua tung user
                $lasttime = EditorPaymentPeer::getLastPayment($tmp_user_trans['id']);
                if (intval($lasttime) == 0)
                    $lasttime = $tmp_user_trans['time_join'];

                $users_last_time[$tmp_user_trans['id']] = $lasttime;
            }
        }
        $view->assign('users', $users);

        $filter = array('month' => $month, 'year' => $year);
        //danh sach cac ket qua da chot
        $records = EditorPaymentPeer::getPayments($filter);

        $view->assign('users_last_time', $users_last_time);
        $view->assign('records', $records);

        $view->assign('message', MessageUtil::render($message));
    }

    public function executeAddAllPayment() {

        $request = Flywheel_Factory::getRequest();

        EditorPaymentPeer::addPaymentForAll();
        echo 'SUCCESS';
        die;
    }

    public function executeAddPayment() {

        $request = Flywheel_Factory::getRequest();

        $start_date = $request->post('start_date', Flywheel_Filter::TYPE_STRING, '');
        $end_date = $request->post('end_date', Flywheel_Filter::TYPE_STRING, '');
        $user_id = $request->post('user_id', Flywheel_Filter::TYPE_INT, 0);

        $start_date = explode('/', $start_date);
        $start_date = mktime(1, 1, 1, $start_date[1], $start_date[0], $start_date[2]);

        $end_date = explode('/', $end_date);
        $end_date = mktime(1, 1, 1, $end_date[1], $end_date[0], $end_date[2]);

        $editorPayment = new EditorPayment();
        $item = new Item();

        $user = Users::getUserInfo('id,username', $user_id);

        //dem so san pham tu lan chot cuoi den thoi diem $time   
        $num = $item->getProcessedItemsByUserId($user_id, $start_date, $end_date);

        //add so ban ghi chot
        $data = array(
            'id' => 0,
            'user_id' => $user['id'],
            'user_name' => $user['username'],
            'start_time' => $start_date,
            'end_time' => $end_date,
            'amount' => $num,
            'status' => 0,
            'created_by' => 0,
            'created_at' => time(),
        );
        $editorPayment->insert($data);

        echo 'SUCCESS';
        die;
    }

    public function executePaymentPaid() {
        $request = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        $id = $request->post('id');
        //     $user_name    = $request->post('user_name');
        //lay danh sach cac shop da duoc assign 
        $editorPayment = new EditorPayment();
        $editorPayment->read($id);
        if ($editorPayment->status == 1) {
            $editorPayment->setStatus(0);
        }else
            $editorPayment->setStatus(1);
        $editorPayment->setNew(false);
        $editorPayment->save();

        echo 'OK';
        exit;
    }

    public function executeUpdatePayment() {

        $request = Flywheel_Factory::getRequest();

        $id = $request->post('id', Flywheel_Filter::TYPE_INT, 0);

        $editorPayment = new EditorPayment();
        $editorPayment->read($id);
        $item = new Item();

        $user = Users::getUserInfo('id,username', $user_id);

        //dem so san pham tu lan chot cuoi den thoi diem $time   
        $num = $item->getProcessedItemsByUserId($editorPayment->user_id, $editorPayment->start_time, $editorPayment->end_time);

        if ($editorPayment->status == 0) {
            $editorPayment->setAmount($num);
            $editorPayment->setNew(false);
            $editorPayment->save();
        }

        echo 'SUCCESS';
        die;
    }

    /**
     * Synchronous GiangMinh user to Alimama
     * Password for all default with 'abc12345'. encode '5a719d81b9bd73141c33a9920baa34159e3c2de77130da4cd5fadba28d5d422c11e9458e'
     */
    public function executeSynGmUser() {
        
        // Giangminh users
        $con        = Flywheel_DB::getConnection('giangminh');
        $con->query("SET NAMES 'latin1'");
        $table      = 'user';
        $gm_users   = $con->select($table, '*', '`level` < 2', null, null, 'user_name');
        // Get zone
        $gm_zone    = $con->select('zone_general', '*', '', null, null, 'id');
        //var_dump($gm_zone); die();
        
        // Alimama users
        $con_alimama = Flywheel_DB::getConnection();
        $ali_users = $con_alimama->select(Users::TABLE, '`id`, `username`, `from`', '', null, null, 'username');
        if(!$ali_users) {
            $ali_users = array(array('id' => 0, 'username' => '', 'from' => 0));
        }
        
        // Filter user GM in Ali
        $gm_exist_ali = array();
        $string_insert = '';
        if(!empty($gm_users)) {
            foreach($gm_users as $k => $v) {
                if(array_key_exists($k, $ali_users) & @$ali_users[$k]['from'] == 0) {
                    $gm_exist_ali[] = $ali_users[$k];
                    unset($gm_users[$k]);
                    continue;
                }
                
                // Add GM user to datas
                $phone = preg_split('/,/', $v['phone']);
                $data = array (
                        'username'      => $v['user_name'],
                        'password'      => '5a719d81b9bd73141c33a9920baa34159e3c2de77130da4cd5fadba28d5d422c11e9458e',
                        'type'          => 0,
                        'code'          => !empty($v['code']) ? $v['code'] : '',
                        'fullname'      => !empty($v['full_name']) ? $v['full_name'] : '',
                        'gender'        => ($v['gender'] == 0 ? 1 : 0), // Gender Ali against with GM
                        'birthday'      => !empty($v['birth_date']) ? $v['birth_date'] : 0,
                        'phone'         => array_key_exists(1, $phone) ? $phone[1] : '',
                        'mobile'        => $phone[0],
                        'active'        => 1,
                        'avatar'        => '',
                        'status'        => 0,
                        'status_time'   => 0,
                        'email'         => !empty($v['email']) ? $v['email'] :'',
                        'detail_address'=> !empty($v['address']) ? $v['address'] : '',
                        'qh_id'         => 0,
                        'qh_address'    => empty($gm_zone[$v['district']]['name']) ? '' : $gm_zone[$v['district']]['name'],
                        'tt_id'         => 0,
                        'tt_address'    => empty($gm_zone[$v['province']]['name']) ? '' : $gm_zone[$v['province']]['name'],
                        'time_join'     => !empty($v['register_date']) ? $v['register_date'] : '',
                        'lock_status'   => 0,
                        'lock_time_begin'   => 0,
                        'lock_time_end'     => 0,
                        'lock_reason'       => '',
                        'bank_account'      => '',
                        'bank_self'         => '',
                        'bank_name'         => '',
                        'bank_brand'        => '',
                        'cmtnd'             => '',
                        'acc_nl'            => '',
                        'acc_bk'            => '',
                        'yahoo'             => !empty($v['ym']) ? $v['ym'] : '', 
                        'skype'             => !empty($v['sky']) ? $v['sky'] : '',
                        'qq'                => '', 
                        'alioangoang'       => '',
                        'aliwangwang_ali'   => '',
                        'rate'              => 0,
                        'ip'                => '',
                        'is_auto_create'    => 0,
                        'is_make_promotion' => 0,
                        'is_translate_item' => 0,
                        'is_crawler'        => 0,
                        'is_shop_marketer'  => 0,
                        'is_staff'          => 0,
                        'order_code'        => !empty($v['code']) ? $v['code'] : '',
                        'is_edit_order_code'=> '',
                        'from'              => $v['level'] == 0 ? 1:2, // 1-> GM User, 2->GM agent
                        'site_id'           => $v['id']
                     );
                
                // Insert GM users to Ali database
                if($ali_users[$k]['from'] == 0) {
                    $db->insert(Users::TABLE, $data);
                } else {
                    $string_insert .= $v['user_name'] . ', ';
                    $con_alimama->update(Users::TABLE, $data, "`id`={$ali_users[$k]['id']}");
                }
                // var_dump($data);
            }
        }
        echo 'User inserted: ' . $string_insert;
        //var_dump($gm_exist_ali);
                
        die();
    }
    
    /**
     * Synchronous Giangminh user transactions to Alimama
     * by username from GM
     */
    public function executeSynGmTransaction() {
        
        // Get GM transactions
        $gm_conn    = Flywheel_DB::getConnection('giangminh');
        // Select by username "hauthanhn88"
        $gm_user = 'hauthanhn88'; // aceplanet
        $gm_trans   = $gm_conn->select('cms_transactions', '*', "`user_name`='{$gm_user}' AND `method`=2");
        //var_dump($gm_trans); die();
        
        // Get Ali Transactions
        $ali_acc_conn   = Flywheel_DB::getConnection('accountant');
        // Add transaction for username "hauthanh88" from account of alimama with id "5054"
        $ali_user_id = '5054'; // 759
        $hauthanh_trans = $ali_acc_conn->select(Transaction::TABLE, '*', "`user_name`='hauthanh88' AND `user_id`={$ali_user_id}");
        // var_dump($hauthanh_trans); die();
        
        $data = array();
        if(!empty($gm_trans)) {
            foreach($gm_trans as $k => $v) {
                // Init update variable
                $is_new = true;
                // Check exist 
                if(!empty($hauthanh_trans)) {
                    foreach($hauthanh_trans as $k2 => $v2) {
                        // The same time create and money charge
                        if($v['time_create'] == $v2['time_created'] & $v['money_nap'] == $v2['money']) {
                            // Init data update
                            $data = array(
                                'time_created'      => intval($v['time_create']) + rand(28800, 79200),
                                'time_approve'      => !empty($v['time_approve']) ? $v['time_approve'] : 0,
                                'time_tmp_approve'  => !empty($v['time_approve_view']) ? $v['time_approve_view'] : 0
                            );
                            $ali_acc_conn->update(Transaction::TABLE, $data, "`id`={$v2['id']}");
                            $is_new = false;
                            break;
                        } // End if
                    } // End for
                } // End if
                if(!$is_new) {
                    continue;
                }
                
                // Init data insert
                $data = array(
                        'user_id'       => $ali_user_id,
                        'user_name'     => 'hauthanh88',
                        'full_name'     => 'Nguyen Thanh Hau',
                        'money'         => $v['money_nap'],
                        'transaction_type'=> $v['payment_account_id'] == 4 ? 1: 2, // 1-> Tiền mặt, 2-> Chuyển khoản
                        'time_created'  => $v['time_create'],
                        'created_by'    => $ali_user_id,
                        'service_type'  => 1,
                        'transaction_code'  => $v['payment_account_id'] == 4 ? "" : $v['payment_acc'],
                        'bank'              => $v['payment_account_id'] == 4 ? "" : $v['payment_bank'],
                        'bank_transaction'  => $v['payment_account_id'] == 4 ? "" : $v['payment_transaction'],
                        'content'           => $v['brief']
                    );
                $ali_acc_conn->insert(Transaction::TABLE, $data);
            }
        }
        
        // Finish convert transaction
        echo 'Finished'; die();
        
    }
    
    public function executeInsertZone() {
        
        // Giangminh users
        $con        = Flywheel_DB::getConnection('giangminh');
        $con->query("SET NAMES latin1");
        // Get zone
        $gm_zone    = $con->select('zone', '*', '', null, null, 'id');
        // var_dump($gm_zone); die();
        
        $datas = array();
        foreach($gm_zone as $k => $v) {
            $datas[] = array(
                'id' => $v['id'],
                'parent_id' => $v['parent_id'],
                'name'  => $v['name'],
                'brief' => $v['brief'],
                'region' => $v['region']
            );
        }
        $con->query("SET NAMES utf-8");
        $con->insertMulti('zone_general', $datas);
        var_dump($gm_zone); die();
    }
}