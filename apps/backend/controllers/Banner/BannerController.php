<?php

require_once CONTROLLERS_DIR . 'BackendController.php';
class BannerController extends BackendController {
    
    public $img = "";
    public function beforeExecute() {
        parent::beforeExecute();
    }
    
    /**
     * Load items
     */
    public function executeDefault() {
        
        $view = Flywheel_Factory::getView();
        $user = Flywheel_Authen::getInstance()->getUser();
        $req = Flywheel_Factory::getRequest();
        $itemObj = new Banner();
        
        $page = $req->get('p', Flywheel_Filter::TYPE_INT, 1);
        $pageSize = 20;
        $filter = array();
        
        $pagination = new Flywheel_Pagination();
        $pagination->pageSize = $pageSize;
        $pagination->currentPage = $page;
        $pagination->template = TEMPLATE_DIR . 'modules' . DS . 'pagination';
        $pagination->total = $itemObj->countItem();
        $pagination->urlStructure = "?com=item&page=<page>";
        $pagination->initialize();
        $view->assign('pagination', $pagination->render());
        
        // Load list items
        $limit = $pageSize * ($page - 1) . "," . $pageSize;
        $_itemList = $itemObj->selectList("1=1", "`pos` DESC", $limit, "id");
        
        $this->document->title = $this->document->title . ": Quản lý banner";
        // Assign
        $view->assign('_itemList', $_itemList);
    }
    
    /**
     * Edit, add item
     */
    public function executeEdit() {
        $view       = Flywheel_Factory::getView();
        $request    = Flywheel_Factory::getRequest();
        $result     = array('msg' => '', 'status' => true);
        
        // Get item detail
        $itemid     = $request->get('id', Flywheel_Filter::TYPE_INT, 0);
        
        // Check post form (save item)
        if($request->getMethod()  == Flywheel_Application_Request::POST) {
            $result = $this->_save();
        }
        
        $itemObj    = new Banner();
        $_item      = $itemObj->retrieveByKey($itemid);
        
        // Assign html
        $view->assign('_item', $_item);
        $view->assign('_result', $result);
        $this->setView('form');
        $this->document->title = $this->document->title . ": " . ($itemid != 0 ? 'Cập nhật' : 'Thêm mới') . " banner";
    }
    
    /**
     * Save data
     */
    public function _save() {
        // Validate item info
        $result = array('msg' => '', 'status' => true);
        
        $req = Flywheel_Factory::getRequest();
        $id = $req->get('id', Flywheel_Filter::TYPE_INT, 0);
        
        $_itemObj = new Banner();
        $_itemObj = $_itemObj->retrieveByKey($id);
        if($id == 0) {
            $_itemObj->setNew(true);
        }
        $_itemObj->setTitle($req->post('title'));
        $_itemObj->setLink($req->post('link'));
        $_itemObj->setPos($req->post('pos', Flywheel_Filter::TYPE_INT, 0));
        $_itemObj->setStatus($req->post('status', Flywheel_Filter::TYPE_STRING, "") != "" ? 1 : 0);
        $_itemObj->setCreatedTime(time());
        
        if($req->post('title') == "") {
            $result['status'] = false;
            $result['msg']  = 'Tiêu đề sản phẩm rỗng!';
        }

        if($result['status']) {
            $config = array(
                'filter_type' => '.jpg, .gif, .bmp, .png',
                'max_size' => 512, //512 kb
                'encrypt_file_name' => false,
                'ansi_name' => false,
                'remove_white_space' => true,
                'new_name' => Flywheel_Inflector::slug($_itemObj->title)
            );
            // Upload banner
            $upload['status'] = true;
            $upload = $this->__upload('img', STATIC_DIR . 'banners' . DS, $config);
            if(!empty($this->img)) {
                $_itemObj->setImg($this->img);
            }

            if($upload['status']) {
                if(!$_itemObj->save()) {
                    $result['status'] = false;
                    $result['msg'] = 'Lỗi hệ thống! Liên hệ với ban quản trị.';
                } else {
                    $result['msg'] = ($id != 0 ? "Cập nhật" : "Thêm mới") . ' thành công.';
                }
            } else {
                $result = $upload;
            }
        }
        
        return $result;
    }
    
    public function executeDel() {
        $req = Flywheel_Factory::getRequest();
        $id = $req->get('id', Flywheel_Filter::TYPE_INT, 0);
        
        $item = new Item();
        $item->read($id);
        $item->delete();
        
        $req->redirect("?com=item&act=default");
        
    }
    
    /**
     *
     * @param type $file_name Name of post file controller
     * @param type $path Fhysical directory
     * @param type $config Config param upload
     * @return type STRING
     */
    public function __upload($file_name = null, $path = "", $config = null) {
        $result = array('msg' => "", "status" => true);
        if($_FILES[$file_name]['name'] == "") {
            return $result;
        }
        // Config file upload
        if($config == null) {
            $config = array(
                'filter_type' => '.jpg, .gif, .bmp, .png',
                'max_size' => 512, //512 kb
                'encrypt_file_name' => false,
                'ansi_name' => false,
                'remove_white_space' => true,
                'overwrite' => true
            );
        }
        
        $fileHandler = new Flywheel_File_Handler($path, $file_name, $config);
        if ($fileHandler->upload($file_name, 0777)) {
            $data = $fileHandler->getData();
            $result['msg'] = "Upload file thành công!";
            $this->img = $data['file_name'];
        } else {
            $err = $fileHandler->hasError();
            // Error upload file handle
            $result['msg'] = implode(', ', $err);
            $result['status'] = false;
        }
        return $result;
    }

}
?>
