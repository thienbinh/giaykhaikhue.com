<?php
require_once CONTROLLERS_DIR . 'BackendController.php';
class AccountController extends BackendController {
    public $tran = '';
    public function beforeExecute() {
        parent::beforeExecute();
            $this->view       = Flywheel_Factory::getView();
           
            $this->request    = Flywheel_Factory::getRequest();
            $this->user       = Flywheel_Authen::getInstance()->getUser();
            $this->document   = Flywheel_Factory::getDocument();
            $this->acc      = new Account();
            $this->url_path = $this->document->getPublicPath();
            $d = Flywheel_Factory::getDocument();
            $d->addJsCode("var base_url = ".$this->url_path);
            $this->view->assign('url_path',$this->url_path);
    }

    public function executeDefault() {
      
        $accounts = array();
        //$request = $this->request;
        $pageSize    = 20; // 20 content per page
        $currentPage = $this->request->get('page', Flywheel_Filter::TYPE_INT, 1); 
       
        $filter = array();
        $user_name = $this->request->get('user_name');
        $money_sort = $this->request->get('order');
       
        if(null != $user_name) {
            $filter['user_name'] = $user_name;            
        }
        $con = Account::getCondition($filter);
      //  $con.=' user_id not in('.Transaction::USER_NOT_ACCEPT.')';
        if($money_sort){
            $order = 'money '.$money_sort;
        }else{
            $order = 'id DESC';
        }    
        
        $limit = ''.(($currentPage-1)*$pageSize).','.$pageSize.'';
        $accounts = $this->acc->selectList($con,$order,$limit);
        
       
        $total = $this->acc->count($con);

        $this->view->assign('accounts',$accounts);
        $list_user_ids=array();
        $list_orders=array();
        if(!empty($accounts))
        {
            foreach($accounts as $acc)    
            {
                $list_user_ids[]=$acc['user_id'];
            }
        }
        $list_orders=Order::getOrderDn($list_user_ids);
            
        $this->view->assign('list_orders',$list_orders);
        
        // Set pagination   
        $pagination = new Flywheel_Pagination();
        $urlStructure = '';
        
        $pagination->urlStructure    = '?com=account&page=<page>' 
                            .((null != $urlStructure)? '&' .$urlStructure : '');
        
        
        $pagination->pageSize      = $pageSize;
        $pagination->currentPage= $currentPage;        
        $pagination->template    = TEMPLATE_DIR .'modules'. DS.'pagination';        
        $pagination->total = $total;
     
        $pagination->initialize();
        $this->view->assign('pagination', $pagination->render()) ;
        $this->view->assign('page', $currentPage);  
        
    }
    public function executeCheckList() {
        $this->setView('check_list');   
        $this->setLayout('default');   
        $currentPage = $this->request->get('page', Flywheel_Filter::TYPE_INT, 1);
        
        $accounts = array();
        $pageSize=700;
        //$request = $this->request;
     
        $filter = array();
        $user_name = $this->request->get('user_name');
            
        if(null != $user_name) {
            $filter['user_name'] = $user_name;            
        }
        $con = Account::getCondition($filter);
        $order = 'id DESC';       
        
        $limit = '0,700';
        $accounts = $this->acc->selectList($con,$order,$limit);
        $total = $this->acc->count($con);
        $this->view->assign('accounts', $accounts);              
        
         // Set pagination   
        $pagination = new Flywheel_Pagination();
        $urlStructure = '';
        
        $pagination->urlStructure    = '?com=account&act=check_list&page=<page>' 
                            .((null != $urlStructure)? '&' .$urlStructure : '');
        
        
        $pagination->pageSize      = $pageSize;
        $pagination->currentPage= $currentPage;        
        $pagination->template    = TEMPLATE_DIR .'modules'. DS.'pagination';        
        $pagination->total = $total;
     
        $pagination->initialize();
        $this->view->assign('pagination', $pagination->render()) ;
        $this->view->assign('page', $currentPage);  
    }
    /*delete**/
    public function executeDelete(){
       $id =$this->request->post('id');
        if(!$id)return false;
        $array_update = array(
            'status'    => 4,//mac dinh status 4 là delete
        ); 
        $data = $this->tran->update($array_update,'id='.$id);
        if($data){
            echo json_encode(array('error'=>'1','msg'=>'done'));
            die;
        }else{
            echo json_encode(array('error'=>'0','msg'=>'done'));
            die;
        }
        
    }
  /*  public function executeCreateDebit(){
        $debit_money =$this->request->post('debit_money');
        $debit_username =$this->request->post('debit_username');
        $debit_log =$this->request->post('debit_log');
        //lay thong tin user theo username
        $user=Users::getUserByUsername($debit_username);
        
        //tao giao dich doi no
        $tran = new Transaction();
        $tran->setUserId(intval($user['id']));
        $tran->setUserName($user['username']);
        $tran->setFullName($user['fullname']);
        $tran->setMoney(intval(str_replace('.', '', $debit_money)));
        $tran->setTransactionType(0);
      
        $tran->setIsAddMoney(-1);
        $tran->setServiceType(Transaction::SERVICES_REPAY);
        $tran->setBankTransaction('');
        
        $tran->setTimeCreated(time());
        $tran->setStatus(0);
        $tran->setContent($debit_log);
        $tran->setNew(true);

        if ($id = $tran->save()) {

            $transaction_code = Transaction::getTransactionCode(Transaction::SERVICES_REPAY, $id, '') ;
            $update = array('transaction_code' => $transaction_code);
            $condition = 'id = ' . $id;
            $tran->update($update, $condition);
            
            echo json_encode(array('error' => 1, 'msg' => 'DONE','tran_id'=>$id));
        } else {
            echo json_encode(array('error' => 0, 'msg' => 'ERROR'));
        }
        die;
        
    } */
}

?>
