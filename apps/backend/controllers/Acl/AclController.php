<?php

require_once CONTROLLERS_DIR . 'BackendController.php';

class AclController extends BackendController {

    public function beforeExecute() {
        parent::beforeExecute();
    }

    public function executeDefault() {
        $view = Flywheel_Factory::getView();
        $user = Flywheel_Authen::getInstance()->getUser();
        
        $data = array();
        if($user->username != 'admin') {
            
            $obj = new AdminMenu;
            $data = $obj->getUserMenu();
            $view->assign('_menu', $data);
        }
        
        $this->setLayout('home');
    }
}