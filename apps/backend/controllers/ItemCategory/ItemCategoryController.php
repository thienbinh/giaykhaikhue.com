<?php

require_once CONTROLLERS_DIR . 'BackendController.php';

class ItemCategoryController extends BackendController {

    public function beforeExecute() {
        parent::beforeExecute();
    }

    /**
     * Load items
     */
    public function executeDefault() {
        $view = Flywheel_Factory::getView();
        $user = Flywheel_Authen::getInstance()->getUser();
        $req = Flywheel_Factory::getRequest();
        $cateObj = new ItemCategory();

        $page = $req->get('page', Flywheel_Filter::TYPE_INT, 1);
        $pageSize = 20;
        $filter = array();

        $pagination = new Flywheel_Pagination();
        $pagination->pageSize = $pageSize;
        $pagination->currentPage = $page;
        $pagination->template = TEMPLATE_DIR . 'modules' . DS . 'pagination';
        $pagination->total = $cateObj->countCate();
        $pagination->urlStructure = "?com=item_category&page=<page>";
        $pagination->initialize();
        $view->assign('pagination', $pagination->render());

        // Load list categories
        $limit = $pageSize * ($page - 1) . "," . $pageSize;
        $_cateList = $cateObj->selectList("1=1", "`id` DESC", $limit, "id");

        $_allCate = $cateObj->selectList("1=1", "`id` DESC", null, "id");

        // Assign
        $view->assign('_cateList', $_cateList);
        $view->assign('_allCate', $_allCate);
        $view->assign('_perPage', $pageSize);
        $view->assign('_page', $page);
    }

    /**
     * Edit, add item
     */
    public function executeEdit() {
        $view = Flywheel_Factory::getView();
        $request = Flywheel_Factory::getRequest();
        $result = array('msg' => '', 'status' => true);

        // Get item detail
        $cateid = $request->get('id', Flywheel_Filter::TYPE_INT, 0);

        // Check post form (save item)
        if ($request->getMethod() == Flywheel_Application_Request::POST) {
            $result = $this->_save();
        }

        $cateObj = new ItemCategory();
        $_cate = $cateObj->retrieveByKey($cateid);

        // Load list categories
        $categoryObj = new ItemCategory();
        $_cateList = $categoryObj->selectList("`id` != {$cateid}", "`pid`, `pos` ASC", null, "id");

        $_cateSelect = array(' - Chọn danh mục' => 0);
        if (!empty($_cateList)) {
            // Filter childs to parent
            foreach ($_cateList as $k => $v) {
                foreach ($_cateList as $k_child => $v_child) {
                    if ($v_child['pid'] == $k) {
                        $_cateList[$k]['childs'][] = $v_child;
                        unset($_cateList[$k_child]);
                    }
                }
            }
            // Filter to array key
            foreach ($_cateList as $k => $v) {
                $_cateSelect[$v['title']] = $v['id'];
                if (array_key_exists('childs', $v)) {
                    foreach ($v['childs'] as $k_child => $v_child) {
                        $_cateSelect['.....' . $v_child['title']] = $v_child['id'];
                    }
                }
            }
        }

        // Assign html
        $view->assign('_item', $_cate);
        $view->assign('_cateSelect', render_form_select('cate', $_cateSelect, $_cate->pid));
        $view->assign('_result', $result);

        $this->setView('edit');
    }

    /**
     * Save data
     */
    public function _save() {
        // Validate item info
        $result = array('msg' => '', 'status' => true);

        $req = Flywheel_Factory::getRequest();
        $cid = $req->get('id', Flywheel_Filter::TYPE_INT, 0);

        $_categoryObj = new ItemCategory();
        $_categoryObj->read($cid);
        if ($cid == 0) {
            $_categoryObj->setNew(true);
        }
        $_categoryObj->setTitle($req->post('title'));
        $_categoryObj->setPid($req->post('cate', Flywheel_Filter::TYPE_INT, 0));
        $_categoryObj->setDes($req->post('des'));
        $_categoryObj->setPos($req->post('pos', Flywheel_Filter::TYPE_INT, 0));
        $_categoryObj->setTags($req->post('des'));
        $_categoryObj->setStatus($req->post('status') != "" ? 1 : 0);
        $_categoryObj->setCreatedTime(time());

        if ($_categoryObj->title == '') {
            $result['status'] = false;
            $result['msg'] = 'Tiêu đề danh mục rỗng!';
        }

        if ($result['status']) {
            if (!$_categoryObj->save()) {
                $result['status'] = false;
                $result['msg'] = 'Lỗi hệ thống! Liên hệ với ban quản trị.';
            } else {
                $result['msg'] = "Lưu thành công!";
            }
        }

        return $result;
    }

    public function executeDel() {
        $req = Flywheel_Factory::getRequest();
        $cid = $req->get('id', Flywheel_Filter::TYPE_INT, 0);

        $_cateteogryObj = new ItemCategory();
        $_cateteogryObj->read($id);
        $_cateteogryObj->delete();

        $req->redirect("?com=item_category&act=default");
    }

}
