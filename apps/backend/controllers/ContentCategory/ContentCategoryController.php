<?php

require_once CONTROLLERS_DIR . 'BackendController.php';
require_once dirname(__FILE__) . DS . 'ContentCategoryHelper.php';

class ContentCategoryController extends BackendController {

    public function executeDefault() {
        $view = Flywheel_Factory::getView();
        $document = Flywheel_Factory::getDocument();
        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('category');

        $document->title = "Quản lý danh mục - Admin CP -  " . $document->title;

        $categoryTree = ContentCategory::getTree();

        if (0 == sizeof($categoryTree)) {
            $message->display('Hiện chưa có danh mục nào trong hệ thống', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
        } else {
            $view->assign('categoryTree', $categoryTree);
        }
        // delete a category
        if (null != $request->get('del')) {
            $id = $request->get('del', Flywheel_Filter::TYPE_INT);
            $this->_deleteCategory($id);
        }
        $view->assign('message', MessageUtil::render($message));
    }

    public function executeNew() {
        $view = Flywheel_Factory::getView();
        $document = Flywheel_Factory::getDocument();
        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('category');

        $document->title = "Thêm danh mục mới - Admin CP -  " . $document->title;
        $category = new ContentCategory();

        if (Flywheel_Application_Request::POST == $request->getMethod()) {
            if (true == $this->_save($category)) {
                $message->add('Tạo mới danh mục <b>' . $category->name . '</b> thành công!', 'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
                $request->redirect('?com=content_category');
            }
        }
        $this->_drawCategoryForm($category);
        Flywheel_Factory::getView()->assign('message', MessageUtil::render($message));
    }

    public function executeRenderParentCategoryOption() {
        $request = Flywheel_Factory::getRequest();
        $parentCat = ContentCategory::retrieveByPk($request->get('parent_id', Flywheel_Filter::TYPE_INT, 0));
        if (false === $parentCat) {
            $parentCat = new ContentCategory();
        }
        Flywheel_Factory::getView()->assign('category', $parentCat);
        return $this->renderText($this->_drawCategoryOption($parentCat));
    }

    private function _drawCategoryOption($config) {
        return Flywheel_Factory::getView()
                ->render($this->getViewPath() . 'option_form', array('config' => $config));
    }

    public function executeEdit() {

        $view = Flywheel_Factory::getView();
        $document = Flywheel_Factory::getDocument();
        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('category');

        $document->title = "Sửa danh mục  - Admin CP -  " . $document->title;
        $id = $request->get('id', Flywheel_Filter::TYPE_INT, 0);
        $category = ContentCategory::retrieveByPk($id);

        if ((0 === $id) || (false == $category)) {
            $message->add('Không tìm thấy danh muc id[' . $id . '] !', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
            $request->redirect('?com=content_category');
        }
        if (Flywheel_Application_Request::POST == $request->getMethod()) {
            if (true === $this->_save($category)) {
                $message->add('Lưu thành công !', 'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
            }
        }
        $this->_drawCategoryForm($category);
        Flywheel_Factory::getView()->assign('message', MessageUtil::render($message));
    }

    private function _save(ContentCategory &$category) {

        $request = Flywheel_Factory::getRequest();
        if (false === $request->checkToken()) {
            //	Flywheel_Application::end('Invalid token!');
        }
        $valid = true;
        $message = MessageUtil::getMessage('category');

        $category->setName($request->post('name'));
        $category->setParentId($request->post('parent_id', Flywheel_Filter::TYPE_INT, 0));
        $category->setOrdering($request->post('ordering', Flywheel_Filter::TYPE_INT, 0));
        $category->setPublished($request->post('published', Flywheel_Filter::TYPE_INT, 1));
        $category->setDescription($request->post('description'));

        $config = $request->post('config', Flywheel_Filter::TYPE_ARRAY);
        if (null !== $config) {
            $category->setConfig(json_encode($config));
        }

        if (null == $category->name) {
            $valid = false;
            $message->add('Tên danh mục không được để trống !', 'LỖI', MessageUtil::TYPE_ERROR);
        }

        if ((null != $category->ordering) && (false == is_numeric($category->ordering))) {
            $valid = false;
            $message->add(' Thứ tự danh mục chỉ được nhập là số !', 'LỖI', MessageUtil::TYPE_ERROR);
        }
        if (true === $valid) {
            if ($category->save()) {
                return true;
            }
        }
        return false;
    }

    /**
     * draw Category form
     *
     * @param ContentCategory $category
     */
    public function _drawCategoryForm(ContentCategory &$category) {

        $view = Flywheel_Factory::getView();
        $this->setView('form');
        $view->assign('category', $category);

        $status = array(
            'Mở' => ContentCategory::STATUS_PUBLISHED,
            'Khóa' => ContentCategory::STATUS_UNPUBLISHED
        );

        $view->assign('radio_status', render_radio_option('published', $status, $category->published));

        // Get categories tree
        $tree = array(0 => array('id' => 0, 'level' => 0));
        ContentCategory::getTreeScalar(0, $tree);
        for ($i = 0, $size = sizeof($tree); $i < $size; ++$i) {
            if ($tree[$i]['id'] == 0 || $category->id != $tree[$i]['id']) {
                $option = ($tree[$i]['level'] > 0 ) ? '|' . str_repeat('--', $tree[$i]['level']) . ' ' : '';
                if ($tree[$i]['id'] != 0) {
                    $option .= ContentCategory::retrieveByPk($tree[$i]['id'])->name;
                } else {
                    $option = 'Mục chính';
                }
                $optCat[$option] = $tree[$i]['id'];
            }
        }
        $view->assign('category_tree', render_form_select('parent_id', $optCat, $category->parent_id, 'select-parent-categories'));

        if ($category->isNew()) {
            $config = array();
        } else {
            $config = $category->getConfig();
        }

        $view->assign('config_form', $this->_drawCategoryOption($config));
    }

    /**
     * delete a category
     *
     * @param  $id
     * @return true or false
     */
    private function _deleteCategory($id) {

        $request = Flywheel_Factory::getRequest();
        $message = MessageUtil::getMessage('category');
        $category = ContentCategory::retrieveByPk($id);

        if ((0 === $id) || (false == $category)) {
            $message->add('Không tìm thấy danh muc id[' . $id . '] !', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
            $request->redirect('?com=content_category');
        }
        if (false != $category->getChildsId()) {
            $message->add('Không xóa được ! Danh mục  <b>' . $category->name . '</b> vẫn còn danh mục con !', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
            $request->redirect('?com=content_category');
        }
        if (0 != $category->content_amount) {
            $message->add(' Không xóa được ! Danh mục <b>' . $category->name . '</b> vẫn còn bài viết  !', 'CẢNH BÁO', MessageUtil::TYPE_WARNING);
            $request->redirect('?com=content_category');
        }
        if (true == $category->delete()) {
            $message->add('Xóa danh mục <b>' . $category->name . '</b> thành công !', 'THÔNG BÁO', MessageUtil::TYPE_SUCCESS);
            $request->redirect('?com=content_category');
        }
    }

}