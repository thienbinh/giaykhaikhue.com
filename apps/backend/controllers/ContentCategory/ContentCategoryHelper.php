<?php
function draw_select_category($categories, $currentCategory, &$html = '', $level = 0) {	
	foreach ($categories as $item) {
		if ($item->id !== $currentCategory->id) {
			$html .= '<option value="' .$item->id .'"' 
				.(($item->id == $currentCategory->parent_id)? 'selected="selected"' :''). '>';
			if ($level > 0) {
				$html .= '|'. str_repeat('--;', $level);
			}
			$html .= $item->name;		
			$html .= '</option>';
		}
	}	
}

function render_content_category_layout_option($view) {
	$dir = BASE_DIR .DS .'apps' .DS .'frontend' .DS .'templates' .DS .'controllers' .DS. 'ContentCategory';	
	$views = folder_list_files($dir);
	$optionsView = array('mặc định' => 'default');
	for($i = 0, $size = sizeof($views); $i < $size; ++$i) {
		$_v = str_replace('.phtml', '', $views[$i]);
		$optionsView[$_v] = $_v;
	}
	
	return render_form_select('config[view]', $optionsView, $view);
}