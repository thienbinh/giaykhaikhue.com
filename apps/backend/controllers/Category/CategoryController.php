<?php
//ALTER TABLE `item_category` ADD `category_id` INT( 11 ) NULL DEFAULT '0' AFTER `id`
require_once CONTROLLERS_DIR .'BackendController.php';
class CategoryController extends BackendController 
{
    public $view     = '';
    public $message  = '';
    public $request  = '';
    public $user     = '';
    public $document = '';
    public $conn     = '';
    public $obj      = '';
    
	public function  beforeExecute() 
    {
		parent::beforeExecute();
		$this->view       = Flywheel_Factory::getView();
		$this->message    = MessageUtil::getMessage('category');
		$this->request    = Flywheel_Factory::getRequest();
		$this->user       = (array)Flywheel_Authen::getInstance()->getUser();
        $this->document   = Flywheel_Factory::getDocument();
        $this->conn       = Flywheel_DB::getConnection(Category::TABLE);
        $this->obj        = new Category;
        $url_path         = $this->document->getPublicPath();
        $this->view->assign('url_path',$url_path);
        $this->view->assign('com','category');
        $this->document->title = "Quản trị danh mục sản phẩm";
	}

	public function executeDefault() 
    {   
        #Lấy level 1
        $condition = "parent_id = 0 AND status = 1";
        $datas = $this->obj->selectList($condition,'sort asc');
        
        #Lấy level 2 của level 1 đầu tiên
        $data_level2 = array();
        if($datas)
        {
            $condition = "parent_id={$datas[0]['id']}";
            $data_level2 = $this->obj->selectList($condition,'sort asc');
        }
        #Lấy level 3 của level 1 đầu tiên
        $data_level3 = array();
        if($data_level2)
        {
            $condition = "parent_id={$data_level2[0]['id']}";
            $data_level3 = $this->obj->selectList($condition,'sort asc');
        }
        $this->view->assign('data1',$datas);
        $this->view->assign('data2',$data_level2);
        $this->view->assign('data3',$data_level3);
        
        $html_option1 = $this->obj->optionSelectCat($datas);
        $html_option2 = $this->obj->optionSelectCat($data_level2);
        $html_option3 = $this->obj->optionSelectCat($data_level3);
        $this->view->assign('html_option1',$html_option1);
        $this->view->assign('html_option2',$html_option2);
        $this->view->assign('html_option3',$html_option3);
        $this->view->assign('frontend_path', ROOT_URL. 'public_html/frontend/images/');
        
        $objT = new Tags;
        $this->view->assign('tags',$objT->selectList('1=1'));
	}
    
	public function executeEditAjax() 
    {
		$action = $this->request->get('action');
        $id = $this->request->get('id');
        if(!$id || $id == 0) {echo null;exit;};
        if($action == 0)
        {
            #Load
            $data = $this->obj->selectOne($id);
            if($data)
            {
                $arr = array();
                #Lấy parent_id
                $dataLevel = $this->obj->selectList('level='.($data['level']-1),'sort asc');
                if($dataLevel) $arr[3] = $this->obj->optionCategory($data['parent_id'],3);
                else $arr[3] = '';
                    
                $arr[0] = $data['id'];
                $arr[1] = $data['title'];
                $arr[2] = $data['show_at_foot'];
                $arr[4] = $data['level'];
                $arr[5] = $data['path'];
                $arr[6] = $data['show_at_home'];
                $arr[7] = $data['created_by'];
                $arr[8] = ROOT_URL.'public_html/data/uploadsys/categorys/'.$data['icon_class'];
                $arr[9] = $data['comment'];
                $arr[10] = $data['sort'];
                $arr[11] = $data['status'];
                $arr[12] = $data['icon_class'];
                $arr[13] = $data['p_title'];
                $arr[14] = $data['p_description'];
                $arr[15] = $data['keywords'];
                $arr[16] = $data['slug'];
                echo json_encode($arr);exit;
            }
            else {echo null;exit;};
        }
        elseif($action == 1)
        {
            $title = trim($this->request->get('name'));
            $slug = $this->request->get('slug');
            if($slug)
                $slug = BuildLink::slugify($slug);
            else
                $slug = BuildLink::slugify($title);
            #Save
            $keywords = $this->request->get('keywords');
            $p_id = trim($this->request->get('parent_id'));
            $dataP = $this->obj->selectOne($p_id);
            $array_update = array(
                'parent_id'         =>  trim($this->request->get('parent_id')),
                'title'             =>  $title,
                'slug'              =>  $slug,
                'status'            =>  trim($this->request->get('status')),
                'show_at_home'      =>  trim($this->request->get('home')),
                'sort'              =>  trim($this->request->get('position')),
                'show_at_foot'      =>  trim($this->request->get('foot',Flywheel_Filter::TYPE_INT,0)),
                'icon_class'        =>  trim($this->request->get('img_icon')), 
                'level'             =>  $dataP['level']+1, 
                'comment'           =>  trim($this->request->get('comment')),
                'p_title'           =>  trim($this->request->get('p_title')),
                'p_description'     =>  trim($this->request->get('p_description')),
                'keywords'          =>  trim($keywords), 
                'updated_at'        =>  time(),
                'updated_by'        =>  $this->user['username'],  
            );
            
            if($array_update['level'] == 1) $array_update['path'] = $array_update['title'];
            elseif($array_update['level'] == 2)
            {
                $data = $this->obj->selectOne($array_update['parent_id']);
                $array_update['path'] = $data['title'] .'>'. $array_update['title'];
            }
            elseif($array_update['level'] == 3)
            {
                //Lấy cấp 2
                $data2 = $this->obj->selectOne($array_update['parent_id']);
                
                //Lấy cấp 1
                $data1 = $this->obj->selectOne($data2['parent_id']);
                
                $array_update['path'] = $data1['title'] .'>'. $data2['title'] .'>'. $array_update['title'];
            }
            if($this->obj->update($array_update,'id='.$id))
            {
                echo 1;die;
            }
            else
            {
                echo null;die;
            } 
        }
	}
    
	public function executeUpload()
    { 
        if(!is_dir(UPLOADS_DIR .'categorys')) mkdir(UPLOADS_DIR .'categorys',0777);
        $time = time();
        $file = UPLOADS_DIR .'categorys'.DS. $time.'_'.$_FILES['uploadfile']['name']; 
        $tmp_file = $_FILES['uploadfile']['tmp_name'];
        if (move_uploaded_file($tmp_file, $file)) 
        { 
            echo ROOT_URL.'public_html/data/uploadsys/categorys/'.$time.'_'.$_FILES['uploadfile']['name']; 
        } 
        else 
        {
        	echo 0;
        }
        exit;
    }
    public function executeNew() 
    {
		
	}
    
    public function executeNewAjax() 
    {
        $title = trim($this->request->get('name'));
        $slug = $this->request->get('slug');
        if($slug)
            $slug = BuildLink::slugify($slug);
        else
            $slug = BuildLink::slugify($title);
        
        $array_insert = array(
            'parent_id'         =>  trim($this->request->get('parent_id')),
            'title'             =>  trim($title),
            'slug'              =>  trim($slug),
            'status'            =>  trim($this->request->get('status')),
            'show_at_home'      =>  trim($this->request->get('home')),
            'show_at_foot'      =>  trim($this->request->get('foot',Flywheel_Filter::TYPE_INT,0)),
            'sort'              =>  trim($this->request->get('position')),
            'icon_class'        =>  trim($this->request->get('img_icon')), 
            'level'             =>  trim($this->request->get('level')), 
            'created_at'        =>  time(),
            'created_by'        =>  $this->user['username'],
            'comment'           =>  trim($this->request->get('comment')),
            'p_title'           =>  trim($this->request->get('p_title')),
            'p_description'     =>  trim($this->request->get('p_description')),
            'keywords'          =>  trim($this->request->get('keywords')) 
        );
        if($array_insert['level'] == 1) $array_insert['path'] = $array_insert['title'];
        elseif($array_insert['level'] == 2)
        {
            $data = $this->obj->selectOne($array_insert['parent_id']);
            $array_insert['path'] = $data['title'] .'>'. $array_insert['title'];
        }
        elseif($array_insert['level'] == 3)
        {
            //Lấy cấp 2
            $data2 = $this->obj->selectOne($array_insert['parent_id']);
            
            //Lấy cấp 1
            $data1 = $this->obj->selectOne($data2['parent_id']);
            
            $array_insert['path'] = $data1['title'] .'>'. $data2['title'] .'>'. $array_insert['title'];
        }
        ///var_dump($array_insert);exit;
        $id_last = $this->obj->insert($array_insert);
        if($id_last)
        {
            $this->obj->update(array('category_id' => $id_last),'id='.$id_last);
            echo 1;die;
        }
        else
        {
            echo null;die;
        }    
	}
    
    public function executeDelete()
    {
        $id = $this->request->get('id');
        
        if(is_numeric($id))
        {        
            $this->obj->id = $id;
            $this->obj->delete();
        }    
        $this->obj->deleteCondition('parent_id='.$id);
        echo 1;exit;
    }
    
    public function executeDelajax()
    {
        $id = $this->request->get('id');
        $data = $this->obj->selectOne($id);
        $level = $data['level'];
        if(is_numeric($id))
        {        
            $this->obj->id = $id;
            $this->obj->delete();
        }    
        echo $level;exit;
    }
    
    function executeCatlevel()
    {
        $pid = $this->request->get('pid');
        $level = $this->request->get('level');
        if($level == 0) 
        {
            echo '<option value="0">Là danh mục cha</option>';exit;
        }
        $condition = '';
        if($pid == 0)
            $condition .= "level=$level";
        else
            $condition .= "level=$level AND parent_id=$pid";
        $data = $this->obj->selectList($condition,'sort asc');
        if(!$data) 
        {
            echo '<option value="0">Chưa có danh mục cha</option>';exit;
        } 
        
        echo $this->obj->optionSelectCat($data,$pid);exit;
    }
    
    function executeGetCat()
    {
        $selectedId = $this->request->get('id');
        $level = $this->request->get('level');
        if($level == 0) 
        {
            echo '<option value="0">Là danh mục cha</option>';exit;
        }
        $condition .= "level=$level";
        $data = $this->obj->selectList($condition,'sort asc');
        if(!$data) 
        {
            echo '<option value="0">Chưa có danh mục cha</option>';exit;
        }
        echo $this->obj->optionSelectCat($data,$selectedId);exit;
    }
    /**
     * array(
     *    0 =>array(
     *      'parent_id' => 1,
     *      'child'     => array(2,3,4,5,6)
     *    ),
     *    1 =>array(
     *      'parent_id' => 2,
     *      'child'     => array(12,13,14,15,16)
     *    ),
     *  
     * )
    */
    function executeProperties()
    {
        $id = $this->request->get('id');
        if(!$id || $id == 0) {echo null;exit;}
        
        $dataC = $this->obj->selectOne($id);
        $objP = new Property;
        $data_p = $objP->selectList('category_id='.$id,'sort asc');
        
        //Lấy danh sách giá trị thuộc tính theo id thuộc tính
        $objPV = new PropertyValue;
        $pvdata = $objPV->getPValue($data_p);
        
        $i=0;
        $html = 
        '<table class="admin-table user" border="0" cellspacing="0" cellpadding="0">
            <tr><td colspan="3"><strong><u>Thuộc tính danh mục:</u></strong>&nbsp;<span id="p-catname" style="color:red"><strong>'.$dataC['title'].'</strong>&nbsp;[<a href="javascript:;" onclick="addPropertyFCat('.$dataC['id'].')">Thêm mới</a>]</span></td></tr>
            <tr>
                <th width="3%">STT</th>
                <th width="15%">Tên thuộc tính</th>
                <th width="15%">Số giá trị</th>
                <th width="15%">Hiển thị trang chi tiết</th>
                <th width="7%">Thao tác</th>   
            </tr>';
            $arr_status = array(0 => 'Tắt', 1 => 'Bật');
            if($data_p)
            foreach($data_p as $row)
            {
                if(isset($pvdata[$row['id']])) 
                    $count = count($pvdata[$row['id']]);
                else
                    $count = 0;
            $html.='
            <tr id="p-row-'.$row['id'].'">
                <td>'.(++$i).'</td>
                <td><a href="javascript:;" class="show-zproperty" id="show-zproperty-'.$row['id'].'" rel="'.$id.'-'.$row['id'].'">'.$row['title'].'</a></td>
                <td>'.$count.'</td>
                <td>'.$arr_status[$row['status']].'</td>
                <td>
                    <span><a class="c-button-edit" href="javascript:;" onclick="editCatProperty('.$row['id'].',0)"></a></span>
                    <span><a class="c-button-del" href="javascript:;" onclick="delPrt('.$row['id'].')"></a></span>
                    <span><a class="c-button-copypt" href="javascript:;" onclick="copyPrt('.$row['id'].')"></a></span>
                </td>
            </tr>';
            }
            $html.='
        </table>';
        echo $html;exit; 
    }
    
    
    function executeChildProperties()
    {
        $id = $this->request->get('id');
        $pid = $this->request->get('pid');
        if(!$id || $id == 0) {echo null;exit;}
        $data = $this->obj->selectOne($id);
        if(!$data) {echo null;exit;}
        
        $objP = new Property();
        $dataP = $objP->selectOne($pid);
        
        $arr_status = array(0 => 'Tắt', 1 => 'Bật');
        $objPV = new PropertyValue;
        $data_p = $objPV->selectList('category_id='.$id.' AND property_id='.$pid,'sort asc');
        
        $i=0;
        $html = 
        '<table class="admin-table user" border="0" cellspacing="0" cellpadding="0">
            <tr><td colspan="3"><strong><u>Các giá trị TT của Thuộc tính</u> &nbsp;<span id="pv-catname" style="color:red">"'.$dataP['title'].'"</span> <u>của danh mục </u>&nbsp;<span id="gpv-catname" style="color:red">'.$data['title'].'</span></strong>&nbsp;[<a href="javascript:;" onclick="addPropertyVFCat('.$id.','.$pid.')">Thêm mới</a>]</td></tr>
            <tr>
                <th width="3%">STT</th>
                <th width="15%">Giá trị thuộc tính</th>
                <th width="15%">Hiển thị trang chi tiết</th>
                <th width="7%">Thao tác</th>   
            </tr>';
            if($data_p)
            foreach($data_p as $row)
            {
                $html.='
                <tr id="pv-row-'.$row['id'].'">
                    <td>'.(++$i).'</td>
                    <td id="pv-title-'.$row['id'].'">'.$row['title'].'</td>
                    <td>'.$arr_status[$row['status']].'</td>
                    <td>
                        <span><a class="c-button-edit" href="javascript:;" onclick="editPropertyValueCat('.$row['id'].',0)"></a></span>
                        <span><a class="c-button-del" href="javascript:;" onclick="delChildPrt('.$row['id'].')"></a></span>
                    </td>
                </tr>';
            }
            $html.='
        </table>';
        echo $html;exit; 
    }
    
    function executeDelPrtCat()
    {
        $id = $this->request->get('id');
        $pid = $this->request->get('pid');
        if(!$id || !$pid) {echo null;exit;}
        $data = $this->obj->selectOne($id);
        if(!$data || !$data['properties']) {echo null;exit;}
        $arr = array();
        $dataP = unserialize($data['properties']);
        foreach($dataP as $prt)
        {
            if($prt['parent_id'] != $pid)
            $arr[] = $prt;
        }
        #Update lại
        $this->obj->update(array('properties' => serialize($arr)),$id);
        echo 1;exit;
    }
    
    function executeDelChildPrtCat()
    {
        $id = $this->request->get('id');
        $cid = $this->request->get('cid');
        if(!$id || !$pid) {echo null;exit;}
        $data = $this->obj->selectOne($id);
        if(!$data || !$data['properties']) {echo null;exit;}
        $arr = array();
        $dataP = unserialize($data['properties']);
        foreach($dataP as $prt)
        {
            if(!in_array($cid,$prt['child']))
            $arr[] = $prt;
        }
        #Update lại
        $this->obj->update(array('properties' => serialize($arr)),$id);
        echo 1;exit;
    }
    
    function executeGetList()
    {
        $level = $this->request->get('level');
        $title = $this->request->get('title');
        $condition = "level=$level";
        if(is_numeric($title))
        {
            $condition .= ' AND category_id='.trim($title);
        }
        else
            $condition .= " AND title LIKE '%{$title}%'";
        $data = $this->obj->selectList($condition,'sort asc');
        echo json_encode($this->obj->optionSelectCat($data));
        die;
    }
    
    function executeExcel()
    {
        require_once MODELS_DIR.'Reader.php';  
        $filename = $_FILES["fx-name"]["tmp_name"];;
        $data = new Spreadsheet_Excel_Reader();
        
        // Set output Encoding.
        $data->setOutputEncoding('UTF-8');
        $data->read($filename);
        error_reporting(E_ALL ^ E_NOTICE);
        
        //print_r($data);die;
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) 
        {
            $id = $data->sheets[0]['cells'][$i][2];
            $array = array(
                'title'             =>  trim($data->sheets[0]['cells'][$i][3]),
                'slug'              =>  BuildLink::slugify(trim($data->sheets[0]['cells'][$i][3])),
                'comment'           =>  trim($data->sheets[0]['cells'][$i][5]),
                'parent_id'         =>  (int)trim($data->sheets[0]['cells'][$i][6]),
                'path'              =>  trim($data->sheets[0]['cells'][$i][7]),
                'level'             =>  (int)trim($data->sheets[0]['cells'][$i][8]),
                'icon_class'        =>  trim($data->sheets[0]['cells'][$i][9]),
                'show_at_home'      =>  (int)trim($data->sheets[0]['cells'][$i][10]),
                'show_at_foot'      =>  (int)trim($data->sheets[0]['cells'][$i][11]),
                'sort'              =>  (int)trim($data->sheets[0]['cells'][$i][4]),
                'status'            =>  (int)trim($data->sheets[0]['cells'][$i][12]),
                'created_at'        =>  time(),
                'created_by'        =>  $this->user['username']
            );
            //var_dump($array);
            try
            {
                if($array && $array['title'])
                    $this->obj->insert($array);
                
            }
            catch(exception $e) {
                
            }
        }
        $this->executeUpdatePath();
    }
    
    function executeUpdatePath()
    {
        $this->obj = new Category;
        $this->obj->update(array('path' => ''),'1=1');
        $data = $this->obj->selectList('path = ""');//var_dump($data);die;
        foreach($data as $cat1)
        {
            if($cat1['level'] == 1)
            {
                $this->obj->update(array('path' => $cat1['title'],'slug' => BuildLink::slugify($cat1['title'])),'id='.$cat1['id']);
                foreach($data as $cat2)
                {
                    if($cat2['parent_id'] == $cat1['id'])
                    {
                        $this->obj->update(array('path' => $cat1['title'].'>'.$cat2['title'],'slug' => BuildLink::slugify($cat2['title'])),'id='.$cat2['id']);
                        foreach($data as $cat3)
                        {
                            if($cat3['parent_id'] == $cat2['id'])
                            {
                                $this->obj->update(array('path' => $cat1['title'].'>'.$cat2['title'].'>'.$cat3['title'],'slug' => BuildLink::slugify($cat3['title'])),'id='.$cat3['id']);
                            }
                        }
                    }
                }   
            }
        }
        echo '<script> window.location = "?com=category";</script>';
    }
    
    public function executeAddTag()
    {
        $edit = $this->request->get('edit');
        $id = $this->request->get('id');
        $keyword = $this->request->get('keyword');
        $objT = new Tags;
        $data = $objT->selectList('title="'.$keyword.'"');
        //Thêm
        if(!$data)
        {
            $url = $this->document->getPublicPath().'tim-kiem/'.str_replace(' ','+',trim($keyword)).'.html'; 
            $objT->insert(array('title' => $keyword, 'url' => $url, 'status' => 1));
            echo $url;        
        }
        else echo 0;
        die;
    }
    
    function executeDelTag()
    {
        $keyword = $this->request->get('keyword');
        $objT = new Tags;
        $sql = "Delete From tags Where title='{$keyword}'";
        $objT->db->prepare($sql);
        $objT->db->execute();
        echo 1;die;
    }
    
    function executeGetTag() 
    {
        $ids = $this->request->get('ids');
        
        if($ids)
        {
            $ids = substr($ids,0,-1);
            $objT = new Tags;
            $datas = $objT->selectList('id IN('.$ids.')');
            
            $html = '';
            $i = 0;
            foreach($datas as $row)
            {
                $html .= "<a id='t-{$row['id']}' href='{$row['url']}'>{$row['title']}</a><a class='d-tags' rel='{$row['id']}' style='color:#F00' href='javascript:;'>(x)</a>, ";
            }
            echo $html;
        }
        die;
    }
    
    function executeExportExcel()
    {
        require_once MODELS_DIR.'ExcelExpand.php';
        $filename = "Danh-sach-danh-muc";
        $excel = new Excel();
        // style
        $style = $excel->addStyle()->addProperty('ID', 'sHeader')->addTag('Alignment')->addProperty('Horizontal', 'Center')->addProperty('Vertical', 'Center')->parent;
        $style->addTag('Font')->addProperty('Color', 'blue')->addProperty('Bold', '1');
        $borders = $style->addTag('Borders')->addTag('Border')->addProperty('Position', 'Bottom')->addProperty('LineStyle', 'Continuous')->addProperty('Weight', '1')->parent;
        $borders->addTag('Border')->addProperty('Position', 'Left')->addProperty('LineStyle', 'Continuous')->addProperty('Weight', '1');
        $borders->addTag('Border')->addProperty('Position', 'Right')->addProperty('LineStyle', 'Continuous')->addProperty('Weight', '1');
        $borders->addTag('Border')->addProperty('Position', 'Top')->addProperty('LineStyle', 'Continuous')->addProperty('Weight', '1');
        $style = $excel->addStyle()->addProperty('ID', 'sBody')->tags[] = $borders;
        $style = $excel->addStyle()->addProperty('ID', 'sCaption')->addTag('Alignment')->addProperty('Horizontal', 'Center')->addProperty('Vertical', 'Center')->parent;
        $style->addTag('Font')->addProperty('Color', 'red')->addProperty('Size', 12)->addProperty('Bold', '1')->parent->tags[] = $borders;
        // table
        $table = $excel->addWorksheet('Danh mục')->addTable();
        $table->addColumn()->addProperty('AutoFitWidth', 0)->addProperty('Width', 30);
        $table->addColumn()->addProperty('AutoFitWidth', 0)->addProperty('Width', 100);
        $table->addColumn()->addProperty('AutoFitWidth', 0)->addProperty('Width', 200);
        $table->addColumn()->addProperty('AutoFitWidth', 0)->addProperty('Width', 80);
        $table->addColumn()->addProperty('AutoFitWidth', 0)->addProperty('Width', 70);
        $table->addColumn()->addProperty('AutoFitWidth', 0)->addProperty('Width', 200);
        // header
        $table->addRow();
        $row = $table->addRow()->addProperty('AutoFitHeight', 0)->addProperty('Height', 20);
        $row->addCell('Danh mục')->addProperty('StyleID', 'sCaption')->addProperty('MergeAcross', '5')->data->addProperty('Type', 'String');
        $row = $table->addRow()->addProperty('AutoFitHeight', 0)->addProperty('Height', 20);
        $row->addCell('STT')->addProperty('StyleID', 'sHeader')->data->addProperty('Type', 'String')->parent->parent;
        $row->addCell('Mã danh mục')->addProperty('StyleID', 'sHeader')->data->addProperty('Type', 'String')->parent->parent;
        $row->addCell('Tên Tiếng Việt')->addProperty('StyleID', 'sHeader')->data->addProperty('Type', 'String')->parent->parent;
        $row->addCell('Mã DM cha')->addProperty('StyleID', 'sHeader')->data->addProperty('Type', 'String')->parent->parent;
        $row->addCell('Cấp')->addProperty('StyleID', 'sHeader')->data->addProperty('Type', 'String')->parent->parent;
        $row->addCell('Path')->addProperty('StyleID', 'sHeader')->data->addProperty('Type', 'String')->parent->parent;
        $datas = $this->obj->selectList('1=1');
        if (!empty($datas)) 
        {
            $no = 0;
            foreach ($datas as $ex) 
            {
                if($ex['level'] == 1)
                {
                    $no = $no + 1;
                    $row = $table->addRow()->addProperty('AutoFitHeight', 0)->addProperty('Height', 20)->addCell($no)->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number')->parent->parent;
                    $row->addCell($ex['id'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number');
                    $row->addCell($ex['title'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'String');
                    $row->addCell($ex['parent_id'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number');
                    $row->addCell($ex['level'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number');
                    $row->addCell($ex['path'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'String');
                    foreach ($datas as $ex2) 
                    {
                        if($ex2['level'] == 2 && $ex2['parent_id'] == $ex['id'])
                        {
                            $no = $no + 1;
                            $row = $table->addRow()->addProperty('AutoFitHeight', 0)->addProperty('Height', 20)->addCell($no)->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number')->parent->parent;
                            $row->addCell($ex2['id'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number');
                            $row->addCell('-'.$ex2['title'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'String');
                            $row->addCell($ex2['parent_id'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number');
                            $row->addCell($ex2['level'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number');
                            $row->addCell($ex2['path'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'String');
                            foreach ($datas as $ex3) 
                            {
                                if($ex3['level'] == 3 && $ex3['parent_id'] == $ex2['id'])
                                {
                                    $no = $no + 1;
                                    $row = $table->addRow()->addProperty('AutoFitHeight', 0)->addProperty('Height', 20)->addCell($no)->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number')->parent->parent;
                                    $row->addCell($ex3['id'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number');
                                    $row->addCell('--'.$ex3['title'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'String');
                                    $row->addCell($ex3['parent_id'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number');
                                    $row->addCell($ex3['level'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'Number');
                                    $row->addCell($ex3['path'])->addProperty('StyleID', 'sBody')->data->addProperty('Type', 'String');
                                }
                            }
                        }
                    }       
                }
            }
        }
        $excel->getFile($filename);
        exit();
    }
    
    /**
     * INSERT INTO images(`title`,`image_url`,`description`,`status`,`position`,`home`,`index`,`category_id`) 
       SELECT `title`,`image_url`,`description`,`status`,`position`,`home`,`index`,`category_id` FROM images WHERE id=1
       
       
    */
    
    public function executeCopyCat()
    {
        $cat_id = $this->request->get('id');
        $sql = "INSERT INTO item_category(`category_id`,`title`,`slug`,`comment`,`parent_id`,`path`,`level`,`icon_class`,`show_at_home`,`show_at_foot`,`sort`,`status`,`p_title`,`p_description`,`keywords`,`num_item`,`created_at`,`created_by`,`updated_at`,`updated_by`)
                SELECT `category_id`,`title`,`slug`,`comment`,`parent_id`,`path`,`level`,`icon_class`,`show_at_home`,`show_at_foot`,`sort`,`status`,`p_title`,`p_description`,`keywords`,`num_item`,`created_at`,`created_by`,`updated_at`,`updated_by` FROM item_category WHERE id={$cat_id}";
        $this->conn->prepare($sql);
        $this->conn->execute();
        echo $this->conn->prepare('SELECT LAST_INSERT_ID() FROM item_category');
        $this->conn->execute();
        $lastID = $this->conn->fetch();
        //echo $lastID["LAST_INSERT_ID()"];
        
        //Thuộc tính
        $objP = new Property;
        $dataP = $objP->selectList('category_id='.$cat_id);
        if($dataP)
        {
            $sql_p = 'INSERT INTO `property` (`category_id`, `sort`, `title`, `path`, `is_detail`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`, `is_check`) VALUES'; 
            foreach($dataP as $row)
            {
                $title = str_replace("'","\'",$row['title']);
                $path = str_replace("'","\'",$row['path']);
                
                $sql_p .= "('{$lastID["LAST_INSERT_ID()"]}','{$row['sort']}','{$title}','{$path}','{$row['is_detail']}','{$row['created_at']}','{$row['created_by']}','{$row['updated_at']}','{$row['updated_by']}','{$row['status']}','{$row['is_check']}'),";
            }  
            $sql_p = substr($sql_p,0,-1);
            $objP->db->prepare($sql_p);
            $objP->db->execute();
            
            $dataP = $objP->selectList('category_id='.$lastID["LAST_INSERT_ID()"]);
            //var_dump($dataP);
            if($dataP)
            {
                $arrayP = array();
                foreach($dataP as $row)
                {
                    $arrayP[$row['title']]['title'] = $row['title'];
                    $arrayP[$row['title']]['category_id'] = $row['category_id'];
                    $arrayP[$row['title']]['id'] = $row['id'];
                }
                $objPV = new PropertyValue;
                $datas=$objPV->selectList('category_id='.$cat_id.' AND status=1');
                if($datas)
                {
                    $sql_pv = 'INSERT INTO `property_value` (`title`, `property_id`, `property_name`, `category_id`, `path`, `status`, `sort`, `created_at`, `created_by`) VALUES';
                    foreach($datas as $row)
                    {
                        $title = str_replace("'","\'",$row['title']);
                        $property_name = str_replace("'","\'",$arrayP[$row['property_name']]['title']);
                        //$path = str_replace("'","\'",$row['path']);                        
                        $sql_pv .= "('{$title}','{$arrayP[$row['property_name']]['id']}','{$property_name}','{$arrayP[$row['property_name']]['category_id']}','',1,1,".time().",'".$this->user['username']."'),";
                    }
                    $sql_pv = substr($sql_pv,0,-1);//echo $sql_pv;
                    $objPV->db->prepare($sql_pv);
                    $objPV->db->execute();
                }
            }
        }
        echo 1;
        die; 
    }
    
    public function executeCopyPrt()
    {
        $pid = $this->request->get('pid');
        $cid = $this->request->get('cid');
        $sql_p = 'INSERT INTO `property` (`category_id`, `sort`, `title`, `path`, `is_detail`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`, `is_check`) 
                  SELECT `category_id`, `sort`, `title`, `path`, `is_detail`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`, `is_check` FROM property WHERE id='.$pid;
        $obj = new Property;
        $dataP=$obj->selectOne($pid);
        $obj->db->prepare($sql_p);
        $obj->db->execute();
        $obj->db->prepare('SELECT LAST_INSERT_ID() FROM property');
        $obj->db->execute();
        $lastID = $obj->db->fetch();
        $obj->update(array('category_id' => $cid),'id='.$lastID["LAST_INSERT_ID()"]);
        
        $objPV = new PropertyValue;
        $datas=$objPV->selectList('property_id='.$pid.' AND category_id='.$dataP['category_id'].' AND status=1');
        if($datas)
        {
            $sql_pv = 'INSERT INTO `property_value` (`title`, `property_id`, `property_name`, `category_id`, `path`, `status`, `sort`, `created_at`, `created_by`) VALUES';
            foreach($datas as $row)
            {
                $title = str_replace("'","\'",$row['title']);
                $property_name = str_replace("'","\'", $row['title']);
                //$path = str_replace("'","\'",$row['path']);                        
                $sql_pv .= "('{$title}','{$lastID["LAST_INSERT_ID()"]}','{$property_name}','{$cid}','',1,1,".time().",'".$this->user['username']."'),";
            }
            $sql_pv = substr($sql_pv,0,-1);//echo $sql_pv;
            $objPV->db->prepare($sql_pv);
            $objPV->db->execute();
        }
        echo 1;
        die;
    }
}