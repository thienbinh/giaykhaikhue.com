<?php

require_once CONTROLLERS_DIR . 'BackendController.php';
/**
 * Require library to convert page data to HTML DOM
 */
require_once GLOBAL_HELPER_DIR . 'simple_html_dom.php';

/**
 * Class execute item controller
 */
class ItemController extends BackendController {

    public $img = "";

    public function beforeExecute() {
        parent::beforeExecute();
    }

    /**
     * Load items
     */
    public function executeDefault() {
        $view = Flywheel_Factory::getView();
        $user = Flywheel_Authen::getInstance()->getUser();
        $req = Flywheel_Factory::getRequest();
        $cateObj = new ItemCategory();
        $itemObj = new Item();

        $page = $req->get('page', Flywheel_Filter::TYPE_INT, 1);
        $pageSize = 20;
        $filter = array();

        $pagination = new Flywheel_Pagination();
        $pagination->pageSize = $pageSize;
        $pagination->currentPage = $page;
        $pagination->template = TEMPLATE_DIR . 'modules' . DS . 'pagination';
        $pagination->total = $itemObj->countItem();
        $pagination->urlStructure = "?com=item&page=<page>";
        $pagination->initialize();
        $view->assign('pagination', $pagination->render());

        // Load list items
        $limit = $pageSize * ($page - 1) . "," . $pageSize;
        $_itemList = $itemObj->selectList("1=1", "`pos` DESC", $limit, "id");
        $_allCate = $cateObj->selectList("1=1", "`id` DESC", null, "id");

        $_cateSelect = array(' - Chọn danh mục' => 0);
        $_allCate_tmp = $_allCate;
        if (!empty($_allCate_tmp)) {
            // Filter childs to parent
            foreach ($_allCate_tmp as $k => $v) {
                foreach ($_allCate_tmp as $k_child => $v_child) {
                    if ($v_child['pid'] == $k) {
                        $_allCate_tmp[$k]['childs'][] = $v_child;
                        unset($_allCate_tmp[$k_child]);
                    }
                }
            }
            // Filter to array key
            foreach ($_allCate_tmp as $k => $v) {
                $_cateSelect[$v['title']] = $v['id'];
                if (array_key_exists('childs', $v)) {
                    foreach ($v['childs'] as $k_child => $v_child) {
                        $_cateSelect['.....' . $v_child['title']] = $v_child['id'];
                    }
                }
            }
        }

        // Assign
        $view->assign('_itemList', $_itemList);
        $view->assign('_allCate', $_allCate);
        $view->assign('_cateSelect', render_form_select('cate', $_cateSelect, $req->get('cate')));
        $view->assign('_page', $page);
        $view->assign('_perPage', $pageSize);
    }

    /**
     * Edit, add item
     */
    public function executeEdit() {
        $view = Flywheel_Factory::getView();
        $request = Flywheel_Factory::getRequest();
        $result = array('msg' => '', 'status' => true);

        // Get item detail
        $itemid = $request->get('id');

        // Check post form (save item)
        if ($request->getMethod() == Flywheel_Application_Request::POST) {
            $result = $this->_save();
        }

        $itemObj = new Item();
        $_item = $itemObj->retrieveByKey($itemid);

        // Load list categories
        $categoryObj = new ItemCategory();
        $_cateList = $categoryObj->selectList("", "`pid`, `pos` ASC", null, "id");

        $_cateSelect = array(' - Chọn danh mục' => 0);
        if (!empty($_cateList)) {
            // Filter childs to parent
            foreach ($_cateList as $k => $v) {
                foreach ($_cateList as $k_child => $v_child) {
                    if ($v_child['pid'] == $k) {
                        $_cateList[$k]['childs'][] = $v_child;
                        unset($_cateList[$k_child]);
                    }
                }
            }
            // Filter to array key
            foreach ($_cateList as $k => $v) {
                $_cateSelect[$v['title']] = $v['id'];
                if (array_key_exists('childs', $v)) {
                    foreach ($v['childs'] as $k_child => $v_child) {
                        $_cateSelect['.....' . $v_child['title']] = $v_child['id'];
                    }
                }
            }
        }

        // Assign html
        $view->assign('_item', $_item);
        $view->assign('_cateSelect', render_form_select('cate', $_cateSelect, $_item->cid));
        $view->assign('_result', $result);
        $this->setView('form');
    }

    /**
     * Save data
     */
    public function _save($crawl = false) {
        // Validate item info
        $result = array('msg' => '', 'status' => true);

        $req    = Flywheel_Factory::getRequest();
        $id     = $req->get('id', Flywheel_Filter::TYPE_INT, 0);
        $type   = $req->post('type', Flywheel_Filter::TYPE_INT, 0);
        $id_craw = $req->post('type', Flywheel_Filter::TYPE_DOUBLE, 0);

        $_itemObj = new Item();
        $_itemObj = $_itemObj->retrieveByKey($id);
        if ($id == 0) {
            $_itemObj->setNew(true);
        }
        
        $_itemObj->setTitle($req->post('title'));
        $_itemObj->setTitleOrigin($req->post('title_origin'));
        $_itemObj->setCode($req->post('code'));
        $_itemObj->setCid($req->post('cate', Flywheel_Filter::TYPE_INT, 0));
        $_itemObj->setPrice($req->post('price', Flywheel_Filter::TYPE_DOUBLE, 0));
        $_itemObj->setProPrice($req->post('pro_price', Flywheel_Filter::TYPE_DOUBLE, 0)); // Promotion price
        $_itemObj->setProDes($req->post('pro_des')); // Description for promotion
        $_itemObj->setSitePrice($req->post('site_price', Flywheel_Filter::TYPE_DOUBLE, 0)); // Price of item crawl
        $_itemObj->setBrief($req->post('brief'));
        $_itemObj->setDes($req->post('des'));
        $_itemObj->setIsFront($req->post('is_front', Flywheel_Filter::TYPE_STRING, "") != "" ? 1 : 0);
        $_itemObj->setIsNew($req->post('is_new', Flywheel_Filter::TYPE_STRING, "") != "" ? 1 : 0);
        $_itemObj->setPos($req->post('pos', Flywheel_Filter::TYPE_INT, 0));
        $_itemObj->setInstock($req->post('instock', Flywheel_Filter::TYPE_INT, 0));
        $_itemObj->setTags($req->post('tags'));
        $_itemObj->setStatus($req->post('status', Flywheel_Filter::TYPE_STRING, "") != "" ? 1 : 0);
        $_itemObj->setCreatedTime(time());
        $_itemObj->setLinkCrawl($req->post('link')); // Link crawled
        $_itemObj->setIdCraw($req->post("id_craw", Flywheel_Filter::TYPE_DOUBLE, 0));
        $_itemObj->setType($type);

        if ($_itemObj->cid == 0) {
            $result['status'] = false;
            $result['msg'] = 'Chưa chọn danh mục!';
        }

        if ($_itemObj->title == '') {
            $result['status'] = false;
            $result['msg'] = 'Tiêu đề sản phẩm rỗng!';
        }

        if ($result['status']) {
            if (!$type) {
                // Upload image
                $this->__upload('img', STATIC_DIR . 'items' . DS);
                if (!empty($this->img)) {
                    $_itemObj->setImg($this->img);
                }
            } else { // Add image full link by crawler
                $_itemObj->setImg($req->post('img'));
            }
            
            /*
            echo '<pre>';
            var_dump($_itemObj->isNew());
            print_r($_itemObj); 
            die();
             * 
             */
            
            if (!$_itemObj->save()) {
                $result['status'] = false;
                $result['msg'] = 'Lỗi hệ thống! Liên hệ với ban quản trị.';
            } else {
                $result['msg'] = ($id == 0 ? "Thêm mới" : 'Cập nhật') . " thành công!";
            }
        }

        return $result;
    }

    public function executeDel() {
        $req = Flywheel_Factory::getRequest();
        $id = $req->get('id', Flywheel_Filter::TYPE_INT, 0);

        $item = new Item();
        $item->read($id);
        $item->delete();

        $req->redirect("?com=item&act=default");
    }

    /**
     * Crawl item from some website
     */
    public function executeCrawl() {
        $req = Flywheel_Factory::getRequest();
        $view = Flywheel_Factory::getView();
        $result = array(); // Crawl data
        $_result = array('status' => true, 'msg' => ''); // Result submit data

        if ($req->getMethod() == Flywheel_Application_Request::POST) {
            $option = $req->post('option', Flywheel_Filter::TYPE_STRING, '');
            switch ($option) {
                case 'crawl':
                    $link = $req->post('link', Flywheel_Filter::TYPE_STRING, null, 0, false);
                    if(preg_replace('/\s+/', '', $link) == "") {
                        $_result['status'] = false;
                        $_result['msg'] = "Chưa nhập link sản phẩm";
                        break;
                    }
                    
                    if(!stripos($link, 'detail.tmall.com/item.htm') & !stripos($link, 'item.taobao.com/item.htm')) {
                        $_result['status'] = false;
                        $_result['msg'] = "Không phải link sản phẩm taobao!";
                        break;
                    }
                    
                    /**
                     * Type crawl: 0-> get data on page, 1-> get link on page
                     */
                    $type = $req->post('type', Flywheel_Filter::TYPE_INT, 0);

                    if ($type == 0) {
                        $result = $this->_crawlData($link);
                    } else {
                        $_links = $this->_crawlLink($link);
                    }
                    break;
                case 'translate':
                    $_trans = $this->_translate();
                    return $this->renderText($_trans);
                    break;
                default:
                    // Check item id of crawl
                    if(!$req->post('id', Flywheel_Filter::TYPE_INT, 0)) {
                        $id_craw = $req->post('id_craw', Flywheel_Filter::TYPE_DOUBLE, 0);
                        $item = new Item();
                        $item = $item->selectOne("`id_craw`={$id_craw}");
                        if(!empty($item)) {
                            $_GET['id'] = $item['id'];
                        }
                    }
                    $_result = $this->_save();
                    if($_result['status']) {
                        $req->redirect('?com=item');
                    } else {
                        var_dump($_result);
                    }
                    break;
            }
        }

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
                && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return $this->renderText(json_encode($result));
        }
        
        // Load list categories
        $categoryObj = new ItemCategory();
        $_cateList = $categoryObj->selectList("", "`pid`, `pos` ASC", null, "id");
        $_cateSelect = array(' - Chọn danh mục' => 0);
        if (!empty($_cateList)) {
            // Filter childs to parent
            foreach ($_cateList as $k => $v) {
                foreach ($_cateList as $k_child => $v_child) {
                    if ($v_child['pid'] == $k) {
                        $_cateList[$k]['childs'][] = $v_child;
                        unset($_cateList[$k_child]);
                    }
                }
            }
            // Filter to array key
            foreach ($_cateList as $k => $v) {
                $_cateSelect[$v['title']] = $v['id'];
                if (array_key_exists('childs', $v)) {
                    foreach ($v['childs'] as $k_child => $v_child) {
                        $_cateSelect['.....' . $v_child['title']] = $v_child['id'];
                    }
                }
            }
        }
        $view->assign('_cateSelect', render_form_select('cate', $_cateSelect, $req->get('cate')));
        $view->assign('result', $result);
        $view->assign('_result', $_result);
        $this->setView('crawl');
    }

    /**
     *
     * @param type $file_name Name of post file controller
     * @param type $path Fhysical directory
     * @param type $config Config param upload
     * @return type STRING
     */
    public function __upload($file_name = null, $path = "", $config = null) {
        $result = array('msg' => "", "status" => true);
        // Config file upload
        if ($config == null) {
            $config = array(
                'filter_type' => '.jpg, .gif, .bmp, .png',
                'max_size' => 1024*5, //5MB
                'encrypt_file_name' => false,
                'ansi_name' => false,
                'remove_white_space' => true
            );
        }

        $fileHandler = new Flywheel_File_Handler($path, $file_name, $config);
        if ($fileHandler->upload($file_name, 0777)) {
            $data = $fileHandler->getData();
            $result['msg'] = "Upload file thành công!";
            $this->img = $data['file_name'];
        } else {
            $err = $fileHandler->hasError();
            // Error upload file handle
            $result['msg'] = implode(', ', $err);
            $result['status'] = false;
        }
        return $result;
    }

    /**
     * Get data on page
     * @param type $link Link to crawl
     */
    private function _crawlData($link_detail) {
        $html = file_get_html($link_detail);

        // Title
        $title = $html->getElementById('detail')->find('h3', 0)->plaintext;
        // Item id
        $item_id = $html->find('input[name=item_id]', 0)->value;
        // price
        $price = $html->getElementById('J_StrPrice')->find('.tb-rmb-num', 0)->innertext;
        // image
        $image = $html->getElementById('J_ImgBooth')->attr['data-src'];
        // properties
        $_prop_html = $_break = "";
        $_prop_html = $html->getElementById('attributes')->outertext;

        /*
          $properties = $html->getElementById('attributes')->find('li');
          if(!empty($properties)) {
          foreach($properties as $prop) {
          $_prop_html .= $_break . $prop->innertext;
          $_break = "<br/>";
          }
          }
         * 
         */
        // Images description
        $_img_description = $imgs = "";
        // Get link description detail
        preg_match('/"apiItemDesc":"(.*?)",/', $html, $link_desc);
        if (array_key_exists(1, $link_desc)) {
            $html_desc = file_get_html($link_desc[1]);
            $imgs = $html_desc->find('img');
        } else {
            $imgs = $html->find('.custom-area');
            if ($imgs != null) {
                $imgs = $imgs[0]->find('img');
            }
        }
        if (!empty($imgs)) {
            foreach ($imgs as $img) {
                $_img_description .= $img->outertext;
            }
        }
        /*
          echo htmlspecialchars_decode($_img_description);
          echo '<meta charset="gbk"/>';
          echo '<pre>';
          print_r(array(
          'title' => $title,
          'item_id' => $item_id,
          'image' => $image,
          'price' => $price,
          'proper_html' => $_prop_html,
          'img_description' => $_img_description
          )); die();
         * 
         */

        return array(
            'title' => mb_convert_encoding($title, "UTF-8", "GBK"),
            'id_craw' => $item_id,
            'image' => $image,
            'price' => $price,
            'proper_html' => mb_convert_encoding($_prop_html, "UTF-8", "GBK"),
            'img_description' => $_img_description
        );
    }

    /**
     * Get link detail on page
     * @param type $link_page Link has contain link of detail
     */
    private function _crawlLink($link_page) {
        
    }

    /**
     * Translate chinese text to VN text
     */
    private function _translate() {
        $req = Flywheel_Factory::getRequest();
        $text = $req->post('text');
        // Translate
        try {
            if (!empty($text)) {
                $text = KeySearchPeer::translate($text);
            }
        } catch (Exception $e) {
            $text = "";
        }

        return html_entity_decode($text);
    }

}
