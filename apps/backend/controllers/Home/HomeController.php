<?php

require_once CONTROLLERS_DIR . 'BackendController.php';
class HomeController extends BackendController {
    
    public function beforeExecute() {
        parent::beforeExecute();
    }
    public function executeDefault() {
        $view = Flywheel_Factory::getView();
        $user = Flywheel_Authen::getInstance()->getUser();
        
        $this->setLayout('home');
    }
}
