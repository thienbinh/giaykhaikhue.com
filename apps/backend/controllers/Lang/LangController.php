<?php
require_once CONTROLLERS_DIR .'BackendController.php';
class LangController extends BackendController {
    public $public_html='';
    public $request = '';
    public $user = '';
    public $lang = '';
    
    public function  beforeExecute() { 
        parent::beforeExecute();
        $view       = Flywheel_Factory::getView();
        $message    = MessageUtil::getMessage('common');
        $view->assign('message', MessageUtil::render($message));
        $this->public_html = Flywheel_Factory::getDocument()->getPublicPath();
        $this->request = Flywheel_Factory::getRequest();
        $this->user = new Users;
        $this->lang = new Lang;
    }

    public function executeDefault() {
        $view = Flywheel_Factory::getView();
        $langs = array();
        $langs = $this->lang->selectList();
        $view->assign('langs', $langs);
    }
    public function executeAdd(){
        $key = $this->request->post('key');
        $vi = $this->request->post('vi');
        $cn = $this->request->post('cn');
        $cond = " `key`='".$key."'";
        $check = array();
        $check = $this->lang->selectList($cond);
        if(!empty($check)){
            die('Từ khóa đã tồn tại');
        }
        $this->lang->insert(array(
            'key'=>$key,
            'vi'=>$vi,
            'cn'=>$cn
        ));
        die('1');
    }
    public function executeUpdate(){
        $lid = $this->request->post('lid');
        $key = $this->request->post('key');
        $vi = $this->request->post('vi');
        $cn = $this->request->post('cn');
        $this->lang->update(
            array(
                'key'=>$key,
                'vi'=>$vi,
                'cn'=>$cn
            ),
            '`id`='.$lid
        );
        die('1');
    }
    public function executeDelete(){
        $lid = $this->request->post('lid');
        $this->lang->id = $lid;
        $this->lang->delete();
    }
}