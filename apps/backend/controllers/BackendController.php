<?php

require_once FLYWHEEL_DIR . 'Util' . DS . 'ValidateUtil.php';
require_once FLYWHEEL_DIR . 'Util' . DS . 'FormUtil.php';
require_once FLYWHEEL_DIR . 'Util' . DS . 'MessageUtil.php';

abstract class BackendController extends Flywheel_Controller {

    public $document;
    public function beforeExecute() {
        $document = Flywheel_Factory::getDocument();
        $this->_checkAuthenticated();
        $view = Flywheel_Factory::getView();
        $user = Flywheel_Authen::getInstance()->getUser();
        $view->assign('_user', $user);
        
        $document->title= "Amin-CP";
        $this->document = $document;
    }

    protected function _checkAuthenticated() {

        $router = Flywheel_Factory::getRouter();
        if ($router->getComponentName() == 'login') { return; }

        // Check login
        $request    = Flywheel_Factory::getRequest();
        $authen     = Flywheel_Authen::getInstance();

        if (!$authen->isAuthenticated()) { // need login						
            $request->redirect('?com=login&redirect=' . urlencode($router->getAbsoluteUri()));
        } else {
            $user = $authen->getUser();
            if ($user->type < Users::LEVEL_MODERATOR) {
                $request->redirect('?com=login&act=access_denied');
            }
        }
    }

}